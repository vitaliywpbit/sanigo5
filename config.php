<?php
if (!isset($dir_path)) {
    $dir_path = null;
}

// MAIN
// ======
define('HTTP_PROTOCOL', 'http://'); // протокол передачи гипертекста
define('DOMAIN_NAME', 'sanigo.1gb.ua'); // основное доменное имя с www или без
define('DB_DIR_PATH', $dir_path . 'DB/'); // путь к директории БД
define('GC_TIME', 'DB/sessions/php_session_last_gc'); // файл - используется для проверки времени последней сборки мусора
define('GC_PERIOD', 86400); // время через которое нужно запустить очистку файлов сессий (1800 сек = 30 мин)
define('SMTP', 'robots.1gb.ua'); // сервер исходящей почты (от хостинга или др. сервиса)
define('SMTP_PORT', '25'); // порт SMTP для исходящей почты
define('SMTP_TIMEOUT', '10'); // тайм-аут соединения в секундах

// MAIN SERVER DIRECTORY
// =======================
define("SERVER_DIR","/home/virtwww/w_oas_c1fc78a2/http/"); // для работы с директориями на сервере (создание, удаление, загрузка файлов и др.)
