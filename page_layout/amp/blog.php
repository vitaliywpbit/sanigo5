<?php
include 'language/amp/' . $url->lang . '/' . $url->page_db_file . '.php'; // загрузка переменных на нужном языке
$date_create = explode('T', $page->date_create);

// Открытие файла БД с записями для извлечения дополнительной информации
$record = fopen(DB_DIR_PATH . "amp/$url->lang/blog.csv", "rt") or die("Error Establishing a Database Connection...");

if($record) {
    
    for ($i = 0; $data = fgetcsv($record, 0, ","); $i++) {
        
        if ($url->record_link == $data[2]) {

            $record_id = $data[0];
            $record_img_1x1 = $data[9];
            $record_img_4x3 = $data[10];
            $record_img_16x9 = $data[11];
            break;
        }
    }
}
fclose($record);
?>
<!doctype html>
<html amp lang="<?php echo $url->lang; ?>">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <script async src="http://cdn.ampproject.org/v0.js"></script>
        <script async custom-element="amp-social-share" src="http://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
        <script async custom-element="amp-analytics" src="http://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
        <script async custom-element="amp-sidebar" src="http://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
        <script async custom-element="amp-position-observer" src="http://cdn.ampproject.org/v0/amp-position-observer-0.1.js"></script>
        <script async custom-element="amp-animation" src="http://cdn.ampproject.org/v0/amp-animation-0.1.js"></script>
        <title><?php echo $page->title; ?></title>
        <meta name="description" content="<?php echo $page->description; ?>">
        <link rel="canonical" href="<?php echo $base_url . $url->lang_for_link . $url->page_db_file . '/' . $url->record_link; ?>">
    
        <!-- Favicons -->
        <link rel="icon" type="image/png" sizes="144x144" href="<?php echo $base_url; ?>images/favicon-144x144.png" />
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $base_url; ?>images/favicon-96x96.png" />
        <link rel="icon" type="image/png" sizes="48x48" href="<?php echo $base_url; ?>images/favicon-48x48.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $base_url; ?>images/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $base_url; ?>images/favicon-16x16.png" />
    
        <!-- JSON-LD (Статьи) -->
        <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "NewsArticle",
          "headline": "<?php echo $page->h1; ?>",
           "mainEntityOfPage": {
            "@type": "WebPage",
            "@id": "<?php echo $current_url; ?>"
          },
          "image": [
            "<?php echo $base_url . $record_img_1x1; ?>",
            "<?php echo $base_url . $record_img_4x3; ?>",
            "<?php echo $base_url . $record_img_16x9; ?>"
           ],
          "datePublished": "<?php echo $page->date_create . $time_zone; ?>",
          "dateModified": "<?php echo $page->date_mod . $time_zone; ?>",
          "author": {
            "@type": "<?php echo $page->author_type; ?>",
            "name": "<?php echo $page->author; ?>"
          },
           "publisher": {
            "@type": "Organization",
            "name": "<?php echo $publisher; ?>",
            "logo": {
              "@type": "ImageObject",
              "url": "<?php echo $img_default; ?>"
            }
          }
        }
        </script>
        
        <style amp-boilerplate>
            body {
                -webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;
                -moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;
                -ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;
                animation:-amp-start 8s steps(1,end) 0s 1 normal both
            }
            @-webkit-keyframes -amp-start {
                from {
                    visibility:hidden
                }
                to {
                    visibility:visible
                }
            }
            @-moz-keyframes -amp-start {
                from {
                    visibility:hidden
                }
                to {
                    visibility:visible
                }
            }
            @-ms-keyframes -amp-start {
                from {
                    visibility:hidden
                }
                to {
                    visibility:visible
                }
            }
            @-o-keyframes -amp-start {
                from {
                    visibility:hidden
                }
                to {
                    visibility:visible
                }
            }
            @keyframes -amp-start {
                from {
                    visibility:hidden
                }
                to {
                    visibility:visible
                }
            }
        </style>
        <noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        
        <style amp-custom>
            :root{--color-primary:#039394;--color-secondary:#396eff;--color-text-light:#fff;--space-2:1rem;--box-shadow-1:0 1px 1px 0 rgba(0,0,0,.14),0 1px 1px -1px rgba(0,0,0,.14),0 1px 5px 0 rgba(0,0,0,.12);--breakpoint-xs:0;--breakpoint-sm:576px;--breakpoint-md:768px;--breakpoint-lg:992px;--breakpoint-xl:1200px;--font-family-sans-serif:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";--font-family-monospace:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}*,*::before,*::after{box-sizing:border-box}p{margin-top:0;margin-bottom:1rem}.lead{font-size:1.25rem;font-weight:300}amp-img{vertical-align:middle;border-style:none}.img-fluid{max-width:100%;height:auto}.img-thumbnail{padding:.25rem;background-color:#fff;border:1px solid #dee2e6;border-radius:.25rem;max-width:100%;height:auto}.figure-img{margin-bottom:.5rem;line-height:1}.figure-caption{font-size:90%;color:#6c757d}.container{max-width:1140px;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.col-1,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-10,.col-11,.col-12,.col,.col-auto,.col-sm-1,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm,.col-sm-auto,.col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12,.col-md,.col-md-auto,.col-lg-1,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg,.col-lg-auto,.col-xl-1,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl,.col-xl-auto{position:relative;width:100%;padding-right:15px;padding-left:15px}.col{-ms-flex-preferred-size:0;flex-basis:0%;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.col-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}@media (min-width:576px){.col-sm{-ms-flex-preferred-size:0;flex-basis:0%;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-sm-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.col-sm-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-sm-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-sm-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-sm-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-sm-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-sm-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-sm-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-sm-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-sm-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-sm-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}@media (min-width:768px){.col-md{-ms-flex-preferred-size:0;flex-basis:0%;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-md-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.col-md-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-md-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-md-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-md-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-md-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-md-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-md-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-md-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-md-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-md-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}@media (min-width:992px){.col-lg{-ms-flex-preferred-size:0;flex-basis:0%;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-lg-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.col-lg-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-lg-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-lg-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-lg-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-lg-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-lg-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-lg-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-lg-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-lg-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-lg-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-lg-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-lg-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}@media (min-width:1200px){.col-xl{-ms-flex-preferred-size:0;flex-basis:0%;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-xl-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.col-xl-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-xl-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-xl-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-xl-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-xl-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-xl-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-xl-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-xl-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-xl-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-xl-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-xl-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-xl-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}.media{display:-ms-flexbox;display:flex;-ms-flex-align:start;align-items:flex-start}.media-body{-ms-flex:1;flex:1}.float-left{float:left}.float-right{float:right}.shadow{box-shadow:0 .5rem 1rem rgba(0,0,0,.15)}.m-0{margin:0}.mt-0,.my-0{margin-top:0}.mr-0,.mx-0{margin-right:0}.mb-0,.my-0{margin-bottom:0}.ml-0,.mx-0{margin-left:0}.m-1{margin:.25rem}.mt-1,.my-1{margin-top:.25rem}.mr-1,.mx-1{margin-right:.25rem}.mb-1,.my-1{margin-bottom:.25rem}.ml-1,.mx-1{margin-left:.25rem}.m-2{margin:.5rem}.mt-2,.my-2{margin-top:.5rem}.mr-2,.mx-2{margin-right:.5rem}.mb-2,.my-2{margin-bottom:.5rem}.ml-2,.mx-2{margin-left:.5rem}.m-3{margin:1rem}.mt-3,.my-3{margin-top:1rem}.mr-3,.mx-3{margin-right:1rem}.mb-3,.my-3{margin-bottom:1rem}.ml-3,.mx-3{margin-left:1rem}.m-4{margin:1.5rem}.mt-4,.my-4{margin-top:1.5rem}.mr-4,.mx-4{margin-right:1.5rem}.mb-4,.my-4{margin-bottom:1.5rem}.ml-4,.mx-4{margin-left:1.5rem}.m-5{margin:3rem}.mt-5,.my-5{margin-top:3rem}.mr-5,.mx-5{margin-right:3rem}.mb-5,.my-5{margin-bottom:3rem}.ml-5,.mx-5{margin-left:3rem}.p-0{padding:0}.pt-0,.py-0{padding-top:0}.pr-0,.px-0{padding-right:0}.pb-0,.py-0{padding-bottom:0}.pl-0,.px-0{padding-left:0}.p-1{padding:.25rem}.pt-1,.py-1{padding-top:.25rem}.pr-1,.px-1{padding-right:.25rem}.pb-1,.py-1{padding-bottom:.25rem}.pl-1,.px-1{padding-left:.25rem}.p-2{padding:.5rem}.pt-2,.py-2{padding-top:.5rem}.pr-2,.px-2{padding-right:.5rem}.pb-2,.py-2{padding-bottom:.5rem}.pl-2,.px-2{padding-left:.5rem}.p-3{padding:1rem}.pt-3,.py-3{padding-top:1rem}.pr-3,.px-3{padding-right:1rem}.pb-3,.py-3{padding-bottom:1rem}.pl-3,.px-3{padding-left:1rem}.p-4{padding:1.5rem}.pt-4,.py-4{padding-top:1.5rem}.pr-4,.px-4{padding-right:1.5rem}.pb-4,.py-4{padding-bottom:1.5rem}.pl-4,.px-4{padding-left:1.5rem}.p-5{padding:3rem}.pt-5,.py-5{padding-top:3rem}.pr-5,.px-5{padding-right:3rem}.pb-5,.py-5{padding-bottom:3rem}.pl-5,.px-5{padding-left:3rem}.text-left{text-align:left}.text-right{text-align:right}.text-center{text-align:center}.text-uppercase{text-transform:uppercase}.text-muted{color:#6c757d}body{font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";line-height:1.5;background-color:#fff}a{color:#039394}h2{margin-top:2rem}.brand{text-transform:uppercase;font-weight:600;color:white}.gradient-border{width:100%;height:10px;background:linear-gradient(to right,#039394,#1467c3)}.navbar{padding-top:1rem;width:100%}.sample-sidebar{width:250px;padding:var(--space-2)}.sample-sidebar>*{margin-top:var(--space-2)}.sample-sidebar li,nav[toolbar] li{margin-bottom:var(--space-2);margin-left:0;margin-right:var(--space-2);list-style:none}.sample-sidebar ul li a{text-decoration:none;color:#039394}.sample-sidebar ul li a:hover{text-decoration:underline}.nav-display-menu a{text-decoration:none;color:black}.previewOnly{margin:var(--space-2)}#sidebar-right nav.amp-sidebar-toolbar-target-shown{display:none}.nav-display-menu{margin-top:.7rem}.nav-item{margin-right:1rem;display:inline-block;vertical-align:middle}.nav-item a:hover{text-decoration:underline}button.btn-menu{background:none;padding:3px 7px;color:#333;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;border:1px solid #666}button.btn-menu:hover{background-color:whitesmoke}footer{padding:4rem 1rem;margin-top:4rem;width:100%;min-height:100px;background-color:black;color:white;text-align:center}footer a{color:white}.scrollToTop{color:var(--color-text-light);font-size:1.4em;box-shadow:var(--box-shadow-1);width:50px;height:50px;border-radius:50%;border:none;outline:none;background:var(--color-primary);z-index:9999;bottom:var(--space-2);right:var(--space-2);position:fixed;opacity:0;visibility:hidden}.target{position:relative}.target-anchor{position:absolute;top:-72px;left:0}
        </style>
    </head>
    <body>
        <amp-analytics type="gtag" data-credentials="include">
            <script type="application/json">
            {
            "vars" : {
                "gtag_id": "<?php echo $google_id; ?>",
                "config" : {
                "<?php echo $google_id; ?>": { "groups": "default" }
                    }
                }
            }
            </script>
        </amp-analytics>
        
        <amp-animation id="showAnim" layout="nodisplay">
          <script type="application/json">
            {
              "duration": "200ms",
               "fill": "both",
               "iterations": "1",
               "direction": "alternate",
               "animations": [
                 {
                   "selector": "#scrollToTopButton",
                   "keyframes": [
                     { "opacity": "1", "visibility": "visible" }
                   ]
                 }
               ]
            }
          </script>
        </amp-animation>
    
        <amp-animation id="hideAnim" layout="nodisplay">
          <script type="application/json">
            {
             "duration": "200ms",
               "fill": "both",
               "iterations": "1",
               "direction": "alternate",
               "animations": [
                 {
                   "selector": "#scrollToTopButton",
                   "keyframes": [
                     { "opacity": "0", "visibility": "hidden" }
                   ]
                 }
               ]
           }
          </script>
        </amp-animation>
        
        <?php include 'components/amp/sidebar.php'; //Блок вывода бокового меню ?>
    
        <div class="navbar target">
            <a class="target-anchor" id="top"></a>
            <amp-position-observer
                on="enter:hideAnim.start; exit:showAnim.start"
                layout="nodisplay">
            </amp-position-observer>
            <div class="container">
                <ul class="nav-items">
                    <li class="nav-item">
                        <?php include 'components/amp/logo.php'; //Блок вывода логотипа и основного меню ?>
                    </li>
                    <li class="nav-item">
                        <button class="btn-menu mt-2" on="tap:sidebar-left.toggle">
                            &#9776; <?php echo $sidebar_btn; ?>
                        </button>
                        <div id="target-element-left"></div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="gradient-border"></div>
        
        <main class="container">
            <p class="text-muted mt-5"><?php echo $date_create[0]; ?></p>
            
            <h1><?php echo $page->h1; ?></h1>
            
            <amp-img src="<?php echo $base_url . $record_img_16x9; ?>" alt="<?php echo $page->image_alt; ?>" height="450" width="800" layout="responsive"></amp-img>
            
            <p class="heading mt-3">
                <amp-social-share type="twitter"
                    width="45"
                    height="33">
                </amp-social-share>
                <amp-social-share type="facebook"
                    width="45"
                    height="33"
                    data-param-app_id="405421110048277">
                </amp-social-share>
                <amp-social-share type="pinterest"
                    width="45"
                    height="33">
                </amp-social-share>
                <amp-social-share type="email"
                    width="45"
                    height="33">
                </amp-social-share>
            </p>
            
            <?php
            //Блок вывода контента для записей
            $path_to_content = DB_DIR_PATH . 'amp/' . $url->lang . '/' . $url->page_db_file . '/' . $page->seo_url . '.tpl';

            if (file_exists($path_to_content)) {
                $content = file_get_contents($path_to_content);
                echo $content;
            }
            ?>
            
            <p class="text-center mt-5"><?php echo $link_all_records ?></p>
        </main>
        
        <footer>
            <div class="container">
                <?php include 'components/amp/footer.php'; //Блок вывода подвала ?>
            </div>
        </footer>
    
        <button id="scrollToTopButton" on="tap:top.scrollTo(duration=200)" class="scrollToTop">^</button>
    </body>
</html>
