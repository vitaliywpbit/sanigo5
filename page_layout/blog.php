<!-- HEADER -->
<header>
    <?php
    include 'components/cart.php'; // подключение модуля с корзиной
    ?>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <a class="navbar-brand" href="<?php echo $base_url . $url->lang_for_link; ?>">
            <?php echo $brand_name; ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon text-center"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <?php include 'components/menu.php'; //блок основного меню ?>
            </ul>
        </div>
        <div id="cart" class="ml-2">
            <a href="#" data-toggle="modal" data-target="#cartModal">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bag-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 4h14v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4zm7-2.5A2.5 2.5 0 0 0 5.5 4h-1a3.5 3.5 0 1 1 7 0h-1A2.5 2.5 0 0 0 8 1.5z"/>
                </svg>
                <span id="cart-items-quantity">(0)</span>
            </a>
        </div>
    </nav>
</header>

<?php 
if($url->record_link) {
    include 'modules/blog.php'; //Блок вывода определенной записи
}
else { ?>
    <!-- MAIN CONTENT -->
    <main>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $base_url . $url->lang_for_link; ?>"><?php echo $home_page_name; ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $page->h1; ?></li>
            </ol>
        </nav>
        <div class="container mt-5 mb-5">
            <h1 class="text-center h1"><?php echo $page->h1; ?></h1>
            
            <?php
            //Блок вывода контента для основных страниц
            $path_to_content = DB_DIR_PATH . $url->lang . '/pages/' . $url->page_db_file . '.tpl';

            if (file_exists($path_to_content)) {
                $content = file_get_contents($path_to_content);
                echo $content;
            }
            
            include 'components/blog.php'; //Блок вывода всего списка записей
            ?>
            
        </div>
    </main>
<?php } ?>

<!-- FOOTER -->
<footer>
    <div class="container">
        <?php include 'components/footer.php'; ?>
    </div>
</footer>
