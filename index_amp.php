<?php
if ($start_links) { // Ex.: http://localhost/site.net/ink_1/link_2/link_3
    $link_1 = 3;
    $link_2 = 4;
    $link_3 = 5;
    $current_url = $_SERVER['REQUEST_URI']; // Актуальный адрес страницы для localhost
    $base_url = '/' . $domain_name . '/'; // Base url для localhost
}
else { // Ex.: http://site.net/ink_1/link_2/link_3
    $link_1 = 2;
    $link_2 = 3;
    $link_3 = 4;
    $current_url = $http_protocol . $domain_name . $_SERVER['REQUEST_URI']; // Актуальный адрес страницы
    $base_url = $http_protocol . $domain_name . '/'; // Base url
}

class Url {
    public $lang;
    public $lang_name;
    public $lang_for_link;
    public $page_link;
    public $record_link;
    public $page_db_file;

    function __construct($url, $page_default, $db_lang, $lang_default) {
        $this->lang = $lang_default;
        $this->lang_name = 'Language';
        $this->lang_for_link = null;
        $this->page_link = null;
        $this->page_db_file = '404'; // 404.csv
        $this->record_link = null;

        foreach ($url as $db_link_key => $link_value) {
            $link_cut = explode('?', $link_value);
            $link_value = $link_cut[0];
            switch ($db_link_key) {
                case $GLOBALS['link_1']:
                    $lang_link = false;
                    foreach ($db_lang as $lang_value) {
                        if ($lang_value['lang_code'] == $link_value && $lang_value['lang_default'] != 'd') {
                            $this->lang = $link_value;
                            $this->lang_name = $lang_value['lang_name'];
                            $this->lang_for_link = $link_value . '/';
                            $lang_link = true;
                            break;
                        }
                        else {
                            foreach ($db_lang as $lang_value) {
                                if ($lang_value['lang_default'] == 'd') {
                                    $this->lang_name = $lang_value['lang_name'];
                                }
                            }
                            if ($link_value == '') {
                                $this->page_link = $page_default;
                                $this->page_db_file = $page_default;
                            }
                            elseif ($link_value == 'home') { // От дубля домашней страницы по ссылке "home"
                                $this->page_link = null;
                            }
                            else {
                                $this->page_link = $link_value;
                                $this->page_db_file = $link_value;
                            }
                        }
                    }
                    break;
                case $GLOBALS['link_2']:
                    if($lang_link) {
                        if ($link_value == '') {
                            $this->page_link = $page_default;
                            $this->page_db_file = $page_default;
                        }
                        elseif ($link_value == 'home') { // От дубля домашней страницы по ссылке "home"
                            $this->page_link = null;
                        }
                        else{
                            $this->page_link = $link_value;
                            $this->page_db_file = $link_value;
                        }
                    }
                    else {
                        $this->record_link = $link_value;
                        $this->page_link = null;
                    }
                    break;
                case $GLOBALS['link_3']:
                    if ($link_value == '') {
                        $this->page_link = null;
                        $this->record_link = null;
                    }
                    else{
                        $this->record_link = $link_value;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
$url = new Url($url, $page_default, $db_lang, $lang_default);

class Pages {
    public $err404;
    public $id;
    public $page_layout;
    public $seo_url;
    public $title;
    public $keywords;
    public $description;
    public $h1;
    public $content;
    public $date_create;
    public $date_mod;
    public $image;
    public $image_alt;
    public $author;
    public $author_type;

    public function __construct($lang, $page_link, $page_db_file, $record_link, $img_default) {
        
        // По умолчанию загрузка мета тегов и контента для страницы 404
        $page_404 = fopen(DB_DIR_PATH . "amp/$lang/404.csv", "rt") or die("Error Establishing a Database Connection...");
        for($i = 0; $data = fgetcsv($page_404, 1000, ","); $i++) {
            $this->err404 = true;
            $this->page_layout = $data[0];
            $this->seo_url = $data[1];
            $this->title = $data[2];
            $this->keywords = $data[3];
            $this->description = $data[4];
            $this->h1 = $data[5];
            $this->content = null;
            $this->image = $img_default;
        }
        fclose($page_404);
        
        // Проверка, существует ли файл БД для статей, новостей или записей и подключение к существующей
        // Если таковой не существует, выполняем подключение к основному файлу БД (pages.csv)
        $filename = DB_DIR_PATH . "amp/$lang/$page_db_file.csv";
        
        if(file_exists($filename) && $record_link) {
            $pages = fopen(DB_DIR_PATH . "amp/$lang/$page_db_file.csv", "rt") or die("Error Establishing a Database Connection...");
        }
        elseif(!$record_link) {
            $pages = fopen(DB_DIR_PATH . "amp/$lang/pages.csv", "rt") or die("Error Establishing a Database Connection...");
        }
        else{
            $page_link = null;
            $record_link = null;
            $pages = fopen(DB_DIR_PATH . "amp/$lang/pages.csv", "rt") or die("Error Establishing a Database Connection...");
        }
        
        // Загрузка мета тегов и контента для основных страниц или записей
        for($i = 0; $data = fgetcsv($pages, 0, ","); $i++) {
            if($data[2] == $page_link or $data[2] == $record_link) {
                $this->err404 = false;
                $this->id = $data[0];
                $this->page_layout = $data[1];
                $this->seo_url = $data[2];
                $this->title = $data[3];
                $this->keywords = $data[4];
                $this->description = $data[5];
                $this->h1 = $data[6];
                $this->content = null;
                if($data[2] == $record_link) {
                    $this->date_create = $data[7];
                    $this->date_mod = $data[8];
                    $this->image = $data[9]; // image 1x1
                    $this->image_alt = $data[13];
                    $this->author = $data[14];
                    $this->author_type = $data[15];
                }
                break;
            }
        }
        fclose($pages);
    }
}
$page = new Pages($url->lang, $url->page_link, $url->page_db_file, $url->record_link, $img_default);

// HTTP Заголовок для несуществующей сраницы с кодом 404
if ($page->err404) {
    header("HTTP/1.0 404 Not Found");
}

//Остальной контент на нужном языке
include 'language/amp/' . $url->lang . '/all_content.php';
//Подключение шаблона страницы и загрузка основного контента
include 'page_layout/amp/' . $page->page_layout . '.php';
