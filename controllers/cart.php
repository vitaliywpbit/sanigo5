<?php

// Добавление нового товара в корзину
function addItemToCart($item_id, $quantity) {
    
    if (isset($_SESSION['items'][$item_id])) {
        
        $_SESSION['items'][$item_id] = $_SESSION['items'][$item_id] + $quantity;
    }
    else {
        
        $_SESSION['items'][$item_id] = $quantity;
    }
}

// Удаление товара из корзины
function removeItemFromCart($item_id) {
    
    unset($_SESSION['items'][$item_id]);
}

// Поиск данных по id товара для корзины
function getItemInfo($lang, $item_id) {
    
    $cart_item_info = null;
    
    //Открытие списка категорий
    $category_for_cart = fopen(DB_DIR_PATH . "$lang/pages.csv", "rt") or die("Error Establishing a Database Connection...");

    if($category_for_cart) {
        
        $db_category_for_cart = null;
        
        for ($i = 0; $data = fgetcsv($category_for_cart, 0, ","); $i++) {
            
            if ($data[1] == 'category') {
                
                $db_category_for_cart[] = [
                    'category_id' => $data[0],
                    'category_url' => $data[2]
                ];
            }
        }
    }
    else {
        
        $db_category_for_cart = null;
    }
    
    fclose($category_for_cart);
    
    
//Открытие списка товаров
    $products = fopen(DB_DIR_PATH . "$lang/items.csv", "rt") or die("Error Establishing a Database Connection...");

    if($products) {
        
        $db_products = null;
        
        for ($i = 0; $data = fgetcsv($products, 0, ","); $i++) {
            
            if ($item_id == $data[0]) {
                
                if ($db_category_for_cart) {
                    
                    $category_url = null;
                    
                    foreach ($db_category_for_cart as $cat_key) { // находим категорию к товару для ссылки url
                        
                        if ($cat_key['category_id'] == $data[7]) {
                            
                            $category_url = $cat_key['category_url'];
                        }
                    }

                    $cart_item_info = [
                        'item_id' => $data[0],
                        'item_url' => $category_url . '/' . $data[2],
                        'item_image' => $data[11], // image preview
                        'item_name' => $data[6], // h1
                        'item_description' => $data[5],
                        'item_price' => $data[15],
                        'item_new_price' => $data[16], // sale
                        'item_sku' => $data[17], // sku
                    ];
                    
                    // Формирование новой цены со скидкой
                    if ($data[16]) {
                        $cart_item_info['item_price'] = $data[16];
                    }
                    
                }
                else {
                    $category_url = null;
                }
            }
        }
    }
    else {
        $db_products = null;
    }
    
    fclose($products);
    
    return $cart_item_info;
}
