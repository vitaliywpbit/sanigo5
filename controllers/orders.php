<?php

// Чтение заключительного номера заказа
function getLastOrderNumber() {
    $order_number = 0;
    
    $orders = fopen(DB_DIR_PATH . 'orders.csv', "rt") or die('Error Establishing a Database Connection...');
        
    if($orders) {

        flock($orders, LOCK_EX); // блокируем файл

            for ($i = 0; $data = fgetcsv($orders, 0, ","); $i++) {
                if ($data[0] != '' && $data[0] != '#') {
                    $order_number = $data[0];
                }
            }

        flock($orders, LOCK_UN); // снимаем блокировку
        
        fclose($orders);
        return $order_number;
    }
    else {
        fclose($orders);
        return $order_number;
    }
}

// Запись нового заказа в БД
function newOrder($order_number, $firstname, $secondname, $phone, $email, $subscription_status, $region, $city, $address, $shipping, $payment, $total_to_pay, $comment, $product_list) {
    
    $orders_filename = DB_DIR_PATH . 'orders.csv';
    $orders = fopen(DB_DIR_PATH . $orders_filename, "a+") or die('Error Establishing a Database Connection...');
    
    if($orders) {
        
        flock($orders, LOCK_EX); // блокируем файл
        
            // Добавление данных о новом заказе
            $db_order[] = [
                'order_number' => $order_number,
                'date' => date("d-m-y, H:i:s"),
                'firstname' => $firstname,
                'secondname' => $secondname,
                'phone' => $phone,
                'email' => $email,
                'subscription_status' => $subscription_status,
                'region' => $region,
                'city' => $city,
                'address' => $address,
                'shipping' => $shipping,
                'payment' => $payment,
                'total_to_pay' => $total_to_pay,
                'comment' => $comment,
                'product_list' => $product_list
            ];

            // Запись новых данных о заказах в файл
            foreach ($db_order as $line => $column) {
                fputcsv($orders, $column);
            }

        flock($orders, LOCK_UN); // снимаем блокировку
        
        fclose($orders);
        return true;
    }
    else {
        fclose($orders);
        return false;
    }
}
