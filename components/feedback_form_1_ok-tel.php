<?php

//Переменные на нужном языке
include 'language/' . $lang . '/q_feedback.php';

?>


<div id="result-q-feedback">
<form class="form-signin" method="POST" id="form-q-feedback" action="javascript:void(null);" onsubmit="q_feedback()">
    <div class="form-row align-items-center">
        <div class="col-md-4 my-1 mt-3">
            <input type="text" class="form-control" id="inlineFormInputName" name="name" placeholder="<?php echo $q_feedback_placeholder_name; ?>" required>
        </div>
        <div class="col-md-4 my-1 mt-3">
            <input type="tel" class="form-control" id="inlineFormInputPhone" name="phone" placeholder="<?php echo $q_feedback_placeholder_phone; ?>" required>
        </div>
        <div class="col-md-4 my-1 mt-3">
            <input type="email" class="form-control" id="inlineFormInputGroupEmail" name="email" placeholder="<?php echo $q_feedback_placeholder_email; ?>" required>
        </div>
        <div class="col-md-12 my-1 mt-4">
            <textarea class="form-control" id="comments" name="message_text" placeholder="<?php echo $q_feedback_message_text; ?>"></textarea>
        </div>
    </div>

    <input type="hidden" name="lang" value="<?php echo $lang; ?>">

    <div class="text-center">
        <button type="submit" class="btn btn-lg btn-primary border border-light mt-4"><?php echo $submit_q_feedback_form; ?></button>
    </div>
</form>
 </div>