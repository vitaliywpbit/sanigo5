<?php
//Совместимость с Bootstrap 4
//===========================

//Переменные для формы быстрого заказа
include 'language/' . $url->lang . '/checkout.php';

$lang = $url->lang;
$lang_for_link = $url->lang_for_link;
?>

<div class="container">
    
    <div id="checkout">
        <?php
        $_POST['sess_start'] = "no";
        $_POST['lang'] = $lang;
        $_POST['lang_for_link'] = $lang_for_link;
        include 'modules/checkout.php';
        ?>
    </div>
    
</div>
