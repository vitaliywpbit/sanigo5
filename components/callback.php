<?php
//Совместимость с Bootstrap 4
//===========================

//Переменные для обратного звонка на нужном языке
include 'language/' . $url->lang . '/callback.php';
?>

<!-- Modal -->
<div class="modal fade" id="callback" tabindex="-1" role="dialog" aria-labelledby="callback" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark-blue text-white">
                <h6 class="modal-title">
                    <svg class="bi bi-phone" width="1.4em" height="1.4em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M13 3H7a1 1 0 00-1 1v11a1 1 0 001 1h6a1 1 0 001-1V4a1 1 0 00-1-1zM7 2a2 2 0 00-2 2v11a2 2 0 002 2h6a2 2 0 002-2V4a2 2 0 00-2-2H7z" clip-rule="evenodd"/>
                        <path fill-rule="evenodd" d="M10 15a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>
                    </svg>
                    <?php echo $request_callback; ?>
                </h6>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div id="results_callback">

                <form class="form-signin" method="POST" id="form_callback" action="javascript:void(null);" onsubmit="callback()">

                    <div class="modal-body">

                        <div class="form-row align-items-center">
                            <div class="col-12 my-1 mt-3">
                                <input type="text" maxlength="25" class="form-control" id="callbackName" name="name" placeholder="<?php echo $callback_placeholder_name; ?>" required>
                            </div>
                            <div class="col-12 my-1 mt-3">
                                <input type="tel" maxlength="15" class="form-control" id="callbackPhone" name="phone" placeholder="<?php echo $callback_placeholder_phone; ?>" required>
                            </div>
                        </div>

                        <input type="hidden" name="lang" value="<?php echo $url->lang; ?>">

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal"><?php echo $close_callback_form; ?></button>
                        <button type="submit" id="submitCallback" class="btn btn-sm btn-primary"><?php echo $submit_callback_form; ?></button>
                    </div>

                </form>
                
            </div>
                
        </div>
    </div>
</div>

<!-- Отправка сообщения для Обратного звонка
============================================ -->
<script>
    function callback() {
            
        var msg   = $('#form_callback').serialize();

        $.ajax({

            type: 'POST',
            url: 'modules/callback.php',
            data: msg

        }).done(function(html) {

            $('#results_callback').html(html);

        });

    }
</script>
