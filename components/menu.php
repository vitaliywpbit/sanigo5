<?php
//Работает на Bootstrap 4.x.x
include 'model/menu.php';

$page_lang = $url->lang . '/' . $url->page_link; //Для других языковых версий сайта
$count_for_id = 0;

foreach ($db_menu as $line => $column) {
    
    if($column['menu_link'] != '##' or $column['menu_link'] == null) {
        
        // Выделение активной ссылке в меню
        if($url->page_db_file == $column['menu_link'] or $page_lang == trim($column['menu_link'])) {
            $menu_active = 'active';
        }
        elseif($url->page_db_file == $page_default && trim($column['menu_link']) == '') {
            $menu_active = 'active';
        }
        elseif($url->page_db_file == $page_default && trim($column['menu_link']) == '#') {
            $menu_active = 'active';
        }
        elseif($url->page_db_file == $page_default && trim($column['menu_link']) == $url->lang . '/') {
            $menu_active = 'active';
        }
        else {
            $menu_active = 'no-active';
        } ?>
        
        <li class="nav-item <?php echo $menu_active; ?>">
            <a class="nav-link" href="<?php echo $column['menu_link']; ?>"><?php echo $column['menu_item']; ?></a>
        </li>
<?php }
    elseif ($column['menu_link'] == '##') {
        ++$count_for_id; ?>
        
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink<?php echo $count_for_id; ?>" 
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $column['menu_item']; ?></a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink<?php echo $count_for_id; ?>">
            <?php           
            foreach ($db_submenu_01 as $line_submenu_01 => $column_submenu_01) {
                
                if($column_submenu_01['submenu_01_link'] != '###' or $column_submenu_01['submenu_01_link'] == null) {
                
                    if($column_submenu_01['submenu_01_menu_id'] == $column['menu_id']) {
                        
                        // Выделение активной ссылке в подменю
                        if($url->page_db_file == $column_submenu_01['submenu_01_link'] or $page_lang == $column_submenu_01['submenu_01_link']) {
                            $menu_active = 'active';
                            }
                            else {
                                $menu_active = 'no-active';
                            } ?>
                        
                            <a class="dropdown-item <?php echo $menu_active; ?>" href="<?php echo $column_submenu_01['submenu_01_link']; ?>">
                                <?php echo $column_submenu_01['submenu_01_item']; ?></a>
                <?php }
                }
                elseif ($column_submenu_01['submenu_01_link'] == '###' and $column_submenu_01['submenu_01_menu_id'] == $column['menu_id']) { ?>
                    
                    <div class="dropdown-submenu">
                        <a class="dropdown-item dropdown-toggle" href="#"> <?php echo $column_submenu_01['submenu_01_item']; ?></a>
                        <div class="dropdown-menu">
                        <?php
                        foreach ($db_submenu_02 as $line_submenu_02 => $column_submenu_02) {
                             if($column_submenu_02['submenu_02_menu_id'] == $column_submenu_01['submenu_01_id']) {
                                  
                                // Выделение активной ссылке в подменю
                                if($url->page_db_file == $column_submenu_02['submenu_02_link'] or $page_lang == $column_submenu_02['submenu_02_link']) {
                                    $menu_active = 'active';
                                    }
                                    else {
                                        $menu_active = 'no-active';
                                    } ?>
                                    
                                    <a class="dropdown-item <?php echo $menu_active; ?>" href="<?php echo $column_submenu_02['submenu_02_link']; ?>">
                                        <?php echo '- ' . $column_submenu_02['submenu_02_item']; ?></a>
                            <?php }
                            } ?>
                        </div>
                    </div>
            <?php }
            } ?>
            </div>
        </li>
<?php }    
}
