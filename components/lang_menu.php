<?php
//Совместимость с Bootstrap 4.x.x
//БД с языками и $GLOBALS['path_page_db'] подключены в файле index.php

if ($page_num > 1) {
    $page_num_link = '?p=' . $page_num;
}
 else {
    $page_num_link = null;
}

if ($page->seo_url == 'home') {
    $temp_page_link = null;
}
else {
    $temp_page_link = $url->page_link . $page_num_link;
}

if (isset($url->record_link)) {
    $temp_page_link = $url->page_db_file . '/' . $url->record_link;
}

foreach ($db_lang as $line => $column) {
    
    $full_path_page_db = DB_DIR_PATH . $column['lang_code'] . '/' . $GLOBALS['path_page_db'];
    
    // Существует ли страница на другом языке или нет
    if (file_exists($full_path_page_db)) {
    
        if ($url->lang == $column['lang_code']) {
            $active_lang = true;
        }
        else {
            $active_lang = false;
        }

        if($column['lang_default'] == 'd') {
            $lang_link = $temp_page_link;
        }
        else {
            $lang_link = $column['lang_code'] . '/' . $temp_page_link;
        } 

        if ($active_lang) { ?>
            <a class="dropdown-item active" href="<?php echo $base_url . $lang_link; ?>"><?php echo $column['lang_name']; ?></a>
    <?php }
        else { ?>
            <a class="dropdown-item" href="<?php echo $base_url . $lang_link; ?>"><?php echo $column['lang_name']; ?></a>
    <?php }
    }
}
