    <amp-sidebar id="sidebar-left" class="sample-sidebar" layout="nodisplay" side="left">
        <h3>«<?php echo $brand_name; ?>»</h3>
        <button on="tap:sidebar-left.close">
            &#10008;
            <?php echo $close_sidebar_btn; ?>
        </button>
        <nav class="nav-display-menu" toolbar="(min-width: 784px)" toolbar-target="target-element-left">
            <ul>
                <li><?php echo $menu_link_01; ?></li>
                <li><?php echo $menu_link_02; ?></li>
            </ul>
        </nav>
        <ul>
            <li><?php echo $menu_link_03; ?></li>
        </ul>
    </amp-sidebar>