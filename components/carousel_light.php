<?php
// Совместимость с Bootstrap 4.x.x
include 'model/carousel.php'; // Подключение к БД
?>

<div id="MainCarousel" class="carousel slide carousel-fade" data-ride="carousel">
    <?php
    if ($db_carousel_images) {
        
        $count_num = 0;
        $count_images = count($db_carousel_images);
        
    
        while ($count_num < $count_images) {
            if ($count_num == 0) { ?>
                <li data-target="#MainCarousel" data-slide-to="<?php echo $count_num; ?>" class="active"></li>
            <?php } else { ?>
                <li data-target="#MainCarousel" data-slide-to="<?php echo $count_num; ?>"></li>
            <?php }
            ++$count_num;
        }

        $count_num = 0;
        echo '</ol><div class="carousel-inner">';
        foreach ($db_carousel_images as $line => $column) {
            if ($count_num == 0) { ?>
                <div class="carousel-item active" style="background-image: url('<?php echo $column['slide_link']; ?>');">
                  <div class="transparent-bg">   
                        <div class="mx-5 text-uppercase text-white text-banner-down text-center h4 img-fluid rounded">
                            
                  

                      <!--      <h1 class="text-uppercase mt-2 mb-4 text-break"><b><?php echo $column['slide_title']; ?></b></h1>  -->
                            
                            <p><?php echo $column['slide_text']; ?></p> 

                            <?php }
        ++$count_num;
        }
        echo '</div>';
    }
    ?>
                            </div></div></div></div>

