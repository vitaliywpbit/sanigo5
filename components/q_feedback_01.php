<?php
//Совместимость с Bootstrap 4
//===========================

//Переменные на нужном языке
include 'language/' . $url->lang . '/q_feedback.php';
?>

<br>

<h2 class="mb-4 h3 text-blue"><?php echo $request_q_feedback; ?></h2>

<div id="result-q-feedback">

<script src='https://www.google.com/recaptcha/api.js'></script>
    
    <form class="form-signin" method="POST" id="form-q-feedback" action="javascript:void(null);" onsubmit="q_feedback()">
        <div class="form-row align-items-center">
            <div class="col-12 my-1">
                <div class="input-group">
                    <input type="text" class="form-control" id="inlineFormInputName" name="name" placeholder="<?php echo $q_feedback_placeholder_name; ?>" required>
                </div>
            </div>
            <div class="col-12 my-1 mt-3">
                <div class="input-group">
                    <input type="email" class="form-control" id="inlineFormInputGroupEmail" name="email" placeholder="<?php echo $q_feedback_placeholder_email; ?>" required>
                </div>
            </div>
            <div class="col-12 my-1 mt-3">
                <div class="input-group">
                    <input type="tel" class="form-control" id="inlineFormInputGroupPhone" name="phone" placeholder="<?php echo $q_feedback_placeholder_phone; ?>" required>
                </div>
            </div>
            <div class="col-sm-12 my-1 mt-3">
                <label for="message-text" class="col-form-label text-muted"><?php echo $q_feedback_message_text; ?></label>
                <textarea class="form-control" id="message-text" name="message_text" required></textarea>
            </div>
        </div>


        
        <input type="hidden" id="callback-lang" name="lang" value="<?php echo $url->lang; ?>">


        <div class="text-center mt-3" style="line-height: 1.2;">
              <br/>
        <div class="g-recaptcha" data-sitekey="6Ld-YfEdAAAAAIfIqh7hkgw1j5mDxTylMEMLXb65"></div>
        <br/>
            <span style="color: red">* </span><input type="checkbox" id="callback-confirm_opd" name="confirm_opd" value="confirm_opd_ok" required>
            <small><?php echo $q_feedback_text_opd; ?></small>
        </div>

        <div class="text-center">
            <button type="submit" class="btn btn-lg btn-primary bg-dark-blue mt-4"><?php echo $submit_q_feedback_form; ?></button>
        </div>
    </form>
    
</div>

<!-- Скрипт динамической отправки сообщения
    ======================================= -->
<script>
    function q_feedback() {
            
        var msg   = $('#form-q-feedback').serialize();

        $.ajax({

            type: 'POST',
            url: 'modules/q_feedback.php',
            data: msg

        }).done(function(html) {

            $('#result-q-feedback').html(html);

        });

    }
</script>
