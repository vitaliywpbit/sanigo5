<?php
//Загрузка переменных на нужном языке
include 'language/' . $lang . '/faqpage.php';

//Подключение к БД
include 'model/faqpage.php';

if ($db_faqpage) {
    $count = 1;
    $count_db_faqpage = count($db_faqpage) ?>
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "FAQPage",
      "mainEntity": [
    <?php foreach ($db_faqpage as $line => $column) {
        if($count != $count_db_faqpage) { ?>
        {
        "@type": "Question",
        "name": "<?php echo $column['question']; ?>",
        "acceptedAnswer": {
          "@type": "Answer",
          "text": "<?php echo $column['answer']; ?>"
            }
        },
    <?php }
        else { ?>
        {
        "@type": "Question",
        "name": "<?php echo $column['question']; ?>",
        "acceptedAnswer": {
          "@type": "Answer",
          "text": "<?php echo $column['answer']; ?>"
            }
        }
        <?php
        break;
        }
    ++$count; } ?>
    ]
    }
    </script>

    <div id="m-faq" class="bg-dark text-white p-3">
        <h2 class="mt-0 mb-0 h2">
            <svg class="bi bi-list-ul mb-1 mr-2" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M5 11.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zm0-4a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zm0-4a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zm-3 1a1 1 0 100-2 1 1 0 000 2zm0 4a1 1 0 100-2 1 1 0 000 2zm0 4a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>
            </svg>
            <?php echo $faq_title; ?>
        </h2>
    </div>
    
    <div class="accordion mb-5" id="faqPage">
    <?php
     foreach ($db_faqpage as $line => $column) { ?>
        <div class="card">
            <div class="card-header position-relative" id="heading-<?php echo $line; ?>">
                <p class="mb-0 h5">
                    <button class="btn btn-link collapsed stretched-link text-dark" type="button" data-toggle="collapse" data-target="#question-<?php echo $line; ?>" aria-expanded="false" aria-controls="question-<?php echo $line; ?>">
                        <svg class="bi bi-chevron-compact-down mb-1 mr-2" width="1.1em" height="1.1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M1.553 6.776a.5.5 0 01.67-.223L8 9.44l5.776-2.888a.5.5 0 11.448.894l-6 3a.5.5 0 01-.448 0l-6-3a.5.5 0 01-.223-.67z" clip-rule="evenodd"/>
                        </svg>
                        <?php echo $column['question']; ?>
                    </button>
                </p>
            </div>
            <div id="question-<?php echo $line; ?>" class="collapse" aria-labelledby="heading-<?php echo $line; ?>" data-parent="#faqPage">
                <div class="card-body">
                    <?php echo $column['answer']; ?>
                </div>
            </div>
        </div>
     <?php }
    ?>
    </div>
<?php }
