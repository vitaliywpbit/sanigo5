 <?php include 'model/jumbotron.php'; ?> 

<div class="jumbotron jumbotron-fluid position-relative">
    
    <div class="jumbotron-transparent"></div>
    
    <div class="jumbotron-content"> 
    
        <div class="container text-center">
            

            <br>
            <br>
            
            <?php
            echo '<h1><b>' . $page->h1 . '</b></h1>';

            if ($jumbotron_text) {
                echo '<p>' . $jumbotron_text . '</p>';
            }

            if ($jumbotron_btn_link && $jumbotron_btn) {
                echo "<a href='$jumbotron_btn_link' class='btn btn-primary mt-3'>$jumbotron_btn</a>";
            }
            ?>
        </div>
        
    </div>
        
</div>

<script>
    /*
    function windowSize(){
        if ($(window).width() < '576'){
            $('.jumbotron').css('background-image', "url('images/jumbotron/576/<?php ##echo $jumbotron_image; ?>')");
        } else {
            $('.jumbotron').css('background-image', "url('images/jumbotron/<?php ##echo $jumbotron_image; ?>')");
        }
    }
    $(window).on('load resize',windowSize);
    */
</script>
