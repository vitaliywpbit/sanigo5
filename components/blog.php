<?php
// Работает на Bootstrap 4
// ========================

//Загрузка переменных на нужном языке
include 'language/' . $url->lang . '/' . $url->page_db_file . '.php';
//Подключение к БД
include "model/$url->page_db_file.php";

$lang_for_link = $url->lang_for_link;
$page_link = $url->page_link;

// Pagination function
// ====================
function pagePagination($lang_for_link, $page_link, $page_num, $how_many_pages, $how_many_links, $how_many_links_r_l) {
    
    if ($how_many_pages > 1) {
        echo '<nav aria-label="Page navigation"><ul class="pagination justify-content-center">'; // стили для блока ссылок по страницам
        if ($how_many_pages <= $how_many_links) {
            for ($i = 1; $i <= $how_many_pages; $i++) {
                if ($i == 1) {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='$lang_for_link$page_link'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='$lang_for_link$page_link'>$i</a>
                            </li>";
                    }
                }
                else {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                    }
                }
            }
        }
        elseif ($how_many_pages > $how_many_links && $page_num < $how_many_links) {
            for ($i = 1; $i <= $how_many_pages; $i++) {
                if ($i == 1) {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='$lang_for_link$page_link'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='$lang_for_link$page_link'>$i</a>
                            </li>";
                    }
                }
                else {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                    }
                }
                if ($i == $how_many_links) {
                    break;
                }
            }
            echo "<li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>
                   <li class='page-item'>
                        <a class='page-link' href='$lang_for_link$page_link?p=$how_many_pages'>$how_many_pages</a>
                   </li>";
        }
        elseif ($how_many_pages > $how_many_links && $page_num >= $how_many_links && $how_many_pages >= $page_num + $how_many_links_r_l) {
            $new_i = $page_num - $how_many_links_r_l;
            $count_page_num = 1;
            $end_of_links = false;
            echo "<li class='page-item'>
                        <a class='page-link' href='$lang_for_link$page_link'>1</a>
                   </li>
                    <li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>";
            for ($i = $new_i; $i <= $how_many_pages; $i++) {
                if ($i == $page_num) {
                    echo "<li class='page-item active'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                }
                else {
                     echo "<li class='page-item'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                }
                if ($i == $how_many_pages) {
                    $end_of_links = true;
                }
                if ($count_page_num == $how_many_links) {
                    break;
                }
                ++$count_page_num;
            }
            if (!$end_of_links) {
                echo "<li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>
                   <li class='page-item'>
                        <a class='page-link' href='$lang_for_link$page_link?p=$how_many_pages'>$how_many_pages</a>
                   </li>";
            }
        }
        elseif ($how_many_pages > $how_many_links && $page_num >= $how_many_links && $how_many_pages < $page_num + $how_many_links_r_l) {
            $new_i = $how_many_pages - ($how_many_links - 1);
            $count_page_num = 1;
            echo "<li class='page-item'>
                        <a class='page-link' href='$lang_for_link$page_link'>1</a>
                   </li>
                    <li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>";
            for ($i = $new_i; $i <= $how_many_pages; $i++) {
                if ($i == $page_num) {
                   echo "<li class='page-item active'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                }
                else {
                     echo "<li class='page-item'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                }
                if ($i == $how_many_pages) {
                    $end_of_links = true;
                }
                if ($count_page_num == $how_many_links) {
                    break;
                }
                ++$count_page_num;
            }
        }
        echo '</ul></nav>'; // стили для блока ссылок по страницам
    }
}
// End Pagination function
// ========================

if ($db_records) {

    $show_records = 12; //Сколько записей на 1 странице показывать
    $how_many_records = 0; //сколько всего записей по умолчанию
    $how_many_pages = 1; //сколько страниц с записями по умолчанию
    $how_many_links = 5; //Сколько ссылок на страницы в одном блоке
    $how_many_links_r_l = floor($how_many_links / 2); //Сколько ссылок слева от активного номера страницы
    if(isset($db_records)) {
        $how_many_records = count($db_records); //сколько всего записей
        $how_many_pages = ceil($how_many_records / $show_records); //сколько страниц с записями
    }
    
    pagePagination($lang_for_link, $page_link, $page_num, $how_many_pages, $how_many_links, $how_many_links_r_l);

    //Показ списка статей, товаров, новостей или записей
    //====================================================
    $start_show_num = $show_records * ($page_num - 1);
    $count_record_by_category = 0;
    $stop_count_show_record = 0;

    echo '<br><div class="row">';
        foreach ($db_records as $line => $column) {
            $id_record = $column['record_id'];
            $record_date_full = $column['record_date']; //Время и дата публикации в формате ISO 8601
            $record_date_delimiter = explode('T', $record_date_full);
            $record_date = $record_date_delimiter[0];
            $record_time = $record_date_delimiter[1];

            if($count_record_by_category >= $start_show_num) { ?>
                <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="record-prev">
                        <span class="text-muted"><small><?php echo $date_title . ' ' . $record_date; ?></small></span>
                        <br>
                        
                        <?php
                        //Код проверки наличия изображения для записи
                        if(isset($column['record_img_preview'])) {

                            $record_image_check = trim($column['record_img_preview']);

                            if($record_image_check != null && file_exists($record_image_check)) {
                                $record_image = $record_image_check;
                            }
                            else {
                                $record_image = 'images/no_image.png';
                            }
                        }
                        else {
                            $record_image = 'images/no_image.png';
                        }

                        //Код проверки наличия тега alt для изображения
                        if(isset($column['record_img_alt'])) {
                            
                            $alt_image_check = trim($column['record_img_alt']);
                            
                            if($alt_image_check != null) {
                                $record_img_alt = $alt_image_check;
                            }
                            else {
                                $record_img_alt = 'Записи сайта, site records';
                            }
                        }
                        else {
                            $record_img_alt = 'Записи сайта, site records';
                        }
                        //===================================
                        ?>
                        
                        <a href="<?php echo $url->lang_for_link . $url->page_link . '/' . $column['url']; ?>">
                            <img class="img-thumbnail img-fluid fade-rec" loading="lazy" src="<?php echo $record_image; ?>" alt="<?php echo $record_img_alt; ?>">
                            <br>
                            <p class="record-title mt-2"><?php echo $column['h1']; ?></p>
                        </a>
                    </div>
                    <br>
                </div>

                <?php 
                $stop_count_show_record++;

                if($stop_count_show_record == $show_records) {
                    break;
                }
            }
            $count_record_by_category++;
        }
    echo '</div>';
    
    pagePagination($lang_for_link, $page_link, $page_num, $how_many_pages, $how_many_links, $how_many_links_r_l);
    
}
else {
    echo '<p style="text-align: center;">No blog post</p>';
}
