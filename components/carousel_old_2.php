<?php
// Совместимость с Bootstrap 4.x.x
include 'model/carousel.php'; // Подключение к БД
?>

<div id="MainCarousel" class="carousel slide carousel-fade" data-ride="carousel">
    <?php
    if ($db_carousel_images) {
        
        $count_num = 0;
        $count_images = count($db_carousel_images);
        
        echo '<ol class="carousel-indicators">'; 
        while ($count_num < $count_images) {
            if ($count_num == 0) { ?>
                <li data-target="#MainCarousel" data-slide-to="<?php echo $count_num; ?>" class="active"></li>
            <?php } else { ?>
                <li data-target="#MainCarousel" data-slide-to="<?php echo $count_num; ?>"></li>
            <?php }
            ++$count_num;
        }

        $count_num = 0;
        echo '</ol><div class="carousel-inner">';
        foreach ($db_carousel_images as $line => $column) {
            if ($count_num == 0) { ?>
                <div class="carousel-item active" style="background-image: url('<?php echo $column['slide_link']; ?>');">
                  <div class="transparent-bg">  <!--  ОСВЕТЛЕНИЕ 
                        <div class="carousel-caption d-none d-block text-blue">
                            
                            <img src="images/logo/logo.png" width="100" height="92" alt="<?php echo $logo_alt; ?>">

                            <h1 class="text-uppercase mt-2 mb-4 text-break"><b><?php echo $column['slide_title']; ?></b></h1>
                            
                            <p><b><?php echo $column['slide_text']; ?></b></p> 



                            

                            <a href="<?php echo $facebook_link; ?>" class="mr-3" aria-label="Facebook" target="_blank" rel="nofollow noopener">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="#00407f" class="bi bi-facebook" viewBox="0 0 16 16">
                                    <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
                                </svg>
                            </a>

                            <a href="<?php echo $instagram_link; ?>" class="mr-3" aria-label="Instagram" target="_blank" rel="nofollow noopener">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="#00407f" class="bi bi-instagram" viewBox="0 0 16 16">
                                    <path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"/>
                                </svg>
                            </a>

<a href="viber://chat?number=%2B380501001567" class="mr-3">
    <img src="images/web-viber.png" width="32" height="32" alt="Viber" data-filename="viber-white.png"></a><br>

<img src="images/google-maps-icon.png" width="32" height="32" alt="Google maps icon"><a href="http://maps.app.goo.gl/79J4ez4dtweJEwMo8" class="text-black" target="_blank" rel="noopener">
                    Відгуки на сайті Google<img src="images/stars-icon.png" height="20" alt="Stars icon"> (33)</a> 

                            <br>
                            <br>
                            
                            <div class="row no-gutters">
                                
                                <div class="col-md-1 col-lg-2"></div>
                                <div class="col-md-5 col-lg-4">
                                    <button type="button" href="<?php echo $column['slide_btn_link']; ?>" class="btn btn-primary text-uppercase p-2 mt-2" data-toggle="modal" data-target="#callback"><?php echo $column['slide_btn_text']; ?></button>
                                </div>
                                <div class="col-md-5 col-lg-4">
                                    <div class="dropup">
                                        <button type="button" class="dropdown-toggle btn btn-primary text-uppercase pt-2 pb-2 pl-2 pr-2 mt-2" id="dropdownPhones" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telephone-outbound mr-3" viewBox="0 0 16 16">
                                            <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511zM11 .5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V1.707l-4.146 4.147a.5.5 0 0 1-.708-.708L14.293 1H11.5a.5.5 0 0 1-.5-.5z"/>
                                            </svg>
                                            <?php echo $call_us_text; ?>
                                        </button>
                                        <div class="dropdown-menu border border-primary" aria-labelledby="dropdownPhones">
                                            <a href="tel:<?php echo $phone1_link; ?>" class="dropdown-item"><?php echo $phone1; ?></a>
                                            <a href="tel:<?php echo $phone2_link; ?>" class="dropdown-item"><?php echo $phone2; ?></a>
                                            <a href="tel:<?php echo $phone3_link; ?>" class="dropdown-item"><?php echo $phone3; ?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2"></div> --> 
                                
                      <!--      </div> -->
                      <div class="mx-5 text-uppercase text-white text-banner-down text-center h3">
                      мы компания, которая предоставляет услуги по факторингу и объединяет команду профессионалов по урегулированию проблемной задолженности.
                        
                        </div>
                    </div>
               
                </div>
        <?php } else { ?>
                <div class="carousel-item" style="background-image: url('<?php echo $column['slide_link']; ?>');">
                    <div class="transparent-bg">
                        <div class="carousel-caption d-none d-block text-blue">
                            
                            <p class="h1 text-uppercase mt-2 mb-4 text-break"><b><?php echo $column['slide_title']; ?></b></p>
                            
                            <p><b><?php echo $column['slide_text']; ?></b></p>
                            

                            

                            <a href="<?php echo $facebook_link; ?>" class="mr-3" aria-label="Facebook" target="_blank" rel="nofollow noopener">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="#00407f" class="bi bi-facebook" viewBox="0 0 16 16">
                                    <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
                                </svg>
                            </a>

                            <a href="<?php echo $instagram_link; ?>" class="mr-3" aria-label="Instagram" target="_blank" rel="nofollow noopener">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="#00407f" class="bi bi-instagram" viewBox="0 0 16 16">
                                    <path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"/>
                                </svg>
                            </a>

                           <a href="viber://chat?number=%2B380501001567" class="mr-3">
    <img src="images/web-viber.png" width="32" height="32" alt="Viber" data-filename="viber-white.png"></a><br>

<img src="images/google-maps-icon.png" width="32" height="32" alt="Google maps icon"><a href="http://maps.app.goo.gl/79J4ez4dtweJEwMo8" class="text-black" target="_blank" rel="noopener">
                    Відгуки на сайті Google<img src="images/stars-icon.png" height="20" alt="Stars icon"> (33)</a>
                            <br>
                            <br>
                            
                            <div class="row no-gutters">
                                
                                <div class="col-md-1 col-lg-2"></div>
                                <div class="col-md-5 col-lg-4">
                                    <button type="button" href="<?php echo $column['slide_btn_link']; ?>" class="btn btn-primary text-uppercase p-2 mt-2" data-toggle="modal" data-target="#callback"><?php echo $column['slide_btn_text']; ?></button>
                                </div>
                                <div class="col-md-5 col-lg-4">
                                    <div class="dropup">
                                        <button type="button" class="dropdown-toggle btn btn-primary text-uppercase pt-2 pb-2 pl-2 pr-2 mt-2" id="dropdownPhones" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telephone-outbound mr-3" viewBox="0 0 16 16">
                                            <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511zM11 .5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V1.707l-4.146 4.147a.5.5 0 0 1-.708-.708L14.293 1H11.5a.5.5 0 0 1-.5-.5z"/>
                                            </svg>
                                            <?php echo $call_us_text; ?>
                                        </button>
                                        <div class="dropdown-menu border border-primary" aria-labelledby="dropdownPhones">
                                            <a href="tel:<?php echo $phone1_link; ?>" class="dropdown-item"><?php echo $phone1; ?></a>
                                            <a href="tel:<?php echo $phone2_link; ?>" class="dropdown-item"><?php echo $phone2; ?></a>
                                            <a href="tel:<?php echo $phone3_link; ?>" class="dropdown-item"><?php echo $phone3; ?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2"></div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
        <?php }
        ++$count_num;
        }
        echo '</div>';
    }
    ?>
    
    <a class="carousel-control-prev" href="#MainCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#MainCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>

</div>
