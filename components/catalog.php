<?php
// Работает на Bootstrap 4
// ========================

// Загрузка переменных на нужном языке
include 'language/' . $url->lang . '/catalog.php';
// Подключение к БД
include "model/catalog.php";

$lang_for_link = $url->lang_for_link;
$page_link = $url->page_link;

$show_records = 15; //Сколько записей на 1 странице показывать
$how_many_records = 0; //сколько всего записей по умолчанию
$how_many_pages = 1; //сколько страниц с записями по умолчанию
$how_many_links = 5; //Сколько ссылок на страницы в одном блоке
$how_many_links_r_l = floor($how_many_links / 2); //Сколько ссылок слева от активного номера страницы
if($db_records) {
    $how_many_records = count($db_records); //сколько всего записей
    $how_many_pages = ceil($how_many_records / $show_records); //сколько страниц с записями
}

// Pagination function
// ====================
function pagePagination($lang_for_link, $page_link, $page_num, $how_many_pages, $how_many_links, $how_many_links_r_l) {
    
    if ($how_many_pages > 1) {
        echo '<nav aria-label="Page navigation"><ul class="pagination justify-content-center">'; // стили для блока ссылок по страницам
        if ($how_many_pages <= $how_many_links) {
            for ($i = 1; $i <= $how_many_pages; $i++) {
                if ($i == 1) {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='$lang_for_link$page_link'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='$lang_for_link$page_link'>$i</a>
                            </li>";
                    }
                }
                else {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                    }
                }
            }
        }
        elseif ($how_many_pages > $how_many_links && $page_num < $how_many_links) {
            for ($i = 1; $i <= $how_many_pages; $i++) {
                if ($i == 1) {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='$lang_for_link$page_link'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='$lang_for_link$page_link'>$i</a>
                            </li>";
                    }
                }
                else {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                    }
                }
                if ($i == $how_many_links) {
                    break;
                }
            }
            echo "<li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>
                   <li class='page-item'>
                        <a class='page-link' href='$lang_for_link$page_link?p=$how_many_pages'>$how_many_pages</a>
                   </li>";
        }
        elseif ($how_many_pages > $how_many_links && $page_num >= $how_many_links && $how_many_pages >= $page_num + $how_many_links_r_l) {
            $new_i = $page_num - $how_many_links_r_l;
            $count_page_num = 1;
            $end_of_links = false;
            echo "<li class='page-item'>
                        <a class='page-link' href='$lang_for_link$page_link'>1</a>
                   </li>
                    <li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>";
            for ($i = $new_i; $i <= $how_many_pages; $i++) {
                if ($i == $page_num) {
                    echo "<li class='page-item active'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                }
                else {
                     echo "<li class='page-item'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                }
                if ($i == $how_many_pages) {
                    $end_of_links = true;
                }
                if ($count_page_num == $how_many_links) {
                    break;
                }
                ++$count_page_num;
            }
            if (!$end_of_links) {
                echo "<li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>
                   <li class='page-item'>
                        <a class='page-link' href='$lang_for_link$page_link?p=$how_many_pages'>$how_many_pages</a>
                   </li>";
            }
        }
        elseif ($how_many_pages > $how_many_links && $page_num >= $how_many_links && $how_many_pages < $page_num + $how_many_links_r_l) {
            $new_i = $how_many_pages - ($how_many_links - 1);
            $count_page_num = 1;
            echo "<li class='page-item'>
                        <a class='page-link' href='$lang_for_link$page_link'>1</a>
                   </li>
                    <li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>";
            for ($i = $new_i; $i <= $how_many_pages; $i++) {
                if ($i == $page_num) {
                   echo "<li class='page-item active'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                }
                else {
                     echo "<li class='page-item'>
                                <a class='page-link' href='$lang_for_link$page_link?p=$i'>$i</a>
                            </li>";
                }
                if ($i == $how_many_pages) {
                    $end_of_links = true;
                }
                if ($count_page_num == $how_many_links) {
                    break;
                }
                ++$count_page_num;
            }
        }
        echo '</ul></nav>'; // стили для блока ссылок по страницам
    }
}
// End Pagination function
// ========================


pagePagination($lang_for_link, $page_link, $page_num, $how_many_pages, $how_many_links, $how_many_links_r_l);

// Показ списка статей, товаров, новостей или записей
// ===================================================
$start_show_num = $show_records * ($page_num - 1);
$count_record_by_category = 0;
$stop_count_show_record = 0;

echo '<div class="container mt-5 mb-5"><div class="row row-cols-1 row-cols-sm-2 row-cols-md-3">';
    
if ($db_records) {
    foreach ($db_records as $line => $column) { 
        $id_record = $column['record_id'];

        if($count_record_by_category >= $start_show_num) {

            //Код проверки наличия изображения для записи
            if(isset($column['record_img_preview'])) {
                
                $record_image_check = trim($column['record_img_preview']);
                
                if($record_image_check != null && file_exists($record_image_check)) {
                    $record_image = $record_image_check;
                }
                else {
                    $record_image = 'images/no_image.png';
                }
            }
            else {
                $record_image = 'images/no_image.png';
            }

            //Код проверки наличия тега alt для изображения
            if(isset($column['record_img_alt'])) {
                
                $alt_image_check = trim($column['record_img_alt']);
                
                if($alt_image_check != null) {
                    $record_img_alt = $alt_image_check;
                }
                else {
                    $record_img_alt = 'Записи сайта, site records';
                }
            }
            else {
                $record_img_alt = 'Записи сайта, site records';
            }
            
            // Формирование наличия товара
            if (isset($column['item_availability'])) {
                switch ($column['item_availability']) {
                    case 'InStock':
                        $availability_status = $instock;
                        $btn_card_text = $buy_button_text;
                        break;
                    case 'OutOfStock':
                        $availability_status = $outofstock;
                        $btn_card_text = $view_button_text;
                        break;
                    case 'PreOrder':
                        $availability_status = $preorder;
                        $btn_card_text = $order_button_text;
                        break;
                    default:
                        $availability_status = '<small><i>no data...</i></small>';
                        $btn_card_text = $view_button_text;
                        break;
                }
            }
             else {
                $availability_status = $outofstock;
                $btn_card_text = $view_button_text;
            }
            
            // Формирование новой цены со скидкой
            if ($column['item_new_price']) {
                $item_price = $column['item_price'];
                $item_old_price = "<span class='align-top text-muted ml-2'><s>$item_price</s></span>";
                $item_price = $column['item_new_price'];
                $sale_label = true;
            }
             else {
                $item_price = $column['item_price'];
                $item_old_price = null;
                $sale_label = false;
            }
            ?>

            <div class="col mb-4 mt-4">
                <div class="card h-100">
                    
                    <?php
                    if ($sale_label) {
                        echo '<span class="bg-danger position-absolute text-white pl-2 pr-2 pt-1 pb-1 ml-n1 mt-n1">SALE</span>';
                    }
                    ?>
                    
                    <div class="text-center">
                        <img src="<?php echo $record_image; ?>" loading="lazy" class="card-img-top" alt="<?php echo $record_img_alt; ?>">
                    </div>
                    <div class="card-body">
                        <p class="card-title mb-0">
                            <?php
                            if ($item_old_price) {
                                echo '<span class="h3 text-danger">' . $item_price . '</span> ' . $item_old_price . ' ' . $currency_ua;
                            }
                            else {
                                echo '<span class="h3">' . $item_price . '</span> ' . $currency_ua;
                            }
                            ?>
                        </p>
                        <p><?php echo $availability_status; ?></p>
                        <p class="card-text">
                            <a href="<?php echo $url->lang_for_link . $url->page_link . '/' . $column['url']; ?>">
                                <?php echo $column['h1']; ?></a>
                        </p>
                    </div>
                    <div class="card-footer text-center bg-transparent border-0">
                        <a href="<?php echo $url->lang_for_link . $url->page_link . '/' . $column['url']; ?>" role="button" class="btn btn-lg btn-primary"><?php echo $btn_card_text; ?></a>
                    </div>
                </div>
            </div>
    
    <?php $stop_count_show_record++;

        if($stop_count_show_record == $show_records) {
            break;
            }
        }
        $count_record_by_category++;
    }
}
    
echo '</div></div>';

pagePagination($lang_for_link, $page_link, $page_num, $how_many_pages, $how_many_links, $how_many_links_r_l);

if ($page_num == 1) {
    
    // Вывод дополнительной информации о категории
    if (file_exists($db_category['category_text'])) {
        $content = file_get_contents($db_category['category_text']);
        echo $content;
    }
}
