<?php
//Совместимость с Bootstrap 4
//===========================

// Переменные для формы с корзиной
include 'language/' . $url->lang . '/cart.php';

$lang = $url->lang;
$lang_for_link = $url->lang_for_link;
$seo_url = $page->seo_url;
?>

<!-- Modal Items List -->
<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title" id="cartModalLabel">
                    <svg width="1.1em" height="1.1em" viewBox="0 0 16 16" class="bi bi-basket2 mb-1 mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M1.111 7.186A.5.5 0 0 1 1.5 7h13a.5.5 0 0 1 .489.605l-1.5 7A.5.5 0 0 1 13 15H3a.5.5 0 0 1-.489-.395l-1.5-7a.5.5 0 0 1 .1-.42zM2.118 8l1.286 6h9.192l1.286-6H2.118z"/>
                        <path fill-rule="evenodd" d="M11.314 1.036a.5.5 0 0 1 .65.278l2 5a.5.5 0 1 1-.928.372l-2-5a.5.5 0 0 1 .278-.65zm-6.628 0a.5.5 0 0 0-.65.278l-2 5a.5.5 0 1 0 .928.372l2-5a.5.5 0 0 0-.278-.65z"/>
                        <path d="M4 10a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zm3 0a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zm3 0a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zM0 6.5A.5.5 0 0 1 .5 6h15a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H.5a.5.5 0 0 1-.5-.5v-1z"/>
                    </svg>
                    <?php echo $cart_text; ?>
                </p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body cart-body">
                <div id="cartResult">
                    <?php
                    $_POST['sess_start'] = "no";
                    $_POST['lang'] = $lang;
                    $_POST['lang_for_link'] = $lang_for_link;
                    $_POST['seo_url'] = $seo_url;
                    include 'modules/cart.php';
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $cart_close_btn_text; ?></button>
                <?php
                
                if($count_items > 0) {
                    $btn_checkout_show = null;
                }
                else {
                    $btn_checkout_hide = 'd-none';
                }
                
                ?>
                <a href="<?php echo $url->lang_for_link; ?>checkout" class="btn-checkout btn btn-primary <?php echo $btn_checkout_hide; ?>"><?php echo $checkout_text; ?></a>
            
            </div>
        </div>
    </div>
</div>

<!-- Modal for product added -->
<div id="productAddedModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="productAddedLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title" id="productAddedLabel">
                    <svg width="1.1em" height="1.1em" viewBox="0 0 16 16" class="bi bi-bag-check mb-1 mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V5zM1 4v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4H1z"/>
                        <path d="M8 1.5A2.5 2.5 0 0 0 5.5 4h-1a3.5 3.5 0 1 1 7 0h-1A2.5 2.5 0 0 0 8 1.5z"/>
                        <path fill-rule="evenodd" d="M10.854 7.646a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 10.293l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                    </svg>
                    <?php echo $product_added_title; ?>
                </p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-center">
                    <?php echo $product_added_text; ?>
                    <br>
                    <br>
                    <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-basket2-fill mr-2 mb-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M11.314 1.036a.5.5 0 0 1 .65.278l2 5a.5.5 0 1 1-.928.372l-2-5a.5.5 0 0 1 .278-.65zm-6.628 0a.5.5 0 0 0-.65.278l-2 5a.5.5 0 1 0 .928.372l2-5a.5.5 0 0 0-.278-.65z"/>
                        <path fill-rule="evenodd" d="M1.5 7a.5.5 0 0 0-.489.605l1.5 7A.5.5 0 0 0 3 15h10a.5.5 0 0 0 .489-.395l1.5-7A.5.5 0 0 0 14.5 7h-13zM4 10a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zm3 0a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zm4-1a1 1 0 0 0-1 1v2a1 1 0 1 0 2 0v-2a1 1 0 0 0-1-1z"/>
                        <path d="M0 6.5A.5.5 0 0 1 .5 6h15a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H.5a.5.5 0 0 1-.5-.5v-1z"/>
                    </svg>
                    <a href="#" data-toggle="modal" data-target="#cartModal">
                        <?php echo $open_cart_text; ?>
                    </a>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $cart_close_btn_text; ?></button>
            </div>
        </div>
    </div>
</div>
