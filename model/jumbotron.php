<?php
$jumbotron = fopen(DB_DIR_PATH . "$url->lang/jumbotron.csv", "rt") or die("Error Establishing a Database Connection...");

if($jumbotron) {
    for ($i = 0; $data = fgetcsv($jumbotron, 1000, ","); $i++) {
        if ($page->seo_url == $data[1]) {
            $jumbotron_id = $data[0]; //ID
            $jumbotron_page = $data[1]; //К какой странице (seo url)
            $jumbotron_image = $data[2]; //Ссылка на картинку
            $jumbotron_text = $data[3]; //Текст на фоне
            $jumbotron_btn = $data[4]; //Текст на кнопке
            $jumbotron_btn_link = $data[5]; //Ссылка для кнопки
            break;
        }
        else {
            $jumbotron_id = null; //ID
            $jumbotron_page = null; //К какой странице (seo url)
            $jumbotron_image = null; //Ссылка на картинку
            $jumbotron_text = null; //Текст на фоне
            $jumbotron_btn = null; //Текст на кнопке
            $jumbotron_btn_link = null; //Ссылка для кнопки
        }
    }
}
fclose($jumbotron);
