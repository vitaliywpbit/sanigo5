<?php

$logo = fopen(DB_DIR_PATH . "$url->lang/logo.csv", "rt") or die("Error Establishing a Database Connection...");

if($logo) {
    for ($i = 0; $data = fgetcsv($logo, 1000, ","); $i++) {
        $db_logo = [
            'logo_image_link' => $data[0], // ссылка на картинку с логотипом
            'logo_img_alt' => $data[1] // тег alt для картинки с логотипом
        ];
    break;
    }
}
fclose($logo);
