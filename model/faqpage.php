<?php

$faqpage = fopen(DB_DIR_PATH . "$lang/faqpage.csv", "rt") or die("Error Establishing a Database Connection...");

if($faqpage) {
    $db_faqpage = null;
    for ($i = 0; $data = fgetcsv($faqpage, 0, ","); $i++) {
        if ($data[0] == $seo_url) {
            $db_faqpage[] = [
                'question' => $data[1],
                'answer' => $data[2]
            ];
        }
    }
}
fclose($faqpage);
