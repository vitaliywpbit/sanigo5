<?php
// Поиск категорий
$category = fopen(DB_DIR_PATH . "$url->lang/pages.csv", "rt") or die("Error Establishing a Database Connection...");

if($category) {
    for ($i = 0; $data = fgetcsv($category, 0, ","); $i++) {
        
        if ($data[1] == 'category' && $url->page_db_file == $data[2]) {
            
            $path_to_content = DB_DIR_PATH . $url->lang . '/categories/' . $data[2] . '.tpl';
            
            $db_category = [
                'category_id' => $data[0],
                'category_url' => $data[2],
                'category_text' => $path_to_content
            ];
            break;
        }
        else {
            $db_category = [
                'category_id' => null,
                'category_url' => null,
                'category_text' => null
            ];
        }
    }
}
fclose($category);

//Открытие списка товаров
$records = fopen(DB_DIR_PATH . "$url->lang/items.csv", "rt") or die("Error Establishing a Database Connection...");

if($records) {
    
    $db_records = null;
    $count_records = 0;
    
    for ($i = 0; $data = fgetcsv($records, 0, ","); $i++) {
        
        if ($db_category['category_id'] == $data[7]) {
            
            $category_url = $db_category['category_url'];
            
            if ($count_records == 0) {
                $db_records = null;
            }
            
            $path_to_content = DB_DIR_PATH . $url->lang . '/' . $data[1] . '/' . $category_url . '/' . $data[2] . '.tpl';
            
            $db_records[] = [
                'record_id' => $data[0],
                'page_layout' => $data[1],
                'url' => $data[2],
                'title' => $data[3],
                'keywords' => $data[4],
                'description' => $data[5],
                'h1' => $data[6],
                'category_id' => $data[7],
                'content' => $path_to_content,
                'record_img_1x1' => $data[8],
                'record_img_4x3' => $data[9],
                'record_img_16x9' => $data[10],
                'record_img_preview' => $data[11],
                'record_img_alt' => $data[12],
                'record_all_images' => $data[13],
                'item_short_description' => $data[14],
                'item_price' => $data[15],
                'item_new_price' => $data[16], // sale - новая цена со скидкой
                'item_sku' => $data[17],
                'item_availability' => $data[18],
                'item_brand_name' => $data[19]
            ];
            ++$count_records;
        }
    }
}
else {
    $db_records = null;
}

fclose($records);

// Если записи есть в данной категории, перезаписываем их в обратном порядке
if ($db_records) {
    $db_records = array_reverse($db_records);
}
