<?php

//ОСНОВНОЕ МЕНЮ
$menu = fopen(DB_DIR_PATH . "$url->lang/menu.csv", "rt") or die("Error Establishing a Database Connection...");

if($menu) {
    for ($i = 0; $data = fgetcsv($menu, 1000, ","); $i++) {
        $db_menu[] = [
            'menu_id' => $data[0], //ID
            'menu_item' => $data[1], //Пункт меню
            'menu_link' => $data[2] //Ссылка. Если ## - открытие подменю
        ];
    }
}
fclose($menu);

//ПОДМЕНЮ 1
$submenu_01 = fopen(DB_DIR_PATH . "$url->lang/submenu_01.csv", "rt") or die("Error Establishing a Database Connection...");

if($submenu_01) {
    for ($i = 0; $data = fgetcsv($submenu_01, 1000, ","); $i++) {
        $db_submenu_01[] = [
            'submenu_01_id' => $data[0], //ID
            'submenu_01_menu_id' => $data[1], //ID главного меню
            'submenu_01_item' => $data[2], //Пункт меню
            'submenu_01_link' => $data[3] //Ссылка. Если ### - открытие 2-го подменю
        ];
    }
}
fclose($submenu_01);

//ПОДМЕНЮ 2
$submenu_02 = fopen(DB_DIR_PATH . "$url->lang/submenu_02.csv", "rt") or die("Error Establishing a Database Connection...");

if($submenu_02) {
    for ($i = 0; $data = fgetcsv($submenu_02, 1000, ","); $i++) {
        $db_submenu_02[] = [
            'submenu_02_id' => $data[0], //ID
            'submenu_02_menu_id' => $data[1], //ID 1-го подменю
            'submenu_02_item' => $data[2], //Пункт меню
            'submenu_02_link' => $data[3] //Ссылка
        ];
    }
}
fclose($submenu_02);
