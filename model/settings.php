<?php
$settings = fopen(DB_DIR_PATH . 'settings.csv', "rt") or die("Error Establishing a Database Connection...");

if($settings)
{
    for ($i = 0; $data = fgetcsv($settings, 1000, ","); $i++)
    {
        $db_settings[] = $data[0]; /*option*/
    }
}
fclose($settings);

$http_protocol = $db_settings[0]; // ex: http://
$domain_name = $db_settings[1]; // ex: web-platinum.com
$page_default = $db_settings[2]; // страница по умолчанию
$main_send_email = $db_settings[3]; // Основной Email для отправки сообщений или заказов
$img_default = $db_settings[4]; // Картинка по умолчанию для OpenGraph и разметки JSON-LD (Логотип min 112×112px)
$time_zone = $db_settings[5]; // TimeZone (ex: +03:00)
$google_id = $db_settings[6]; // Google ID для Google Analytics
