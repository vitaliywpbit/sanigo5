<?php
$carousel = fopen(DB_DIR_PATH . "$url->lang/carousel.csv", "rt") or die("Error Establishing a Database Connection...");

if($carousel) {
    
    $db_carousel_images = null;
    
    for ($i = 0; $data = fgetcsv($carousel, 1000, ","); $i++) {
        if ($page->seo_url == $data[1]) {
            
            $db_carousel_images[] = [
                'slide_id' => $data[0], // ID
                'slide_page' => $data[1], // для какой странице (seo url)
                'slide_link' => $data[2], // ссылка на картинку для слайдшоу
                'slide_btn_text' => $data[3], // тект для кнопки
                'slide_btn_link' => $data[4], // ссылка для кнопки
                'slide_title' => $data[5], // заголовок слайда
                'slide_text' => $data[6] // Дополнительный текст- описание
            ];
        }
    }
}
fclose($carousel);
