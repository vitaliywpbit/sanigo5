<?php

if (trim(file_get_contents(DB_DIR_PATH . "$url->lang/$url->page_db_file.csv")) != false) {
    
    $records = fopen(DB_DIR_PATH . "$url->lang/$url->page_db_file.csv", "rt") or die("Error Establishing a Database Connection...");
    
    for ($i = 0; $data = fgetcsv($records, 0, ","); $i++) {
        
        $path_to_content = DB_DIR_PATH . $url->lang . '/' . $data[1] . '/' . $data[2] . '.tpl';
        
        $db_records[] = [
            'record_id' => $data[0],
            'page_layout' => $data[1],
            'url' => $data[2],
            'title' => $data[3],
            'keywords' => $data[4],
            'description' => $data[5],
            'h1' => $data[6],
            'content' => $path_to_content,
            'record_date' => $data[7],
            'record_mod_date' => $data[8],
            'record_img_1x1' => $data[9],
            'record_img_4x3' => $data[10],
            'record_img_16x9' => $data[11],
            'record_img_preview' => $data[12],
            'record_img_alt' => $data[13],
            'record_author' => $data[14],
            'record_author_type' => $data[15] // Person или Organization для JSON-LD
        ];
    }
    fclose($records);
    $db_records = array_reverse($db_records);
}
 else {
    $db_records = null;
}
