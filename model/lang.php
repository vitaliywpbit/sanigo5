<?php
$lang = fopen(DB_DIR_PATH . 'lang.csv', "rt") or die("Error Establishing a Database Connection...");

if($lang)
{
    for ($i = 0; $data = fgetcsv($lang, 1000, ","); $i++)
    {
        $db_lang[] =[
            'lang_code' => $data[0], // Код языка
            'lang_name' => $data[1], // Название языка
            'lang_default' => $data[2], // Язык по умолчанию
        ];
    }
}
fclose($lang);

//Какой язык по умолчанию
foreach ($db_lang as $line => $column)
{
    if ($column['lang_default'] == 'd')
    {
        $lang_default = $column['lang_code'];
        break;
    }
}
