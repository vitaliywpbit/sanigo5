<?php
/*v6.2*/

// Инициализация сессии
session_name('OASKHARKOVUASESSID');
session_start();

// session_destroy();
// unset($_SESSION['items']);

require_once 'config.php'; // Основной файл конфигурации

// Выполнять сборку мусора (удаление старых файлов сессий)
// по истечении определенного периода
if (file_exists(GC_TIME)) {
    if (filemtime(GC_TIME) < time() - GC_PERIOD) {
        //session_gc();
        touch(GC_TIME);
    }
}
else {
    touch(GC_TIME);
}

// Блок загрузки основных настроек, опций для сайта
include 'model/settings.php';
// Проверка доступных языков для сайта, язык по умолчанию
include 'model/lang.php';
// Параметр ?p= (какой номер страницы)
if(isset($_GET['p'])) {
    $page_num = $_GET['p'];
    if(!is_numeric($page_num)) {
        $page_num = 1;
    }
}
else {
    $page_num = 1;
}

$url = explode("/", $_SERVER['REQUEST_URI']); // Чтение и обработка URL из браузерной строки
$start_links = false;
$amp = false;
foreach ($url as $url_pieces) {
    if ($url_pieces == $domain_name) {
        $start_links = true;
    }
    elseif ($url_pieces == 'amp') {
        $amp = true;
        break;
    }
}

if ($amp) {
    require_once 'index_amp.php';
}
else {
    if ($start_links) { // Ex.: http://localhost/site.net/ink_1/link_2/link_3
        $link_1 = 2;
        $link_2 = 3;
        $link_3 = 4;
        $current_url = $_SERVER['REQUEST_URI']; // Актуальный адрес страницы для localhost
        $base_url = '/' . $domain_name . '/'; // Base url для localhost
    }
    else { // Ex.: http://site.net/ink_1/link_2/link_3
        $link_1 = 1;
        $link_2 = 2;
        $link_3 = 3;
        $current_url = $http_protocol . $domain_name . $_SERVER['REQUEST_URI']; // Актуальный адрес страницы
        $base_url = $http_protocol . $domain_name . '/'; // Base url
    }

    class Url {
        public $lang;
        public $lang_name;
        public $lang_for_link;
        public $page_link;
        public $record_link;
        public $page_db_file;

        function __construct($url, $page_default, $db_lang, $lang_default) {
            $this->lang = $lang_default;
            $this->lang_name = 'Language';
            $this->lang_for_link = null;
            $this->page_link = null;
            $this->page_db_file = '404'; // 404.csv
            $this->record_link = null;

            foreach ($url as $db_link_key => $link_value) {
                $link_cut = explode('?', $link_value);
                $link_value = $link_cut[0];
                switch ($db_link_key) {
                    case $GLOBALS['link_1']:
                        $lang_link = false;
                        foreach ($db_lang as $lang_value) {
                            if ($lang_value['lang_code'] == $link_value && $lang_value['lang_default'] != 'd') {
                                $this->lang = $link_value;
                                $this->lang_name = $lang_value['lang_name'];
                                $this->lang_for_link = $link_value . '/';
                                $lang_link = true;
                                break;
                            }
                            else {
                                foreach ($db_lang as $lang_value) {
                                    if ($lang_value['lang_default'] == 'd') {
                                        $this->lang_name = $lang_value['lang_name'];
                                    }
                                }
                                if ($link_value == '') {
                                    $this->page_link = $page_default;
                                    $this->page_db_file = $page_default;
                                }
                                elseif ($link_value == 'home') { // От дубля домашней страницы по ссылке "home"
                                    $this->page_link = null;
                                }
                                else {
                                    $this->page_link = $link_value;
                                    $this->page_db_file = $link_value;
                                }
                            }
                        }
                        break;
                    case $GLOBALS['link_2']:
                        if($lang_link) {
                            if ($link_value == '') {
                                $this->page_link = $page_default;
                                $this->page_db_file = $page_default;
                            }
                            elseif ($link_value == 'home') { // От дубля домашней страницы по ссылке "home"
                                $this->page_link = null;
                            }
                            else{
                                $this->page_link = $link_value;
                                $this->page_db_file = $link_value;
                            }
                        }
                        else {
                            $this->record_link = $link_value;
                            $this->page_link = null;
                        }
                        break;
                    case $GLOBALS['link_3']:
                        if ($link_value == '') {
                            $this->page_link = null;
                            $this->record_link = null;
                        }
                        else{
                            $this->record_link = $link_value;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
    $url = new Url($url, $page_default, $db_lang, $lang_default);

    class Pages {
        public $err404;
        public $id;
        public $page_layout;
        public $seo_url;
        public $title;
        public $keywords;
        public $description;
        public $h1;
        public $content;
        public $date_create;
        public $date_mod;
        public $image;
        public $image_alt;
        public $author;
        public $author_type;
        public $catalog = false;

        public function __construct($lang, $page_link, $page_db_file, $record_link, $img_default) {
            // По умолчанию загрузка мета тегов и контента для страницы 404
            $page_404 = fopen(DB_DIR_PATH . "$lang/404.csv", "rt") or die("Error Establishing a Database Connection...");
            
            for($i = 0; $data = fgetcsv($page_404, 1000, ","); $i++) {
                $this->err404 = true;
                $this->page_layout = $data[0];
                $this->seo_url = $data[1];
                $this->title = $data[2];
                $this->keywords = $data[3];
                $this->description = $data[4];
                $this->h1 = $data[5];
                $this->content = null;
                $this->image = $img_default;
            }
            fclose($page_404);
            
            // Проверка, существует ли файл БД для статей, новостей или записей и подключение к существующей
            // Если таковой не существует, выполняем подключение к основному файлу БД (pages.csv)
            $filename = DB_DIR_PATH . "$lang/$page_db_file.csv";
            
            if(file_exists($filename) && $record_link) {
                $pages = fopen(DB_DIR_PATH . "$lang/$page_db_file.csv", "rt") or die("Error Establishing a Database Connection...");
            }
            elseif (!file_exists($filename) && $record_link) {
                $pages = fopen(DB_DIR_PATH . "$lang/items.csv", "rt") or die("Error Establishing a Database Connection...");
            }
            elseif(!$record_link) {
                $pages = fopen(DB_DIR_PATH . "$lang/pages.csv", "rt") or die("Error Establishing a Database Connection...");
            }
            else{
                $page_link = null;
                $record_link = null;
                $pages = fopen(DB_DIR_PATH . "$lang/pages.csv", "rt") or die("Error Establishing a Database Connection...");
            }
            
            // Загрузка мета тегов и контента для основных страниц
            for($i = 0; $data = fgetcsv($pages, 0, ","); $i++) {
                if($data[2] == $page_link or $data[2] == $record_link) {
                    $this->err404 = false;
                    $this->id = $data[0];
                    if ($data[1] == 'category') {
                        $this->page_layout = 'catalog';
                    }
                    else {
                        $this->page_layout = $data[1];
                    }
                    $this->seo_url = $data[2];
                    $this->title = $data[3];
                    $this->keywords = $data[4];
                    $this->description = $data[5];
                    $this->h1 = $data[6];
                    $this->content = null;
                    if($data[2] == $record_link && $data[1] != 'catalog') {
                        //Данные из БД для записей
                        $this->date_create = $data[7];
                        $this->date_mod = $data[8];
                        $this->image = $data[9]; // image 1x1
                        $this->image_alt = $data[13];
                        $this->author = $data[14];
                        $this->author_type = $data[15];
                    }
                    elseif ($data[2] == $record_link && $data[1] == 'catalog') {
                        //Данные из БД для товаров
                        $this->catalog = true;
                        $this->image = $data[8]; // image 1x1
                        $this->image_alt = $data[12];
                    }
                    break;
                }
            }
            fclose($pages);
        }
    }
    $page = new Pages($url->lang, $url->page_link, $url->page_db_file, $url->record_link, $img_default);

    // HTTP Заголовок для несуществующей сраницы с кодом 404
    if ($page->err404) {
        header("HTTP/1.0 404 Not Found");
    }

    // Языковые версии сайта
    function HrefLang($page, $page_db, $record, $db_lang, $base_url, $lang, $catalog) {
        
        if ($record && !$catalog) {
            $page_link = $page_db . '/' . $record;
            $GLOBALS['path_page_db'] = $page_db . '/'. $record . '.tpl';
        }
        elseif ($record && $catalog) {
            $page_link = $page_db . '/' . $record;
            $GLOBALS['path_page_db'] = 'catalog/' . $page_db . '/'. $record . '.tpl';
        }
        elseif ($page == 'home') {
            $page_link = null;
            $GLOBALS['path_page_db'] = 'pages/' . $page_db . '.tpl';
        }
        else {
            $page_link = $page;
            $path_category_db = DB_DIR_PATH . $lang . '/categories/' . $page_db . '.tpl';
            
            if (file_exists($path_category_db)) {
                $GLOBALS['path_page_db'] = 'categories/' . $page_db . '.tpl';
            }
            else {
                $GLOBALS['path_page_db'] = 'pages/' . $page_db . '.tpl';
            }
        }
        
        foreach ($db_lang as $lang_value) {
            
            $full_path_page_db = DB_DIR_PATH . $lang_value['lang_code'] . '/' . $GLOBALS['path_page_db'];
            
            // Существует ли страница на другом языке или нет
            if (file_exists($full_path_page_db)) {
            
                if ($lang_value['lang_default'] == 'd') { ?>
                    <link rel="alternate" hreflang="<?php echo $lang_value['lang_code']; ?>" href="<?php echo $base_url . $page_link; ?>" />
            <?php }
                else { ?>
                    <link rel="alternate" hreflang="<?php echo $lang_value['lang_code']; ?>" href="<?php echo $base_url . $lang_value['lang_code'] . '/' . $page_link; ?>" />
            <?php }
            }
        }
    }
    
    // Поиск AMP версий основных страниц и записей, создание ссылки для тега link rel="amphtml"
    // <link rel="amphtml" href="http://www.example.com/amp/page/document.html">
    function SearchAmpHtml($page, $page_db, $record, $lang, $lang_for_link, $base_url) {
        if ($record && file_exists(DB_DIR_PATH . 'amp/' . $lang . '/' . $page_db . '.csv')) {
            $records = fopen(DB_DIR_PATH . 'amp/' . $lang . '/' . $page_db . '.csv', "rt") or die("Error Establishing a Database Connection...");
            if($records) {
                for ($i = 0; $data = fgetcsv($records, 0, ","); $i++) {
                    if ($record == $data[2]) {
                        echo '<link rel="amphtml" href="' . $base_url . 'amp/' . $lang_for_link . $page_db . '/' . $record . '">';
                        break;
                    }
                }
                fclose($records);
            }
        }
        elseif (file_exists(DB_DIR_PATH . 'amp/' . $lang . '/pages.csv')) {
            $pages = fopen(DB_DIR_PATH . 'amp/' . $lang . '/pages.csv', "rt") or die("Error Establishing a Database Connection...");
            if ($pages) {
                for ($i = 0; $data = fgetcsv($pages, 0, ","); $i++) {
                    if ($page_db == $data[2] && $data[2] != 'home') {
                        echo '<link rel="amphtml" href="' . $base_url . 'amp/' . $lang_for_link . $page_db . '">';
                        break;
                    }
                    elseif ($page_db == $data[2] && $data[2] == 'home') {
                        echo '<link rel="amphtml" href="' . $base_url . 'amp/' . $lang_for_link . '">';
                        break;
                    }
                }
                fclose($pages);
            }
        }
    }

    // Изображение для Open Graph из записей и каталога
    if ($url->record_link && !$page->catalog) {
        $image = $base_url . $page->image;
    }
    elseif ($url->record_link && $page->catalog) {
        $image = $base_url . $page->image;
    }
    else {
        $image = $page->image;
    }
    
    // Буферизации вывода для включения файла PHP в строку 
    // (для вставки и обработки файлов шаблона: .tpl)
    function get_include_contents($lang, $seo_url, $file) {
    
        if (is_file($file)) {

            ob_start();

            include $file;

            return ob_get_clean();
        }
        
        return false;
    }

    // Остальной контент на нужном языке
    include 'language/' . $url->lang . '/all_content.php';
    
    // ДОПОЛНИТЕЛЬНЫЕ ПОДКЛЮЧЕНИЯ И НАСТРОЙКИ
    // ***
    ?>
    <!DOCTYPE html>
    <html lang="<?php echo $url->lang; ?>">
        <head>
            <meta charset="utf-8">
            <base href="<?php echo $base_url; ?>">
            <title><?php echo $page->title; ?></title>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <?php
            if ($page->page_layout == '404' or $page->page_layout == 'checkout' or $page->page_layout == 'terms') { ?>
                <meta name="robots" content="noindex">
            <?php } ?>
            <meta name="keywords" content="<?php echo $page->keywords; ?>"/>
            <meta name="description" content="<?php echo $page->description; ?>"/>

            <?php
            //Языковые версии сайта
            HrefLang($url->page_link, $url->page_db_file, $url->record_link, $db_lang, $base_url, $url->lang, $page->catalog);
            //Версии AMP страниц
            SearchAmpHtml($url->page_link, $url->page_db_file, $url->record_link, $url->lang, $url->lang_for_link, $base_url);
            ?>

            <!-- Favicons -->
            <link rel="apple-touch-icon" href="images/favicon-192x192.png" />
            <link rel="icon" type="image/png" sizes="192x192" href="images/favicon-192x192.png" />
            <link rel="icon" type="image/png" sizes="144x144" href="images/favicon-144x144.png" />
            <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png" />
            <link rel="icon" type="image/png" sizes="48x48" href="images/favicon-48x48.png" />
            <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png" />
            <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png" />

            <!-- Twitter Cards -->
            <meta name="twitter:card" content="summary" />
            <meta name="twitter:title" content="<?php echo $page->title; ?>" />
            <meta name="twitter:description" content="<?php echo $page->description; ?>" />
            <meta name="twitter:image" content="<?php echo $image; ?>" />
            <!-- Facebook & Open Graph -->
            <meta property="og:type" content="website" />
            <meta property="og:title" content="<?php echo $page->title; ?>" />
            <meta property="og:description" content="<?php echo $page->description; ?>" />
            <meta property="og:url" content="<?php echo $current_url; ?>" />
            <meta property="og:image" content="<?php echo $image; ?>" />

            <!-- PRELOAD CSS & JS -->
            <link rel="preload" href="css/bootstrap.min.css" as="style">
            <link rel="preload" href="css/styles.css" as="style">
            <link rel="preload" href="css/lightbox.min.css" as="style" onload="this.rel='stylesheet'">
            <link rel="preload" href="js/jquery.min.js" as="script">

            <!-- CSS -->
            <link href="css/bootstrap.min.css" rel="stylesheet">
            <link href="css/styles.css" rel="stylesheet">
            <link href="css/lightbox.min.css" rel="stylesheet">
            
            <!-- JS -->
          <script src="js/jquery.min.js"></script> 

 <!-- 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>  -->




            
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <?php
            if ($google_id) { ?>
                <script async src="http://www.googletagmanager.com/gtag/js?id=<?php echo $google_id; ?>"></script>
                <script>
                  window.dataLayer = window.dataLayer || [];
                  function gtag(){dataLayer.push(arguments);}
                  gtag('js', new Date());

                  gtag('config', '<?php echo $google_id; ?>');
                </script>
            <?php } ?>

        </head>

        <body>
            
            <?php include 'page_layout/' . $page->page_layout . '.php'; ?>
            
            <!-- CALLBACK BUTTON -->
            <div class="cursor" id="popup__toggle" data-toggle="modal" data-target="#callback">
                <div class="circlephone" style="transform-origin: center;"></div>
                <div class="circle-fill" style="transform-origin: center;"></div>
                <div class="img-circle" style="transform-origin: center;">
                    <div class="img-circleblock" style="transform-origin: center;">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="#ffffff" class="bi bi-telephone-fill" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
                        </svg>
                    </div>
                </div>
            </div>
            
            <!-- BUTTON UP -->
            <div id="toTop" >^</div>
            
            <script>
                $(function() {
                    
                    // BUTTON UP
                    $(window).scroll(function() {
                        if($(this).scrollTop() != 0) {
                            $('#toTop').fadeIn();
                        } else {
                            $('#toTop').fadeOut();
                        }
                    });
                    $('#toTop').click(function() {
                        $('body,html').animate({scrollTop:0},800);
                    });
                    
                    // SCROLL TO #MENU
                    $('a[href*="#m-"]').click(function() {
                        if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
                            var $target = $(this.hash);
                            $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
                            if ($target.length) {
                                var targetOffset = $target.offset().top - 100;//отступ блока сверху
                                $('html,body').animate({scrollTop: targetOffset}, 800);//скорость прокрутки
                                return false;
                            }
                        }
                    });
                    
                    // BOOTSTRAP 4.x.x SUBMENU
                    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
                        if (!$(this).next().hasClass('show')) {
                            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
                        }

                        var $subMenu = $(this).next(".dropdown-menu");
                        $subMenu.toggleClass('show');

                        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
                            $('.dropdown-submenu .show').removeClass("show");
                        });
                    return false;
                    });
                    
                    // POPOVER
                    $('[data-toggle="popover"]').popover();
                
                });
            </script>
            
            <!--YOUTUBE OPTIMIZATION-->
            <script>
            'use strict';
            function r(f){/in/.test(document.readyState)?setTimeout('r('+f+')',9):f()}
            r(function(){
                if (!document.getElementsByClassName) {
                    // Поддержка IE8
                    var getElementsByClassName = function(node, classname) {
                        var a = [];
                        var re = new RegExp('(^| )'+classname+'( |$)');
                        var els = node.getElementsByTagName("*");
                        for(var i=0,j=els.length; i<j; i++)
                            if(re.test(els[i].className))a.push(els[i]);
                        return a;
                    }
                    var videos = getElementsByClassName(document.body,"youtube");
                } else {
                    var videos = document.getElementsByClassName("youtube");
                }

                var nb_videos = videos.length;
                for (var i=0; i<nb_videos; i++) {
                    // Находим постер для видео, зная ID нашего видео
                    videos[i].style.backgroundImage = 'url(http://i.ytimg.com/vi/' + videos[i].id + '/sddefault.jpg)';

                    // Размещаем над постером кнопку Play, чтобы создать эффект плеера
                    var play = document.createElement("div");
                    play.setAttribute("class","play");
                    videos[i].appendChild(play);

                    videos[i].onclick = function() {
                        // Создаем iFrame и сразу начинаем проигрывать видео, т.е. атрибут autoplay у видео в значении 1
                        var iframe = document.createElement("iframe");
                        var iframe_url = "http://www.youtube.com/embed/" + this.id + "?rel=0&autoplay=1";
                        if (this.getAttribute("data-params")) iframe_url+='&'+this.getAttribute("data-params");
                        iframe.setAttribute("src",iframe_url);
                        iframe.setAttribute("frameborder",'0');
                        iframe.setAttribute("allowfullscreen","allowfullscreen");
                        iframe.setAttribute("class",'embed-responsive-item');

                        // Заменяем начальное изображение (постер) на iFrame
                        this.parentNode.replaceChild(iframe, this);
                    }
                }
            });
            </script>

            <!-- JS -->
            <script src="js/popper.min.js" defer></script>
            <script src="js/bootstrap.min.js" defer></script>
            <script src="js/lightbox.min.js" defer></script>

        </body>
    </html>
<?php }
