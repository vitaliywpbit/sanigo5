<?php
include 'language/' . $url->lang . '/' . $url->page_db_file . '.php'; // загрузка переменных на нужном языке
$date_create = explode('T', $page->date_create);

// Открытие файла БД с записями для извлечения дополнительной информации
$record = fopen(DB_DIR_PATH . "$url->lang/blog.csv", "rt") or die("Error Establishing a Database Connection...");

if($record) {
    
    for ($i = 0; $data = fgetcsv($record, 0, ","); $i++) {
        
        if ($url->record_link == $data[2]) {

            $record_id = $data[0];
            $record_img_1x1 = $data[9];
            $record_img_4x3 = $data[10];
            $record_img_16x9 = $data[11];
            break;
        }
    }
}
fclose($record);
?>

<!-- JSON-LD (Статья и Строка навигации) -->
<script type="application/ld+json">
  [{
  "@context": "http://schema.org",
  "@type": "NewsArticle",
  "headline": "<?php echo $page->h1; ?>",
   "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "<?php echo $current_url; ?>"
  },
  "image": [
    "<?php echo $base_url . $record_img_1x1; ?>",
    "<?php echo $base_url . $record_img_4x3; ?>",
    "<?php echo $base_url . $record_img_16x9; ?>"
   ],
  "datePublished": "<?php echo $page->date_create . $time_zone; ?>",
  "dateModified": "<?php echo $page->date_mod . $time_zone; ?>",
  "author": {
    "@type": "<?php echo $page->author_type; ?>",
    "name": "<?php echo $page->author; ?>"
  },
   "publisher": {
    "@type": "Organization",
    "name": "<?php echo $publisher; ?>",
    "logo": {
      "@type": "ImageObject",
      "url": "<?php echo $img_default; ?>"
    }
  },
  "description": "<?php echo $page->description; ?>"
},
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "name": "<?php echo $record_title; ?>",
    "item": "<?php echo $base_url . $url->lang_for_link . $url->page_db_file; ?>"
  },{
    "@type": "ListItem",
    "position": 2,
    "name": "<?php echo $page->h1; ?>",
    "item": "<?php echo $base_url . $url->lang_for_link . $url->page_db_file . '/' . $url->record_link; ?>"
  }]
}]
</script>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo $base_url . $url->lang_for_link; ?>"><?php echo $main_page; ?></a></li>
        <li class="breadcrumb-item"><a href="<?php echo $base_url . $url->lang_for_link . $url->page_db_file; ?>"><?php echo $record_title; ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $page->h1; ?></li>
    </ol>
</nav>

<!-- MAIN CONTENT -->
<main>
    <div class="mb-5 mt-5">
        <div class="container article-box">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center">
                    <img class="img-thumbnail img-fluid rounded" src="<?php echo $base_url . $record_img_16x9; ?>" alt="<?php echo $page->image_alt; ?>">
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <h1 class="mt-3"><?php echo $page->h1; ?></h1>
                    <p class="text-muted mt-3"><?php echo $author_rec . '&nbsp;' . $page->author; ?><br/>
                    <span class="text-muted"><small><?php echo $date_create[0]; ?></small></span></p>
                </div>
            </div>
            <hr>
            
            <?php
            //Блок вывода контента для записей
            $path_to_content = DB_DIR_PATH . $url->lang . '/' . $url->page_db_file . '/' . $page->seo_url . '.tpl';

            if (file_exists($path_to_content)) {
                $content = file_get_contents($path_to_content);
                echo $content;
            }
            ?>
            
        </div>
    </div>
</main>
