<?php
//Совместимость с Bootstrap 4
//===========================

if (isset($_POST['sess_start']) != 'no') {
    
    sleep(1);
    
    // Инициализация сессии
    session_name('OASKHARKOVUASESSID');
    session_start();
    
    $dir_path = '../';
    
}
else {
    $dir_path = null;
}

require_once $dir_path . 'config.php'; // Основной файл конфигурации
require_once $dir_path . 'controllers/cart.php'; // Блок с функциями для работы с корзиной

if (isset($_POST['lang'])) {
    $lang = $_POST['lang'];
}
 else {
    $lang = 'ru';
}

if (isset($_POST['lang_for_link'])) {
    $lang_for_link = $_POST['lang_for_link'];
}
 else {
    $lang_for_link = '';
}

if (isset($_POST['seo_url'])) {
         $seo_url = $_POST['seo_url'];
}
else {
    $seo_url = 'home';
}

include $dir_path . 'language/' . $lang . '/cart.php'; // Подключение языкового файла

//==============================================

// Если пришел запрос на добавление товара со страницы с товаром или категорией
if (isset($_POST['item_id']) &&
    isset($_POST['add_to_cart']) == 'from_product' &&
    isset($_POST['item_quantity'])) {
    
    $item_quantity = $_POST['item_quantity'];
    if ($item_quantity <= 0) {
        $item_quantity = 1;
    }
    
    addItemToCart($_POST['item_id'], $item_quantity); ?>
    
    <script>
        $('#productAddedModal').modal('show');
    </script>
    
<?php }

// Если пришел запрос на удаление товара их корзины
if (isset($_POST['item_id']) && isset($_POST['item_action']) == 'delete') {
    
    removeItemFromCart($_POST['item_id']);
    
    if ($seo_url == 'checkout') { ?>
    
        <script>
            // Если один из товаров корзины был удален, обновляем данные
            // о покупках на страницы оформления заказа
            $('#checkoutCart').load("modules/checkout_cart.php", { "lang": '<?php echo $lang; ?>', "lang_for_link": '<?php echo $lang_for_link; ?>' } );
        </script>
    
<?php } }

// Вывод таблицы с товарами в корзине
$count_items = 0;

if (isset($_SESSION['items'])) {
    
    $count_items = count($_SESSION['items']);
    
    if ($count_items >= 1) {
        $count = 1;
        $total_to_pay = 0; ?>
    
        <div class="table-responsive">
            <table class="table table-bordered">
                <tbody>

                <?php foreach ($_SESSION['items'] as $key_id => $value) {

                    $cart_item_info = getItemInfo($lang, $key_id); ?>
                        <tr>
                            <th><img src="<?php echo $cart_item_info['item_image']; ?>" width="64" class="cart-img img-fluid" alt="<?php echo $cart_item_info['item_name']; ?>"></th>
                            <td><a href="<?php echo $lang_for_link . $cart_item_info['item_url']; ?>"><?php echo $cart_item_info['item_name']; ?></a></td>
                            <td><?php echo $cart_item_info['item_price']; ?></td>
                            <td><?php echo 'x' . $value; ?></td>
                            <td>
                                <div id="btnDelSpinnerBorder<?php echo $key_id; ?>" class="spinner-border text-danger d-none" style="width: 1.2rem; height: 1.2rem;" role="status"></div>
                                <div id="deleteItem<?php echo $key_id; ?>" role="button">
                                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-x-circle-fill" fill="#ff0000" xmlns="http://www.w3.org/2000/svg">
                                      <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.146-3.146a.5.5 0 0 0-.708-.708L8 7.293 4.854 4.146a.5.5 0 1 0-.708.708L7.293 8l-3.147 3.146a.5.5 0 0 0 .708.708L8 8.707l3.146 3.147a.5.5 0 0 0 .708-.708L8.707 8l3.147-3.146z"/>
                                    </svg>
                                </div>

                                <script>
                                $('#deleteItem<?php echo $key_id; ?>').click(
                                    function (){
                                        $('#btnDelSpinnerBorder<?php echo $key_id; ?>').removeClass('d-none');
                                        $('#deleteItem<?php echo $key_id; ?>').addClass('d-none');
                                        $('#cartResult').load("modules/cart.php",
                                            { "lang": '<?php echo $lang; ?>', "lang_for_link": '<?php echo $lang_for_link; ?>', "seo_url": '<?php echo $seo_url; ?>', "item_action": 'delete', "item_id": '<?php echo $key_id; ?>' }
                                        );
                                    }
                                );
                                </script>
                            </td>
                        </tr>

                <?php ++$count;
                    $tmp_total = $cart_item_info['item_price'] * $value;
                    $total_to_pay = $total_to_pay + $tmp_total;
                } ?>

                </tbody>
            </table>
        </div>

        <p class="text-right"><?php echo $total_to_pay_text . $total_to_pay . $cart_currency; ?></p>
        
        <script>
            // Если в корзине есть товар, показываем кнопку Оформить заказ
            $('.btn-checkout').removeClass('d-none');
        </script>
    <?php }
        else { ?>
            <p class="text-center"><?php echo $cart_empty_text; ?></p>
            <script>
                // Если корзина пуста, убираем кнопку Оформить заказ
                $('.btn-checkout').addClass('d-none');
                // Если корзина пуста, обновляем страницу с формой оформления заказа
                $('#checkout').load("modules/checkout.php", { "lang": '<?php echo $lang; ?>' } );
            </script>
    <?php }
}
else { ?>
    <p class="text-center"><?php echo $cart_empty_text; ?></p>
    <script>
        // Если корзина пуста, убираем кнопку Оформить заказ
        $('.btn-checkout').addClass('d-none');
        // Если корзина пуста, обновляем страницу с формой оформления заказа
        $('#checkout').load("modules/checkout.php", { "lang": '<?php echo $lang; ?>' } );
    </script>
<?php } ?>

<script>
    // Добавляем количество товара рядом с корзиной
    var text = '(<?php echo $count_items; ?>)';
    $('#cart-items-quantity').text(text);
</script>
