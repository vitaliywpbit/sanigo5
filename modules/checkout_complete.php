<?php

// Функция отправки сообщения с использованием mail()
function sendMessageMail($From, $ReplyTo, $Host, $To, $Cc, $Subject, $html_text) {
    
    $headers  = "Content-type: text/html; charset=utf-8 \r\n"; // Кодировка письма
    // $headers .= "From: Web Platinum Studio <info@web-platinum.net>\r\n"; // Наименование и почта отправителя
    
    $sendTo = $To . ', ' . $Cc;
    
    if (mail($sendTo, $Subject, $html_text, $headers)) { // Отправка письма с помощью функции mail()
        return true;
    }
    else {
        return false;
    }
}

// Функция отправки сообщения через SMTP сервер
function sendMessage($From, $ReplyTo, $Host, $To, $Cc, $Subject, $html_text) {

    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    function get_data($smtp_conn) {

        $data="";

        while($str = fgets($smtp_conn,515)) {
            $data .= $str;

            if(substr($str,3,1) == " ") {
                break;
            }
        }
        return $data;
    }

    $header="Date: ".date("D, j M Y G:i:s")." +0300\r\n";
    $header.="From: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode('')))."?= <".$From.">\r\n"; 
    $header.="X-Mailer: The Bat! (v3.99.3) Professional\r\n"; 
    $header.="Reply-To: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode('')))."?= <".$ReplyTo.">\r\n";
    $header.="X-Priority: 3 (Normal)\r\n";
    $header.="Message-ID: <172562218.".date("YmjHis")."@".$Host.">\r\n";
    $header.="To: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode('')))."?= <".$To.">\r\n";
    $header.="Cc: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode('')))."?= <".$Cc.">\r\n";
    $header.="Subject: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode($Subject)))."?=\r\n";
    $header.="MIME-Version: 1.0\r\n";
    $header.="Content-type: text/html; charset=utf-8\r\n"; // или Content-Type: text/plain; charset=utf-8
    $header.="Content-Transfer-Encoding: 8bit\r\n";

    // Соединение с сервером и отправка сообщения
    $smtp_conn = fsockopen(SMTP, SMTP_PORT, $errno, $errstr, SMTP_TIMEOUT);
    if(!$smtp_conn) {
        print "error: connection to server failed";
        fclose($smtp_conn);
        return false;
    }

    $data = get_data($smtp_conn);
    fputs($smtp_conn,"EHLO ".$Host."\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 250) {
        print "error EHLO $code";
        fclose($smtp_conn);
        return false;
    }
    // print "EHLO\r\n $code <br/>";

    //fputs($smtp_conn,"AUTH LOGIN\r\n");
    //$code = substr(get_data($smtp_conn),0,3);
    //if($code != 334) {print "сервер не разрешил начать авторизацию $code"; fclose($smtp_conn); exit;}

    //fputs($smtp_conn,base64_encode("login")."\r\n");
    //$code = substr(get_data($smtp_conn),0,3);
    //if($code != 334) {print "ошибка доступа к такому юзеру $code"; fclose($smtp_conn); exit;}


    //fputs($smtp_conn,base64_encode("password")."\r\n");
    //$code = substr(get_data($smtp_conn),0,3);
    //if($code != 235) {print "не правильный пароль $code"; fclose($smtp_conn); exit;}

    fputs($smtp_conn,"MAIL FROM:<".$From.">\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 250) {
        print "server refused command MAIL FROM $code";
        fclose($smtp_conn);
        return false;
    }
    // print "MAIL FROM\r\n $code<br/>";

    fputs($smtp_conn,"RCPT TO:<".$To.">\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 250 AND $code != 251) {
        print "the server did not accept the command RCPT TO $code";
        fclose($smtp_conn);
        return false;
    }
    // print "RCPT TO\r\n $code <br/>";
    
    fputs($smtp_conn,"RCPT CC:<".$Cc.">\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 250 AND $code != 251) {
        print "the server did not accept the command RCPT CC $code";
        fclose($smtp_conn);
        return false;
    }
    // print "RCPT CC\r\n $code <br/>";

    fputs($smtp_conn,"DATA\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 354) {
        print "server did not accept DATA $code";
        fclose($smtp_conn);
        return false;
    }
    // print "DATA\r\n $code <br/>";

    fputs($smtp_conn,$header."\r\n".$html_text."\r\n.\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 250) {
        print "message sending error $code";
        fclose($smtp_conn);
        return false;
    }
    // print "отправка письма\r\n $code <br/>";

    fputs($smtp_conn,"QUIT\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    // print "QUIT\r\n $code <br/>";

    fclose($smtp_conn);
    return true;
}

// Инициализация сессии
session_name('OASKHARKOVUASESSID');
session_start();

// На каком языке загружать данные формы
if (isset($_POST['lang'])) {
    $lang = $_POST['lang'];
}
 else {
    $lang = 'ru';
}

// Подключение файла конфигурации и др.
$dir_path = '../';
include_once '../config.php';
require_once '../controllers/cart.php'; // Блок с функциями для работы с корзиной
require_once '../controllers/orders.php'; // Блок с функциями для работы с заказами
include '../language/' . $lang . '/checkout.php';

// Проверка приходящих данных
$checkout_complete = false;

if ((isset($_POST['firstname']) && $_POST['firstname']!="") &&
    (isset($_POST['secondname']) && $_POST['secondname']!="") &&
    (isset($_POST['email']) && $_POST['email']!="") &&
    (isset($_POST['phone']) && $_POST['phone']!="") &&
    (isset($_POST['region']) && $_POST['region']!="") &&
    (isset($_POST['city']) && $_POST['city']!="") &&
    (isset($_POST['shipping']) && $_POST['shipping']!="") &&
    (isset($_POST['payment']) && $_POST['payment']!="")) {
    
        if (isset($_POST['address']) && $_POST['address']!="") {
            $address = $_POST['address'];
        }
        else {
             $address = '***';
        }

        if (isset($_POST['comment']) && $_POST['comment']!="") {
            $comment = $_POST['comment'];
        }
        else {
             $comment = '***';
        }
        
        if (isset($_POST['subscription']) == 'yes') {
            $subscription_status = 'y';
        }
        else {
            $subscription_status = 'n';
        }

    $checkout_complete = true;
}
else {
    $checkout_complete = false;
}

// Отправка сообщения
if ($checkout_complete == true) {
    
    $total_to_pay = 0;
    $count = 1;
    
    $last_order_number = getLastOrderNumber();
    $new_order_number = ++$last_order_number;
    
    // Создание временного файла и формирование таблицы с товарами
    $temp = tmpfile();
    
    fwrite($temp, '<table border="1" cellpadding="5">');
    
    foreach ($_SESSION['items'] as $key_id => $value) {

        $cart_item_info = getItemInfo($lang, $key_id);
        
        fwrite($temp, '<tr>');
        fwrite($temp, '<td>' . $cart_item_info['item_id'] . '</td>');
        fwrite($temp, '<td>' . $cart_item_info['item_name'] . '</td>');
        fwrite($temp, '<td>' . $cart_item_info['item_sku'] . '</td>');
        fwrite($temp, '<td>' . $cart_item_info['item_price'] . '</td>');
        fwrite($temp, '<td>x' . $value . '</td>');
        fwrite($temp, '</tr>');

        $tmp_total = $cart_item_info['item_price'] * $value;
        $total_to_pay = $total_to_pay + $tmp_total;
        
        ++$count;
    }
    
    fwrite($temp, '</table>');

    fseek($temp, 0); // перемещаем указатель в начало файла
    $product_list = fread($temp, 512000); // чтение файла в байтах
    fclose($temp); // происходит закрытие (удаление) файла

    $html_text = '
        <html>
            <head>
                <title>' . $subject_title . '</title>
            </head>
            <body>
                <h2>' . $checkout_subject . '</h2>
                <p><b>' . $checkout_order_number . '</b>'. $new_order_number .'</p>
                <p><b>' . $checkout_firstname_title . '</b>'. $_POST['firstname'] .'</p>
                <p><b>' . $checkout_secondname_title . '</b>'. $_POST['secondname'] .'</p>
                <p><b>' . $checkout_email_title . '</b>' . $_POST['email'] . '</p>
                <p><b>' . $checkout_phone_title . '</b>+38' . $_POST['phone'] . '</p>
                <p><b>' . $checkout_region_title . '</b>'. $_POST['region'] . '</p>
                <p><b>' . $checkout_city_title . '</b>' . $_POST['city'] . '</p>
                <p><b>' . $checkout_address_title . '</b>' . $address . '</p>
                <p><b>' . $checkout_item_shipping . '</b>' . $_POST['shipping'] . '</p>
                <p><b>' . $checkout_item_payment . '</b>' . $_POST['payment'] . '</p>
                <p><b>' . $checkout_payment_card . '</b>' . $payment_card_number . '</p>
                <p><b>' . $checkout_comment_title . '</b>' . $comment . '</p>
                    <div>' . $product_list . '</div>
                        <p><b>' . $total_to_pay_text . '</b>' . $total_to_pay . $cart_currency . '</p>
            </body>
        </html>
    ';
    
    $cc_email = $_POST['email'];
    $cc_to = trim($cc_email);

    if (newOrder($new_order_number, $_POST['firstname'], $_POST['secondname'], $_POST['phone'], $_POST['email'], $subscription_status, $_POST['region'], $_POST['city'], $address, $_POST['shipping'], $_POST['payment'], $total_to_pay, $comment, $product_list)) {
        
        $result_checkout = $successful_checkout . '<p><b>' . $checkout_order_number . $new_order_number . '</b></p>'; // успешный заказ

        // Очистить данные сессии для текущего сценария
        $_SESSION = [];
        // Уничтожить хранилище сессии
        session_destroy(); ?>

        <script>
            // После очистки сессии очищаем корзину и количество товаров рядом
            var text = '<p class="text-center"><?php echo $cart_empty_text; ?></p>';
            $('#cartResult').html(text);
            $('#cart-items-quantity').text('(0)');
        </script>
        
        <?php
        // 2 функции отправки заказа Mail() / SMTP: sendMessageMail() / sendMessage()
        sendMessageMail($email_from, $reply_to, DOMAIN_NAME, $email_to, $cc_to, $subject_title, $html_text);

    }
    else {
        $result_checkout = $err_checkout;
    }
}
else {
    $result_checkout = $err_checkout;
}
?>

<div class="text-center">
    <?php echo $result_checkout; ?>
</div>
