<?php
//Совместимость с Bootstrap 4
//===========================

// Инициализация сессии
session_name('OASKHARKOVUASESSID');
session_start();

$dir_path = '../';
require_once $dir_path . 'config.php'; // Основной файл конфигурации
require_once $dir_path . 'controllers/cart.php'; // Блок с функциями для работы с корзиной

if (isset($_POST['lang'])) {
    $lang = $_POST['lang'];
}
 else {
    $lang = 'ru';
}

if (isset($_POST['lang_for_link'])) {
    $lang_for_link = $_POST['lang_for_link'];
}
 else {
    $lang_for_link = '';
}

include $dir_path . 'language/' . $lang . '/checkout.php'; // Подключение языкового файла

//==============================================
?>

<table class="table table-checkout table-bordered table-sm">
    <tbody>

    <?php 
    $total_to_pay = 0;
    
    foreach ($_SESSION['items'] as $key_id => $value) {

        $cart_item_info = getItemInfo($lang, $key_id); ?>
        
        <tr>
            <th class="text-center"><img src="<?php echo $cart_item_info['item_image']; ?>" width="96" class="cart-img img-fluid" alt="<?php echo $cart_item_info['item_name']; ?>"></th>
            <td><a href="<?php echo $lang_for_link . $cart_item_info['item_url']; ?>"><?php echo $cart_item_info['item_name']; ?></a></td>
            <td><?php echo $cart_item_info['item_price']; ?></td>
            <td><?php echo 'x' . $value; ?></td>
        </tr>

    <?php
        $tmp_total = $cart_item_info['item_price'] * $value;
        $total_to_pay = $total_to_pay + $tmp_total;
    } ?>

    </tbody>
</table>

<p class="text-right"><?php echo $total_to_pay_text . $total_to_pay . $cart_currency; ?></p>
