<?php
//Совместимость с Bootstrap 4
//===========================

if (isset($_POST['sess_start']) != 'no') {
    
    // Инициализация сессии
    session_name('OASKHARKOVUASESSID');
    session_start();
    
    $dir_path = '../';
    
}
else {
    $dir_path = null;
}

require_once $dir_path . 'config.php'; // Основной файл конфигурации
require_once $dir_path . 'controllers/cart.php'; // Блок с функциями для работы с корзиной

if (isset($_POST['lang'])) {
    $lang = $_POST['lang'];
}
 else {
    $lang = 'ru';
}

if (isset($_POST['lang_for_link'])) {
    $lang_for_link = $_POST['lang_for_link'];
}
 else {
    $lang_for_link = '';
}

include $dir_path . 'language/' . $lang . '/checkout.php'; // Подключение языкового файла

//==============================================

// Вывод формы оформления заказа, если в корзине есть товар
if (isset($_SESSION['items'])) {
    
    $count_items = count($_SESSION['items']);
    
    if ($count_items >= 1) { ?>

    <form class="form-signin" method="POST" id="form_checkout" action="javascript:void(null);" onsubmit="checkout()">

        <h2 class="h5 bg-light p-3"><b><?php echo $heading_01; ?></b></h2>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputFirstName"><small><?php echo $checkout_firstname_title; ?></small> <span class="field_required">*</span></label>
                <input type="text" name="firstname" class="form-control" id="inputFirstName" placeholder="<?php echo $checkout_placeholder_name; ?>" required>
            </div>
            <div class="form-group col-md-6">
                <label for="inputSecondName"><small><?php echo $checkout_secondname_title; ?></small> <span class="field_required">*</span></label>
                <input type="text" name="secondname" class="form-control" id="inputSecondName" placeholder="<?php echo $checkout_placeholder_s_name; ?>" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail"><small><?php echo $checkout_email_title; ?></small> <span class="field_required">*</span></label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                    </div>
                    <input type="email" name="email" class="form-control" id="inputEmail" placeholder="<?php echo $checkout_placeholder_email; ?>" required>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="inputPhone"><small><?php echo $checkout_phone_title; ?></small> <span class="field_required">*</span></label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupPrepend">
                            <svg width="1.1em" height="1.1em" viewBox="0 0 16 16" class="bi bi-telephone" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M3.925 1.745a.636.636 0 0 0-.951-.059l-.97.97c-.453.453-.62 1.095-.421 1.658A16.47 16.47 0 0 0 5.49 10.51a16.471 16.471 0 0 0 6.196 3.907c.563.198 1.205.032 1.658-.421l.97-.97a.636.636 0 0 0-.06-.951l-2.162-1.682a.636.636 0 0 0-.544-.115l-2.052.513a1.636 1.636 0 0 1-1.554-.43L5.64 8.058a1.636 1.636 0 0 1-.43-1.554l.513-2.052a.636.636 0 0 0-.115-.544L3.925 1.745zM2.267.98a1.636 1.636 0 0 1 2.448.153l1.681 2.162c.309.396.418.913.296 1.4l-.513 2.053a.636.636 0 0 0 .167.604L8.65 9.654a.636.636 0 0 0 .604.167l2.052-.513a1.636 1.636 0 0 1 1.401.296l2.162 1.681c.777.604.849 1.753.153 2.448l-.97.97c-.693.693-1.73.998-2.697.658a17.47 17.47 0 0 1-6.571-4.144A17.47 17.47 0 0 1 .639 4.646c-.34-.967-.035-2.004.658-2.698l.97-.969z"/>
                            </svg>+38
                        </span>
                    </div>
                    <input type="number" name="phone" class="form-control" id="inputPhone" placeholder="<?php echo $checkout_placeholder_phone; ?>" required>
                </div>
            </div>
        </div>

        <h2 class="h5 mt-5 bg-light p-3"><b><?php echo $heading_02; ?></b></h2>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="selectRegion"><small><?php echo $checkout_choose_region; ?></small> <span class="field_required">*</span></label>
                <select class="custom-select" id="selectRegion" name="region" required>
                    <option value="">...</option>
                    <?php
                    if ($db_regions['ua']) {
                        foreach ($db_regions['ua'] as $value) { ?>
                            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                    <?php }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="inputCityName"><small><?php echo $checkout_city; ?></small> <span class="field_required">*</span></label>
                <input type="text" name="city" class="form-control" id="inputCityName" placeholder="<?php echo $checkout_placeholder_city; ?>" required>
            </div>
            <div class="form-group col-md-12">
                <label for="inputBuyerAddress"><small><?php echo $checkout_address; ?></small></label>
                <input type="text" name="address" class="form-control" id="inputBuyerAddress" placeholder="<?php echo $checkout_placeholder_address; ?>">
            </div>
        </div>

        <h2 class="h5 mt-5 bg-light p-3"><b><?php echo $heading_03; ?></b></h2>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="selectShippingMethod"><small><?php echo $checkout_choose_shipping_method; ?></small> <span class="field_required">*</span></label>
                <select class="custom-select" id="selectShippingMethod" name="shipping" required>
                    <option value="">...</option>
                    <?php
                    if ($db_shipping_method['ua']) {
                        foreach ($db_shipping_method['ua'] as $value) { ?>
                            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                    <?php }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="selectPaymentMethod"><small><?php echo $checkout_choose_payment_method; ?></small> <span class="field_required">*</span></label>
                <select class="custom-select" id="selectPaymentMethod" name="payment" required>
                    <option value="">...</option>
                    <?php
                    if ($db_payment_method['ua']) {
                        foreach ($db_payment_method['ua'] as $value) { ?>
                            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                    <?php }
                    }
                    ?>
                </select>
            </div>
        </div>

        <h2 class="h5 mt-5 bg-light p-3"><b><?php echo $heading_04; ?></b></h2>

        <textarea class="form-control" name="comment" placeholder="<?php echo $checkout_placeholder_textarea; ?>"></textarea>
        
        <div class="mt-3">
            <input type="checkbox" id="subscription" name="subscription" value="yes" checked>
            <small><?php echo $checkout_subscription_text; ?></small>
        </div>

        <h2 class="h5 mt-5 bg-light p-3"><b><?php echo $heading_05; ?></b></h2>
        
        <div id="checkoutCart" class="mt-2">
            <script>
                // Загрузка информации о покупках из корзины
                $('#checkoutCart').load("modules/checkout_cart.php", { "lang": '<?php echo $lang; ?>', "lang_for_link": '<?php echo $lang_for_link; ?>' } );
            </script>
        </div>
        
        
        <div class="text-right mt-4">
            <button type="submit" class="btn btn-primary"><?php echo $checkout_text; ?></button>
        </div>
    </form>

<?php }
    else { ?>

        <p class="text-center"><?php echo $cart_empty_text; ?></p>
        
<?php }
}
else { ?>
        
    <p class="text-center"><?php echo $cart_empty_text; ?></p>
    
<?php } ?>

<script>
    // От двойного нажатия кнопки
    $('form').on('submit', function (e) {
        
        $('[type="submit"]').prop('disabled','disabled');
        
    });
    
    function checkout() {
        
        var msg   = $('#form_checkout').serialize();
        
        $.ajax({
            type: 'POST',
            url: 'modules/checkout_complete.php',
            data: msg
          }).done(function( html ) {
            
            $("#checkout").html(html);
        
        });
    }
</script>
