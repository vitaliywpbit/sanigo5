<?php
//Совместимость с Bootstrap 4
//===========================

include 'language/' . $url->lang . '/catalog.php'; // загрузка переменных на нужном языке

// Открытие файла БД с основными страницами для формирования строки навигации
// (чтение названия категории)
$category = fopen(DB_DIR_PATH . "$url->lang/pages.csv", "rt") or die("Error Establishing a Database Connection...");
if($category) {
    for ($i = 0; $data = fgetcsv($category, 0, ","); $i++) {
        if ($url->page_db_file == $data[2]) {
            $category_url = $data[2];
            $record_title = $data[6];
            break;
        }
    }
}
fclose($category);

// Открытие файла БД с товарами для извлечения дополнительной информации о продукте
$item = fopen(DB_DIR_PATH . "$url->lang/items.csv", "rt") or die("Error Establishing a Database Connection...");
if($item) {
    for ($i = 0; $data = fgetcsv($item, 0, ","); $i++) {
        if ($url->record_link == $data[2]) {
            
            $path_to_content = DB_DIR_PATH . $url->lang . '/' . $data[1] . '/' . $category_url . '/' . $data[2] . '.tpl';
            
            $item_id = $data[0];
            $item_img_1x1 = $data[8];
            $item_img_4x3 = $data[9];
            $item_img_16x9 = $data[10];
            $item_img_preview = $data[11];
            $item_description = $data[14];
            $item_price = $data[15];
            $item_new_price = $data[16]; // sale
            $item_sku = $data[17];
            $item_availability = $data[18];
            $item_brand_name = $data[19];
            break;
        }
    }
}
fclose($item);

// Формирование наличия товара
if (isset($item_availability)) {
    switch ($item_availability) {
        case 'InStock':
            $availability_status = $instock;
            break;
        case 'OutOfStock':
            $availability_status = $outofstock;
            break;
        case 'PreOrder':
            $availability_status = $preorder;
            break;
        default:
            $availability_status = '<small><i>no data...</i></small>';
            $item_availability = 'OutOfStock';
            break;
    }
}
 else {
    $availability_status = $outofstock;
    $item_availability = 'OutOfStock';
}

// Формирование новой цены со скидкой
if ($item_new_price) {
    $item_old_price = "<span class='align-top text-muted ml-2'><s>$item_price</s></span>";
    $item_price = $item_new_price;
    $sale_label = true;
}
 else {
    $sale_label = false;
    $item_old_price = null;
}

// Автоматическое формирование даты на 1 год вперед: цена действительна до
$year = date('Y');
$new_year = $year + 1;
$price_valid_until = $new_year . $month_day; // в формате: 2020-09-22
?>

<!-- JSON-LD (Товар и Строка навигации) -->
<script type="application/ld+json">
   [{
    "@context": "http://schema.org/",
    "@type": "Product",
    "name": "<?php echo $page->h1; ?>",
    "image": [
        "<?php echo $base_url . $item_img_1x1; ?>",
        "<?php echo $base_url . $item_img_4x3; ?>",
        "<?php echo $base_url . $item_img_16x9; ?>"
     ],
    "description": "<?php echo $page->description; ?>",
    "sku": "<?php echo $item_sku; ?>",
    "mpn": "<?php echo $item_sku; ?>",
    "brand": {
      "@type": "Brand",
      "name": "<?php echo $item_brand_name; ?>"
    },
    "offers": {
      "@type": "Offer",
      "url": "<?php echo $current_url; ?>",
      "availability": "http://schema.org/<?php echo $item_availability; ?>",
      "priceCurrency": "<?php echo $currency_iso; ?>",
      "price": "<?php echo $item_price; ?>",
      "priceValidUntil": "<?php echo $price_valid_until; ?>"
    }
  },
  {
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "name": "<?php echo $record_title; ?>",
    "item": "<?php echo $base_url . $url->lang_for_link . $url->page_db_file; ?>"
  },{
    "@type": "ListItem",
    "position": 2,
    "name": "<?php echo $page->h1; ?>",
    "item": "<?php echo $base_url . $url->lang_for_link . $url->page_db_file . '/' . $url->record_link; ?>"
  }]
}]
</script>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo $base_url . $url->lang_for_link; ?>"><?php echo $main_page; ?></a></li>
        <li class="breadcrumb-item"><a href="<?php echo $base_url . $url->lang_for_link . $url->page_db_file; ?>"><?php echo $record_title; ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $page->h1; ?></li>
    </ol>
</nav>

<!-- MAIN CONTENT -->
<main>
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center">
                
                <?php
                if ($sale_label) {
                    echo '<span class="bg-danger position-absolute text-white pl-2 pr-2 pt-1 pb-1 ml-n1 mt-n1">SALE</span>';
                }
                
                //Код проверки наличия изображения для товара
                if($item_img_4x3) { // image 4x3

                    $image_check = trim($item_img_4x3);

                    if($image_check != null && file_exists($image_check)) {
                        
                        $item_image = $image_check;
                    }
                    else {
                        $item_image = 'images/no_image.png';
                    }
                }
                else {
                    $item_image = 'images/no_image.png';
                }
                ?>
                
                <a href="<?php echo $base_url . $item_image; ?>" data-lightbox="gallery-images" data-title="<?php echo $page->image_alt; ?>">
                    <img class="img-thumbnail img-fluid rounded" src="<?php echo $base_url . $item_image; ?>" alt="<?php echo $page->image_alt; ?>">
                </a>
                <br/>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                <h1 class="mt-3"><?php echo $page->h1; ?></h1>
                
                <p class="text-muted">
                    <?php echo $sku_title . $item_sku; ?><br>
                    <?php echo '<b>' . $brand_title . '</b>' . $item_brand_name; ?>
                </p>
                
                <p class="mt-4">
                    <?php
                    if ($item_old_price) {
                        echo '<span class="h1 text-danger">₴ ' . $item_price . '</span>' . $item_old_price;
                    }
                    else {
                        echo '<span class="h1">₴ ' . $item_price . '</span>';
                    }
                    ?>
                    <br>
                    <?php echo $availability_status; ?>
                </p>
                
                <?php if ($item_availability != 'OutOfStock') { ?>
                    <form class="form-send-msg mt-4" method="POST" id="add-item-to-cart" action="javascript:void(null);" onsubmit="addToCart()">
                        <input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
                        <input type="hidden" name="lang" value="<?php echo $url->lang; ?>">
                        <input type="hidden" name="lang_for_link" value="<?php echo $url->lang_for_link; ?>">
                        <input type="hidden" name="add_to_cart" value="from_product">

                        <div class="form-group">
                            <label for="cartItemQuantity">
                                <svg class="bi bi-basket2 mb-1 mr-2" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M1.111 7.186A.5.5 0 0 1 1.5 7h13a.5.5 0 0 1 .489.605l-1.5 7A.5.5 0 0 1 13 15H3a.5.5 0 0 1-.489-.395l-1.5-7a.5.5 0 0 1 .1-.42zM2.118 8l1.286 6h9.192l1.286-6H2.118z"/>
                                    <path fill-rule="evenodd" d="M11.314 1.036a.5.5 0 0 1 .65.278l2 5a.5.5 0 1 1-.928.372l-2-5a.5.5 0 0 1 .278-.65zm-6.628 0a.5.5 0 0 0-.65.278l-2 5a.5.5 0 1 0 .928.372l2-5a.5.5 0 0 0-.278-.65z"/>
                                    <path d="M4 10a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zm3 0a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zm3 0a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zM0 6.5A.5.5 0 0 1 .5 6h15a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H.5a.5.5 0 0 1-.5-.5v-1z"/>
                                </svg>
                                <?php echo $order_quantity; ?>
                            </label>
                            <div class="form-inline">
                                <span class="btn btn-outline-dark minus" role="button">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash" viewBox="0 0 16 16">
                                        <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                    </svg>
                                </span>
                                <input type="number" class="form-control item-quantity w-25" name="item_quantity" value="1" id="cartItemQuantity" placeholder="<?php echo $order_placeholder_quantity; ?>" required readonly>
                                <span class="btn btn-outline-dark plus" role="button">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                    </svg>
                                </span>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-lg btn-primary mt-3 mb-2">
                            <div id="btnSpinnerBorder" class="spinner-border mr-2 d-none" style="width: 1.4rem; height: 1.4rem;" role="status"></div>
                            <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" id="btnBasket2" class="bi bi-basket2-fill mb-1 mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M11.314 1.036a.5.5 0 0 1 .65.278l2 5a.5.5 0 1 1-.928.372l-2-5a.5.5 0 0 1 .278-.65zm-6.628 0a.5.5 0 0 0-.65.278l-2 5a.5.5 0 1 0 .928.372l2-5a.5.5 0 0 0-.278-.65z"/>
                                <path fill-rule="evenodd" d="M1.5 7a.5.5 0 0 0-.489.605l1.5 7A.5.5 0 0 0 3 15h10a.5.5 0 0 0 .489-.395l1.5-7A.5.5 0 0 0 14.5 7h-13zM4 10a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zm3 0a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zm4-1a1 1 0 0 0-1 1v2a1 1 0 1 0 2 0v-2a1 1 0 0 0-1-1z"/>
                                <path d="M0 6.5A.5.5 0 0 1 .5 6h15a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H.5a.5.5 0 0 1-.5-.5v-1z"/>
                            </svg>
                            <?php echo $add_to_cart; ?>
                        </button>
                    </form>
                <?php } ?>
                
                <p>
                    <small><?php echo $shipping_payment_title; ?></small>
                    <a role="button" tabindex="0" data-toggle="popover" data-html="true" data-trigger="focus" title="<?php echo $shipping_payment_popover_title; ?>" data-content="<?php echo $shipping_payment_body; ?>">
                        <svg class="bi bi-question-circle ml-2" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path d="M5.25 6.033h1.32c0-.781.458-1.384 1.36-1.384.685 0 1.313.343 1.313 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.007.463h1.307v-.355c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.326 0-2.786.647-2.754 2.533zm1.562 5.516c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
                        </svg></a>
                </p>
            </div>
        </div>
        
    </div>
    
    <!-- ====================== -->
    
    <ul class="nav nav-pills mt-4 mb-5 justify-content-center bg-light pt-3 pb-3 w-100" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="pills-description-tab" data-toggle="pill" href="#pills-description" role="tab" aria-controls="description-home" aria-selected="true"><?php echo $description_tab_text; ?></a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="pills-specification-tab" data-toggle="pill" href="#pills-specification" role="tab" aria-controls="pills-specification" aria-selected="false"><?php echo $specification_tab_text; ?></a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link disabled" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews" role="tab" aria-controls="pills-reviews" aria-selected="false"><?php echo $reviews_tab_text . ' (' . $reviews_count . ') '; ?></a>
        </li>
    </ul>

    <div class="tab-content mb-5" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-description" role="tabpanel" aria-labelledby="pills-description-tab">

            <div class="container">
                
            <?php
            //Блок вывода основного контента
            if (file_exists($path_to_content)) {
                $content = file_get_contents($path_to_content);
                echo $content;
            }
            ?>
                
            </div>

        </div>
        <div class="tab-pane fade" id="pills-specification" role="tabpanel" aria-labelledby="pills-specification-tab">

            <div class="container">
                
                Specification content
            
            </div>

        </div>
        <div class="tab-pane fade" id="pills-reviews" role="tabpanel" aria-labelledby="pills-reviews-tab">

            <div class="container">
                
                <!-- reviews content -->
                
            </div>

        </div>
    </div>
    
</main>

<script>
    // От двойного нажатия кнопки
    $('form').on('submit', function (e) {
        
        $('[type="submit"]').prop('disabled','disabled');
        
    });
    
    function addToCart() {
        
        var msg = $('#add-item-to-cart').serialize();
        
        $('#btnSpinnerBorder').removeClass('d-none');
        $('#btnBasket2').addClass('d-none');
        
        $.ajax({
            type: 'POST',
            url: 'modules/cart.php',
            data: msg
          }).done(function( html ) {
            
            $("#cartResult").html(html);
            
            $('#btnSpinnerBorder').addClass('d-none');
            $('#btnBasket2').removeClass('d-none');
            $('[type="submit"]').prop("disabled", false);
        
        });
    }
    
    // Прибавление или уменьшение кол-ва товара по кнопкам +/-
    $(function() {
        $('.minus').click(function () {
            var $input = $(this).parent().find('.item-quantity');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.plus').click(function () {
            var $input = $(this).parent().find('.item-quantity');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
</script>
