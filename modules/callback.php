<?php

// Функция отправки сообщения с использованием mail()
function sendMessageMail($From, $ReplyTo, $Host, $To, $Subject, $html_text) {
    
    $headers  = "Content-type: text/html; charset=utf-8 \r\n"; // Кодировка письма
    // $headers .= "From: Web Platinum Studio <info@sanigo.1gb.ua>\r\n"; // Наименование и почта отправителя
    
    if (mail($To, $Subject, $html_text, $headers)) { // Отправка письма с помощью функции mail()
        return true;
    }
    else {
        return false;
    }
}

// Функция отправки сообщения через SMTP сервер без авторизации
function sendMessage($From, $ReplyTo, $Host, $To, $Subject, $html_text) {
    
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    function get_data($smtp_conn) {
        
        $data="";
        
        while($str = fgets($smtp_conn,515)) {
            $data .= $str;

            if(substr($str,3,1) == " ") {
                break;
            }
        }
        return $data;
    }

    $header="Date: ".date("D, j M Y G:i:s")." +0200\r\n";
    $header.="From: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode('')))."?= <".$From.">\r\n"; 
    $header.="X-Mailer: The Bat! (v3.99.3) Professional\r\n"; 
    $header.="Reply-To: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode('')))."?= <".$ReplyTo.">\r\n";
    $header.="X-Priority: 3 (Normal)\r\n";
    $header.="Message-ID: <172562218.".date("YmjHis")."@".$Host.">\r\n";
    $header.="To: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode('')))."?= <".$To.">\r\n";
    $header.="Subject: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode($Subject)))."?=\r\n";
    $header.="MIME-Version: 1.0\r\n";
    $header.="Content-type: text/html; charset=utf-8\r\n"; // или Content-Type: text/plain; charset=utf-8
    $header.="Content-Transfer-Encoding: 8bit\r\n";
    
    // Соединение с сервером и отправка сообщения
    $smtp_conn = fsockopen(SMTP, SMTP_PORT, $errno, $errstr, SMTP_TIMEOUT);
    if(!$smtp_conn) {
        print "error: connection to server failed";
        fclose($smtp_conn);
        return false;
    }

    $data = get_data($smtp_conn);
    fputs($smtp_conn,"EHLO ".$Host."\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 250) {
        print "error EHLO $code";
        fclose($smtp_conn);
        return false;
    }
    // print "EHLO\r\n $code <br/>";

    //fputs($smtp_conn,"AUTH LOGIN\r\n");
    //$code = substr(get_data($smtp_conn),0,3);
    //if($code != 334) {print "сервер не разрешил начать авторизацию $code"; fclose($smtp_conn); exit;}

    //fputs($smtp_conn,base64_encode("login")."\r\n");
    //$code = substr(get_data($smtp_conn),0,3);
    //if($code != 334) {print "ошибка доступа к такому юзеру $code"; fclose($smtp_conn); exit;}


    //fputs($smtp_conn,base64_encode("password")."\r\n");
    //$code = substr(get_data($smtp_conn),0,3);
    //if($code != 235) {print "не правильный пароль $code"; fclose($smtp_conn); exit;}

    fputs($smtp_conn,"MAIL FROM:<".$From.">\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 250) {
        print "server refused command MAIL FROM $code";
        fclose($smtp_conn);
        return false;
    }
    // print "MAIL FROM\r\n $code<br/>";

    fputs($smtp_conn,"RCPT TO:<".$To.">\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 250 AND $code != 251) {
        print "the server did not accept the command RCPT TO $code";
        fclose($smtp_conn);
        return false;
    }
    // print "RCPT TO\r\n $code <br/>";
    
    // Дополнительная копия
    $Cc_2 = 'palpod14@gmail.com';
    
    fputs($smtp_conn,"RCPT CC:<".$Cc_2.">\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 250 AND $code != 251) {
        print "the server did not accept the command RCPT CC $code";
        fclose($smtp_conn);
        return false;
    }
    // print "RCPT CC\r\n $code <br/>";
    
    // Дополнительная копия
    $Cc_3 = 'oas_apteki@ukr.net';
    
    fputs($smtp_conn,"RCPT CC:<".$Cc_3.">\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 250 AND $code != 251) {
        print "the server did not accept the command RCPT CC $code";
        fclose($smtp_conn);
        return false;
    }
    // print "RCPT CC\r\n $code <br/>";
    
    // Дополнительная копия
    $Cc_4 = 'baydakzhanna@gmail.com';
    
    fputs($smtp_conn,"RCPT CC:<".$Cc_4.">\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 250 AND $code != 251) {
        print "the server did not accept the command RCPT CC $code";
        fclose($smtp_conn);
        return false;
    }
    // print "RCPT CC\r\n $code <br/>";

    fputs($smtp_conn,"DATA\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 354) {
        print "server did not accept DATA $code";
        fclose($smtp_conn);
        return false;
    }
    // print "DATA\r\n $code <br/>";

    fputs($smtp_conn,$header."\r\n".$html_text."\r\n.\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    if($code != 250) {
        print "message sending error $code";
        fclose($smtp_conn);
        return false;
    }
    // print "отправка письма\r\n $code <br/>";

    fputs($smtp_conn,"QUIT\r\n");
    $code = substr(get_data($smtp_conn),0,3);
    // print "QUIT\r\n $code <br/>";

    fclose($smtp_conn);
    return true;
}

// На каком языке загружать данные формы
if (isset($_POST['lang'])) {
    $lang = $_POST['lang'];
}
else {
    $lang = 'ru';
}

// Подключение файла конфигурации и др.
$dir_path = '../';
include_once '../config.php';
include_once '../language/' . $lang . '/callback.php';

// Проверка приходящих данных
$send_callback = false;

if((isset($_POST['phone']) && $_POST['phone']!="") &&
   (isset($_POST['name']) && $_POST['name']!="")){
    
    $send_callback = true;        
    
    /* if (isset($_POST['confirm_opd']) == 'confirm_opd_ok') {
        $send_callback = true;
    }
    else {
        $callback_alert[] = $callback_confirm_opd_no;
    } */

}
else {
    $callback_alert[] = $callback_fields_check;
}

// Отправка сообщения
if ($send_callback == true) {
    
    $html_text = '
        <html>
            <head>
                <title>' . $subject_title . '</title>
            </head>
            <body>
                <h2>' . $callback_subject . '</h2>
                <p><b>' . $callback_name_title . '</b>' . $_POST['name'] . '</p>
                <p><b>' . $callback_phone_title . '</b>' . $_POST['phone'] . '</p>
            </body>
        </html>
    ';
    
    // 2 функции отправки сообщения Mail() / SMTP: sendMessageMail() / sendMessage()
    if (sendMessage($email_from, $reply_to, DOMAIN_NAME, $email_to, $subject_title, $html_text)) {
        
        $result_callback = $successful_request_callback;
        
    }
    else {
        
        $result_callback = $err_request_callback;
    }
}
 else {
     
    $result_callback = $err_request_callback;
}
?>

<div class="modal-body">
    <div class="text-center">
        <?php echo $result_callback; ?>
    </div>
</div>
