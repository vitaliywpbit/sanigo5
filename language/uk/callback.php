<?php
// Email
$email_to = 'w4promo@gmail.com'; // email на который будут приходить сообщения
$email_from = 'info@sanigo.1gb.ua'; // email с которого будут приходить сообщения (создается на хостинге)
$reply_to = '';

// Для формы
$callback_link = 'Замовити дзвінок'; // название ссылки для вызова формы
$request_callback = 'Замовити дзвінок'; // название формы
$callback_placeholder_name = "Ваше Ім'я"; // заполнение для примера в поле Имя
$callback_placeholder_phone = '095 xxx xx xx'; // заполнение для примера в поле Телефон
$callback_text_opd = 'Відправляючи справжню форму, я згоден(а) з
        <a style="color: darkblue;" href="terms">умовами угоди</a>.';
$submit_callback_form = 'Відправити'; // название кнопки Отправить
$close_callback_form = 'Закрити';

// Тело сообщения
$subject_title = 'Колекторська компанія Sanigo';
$callback_subject = 'Зворотний дзвінок | Sanigo';
$callback_name_title = "Ім'я: ";
$callback_phone_title = 'Телефон: ';

// Результат отправки формы
$successful_request_callback = "
        <svg class='bi bi-check' width='3em' height='3em' viewBox='0 0 16 16' fill='#008000' xmlns='http://www.w3.org/2000/svg'>
            <path fill-rule='evenodd' d='M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z'/>
        </svg>
        <br>
        <p>Ви успішно замовили Зворотний дзвінок!</p>
        <p>Ми зв'яжемося з Вами найближчим часом.</p>
    ";

$err_request_callback = '
        <svg class="bi bi-x" width="3em" height="3em" viewBox="0 0 16 16" fill="#ff0000" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z"/>
            <path fill-rule="evenodd" d="M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z"/>
        </svg>
        <br>
        <p>Помилка.</p>
        <p>Оновіть сторінку і повторіть відправку повідомлення ще раз.</p>
    ';
