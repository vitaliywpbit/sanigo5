<?php
// Brand
$brand_name = 'Sanigo';
$publisher = 'Sanigo'; //Издатель (Brand), разместивший статью без html тегов

// Main Content / Catalog
$navbar_toggler_title = 'Меню';
$home_page_name = 'Головна';
$currency_ua = 'грн.';
$currency_iso = 'UAH'; // ISO 4217
$count_items = 0;

$phone1 = '067-007-15-67';
$phone1_link = '+380670071567';
$phone2 = '050-100-15-67';
$phone2_link = '+380501001567';
$phone3 = '050-100-85-30';
$phone3_link = '+380501008530';

$facebook_link = 'http://www.facebook.com/khar.oas';
$instagram_link = 'http://www.instagram.com/khar.oas/';

$logo_alt = 'Саніго';

// Buttons
$call_us_text = 'Подзвонити';

// Footer
$footer_text_1 = "Зв'яжіться з нами <br>у мессенджерах";
$footer_text_2 = 'Безкоштовний дзвінок ';


// Copyright
$start_date = '2018';
$copyright = ' &copy; Компанія "Sanigo".';
$wpbit = 'Website by <a class="text-light" href="http://web-platinum.net/" target="_blank" rel="noopener">Web Platinum Studio</a>';

$terms = 'Умови угоди';
