<?php
// Email
$email_to = 'w4promo@gmail.com'; // email на который будут приходить сообщения
$email_from = 'info@sanigo.1gb.ua'; // email с которого будут приходить сообщения (создается на хостинге)
$reply_to = '';

// Для формы
$callback_link = 'Заказать звонок'; // название ссылки для вызова формы
$request_callback = 'Заказать звонок'; // название формы
$callback_placeholder_name = 'Ваше Имя'; // заполнение для примера в поле Имя
$callback_placeholder_phone = '095 xxx xx xx'; // заполнение для примера в поле Телефон
$callback_text_opd = 'Отправляя настоящую форму, я согласен(а) с 
        <a style="color: darkblue;" href="terms">условиями соглашения</a>.';
$submit_callback_form = 'Отправить'; // название кнопки Отправить
$close_callback_form = 'Закрыть';

// Тело сообщения
$subject_title = 'Коллекторская Компания Sanigo';
$callback_subject = 'Обратный звонок | Sanigo';
$callback_name_title = 'Имя: ';
$callback_phone_title = 'Телефон: ';

// Результат отправки формы
$successful_request_callback = "
        <svg class='bi bi-check' width='3em' height='3em' viewBox='0 0 16 16' fill='#008000' xmlns='http://www.w3.org/2000/svg'>
            <path fill-rule='evenodd' d='M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z'/>
        </svg>
        <br>
        <p>Вы успешно заказали Обратный звонок!</p>
        <p>Мы свяжемся с Вами в ближайшее время.</p>
    ";

$err_request_callback = '
        <svg class="bi bi-x" width="3em" height="3em" viewBox="0 0 16 16" fill="#ff0000" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z"/>
            <path fill-rule="evenodd" d="M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z"/>
        </svg>
        <br>
        <p>Ошибка.</p>
        <p>Обновите страницу и повторите отправку сообщения еще раз.</p>
    ';
