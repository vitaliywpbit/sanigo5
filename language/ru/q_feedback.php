<?php
// Email
$email_to = 'w4promo@gmail.com'; // email на который будут приходить сообщения (ppaul@ukr.net, paul.podolin@gmail.com)
$email_from = 'info@sanigo.1gb.ua'; // email с которого будут приходить сообщения (создается на хостинге)
$reply_to = '';

//Для формы
$q_feedback_link = 'Форма для связи'; //Название ссылки для вызова формы
$request_q_feedback = 'Задайте вопрос'; //Название формы
$q_feedback_placeholder_name = 'Ваше Имя'; //Заполнение для примера
$q_feedback_placeholder_email = 'Email'; //Заполнение для примера
$q_feedback_placeholder_phone = 'Телефон';
$q_feedback_message_text = 'Текст сообщения:';
$q_feedback_text_opd = 'Отправляя настоящую форму, я согласен(а) с 
        <a style="color: darkblue;" href="terms">условиями соглашения</a>.';
$submit_q_feedback_form = 'Отправить'; //Название кнопки Отправить

//Тело сообщения
$subject_title = 'Коллекторская Компания Sanigo';
$q_feedback_subject = 'Сообщение с сайта | Sanigo';
$q_feedback_name_title = 'Имя: ';
$q_feedback_email_title = 'Email: ';
$q_feedback_phone_title = 'Телефон: ';
$q_feedback_message_title = 'Текст сообщения: ';

//Сообщение об ошибках


// Результат отправки формы
$successful_q_feedback = '
        <svg class="bi bi-check" width="3em" height="3em" viewBox="0 0 16 16" fill="#008000" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
        </svg>
        <br>
        <p>Благодарим Вас за обращение!</p>
        <p>Мы свяжемся с Вами в ближайшее время.</p>
    ';

$err_send_q_feedback = '
        <svg class="bi bi-x" width="3em" height="3em" viewBox="0 0 16 16" fill="#ff0000" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z"/>
            <path fill-rule="evenodd" d="M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z"/>
        </svg>
        <br>
        <p>Сообщение не отправлено.</p>
        <p>Обновите страницу и повторите отправку сообщения еще раз.</p>
    ';
