<?php

$cart_text = 'Корзина покупок';
$cart_empty_text = 'Ваша корзина пуста :(';

$cart_close_btn_text = 'Закрыть';
$checkout_text = 'Оформить заказ';
$total_to_pay_text = 'Итого: ';
$cart_currency = ' грн.';

$product_added_title = 'Товар добавлен';
$product_added_text = 'Выбранный Вами товар успешно добавлен в корзину.';
$open_cart_text = 'Посмотреть корзину';
