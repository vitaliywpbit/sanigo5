<?php
// Brand
$brand_name = 'Sanigo';
$publisher = 'Sanigo'; //Издатель (Brand), разместивший статью без html тегов

// Main Content / Catalog
$navbar_toggler_title = 'Меню';
$home_page_name = 'Главная';
$currency_ua = 'грн.';
$currency_iso = 'UAH'; // ISO 4217 , 050-100-15-67, 050-100-85-30
$count_items = 0;

$phone1 = '067-007-15-67';
$phone1_link = '+380670071567';
$phone2 = '050-100-15-67';
$phone2_link = '+380501001567';
$phone3 = '050-100-85-30';
$phone3_link = '+380501008530';

$facebook_link = 'http://www.facebook.com/khar.oas';
$instagram_link = 'http://www.instagram.com/khar.oas/';

$logo_alt = 'Областной аптечный склад';

// Buttons
$call_us_text = 'Позвонить';

// Footer
$footer_text_1 = 'Свяжитесь с нами<br>в мессенджерах';
$footer_text_2 = 'Бесплатный звонок';


// Copyright
$start_date = '2018';
$copyright = ' &copy; КП ХОР «Областной аптечный склад».';
$wpbit = 'Website by <a class="text-light" href="http://web-platinum.net/" target="_blank" rel="noopener">Web Platinum Studio</a>';

$terms = 'Условия согдашения';
