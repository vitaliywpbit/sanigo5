<?php

// Items
$main_page = 'Главная';
$instock = '<span style="color: green;">В наличии</span>';
$outofstock = '<span class="text-muted">Нет в наличии</span>';
$preorder = '<span class="text-muted">Предзаказ</span>';
$sku_title = 'Артикул: ';
$brand_title = 'Бренд: ';
$add_to_cart = ' В Корзину';
$buy_button_text = 'Купить';
$view_button_text = 'Посмотреть';
$order_button_text = 'Заказать';
$order_quantity = 'Укажите количество товара:';
$order_placeholder_quantity = '0 (только цифра)'; // количество товара для примера
$month_day = '-07-19'; // часть даты для JSON-LD (Товар): цена действительна до

// Available Shipping and Payment Methods
$shipping_payment_title = 'Доступные способы Доставки и Оплаты';
$shipping_payment_popover_title = 'Оплата и Доставка';
$shipping_payment_body = "<p><i>Оплата</i>:</p>
        <ul>
            <li>Оплата на карту;</li>
            <li>Наложенный платеж;</li>
            <li>Наличными.</li>
        </ul>
        <p><i>Доставка</i>:</p>
        <ul>
            <li>Нова Пошта;</li>
            <li>УкрПошта;</li>
            <li>Самовывоз.</li>
        </ul>
    ";

// Buttons
$item_more = 'Подробнее';

// Tabs
$description_tab_text = 'Описание';
$specification_tab_text = 'Характеристики';
$reviews_tab_text = 'Отзывы';
$reviews_count = '0';
