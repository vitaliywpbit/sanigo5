<?php
//Brand
$brand_name = 'WPbit';
$logo_alt = 'Лого WPbit';
$publisher = 'WPbit'; //Издатель (Brand), разместивший статью без html тегов


//Buttons
$sidebar_btn = 'МЕНЮ';
$close_sidebar_btn = 'ЗАКРЫТЬ';

//Sidebar Menu
$menu_link_01 = '<a href="http://wpbit.engine/" target="_blank">Главная</a>';
$menu_link_02 = '<a href="http://wpbit.engine/blog" target="_blank">Все статьи</a>';
$menu_link_03 = '<a href="http://wpbit.engine/about" target="_blank">О компании</a>';

//Footer
$quick_menu = 'ОСНОВНЫЕ РАЗДЕЛЫ:';
$quick_menu_link_01 = '<a href="#" target="_blank">Link 1</a>';
$quick_menu_link_02 = '<a href="#" target="_blank">Link 2</a>';
$quick_menu_link_03 = '<a href="#" target="_blank">Link 3</a>';
$quick_menu_link_04 = '<a href="#" target="_blank">Link 4</a>';
