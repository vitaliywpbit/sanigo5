<div class="container pt-3 pb-5">
    
    <h2 class="h3 text-uppercase text-muted mb-4">г. Киев</h2>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №7</b>
        </div>
        <div class="col-6 col-md-4 mt-1">г. Киев, Гимназическая набережная, 22
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008745">тел. 050-100-87-45</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:apteka_007@ukr.net" style="text-decoration-line: underline;">apteka_007@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 8:00 - 19:00<br> вс - выходной</div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase"><span style="font-size: 12.8px; font-weight: 700;">АПТЕЧНИЙ ПУНКТ №6 АПТЕКИ №43</span><br></div>
        <div class="col-6 col-md-4 mt-1">г. Киев, ул. Героев Сталинграда, 160<br></div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008624">тел. 050-100-86-24</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka43_6@ukr.net" style="text-decoration-line: underline;">gorp.apteka43_6@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1"><span style="font-size: 12.8px;">8:00 - 19:00</span><br style="font-size: 12.8px;"><span style="font-size: 12.8px;">без выходных</span><br></div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №43</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            г. Киев, ул. Гражданская, 25
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008792">тел. 050-100-87-92</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp_apteka43@ukr.net" style="text-decoration-line: underline;">gorp_apteka43@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 8:00 - 19:00<br>вс - 9:00-16:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №5 Аптеки №43</b>
        </div>
        <div class="col-6 col-md-4 mt-1">г. Киев, ул. Салтовское шоссе, 266
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008613">тел. 050-100-86-13</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka43_5@ukr.net"><u>gorp.apteka43_5@ukr.net</u></a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 08:00-20:00<br>вс 08:00-17:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №48</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            г. Киев, ул. Алчевских, 15
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008470">тел. 050-100-84-70</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka48@ukr.net"><u>gorp.apteka48@ukr.net</u></a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            08:00-20:00<br>без выходных</div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №237</b>
        </div>
        <div class="col-6 col-md-4 mt-1">г. Киев, ул. Киевских дивизий, 20
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008437" style="text-decoration-line: underline;">тел. 050-100-84-37</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka237@ukr.net"><u>gorp.apteka237@ukr.net</u></a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            08:00-20:00<br>без выходных</div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №241</b>
        </div>
        <div class="col-6 col-md-4 mt-1">г. Киев, ул. Полтавский шлях, 36
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008402" style="text-decoration-line: underline;">тел. 050-100-84-02</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gopr.apteka241@ukr.net"><u>gopr.apteka241@ukr.net</u></a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:30-17:00<br> сб, вс - выходные
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №249</b>
        </div>
        <div class="col-6 col-md-4 mt-1">г. Киев, ул. Плехановская, 121
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008409">тел. 050-100-84-09</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka249@ukr.net" style="text-decoration-line: underline;">gorp.apteka249@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 08:00-20:00<br>вс - выходной</div>
    </div>
           
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase"><span style="font-weight: bolder; font-size: 12.8px;">АПТЕЧНИЙ ПУНКТ №8 АПТЕКИ №43</span><br></div>
        <div class="col-6 col-md-4 mt-1">
            г. Киев, ул. Озерянская, 5
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008554">тел. 050-100-85-54</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka_287_1@ukr.net"><u>gorp.apteka_287_1@ukr.net</u></a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:30-17:00<br> сб 08:00-15:00<br>вс - выходной
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase"><span style="font-weight: bolder; font-size: 12.8px;">АПТЕЧНИЙ ПУНКТ №9 АПТЕКИ №43</span><br></div>
        <div class="col-6 col-md-4 mt-1">г. Киев, пр-т Ново-Баварский, 90
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008578">тел. 050-100-85-78</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka_287_2@ukr.net" style="text-decoration-line: underline;">gorp.apteka_287_2@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:00-18:00<br> сб 08:00-15:00<br>вс - выходной
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №335</b>
        </div>
        <div class="col-6 col-md-4 mt-1">г. Киев, пр-т Гагарина, 137
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008336">тел. 050-100-83-36</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka335@ukr.net"><u>gorp.apteka335@ukr.net</u></a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 08:00-19:00<br>вс 09:00-17:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний склад №1</b>
        </div>
        <div class="col-6 col-md-4 mt-1">г. Киев, ул. Гражданская, 25
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><span style="font-size: 12.8px;">&nbsp;</span><a href="tel:+380501008930">тел. 050-100-89-30</a><br></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:sertif@sanigo.1gb.ua" style="text-decoration-line: underline;">sertif@sanigo.1gb.ua</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 09:00-17:30<br> сб, вс - выходные</div>
    </div>
    
    
    <h2 class="h3 text-uppercase text-muted mt-5 mb-4"><span class="VIiyi" lang="ru"><span class="JLqJ4b ChMk0b" data-language-for-alternatives="ru" data-language-to-translate-into="uk" data-phrase-index="0"><span>Киевская</span></span> <span class="JLqJ4b ChMk0b" data-language-for-alternatives="ru" data-language-to-translate-into="uk" data-phrase-index="1"><span>область</span></span></span> </h2>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №2</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киевская обл., пгт. Шевченково,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;ул. Центральная, 33
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008739">тел. 050-100-87-39</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka2@ukr.net" style="text-decoration-line: underline;">gorp.apteka2@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 07:00-19:00<br>вс 08:00-17:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №1 Аптеки №2</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киевская обл., пгт. Шевченково&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ул. Бубличенко, 15
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501001567" style="text-decoration-line: underline;">тел. 050-100-15-67 (справочная)</a>&nbsp;</p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:apteka_002_2@ukr.net"><u>apteka_002_2@ukr.net</u></a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 07:30-19:30<br>вс 07:30-15:30
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №1 Аптеки №43</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киевская обл., г. Балаклея, ул. Октябрьская, 10
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501001567">тел. 050-100-15-67 (справочная)</a><span style="font-size: 12.8px;">&nbsp;</span><br></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka_43_1@ukr.net" style="text-decoration-line: underline; background-color: rgb(248, 249, 250); font-size: 12.8px;">gorp.apteka_43_1@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:00-16:00<br> сб 8:00-12:00<br>вс - выходной</div>
    </div>
    
	<div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №2 Аптеки №43</b>
        </div>
        <div class="col-6 col-md-4 mt-1">Киевская обл., г. Балаклея, ул. Победы, 100<br></div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501001567">тел. 050-100-15-67 (справочная)</a><span style="font-size: 12.8px;">&nbsp;</span><br></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka_43_1@ukr.net" style="text-decoration-line: underline; background-color: rgb(248, 249, 250); font-size: 12.8px;">gorp.apteka_43_1@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1"><span style="font-size: 12.8px;">Пн-пт 08:00-15:00</span><br style="font-size: 12.8px;"><span style="font-size: 12.8px;">сб 8:00-13:00</span><br style="font-size: 12.8px;"><span style="font-size: 12.8px;">вс -&nbsp;</span><span style="font-size: 12.8px;">выходной</span><br></div>
    </div>	
	
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №3 Аптеки №43</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киевская обл., г. Валки, пер. Майский, 34
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008669" style="text-decoration-line: underline;">тел. 050-100-86-69</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka43_3@ukr.net"><u>gorp.apteka43_3@ukr.net</u></a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 08:00-18:00<br>вс 08:00-17:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase"><span style="font-size: 12.8px; font-weight: 700;">АПТЕКА №131</span><br></div>
        <div class="col-6 col-md-4 mt-1">Киевская обл., пгт. Краснокутск, ул. Мира, 133<br></div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501001567">тел. 050-100-15-67 (справочная)</a><span style="font-size: 12.8px;">&nbsp;</span><br></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka_43_4@ukr.net" style="text-decoration-line: underline;">gorp.apteka_43_4@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1"><span style="font-size: 12.8px;">Пн-сб&nbsp;</span><span style="font-size: 12.8px;">07:30-19:00</span><br style="font-size: 12.8px;"><span style="font-size: 12.8px;">вс 08:00-19:00</span><br></div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase"><span style="font-weight: bolder; font-size: 12.8px;">АПТЕЧНИЙ ПУНКТ №10 АПТЕКИ №43</span><br></div>
        <div class="col-6 col-md-4 mt-1">Киевская обл., пгт. Двуречная,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ул. Слобожанская, 51<br></div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008599">тел. 050-100-85-99</a><br></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka_43_4@ukr.net"><u>gorp.apteka_43_4@ukr.net</u></a></p>
        </div>
        <div class="col-6 col-md-2 mt-1"><span style="font-size: 12.8px;">Пн-сб 08:00-16:00</span><br style="font-size: 12.8px;"><span style="font-size: 12.8px;">вс - выходной</span><br></div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №354</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киевская обл., пгт. Васищево, ул. Орешкова, 21
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008263">тел. 050-100-82-63</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka354@ukr.net" style="text-decoration-line: underline;">gorp.apteka354@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 08:00-18:00<br>вс 08:00-16:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>ЦРА №79&nbsp;</b></div>
        <div class="col-6 col-md-4 mt-1">
            Киевская обл., г. Изюм, ул. Соборная, 33
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008152">тел. 050-100-81-52</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka79@ukr.net" style="text-decoration-line: underline;">gorp.apteka79@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            07:00-19:00<br>без выходных</div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №81</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киевская обл., Изюмский р-н, с. Оскол,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; пл. Слободская, 3
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008914">тел. 050-100-89-14</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka81@ukr.net" style="text-decoration-line: underline;">gorp.apteka81@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 08:00 -18:00<br>вс 08:00-16:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №154</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киевская обл., Изюмский р-н, с. Долгенькое,&nbsp; &nbsp; ул. Борисова, 14
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501001567" style="text-decoration-line: underline;">тел. 050-100-15-67 (справочная)</a><span style="font-size: 12.8px;">&nbsp;</span><br></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka154@ukr.net" style="text-decoration-line: underline; background-color: rgb(248, 249, 250); font-size: 12.8px;">gorp.apteka154@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:00 -15:00<br> сб 08:30-13:00<br>вс - выходной</div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №157</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киевская обл., Краснокутский р-н, пос. Козиевка, ул. Слобожанская, 91-А
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008116">тел. 050-100-81-16</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka157@ukr.net" style="text-decoration-line: underline;">gorp.apteka157@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:00 -16:00<br> сб 08:00-14:00<br>вс - выходной
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №346</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киевская обл., пгт. Новая Водолага,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ул. Пушкина, 16
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008296">тел. 050-100-82-96</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka346@ukr.net" style="text-decoration-line: underline;">gorp.apteka346@ukr.net</a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            07:00-19:00<br>без выходных</div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №1 Аптеки №346</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киевская обл., Нововодолажский р-н,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;с. Староверовка, ул. Центральная, 72
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501001567" style="text-decoration-line: underline;">тел. 050-100-15-67 (справочная)</a><span style="font-size: 12.8px;">&nbsp;</span><br></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka60@ukr.net"><u>gorp.apteka60@ukr.net</u></a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:00-16:00<br> сб 08:00-14:00<br>вс - выходной</div>
    </div>
    
	<div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №2 Аптеки №346</b>
        </div>
        <div class="col-6 col-md-4 mt-1">Киевская обл., Нововодолажский р-н,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;с. Новоселовка, ул. Воскресенская, 254<br></div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501001567" style="text-decoration-line: underline;">тел. 050-100-15-67 (справочная)</a><span style="font-size: 12.8px;">&nbsp;</span><br></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka60@ukr.net"><u>gorp.apteka60@ukr.net</u></a></p>
        </div>
        <div class="col-6 col-md-2 mt-1"><span style="font-size: 12.8px;">Пн-пт 08:00-14:30</span><br style="font-size: 12.8px;"><span style="font-size: 12.8px;">сб 08:00-13:00</span><br style="font-size: 12.8px;"><span style="font-size: 12.8px;">вс - вихідний</span><br></div>
    </div>	
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>ЦРА №113</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киевская обл., пгт Зачепиловка,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ул. Центральная, 47
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2" style="font-size: 12.8px;"><a href="tel:+380501008226">тел. 050-100-82-26</a></p><p class="mb-2" style="font-size: 12.8px;"><a href="mailto:gorp.apteka113@ukr.net"><u>gorp.apteka113@ukr.net</u></a></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            08:00-18:00<br>без выходных</div>
    </div>
    
</div>
