
<div class="container pt-5 pb-5">
    
    <div class="row">
 
        <div class="col-md-12 mt-3">
        
        <div id="m-about">   
            <h2 class="text-uppercase text-center mb-5">О компании</h2>
          
            <p class="text-sanigo-main text-center text-uppercase">
            <img src="images/logo/logo.webp" width="177" height="91" class="mb-5"><br>
            компания «Sanigo» является финансовым учреждением, включенным в Государственный реестр финансовых 
            учреждений и имеющим действующую лицензию на предоставление услуг по факторингу. Основным направлением
             деятельности является финансирование под уступку права требования, в том числе по финансовым кредитам.
              Наша цель помочь бизнесу избавиться от рисковых, а иногда не ликвидных активов. Мы берем все ваши риски
               возникновения дебиторской задолженности на себя, предоставляя при этом гибкие и конкурентоспособные условия финансирования.  
    </p>
    </div>  
    </div></div></div>


 

    <div class="pt-5 pb-5 bg-sanigo-2" >
    <div class="container mt-5 mb-5">
    <div class="row">
  
    <div class="col-6 col-md-2 text-center text-white">
        <img class="mb-3" src="images/pages/doc1.webp" loading="lazy" alt="загрузить документы SANIGO">
        <a href="files/doc1.pdf"><p class=text-uppercase>документ</p>
       <img class="" src="images/pages/down.webp" loading="lazy" alt="загрузить документы SANIGO">
        <p class="text-uppercase text-white"><small>загрузить</small></a></p>
    </div>

  
    <div class="col-6 col-md-2 text-center text-white">
        <img class="mb-3" src="images/pages/doc2.webp" loading="lazy" alt="загрузить документы SANIGO">
        <a href="files/doc1.pdf"><p class=text-uppercase>документ</p>
       <img class="" src="images/pages/down.webp" loading="lazy" alt="загрузить документы SANIGO">
        <p class="text-uppercase text-white"><small>загрузить</small></a></p>
    </div>
      
    <div class="col-6 col-md-2 text-center text-white">
        <img class="mb-3" src="images/pages/doc3.webp" loading="lazy" alt="загрузить документы SANIGO">
        <a href="files/doc1.pdf"><p class=text-uppercase>документ</p>
       <img class="" src="images/pages/down.webp" loading="lazy" alt="загрузить документы SANIGO">
        <p class="text-uppercase text-white"><small>загрузить</small></a></p>
    </div>

      
    <div class="col-6 col-md-2 text-center text-white">
        <img class="mb-3" src="images/pages/doc4.webp" loading="lazy" alt="загрузить документы SANIGO">
        <a href="files/doc1.pdf"><p class=text-uppercase>документ</p>
       <img class="" src="images/pages/down.webp" loading="lazy" alt="загрузить документы SANIGO">
        <p class="text-uppercase text-white"><small>загрузить</small></a></p>
    </div>
      
    <div class="col-6 col-md-2 text-center text-white">
        <img class="mb-3" src="images/pages/doc5.webp" loading="lazy" alt="загрузить документы SANIGO">
        <a href="files/doc1.pdf"><p class=text-uppercase>документ</p>
       <img class="" src="images/pages/down.webp" loading="lazy" alt="загрузить документы SANIGO">
        <p class="text-uppercase text-white"><small>загрузить</small></a></p>
    </div>
    <div class="col-6 col-md-2 text-center text-white">
        <img class="mb-3" src="images/pages/doc6.webp" loading="lazy" alt="загрузить документы SANIGO">
        <a href="files/doc1.pdf"><p class=text-uppercase>документ</p>
       <img class="" src="images/pages/down.webp" loading="lazy" alt="загрузить документы SANIGO">
        <p class="text-uppercase text-white"><small>загрузить</small></a></p>
    </div>

    </div>
    </div>
    </div>

    

<!--          ****************************************************************************************************************                    -->
<div id="m-services">
<div class="container pt-5 pb-5">
    <div class="row">
        <div class="col-md-12 mt-3">
                <h2 class="text-uppercase text-center mb-5">Услуги</h2>
                    <p class="text-sanigo-main text-center text-uppercase">
            <img src="images/icons/factoring.webp" class="mt-2"><br>
             <h3 class="text-center text-uppercase">Факторинг</h3>
          
            <hr color="orange" width="300">
          <!--  <hr color="f39200" width="300"> -->
<p>Компания «Sanigo» выступает активным участником на рынке покупки залоговых 
и беззалоговых долговых портфелей (согласно главе 73 ГК Украины).</p>

<p>Сотрудничество с компанией гарантирует клиенту:</p>
<ul><b>
<li>высвобождение резервов и улучшение финансовых показателей;</li>
<li>уменьшение расходов на работу с проблемными долгами;</li>
<li>сохранение положительного стиля.</li>
</ul></b>
        </div>
    </div>
</div>
</div>
<!--          ****************************************************************************************************************                    -->
<!--          ****************************************************************************************************************                    -->
<div class="container pt-5 pb-5">
    <div class="row">
        <div class="col-md-12 mt-3">
         
                    <p class="text-sanigo-main text-center text-uppercase">
            <img src="images/icons/dosudebnoe.webp" class="mt-2"><br>
             <h3 class="text-center text-uppercase">Досудебное урегулирование задолженности</h3>
          
            <hr color="orange" width="300">
          <!--  <hr color="f39200" width="300"> -->
<p>Включает консультирование должников по поводу актуального состояния задолженности, а также юридических 
аспектов и последствий неуплаты долга. В компании работает более 50 операторов контакт-центра, а также штат
 юристов, к которым можно обратиться за консультацией по проблемному долгу.</p>
<p>Обычно досудебное урегулирование происходит в два этапа:
          soft-collection – дистанционная работа с должником;
          hard-collection – непосредственный контакт с должником.</p>
<p>Компания опирается на современные системные технологии и опыт лучших европейских коллекторских и 
факторинговых компаний. Благодаря продуманной системе обучения сотрудников контакт-центру и эффективному
 механизму распределения и управления звонками компания обеспечивает высокий результат по каждому делу.
    </p>
        </div>
    </div>
</div>
<!--          ****************************************************************************************************************                    -->
<!--          ****************************************************************************************************************                    -->
<div class="container pt-5 pb-5">
    <div class="row">
        <div class="col-md-12 mt-3">
          
                    <p class="text-sanigo-main text-center text-uppercase">
            <img src="images/icons/sudebnoe.webp" class="mt-2"><br>
             <h3 class="text-center text-uppercase">Судебное урегулирование задолженности</h3>
          
            <hr color="orange" width="300">
          <!--  <hr color="f39200" width="300"> -->
<p>В компании «Sanigo» работают высококвалифицированные юристы с большим опытом в решении вопросов урегулирования
 задолженности в судебном порядке. Они обеспечивают профессиональное юридическое сопровождение исполнительного 
 производства и четкую координацию действий исполнителей в регионах.</p>

<p>Судебное урегулирование долга включает:
формирование пакета документов для судебного производства;
проверку действий должника на предмет мошенничества;
комплексное сопровождение исполнительного производства.</p>
    </p>
        </div>
    </div>
</div>
<!--          ****************************************************************************************************************                    -->

<div class="bg-sanigo-3 pt-5 pb-5">
<div class="row">
<div class="container">


<div class="text-center text-white text-uppercase">
<img src="images/logo/logo.webp" width="177" height="91" class="mb-5"><br>
Сотрудничество с компанией «Sanigo» – <br>это реальный выход из сложной ситуации
что гарантирует лишение долга.
<hr color="orange" width="300"></div>

<p class="text-white text-center">требования, осуществляемого профессиональными участниками рынка финансовых услуг. Обращаем внимание, 
что уступка права (перевод долга) происходит без согласия должника. Если вдруг Вы стали нашим должником,
 мы готовы предложить Вам лояльные и гибкие условия погашения долга. Мы всегда готовы пойти на компромисс 
 для решения проблем погашения долга без репутационных потерь для должника. Мы индивидуально подходим 
 к каждой ситуации. Для получения консультаций по урегулированию задолженности обращайтесь по телефонам горячей линии.</p>

</div>
</div>
</div>
->

<!-- ******************************************************** ЦЕЛЬ ****************************************************************************************-->

<div class="container pt-5 pb-5">
    <div class="row">
        <div class="col-md-12 mt-3">
          
                    <p class="text-sanigo-main text-center text-uppercase">
            <img src="images/icons/target.webp" class="mt-2"><br>
             <h3 class="text-center text-uppercase">Наша цель</h3>
          
            <hr color="orange" width="300">
          <!--  <hr color="f39200" width="300"> -->
         <p> Задача компании – построить взаимодействие с должником так, чтобы и кредитор, и должник выполняли свои обязательства и сохраняли собственное достоинство и репутацию.
          Компания не заинтересована сделать вас банкротом. При этом компания руководствуется принципом «задолженность должна быть погашена», и будет использовать для его 
          реализации все предусмотренные законом способы. Мы рекомендуем в кратчайшие сроки связаться с представителем компании «Sanigo» и согласовать график погашения долга.
    </p>
<div class="border border-warning text-center pt-5 pb-5 px-5 mb-5 mt-5 mx-5">
    <div class="container">
        <div class="row">
        <div class="col-12 col-md-2 mb-3"><img src="images/icons/atten.webp"></div>
            <div class="col-12 col-md-10"><b>Изменение информации о задолженности: </b>если задолженность погашена полностью или частично или если по вашему адресу или номеру телефона нет лица 
    с имеющейся задолженностью по кредиту, а мы связываемся по этим контактным данным, а также в других случаях несоответствия личной информации, просим вас сообщить нам об этом.
            </div>
    </div></div></div>

        </div>
    </div>
</div>

<!-- ***********************************************************      FAQ     *******************************************************************************************-->
<div class="w-100 bg-grey-faq pt-5 pb-5">
    <h2 class="text-sanigo-main text-center text-uppercase">Часто задаваемые вопросы</h2>
   
    <div class="container">
            <div class="row">
                <div class="col-4 col-md-2 mt-3">
                     <img src="images/icons/quest.webp" class="mt-2"><br>
                     <b>ВОПРОС</b><br>
                </div>
            <div class="col-8 col-md-10 mt-3">
            <h3 class="pt-3 bp-1">Как мой кредит попал в коллекторскую компанию? </h3>
            </div>
                <div class="col-4 col-md-2 mt-3 text-center">
                    <img src="images/icons/answ.webp" class="mt-2"><br>
                    ОТВЕТ
                </div>

                <div class="col-12 col-md-10 mt-3">
Финансовое учреждение передало нам право требования по вашему долгу в соответствии с договором факторинга (цессии) согласно ст. 1077, 1078,
 1080 ГК РФ. Причина такого решения заключается в нарушении условий кредитного договора. В настоящее время финансовое учреждение уже не
  является кредитором, о чем вы получили соответствующее уведомление.
                </div>
        <hr align="left" color="orange" width="300">
        </div>
    </div>

 <!-- * -->   
 <div class="container">
 <div class="row">
     <div class="col-4 col-md-2 mt-3">
          <img src="images/icons/quest.webp" class="mt-2"><br>
          <b>ВОПРОС</b><br>
     </div>
 <div class="col-8 col-md-10 mt-3">
 <h3 class="pt-3 bp-1">Почему мне никто не сообщил, что долг выкуплен?</h3>
 </div>
     <div class="col-4 col-md-2 mt-3 text-center">
         <img src="images/icons/answ.webp" class="mt-2"><br>
         ОТВЕТ
     </div>

     <div class="col-12 col-md-10 mt-3">
     На адрес, указанный в кредитном договоре, было отправлено письменное сообщение от нового
     кредитора (фактора). В письме вас проинформировали о факте передачи права денежного 
     требования по вашему договору. В письме указана текущая сумма вашего долга и контактные данные нового кредитора.
     </div>
<hr align="left" color="orange" width="300">
</div>
</div>

 <!-- * -->   

 <div class="container">
 <div class="row">
     <div class="col-4 col-md-2 mt-3">
          <img src="images/icons/quest.webp" class="mt-2"><br>
          <b>ВОПРОС</b><br>
     </div>
 <div class="col-8 col-md-10 mt-3">
 <h3 class="pt-3 bp-1">Как мой кредит попал в коллекторскую компанию? </h3>
 </div>
     <div class="col-4 col-md-2 mt-3">
         <img src="images/icons/answ.webp" class="mt-2"><br>
         ОТВЕТ
     </div>

     <div class="col-12 col-md-10 mt-3">
Финансовое учреждение передало нам право требования по вашему долгу в соответствии с договором факторинга (цессии) согласно ст. 1077, 1078,
1080 ГК РФ. Причина такого решения заключается в нарушении условий кредитного договора. В настоящее время финансовое учреждение уже не
является кредитором, о чем вы получили соответствующее уведомление.
     </div>
<hr align="left" color="orange" width="300">
</div>
</div>

<!-- * -->   
<div class="container">
<div class="row">
<div class="col-4 col-md-2 mt-3">
<img src="images/icons/quest.webp" class="mt-2"><br>
<b>ВОПРОС</b><br>
</div>
<div class="col-8 col-md-10 mt-3">
<h3 class="pt-3 bp-1">Почему мне никто не сообщил, что долг выкуплен?</h3>
</div>
<div class="col-4 col-md-2 mt-3">
<img src="images/icons/answ.webp" class="mt-2"><br>
ОТВЕТ
</div>

<div class="col-12 col-md-10 mt-3">
На адрес, указанный в кредитном договоре, было отправлено письменное сообщение от нового
кредитора (фактора). В письме вас проинформировали о факте передачи права денежного 
требования по вашему договору. В письме указана текущая сумма вашего долга и контактные данные нового кредитора.
</div>
<hr align="left" color="orange" width="300">
</div>
</div>

</div>   


<!--**********************************************************************************************************************************************************************************-->

<!-- ***********************************************************      ІНШІ ПИТАННЯ    *******************************************************************************************-->
<div class="container pt-5 pb-5">
    <h2 class="text-center text-uppercase">Другие вопросы</h2>
    </div>   


    <div class="container">
    <div class="row border border-warning">
         <div class="col-12 col-md-2 mt-3 text-center">
             <img src="images/icons/krest.webp" class="mt-5"><br>
       
        </div>
        <div class="col-12 col-md-10 mt-1">

        <h3 class="pt-3 bp-1">Это правда, что мой долг могут отступать кому-либо множество раз без моего согласия?</h3>

        <p>Уступка права денежного требования осуществляется при наличии у фактора свидетельства о регистрации финансового учреждения, выданного Нацкомфинуслуг и соответствующей лицензии на предоставление услуг по факторингу. Последующая уступка права денежного требования третьему лицу допускается, если это установлено договором факторинга.</p>
        </div>

</div>
</div>



    <div class="container">
    <div class="row border border-warning">
         <div class="col-12 col-md-2 mt-3 text-center">
             <img src="images/icons/plus.webp" class="mt-2"><br>
       
        </div>
        <div class="col-12 col-md-10 mt-1">

        <h3 class="pt-3 bp-1">Должна ли быть лицензия у факторинговых компаний на осуществление финансовых операций?</h3>

<p>Должна ли быть лицензия у факторинговых компаний на осуществление финансовых операций?</p>
        </div>

</div>
</div>



<div class="container">
<div class="row border border-warning">
     <div class="col-12 col-md-2 mt-3 text-center">
         <img src="images/icons/plus.webp" class="mt-2"><br>
   
    </div>
    <div class="col-12 col-md-10 mt-1">

    <h3 class="pt-3 bp-1">На каких основаниях финансовое учреждение передало третьему лицу (фактору) конфиденциальную информацию о моем долге?</h3>

<p>На каких основаниях финансовое учреждение передало третьему лицу (фактору) конфиденциальную информацию о моем долге?</p>
    </div>

</div>
</div>

<div class="container">
<div class="row border border-warning">
     <div class="col-12 col-md-2 mt-3 text-center">
         <img src="images/icons/plus.webp" class="mt-2"><br>
   
    </div>
    <div class="col-12 col-md-10 mt-1">

    <h3 class="pt-3 bp-1">Должна ли быть лицензия у факторинговых компаний на осуществление финансовых операций?</h3>

<p>Должна ли быть лицензия у факторинговых компаний на осуществление финансовых операций?</p>
    </div>

</div>
</div>

<div class="container">
<div class="row border border-warning">
     <div class="col-12 col-md-2 mt-3 text-center">
         <img src="images/icons/plus.webp" class="mt-2"><br>
   
    </div>
    <div class="col-12 col-md-10 mt-1">

    <h3 class="pt-3 bp-1">На каких основаниях финансовое учреждение передало третьему лицу (фактору) конфиденциальную информацию о моем долге?</h3>

<p>На каких основаниях финансовое учреждение передало третьему лицу (фактору) конфиденциальную информацию о моем долге?</p>
    </div>

</div>
</div>



<!--**********************************************************************************************************************************************************************************-->
    


<!-- ***********************************************************      Інформація про фінансову установу    *******************************************************************************************-->
<div class="container pt-5 pb-5">
    <h2 class="text-center text-uppercase">Информация о финансовом учреждении</h2>
    </div>   


    <div class="container">
            <div class="row">




    <div class="col-12 col-md-4 border border-warning">
    <h4 class="pt-3 bp-1"><b>Сведения о финансовом учреждении</b></h4>
    <hr align="left" color="orange" width="200">
<p>Полное наименование:
ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "ТАЛИОН ПЛЮС"</p>
<p>Сокращенное наименование:
ООО "ТАЛИОН ПЛЮС"</p>
<p>Идентификационный код ЕГРПОУ:
39700642</p>
</div>



 
    <div class="col-12 col-md-4 border border-warning">
    <h4 class="pt-3 bp-1"><b>Сведения об исполнительном органе финансового учреждения:</b></h4>
    <hr align="left" color="orange" width="200">
<p>Начало: 23.02.2017
Конец: 06.09.2018
Подписант: Директор
ФИО: Дубовый В.М.</p>

<p>Начало: 07.09.2018
Конец: по настоящее время
Подписант: Директор
ФИО: Патиюк В.В.</p>
</div>

<div class="col-12 col-md-4 border border-warning">
<h4 class="pt-3 bp-1"><b>Местонахождение:</b></h4>
<hr align="left" color="orange" width="200">
<p>Код территории по КОАТУУ: 7410100000</p>
<p>Адрес: Украина, 14027, Киевская обл., город Киев, ул.Рокоссовского, дом 7, первый этаж</p>
</div>




    <div class="col-12 col-md-4 border border-warning">
    <h4 class="pt-3 bp-1"><b>Список финансовых услуг</b></h4>
    <hr align="left" color="orange" width="200">
<p>Предоставление услуг по факторингу</p>
</div>



    <div class="col-12 col-md-4 border border-warning">
    <h4 class="pt-3 bp-1"><b>Стоимость финансовых услуг</b></h4>
    <hr align="left" color="orange" width="200">
<p>Стоимость финансовых услуг определяется в соответствии с
 выбранными клиентом программами, согласно которым последний желает получить финансовые услуги.</p>
</div>

<div class="col-12 col-md-4 border border-warning">
<h4 class="pt-3 bp-1"><b>Контактные данные</b></h4>
<hr align="left" color="orange" width="200">
<p>Контактные телефоны:
+380462941331,
0 800 215 017 – бесплатно со всех операторов Украины</p>

<p>Адреса электронной почты:
info@sanigo.com.ua</p>

<p>Факс: -</p>
</div>


</div>
</div>
<!--********************************************************************************************************************************************************************************** -->
<div class="container pt-5 pb-5">
    <h2 class="text-center text-uppercase">Контакты</h2>
    </div>

<div class="container mb-5">
<div class="row text-uppercase">

<div class="col-12 col-md-6 sec">
Бесплатный звонок: <a href="tel:8003333333">800-333-33-33<a><br>
e-mail: <b><a href="mailto:info@sanigo.1gb.ua">info@sanigo.1gb.ua</a></b><br>

Свяжитесь с нами<br>в мессенджерах:
<a href="viber://chat?number=%2B380500000001" class="mx-1 mt-2 mb-2">
<img src="images/icons/tg.webp" width="32" height="32"></a>
<a href="viber://chat?number=%2B380500000002" class="mx-1 mt-2 mb-2">
<img src="images/icons/wp.webp" width="32" height="32"></a>
<a href="viber://chat?number=%2B380500000003" class="mx-1 mt-2 mb-2">
<img src="images/icons/vi.webp" width="32" height="32"></a><br>

Режим работы: <b>ежедневно с 9:00 до 19:00</b><br>
Кроме воскресенья и праздничных дней<br>
<br>

Компания "Sanigo"<br>
14027, г. Киев,<br>
ул. Рокоссовского,<br>
дом 7, первый этаж
</div>
<div class="col-12 col-md-6">
{feedback-form-1}
</div>


</div>
</div>