<p>Використовуючи сайт <i><a href="http://sanigo.1gb.ua/">http://sanigo.1gb.ua/</a></i>,
    Ви погоджуєтеся на обробку Ваших персональних даних та даєте наступне Згоду</p>
<h2>Згода на обробку персональних даних</h2>
<p>Використовуючи сайт <i><a href="http://sanigo.1gb.ua/">http://sanigo.1gb.ua/</a></i>,
    (Заповнюючи форми, переглядаючи сторінки), своєю волею і в своєму
    інтересі висловлюю свою безумовну згоду на обробку моїх персональних
    даних <i> Компанія "Sanigo", далі - Оператор</i>.</p>
<p>Персональні дані - будь-яка інформація, що відноситься до певного або
    визначається на підставі такої інформації фізичній особі.</p>
<p>Мною видано згоду на обробку наступних персональних даних:</p>
    <ul>
        <li><i>Прізвище ім'я по батькові;</i></li>
        <li><i>Адреса електронної пошти;</i></li>
        <li><i>IP адреса.</i></li>
    </ul>
<p>Справжнє Згода дано Оператору для здійснення наступних дій з
    персональними даними з використанням засобів автоматизації та / або
    без використання таких засобів: збір, систематизація, накопичення, зберігання,
    уточнення (оновлення, зміну), використання, блокування, знеособлення,
    знищення, видалення.</p>
<p>Згода дається Оператору для обробки моїх персональних даних з метою:</p>
    <ul>
        <li><i>напрямок мені електронних листів;</i></li>
        <li><i>надання мені послуг і робіт;</i></li>
        <li><i>напрямок мені інформації, в тому числі рекламної, перевірити наявність та
        пропозиціях Оператора.</i></li>
    </ul>
<p>Згода діє до моменту його відкликання шляхом направлення відповідного
    повідомлення на електронну адресу 
    <i><a href="mailto:w4promo@gmail.com">w4promo@gmail.com</a></i>
