<div class="container pt-3">
    
    <p class="h3 text-muted mb-3">Шановні покупці.</p>
    
    <div class="border-left border-info pl-3">
        <p>
            За допомогою даної послуги Ви завжди зможете <b>замовити необхідний товар 
в нашій мережі в режимі online</b> <span class="VIiyi" lang="uk"><span class="JLqJ4b ChMk0b" data-language-for-alternatives="uk" data-language-to-translate-into="ru" data-phrase-index="0"><span>або <br></span></span> <b><span class="JLqJ4b ChMk0b" data-language-for-alternatives="uk" data-language-to-translate-into="ru" data-phrase-index="1"><span>за телефонами</span></span></b> <u><a href="tel:+380501001567" target="_blank">+38(050) 100-15-67</a></u><span class="JLqJ4b ChMk0b" data-language-for-alternatives="uk" data-language-to-translate-into="ru" data-phrase-index="7"><span>,</span></span> <u><span class="JLqJ4b ChMk0b" data-language-for-alternatives="uk" data-language-to-translate-into="ru" data-phrase-index="8"><span><a href="tel:+380670071567" target="_blank">+38</a></span></span><a href="tel:+380670071567" target="_blank"> </a><span class="JLqJ4b ChMk0b" data-language-for-alternatives="uk" data-language-to-translate-into="ru" data-phrase-index="9"><span>(</span></span><span class="JLqJ4b ChMk0b" data-language-for-alternatives="uk" data-language-to-translate-into="ru" data-phrase-index="10"><span>067</span></span><span class="JLqJ4b ChMk0b" data-language-for-alternatives="uk" data-language-to-translate-into="ru" data-phrase-index="11"><span>)</span></span><a href="tel:+380670071567" target="_blank"> </a><span class="JLqJ4b ChMk0b" data-language-for-alternatives="uk" data-language-to-translate-into="ru" data-phrase-index="12"><span><a href="tel:+380670071567" target="_blank">007-15-67</a></span></span></u></span>, а потім забрати його в найближчій аптеці 
«Обласний аптечний склад» у зручний час.</p>
        <p>
            Купуючи товари таким способом Ви <b>економите час і гроші</b>, а найголовніше -
            <strong>гарантовано отримуєте сертифікований товар високої якості</strong>, 
            що доставляється в місце видачі безпосередньо з нашого аптечного складу. Наше підприємство працює тільки з офіційними постачальниками і виробниками, маючи всі необхідні сертифікати якості та реєстрацію на весь асортимент товару, наданий в аптеках. Фахівці підприємства регулярно контролюють терміни придатності і правильне зберігання препаратів. <br></p>
    </div>
    
    <p class="h5 ml-4 mt-4">
        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#73b3d7" class="bi bi-arrow-right-circle-fill mb-1 mr-2" viewBox="0 0 16 16">
                <path d="M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"></path>
        </svg>
        <b>Увага!</b><br> 
        Остерігайтеся підробок і <em><ins>купуйте медичні препарати тільки в 
        перевірених місцях</ins></em>.
    </p>
    
    <h2 class="h3 text-blue text-uppercase text-center mt-5 mb-3">Як зробити замовлення через Інтернет</h2>
    <p class="text-center text-muted">Для придбання товарів в мережі «Обласний 
        аптечний склад» за допомогою інтернет використовуйте онлайн-сервіс 
        <a href="http://tabletki.ua/uk/" target="_blank" rel="noopener">tabletki.ua</a>. 
        У <b>цьому відероліку показано - як зробити замовлення в найближчій аптеці нашої мережі</b> з його допомогою:</p>
    
    <div class="row align-items-center">
        <div class="col-md-5 mt-4">
            <div class="embed-responsive embed-responsive-16by9 mb-2">
                <div class="youtube embed-responsive-item" id="9c6vFWZQc34" style="background-image: url(&quot;http://i.ytimg.com/vi/9c6vFWZQc34/sddefault.jpg&quot;);" http:="" i.ytimg.com="" vi="" 9c6vfwzqc34="" sddefault.jpg");"=""><div class="play"></div><div class="play"></div><div class="play"></div><div class="play"></div><div class="play"></div><div class="play"></div><div class="play"></div><div class="play"></div><div class="play"></div><div class="play"></div><div class="play"></div></div>
            </div>
            <p class="text-muted">Як зробити замовлення через online-сервіс tabletki.ua</p>
        </div>
        <div class="col-md-7 mt-4">
            <p class="text-blue h5 mb-3"><b>Використовуючи даний сервіс - Ви зможете:</b></p>
            <ul>
                <li>дізнатися про наявність та актуальну ціну на препарати в найближчих до Вас аптеках, вказавши адресу або зразкове місце розташування;</li>
                <li>знайти і зберегти шукану аптеку мережі «Обласний аптечний склад» в Списку обраних для швидкого пошуку і замовлення;</li>
                <li>в будь-який зручний час оформити своє замовлення через інтернет 
                    (при готовності товару до видачі на Ваш телефон і email прийде 
                    відповідне повідомлення);</li>
                <li>забрати товар і оплатити його, відвідавши обрану в замовленні 
                    аптеку в робочий час;</li>
                <li>порівняти ціну на товар з іншими аптеками поблизу;</li>
                <li>зареєструватися в якості постійного користувача, щоб отримати 
                    доступ до історії всіх своїх замовлень, повторювати замовлення, 
                    отримувати бонуси і знижки.</li>
            </ul>
        </div>
    </div>
    
</div>

<div class="w-100 bg-light pt-5 pb-5 mt-5">
    <div class="container text-center">
        <h3 class="text-muted">Замовлення дефіцитних позицій</h3>
        <p class="lead text-blue">
            У разі виникнення питань або недоступності необхідного товару для покупки в мережі 
            аптек - зв'яжіться з нашою Довідковою службою по телефонам
            <a href="tel:+380501001567" target="_blank">+38(050) 100-15-67</a>, <a href="tel:+380670071567">+38(067) 007-15-67</a> і ми зробимо все 
            можливе, щоб виконати Ваше замовлення.</p>
    </div>
</div>
