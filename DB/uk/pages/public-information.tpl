<div class="container pt-3 pb-5">
    
    <h2 class="h3 text-uppercase text-muted">Статутні документи</h2>
    
    <div class="row">
        <div class="col-md-4 text-center mt-4">
            <img src="images/pages/license.jpg" loading="lazy" class="img-fluid img-thumbnail" alt="Ліцензія Sanigo">
            <p class="mt-3">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-download mb-1 mr-2" viewBox="0 0 16 16">
                    <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"></path>
                    <path d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"></path>
                </svg>
                <a href="docs/license.pdf">Ліцензія (237,1 кБ)</a>
            </p>
        </div>
        <div class="col-md-4 text-center mt-4">
            <img src="images/pages/svidotstvo.jpg" loading="lazy" class="img-fluid img-thumbnail" alt="Свідотство про державну реєстрацію">
            <p class="mt-3">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-download mb-1 mr-2" viewBox="0 0 16 16">
                    <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"></path>
                    <path d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"></path>
                </svg>
                <a href="docs/svidotstvo.pdf">Свідотство про державну реєстрацію (503,1 кБ)</a>
            </p>
        </div>
        <div class="col-md-4 text-center mt-4">
            <img src="images/pages/statut.jpg" loading="lazy" class="img-fluid img-thumbnail" alt="Статут">
            <p class="mt-3">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-download mb-1 mr-2" viewBox="0 0 16 16">
                    <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"></path>
                    <path d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"></path>
                </svg>
                <a href="docs/Statut.pdf" target="_blank">Статут (1,1 МБ)</a></p></div></div><div class="row"><div class="col-md-6 col-lg-4"><ul class="list-modern">
            </ul>
        </div>
        
    </div>
    
</div>

<div class="w-100 bg-light pt-5 pb-5">
    <div class="container">
        <p class="lead text-center mb-0">
            Відомості про договори, інформація про які підлягає оприлюдненню відповідно 
            до Закону України "Про відкритість використання публічних коштів" можна 
            знайти за адресою електронного сховища 
            <a href="http://spending.gov.ua/" target="_blank" rel="ugc nofollow noopener">E-data</a> за кодом ЄДРПОУ 22654831
        </p>
    </div>
</div>