<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 mb-3">
        <img class="img-fluid rounded mx-auto d-block" src="images/404.png" alt="Ошибка 404">
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 mb-3">
        <h3 class="text-center">Можливі причини:</h3>
            <br/>
            !&nbsp;<i>сторінка або заголовок був вилучений;</i>
            <br/>
            !&nbsp;<i>був змінений URL сторінки;</i>
            <br/>
            !&nbsp;<i>помилка в написанні посилання;</i>
            <br/>
            <br/>
            <a class="btn btn-outline-dark" href="#" role="button">
                Перейти на головну</a>
    </div>
</div>
