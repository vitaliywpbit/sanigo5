<div class="container pt-3 pb-5">
    
    <h2 class="h3 text-uppercase text-muted mb-4">м. Киів</h2>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №7</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            м. Киів, Гімназійна набережна, 22
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008745">тел. 050-100-87-45</a>
            </p>
            <p class="mb-2">
                <a href="mailto:apteka_007@ukr.net"><u>apteka_007@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 8:00 - 19:00<br> нд - вихідний
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase"><span style="font-weight: bolder; font-size: 12.8px;">АПТЕЧНИЙ ПУНКТ №6 АПТЕКИ №43</span><br></div>
        <div class="col-6 col-md-4 mt-1">
            м. Киів, вул. Героїв Сталінграду,160</div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008624">тел. 050-100-86-24</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka43_6@ukr.net"><u>gorp.apteka43_6@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">8:00 - 19:00<br><span style="font-size: 12.8px;">без вихідних</span><br></div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №43</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            м. Киів, вул. Громадянська, 25
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008792">тел. 050-100-87-92</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp_apteka43@ukr.net"><u>gorp_apteka43@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 8:00 - 19:00<br>сб,нд - 9:00-16:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №5 Аптеки №43</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            м. Киів, Салтівське шоссе, 266
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008613">тел. 050-100-86-13</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka43_5@ukr.net"><u>gorp.apteka43_5@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 08:00-20:00<br> нд 08:00-17:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №48</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            м. Киів, вул. Алчевських, 15
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008470">тел. 050-100-84-70</a></p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka48@ukr.net"><u>gorp.apteka48@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            08:00-20:00<br> без вихідних
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №237</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            м. Киів, вул. Киівських дивізій, 20
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008437">тел. 050-100-84-37</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka237@ukr.net"><u>gorp.apteka237@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            08:00-20:00<br> без вихідних
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №241</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            м. Киів, вул. Полтавський шлях, 36
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008402">тел. 050-100-84-02</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gopr.apteka241@ukr.net"><u>gopr.apteka241@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:30-17:00<br> сб, нд - вихідні
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №249</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            м. Киів, вул. Плеханівська, 121
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008409">тел. 050-100-84-09</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka249@ukr.net"><u>gorp.apteka249@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 08:00-20:00<br> нд - вихідний
        </div>
    </div>  
   
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №8 Аптеки №43</b></div>
        <div class="col-6 col-md-4 mt-1">
            м. Киів, вул. Озерянська, 5
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008554">тел. 050-100-85-54</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka_287_1@ukr.net"><u>gorp.apteka_287_1@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:30-17:00<br> сб 08:00-15:00<br> нд - вихідний
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №9 Аптеки №43</b></div>
        <div class="col-6 col-md-4 mt-1">
            м. Киів, пр-т Ново-Баварський, 90
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008578">тел. 050-100-85-78</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka_287_2@ukr.net"><u>gorp.apteka_287_2@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:00-18:00<br> сб 08:00-15:00<br> нд - вихідний
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №335</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            м. Киів, пр-т Гагарiна, 137
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008336">тел. 050-100-83-36</a>
            </p>
            <p class="mb-2">
                <u><a href="mailto:gorp.apteka335@ukr.net">gorp.apteka335@ukr.net</a>
            </u></p>
        </div>
        <div class="col-6 col-md-2 mt-1"><u>
            Пн-сб 08:00-19:00<br></u> нд 09:00-17:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний склад №1</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            м. Киів, вул. Громадянська, 25&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div><div class="col-6 col-md-4 mt-1">
            <p class="mb-2"><a href="tel:+380501008930">тел. 050-100-89-30</a><br></p>
            <p class="mb-2">
                <a href="mailto:sertif@sanigo.1gb.ua"><u>sertif@sanigo.1gb.ua</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 09:00-17:30<br> сб, нд - вихідні
        </div>
    </div>
    
    
    <h2 class="h3 text-uppercase text-muted mt-5 mb-4">Киівська область</h2>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №2</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., смт. Шевченкове,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; вул. Центральна, 33
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008739">тел. 050-100-87-39</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka2@ukr.net"><u>gorp.apteka2@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 07:00-19:00<br> нд 08:00-17:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №1 Аптеки №2</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., смт. Шевченково&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; вул. Бубліченка, 15
        </div>
        <div class="col-6 col-md-4 mt-1"><p class="mb-2">
                <a href="tel:+380501001567">тел. 050-100-15-67 (довідкова)</a>&nbsp;</p><p class="mb-2"><a href="mailto:apteka_002_2@ukr.net"><u>apteka_002_2@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 07:30-19:30<br> нд 07:30-15:30
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №1 Аптеки №43</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., м. Балаклія, вул. Жовтнева,10
        </div>
        <div class="col-6 col-md-4 mt-1"><p class="mb-2"><a href="tel:+380501001567" style="background-color: rgb(248, 249, 250); font-size: 12.8px;">тел. 050-100-15-67 (довідкова)</a><span style="font-size: 12.8px;">&nbsp;</span><br></p><p class="mb-2"><a href="mailto:gorp.apteka_43_1@ukr.net" style="background-color: rgb(248, 249, 250); font-size: 12.8px;"><u>gorp.apteka_43_1@ukr.net</u></a><br></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:00-16:00<br> сб 8:00-12:00<br> нд - вихідний
        </div>
    </div>
	
	<div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №2 Аптеки №43</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., м. Балаклія, вул. Перемоги, 100</div>
        <div class="col-6 col-md-4 mt-1"><p class="mb-2"><a href="tel:+380501001567" style="background-color: rgb(248, 249, 250); font-size: 12.8px;">тел. 050-100-15-67 (довідкова)</a><span style="font-size: 12.8px;">&nbsp;</span><br></p><p class="mb-2"><a href="mailto:gorp.apteka_43_1@ukr.net" style="background-color: rgb(248, 249, 250); font-size: 12.8px;"><u>gorp.apteka_43_1@ukr.net</u></a><br></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:00-15:00<br> сб 8:00-13:00<br> нд - вихідний
        </div>
    </div>	
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №3 Аптеки №43</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., м. Валки, пров. Майський, 34</div><div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008669">тел. 050-100-86-69</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka43_3@ukr.net"><u>gorp.apteka43_3@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 08:00-18:00<br> нд 08:00-17:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase"><span style="font-weight: bolder; font-size: 12.8px;">АПТЕКА №131</span><br></div>
        <div class="col-6 col-md-4 mt-1"><span style="font-size: 12.8px;">Киівська обл., смт.</span>&nbsp;Краснокутськ, вул. Миру, 133</div><div class="col-6 col-md-4 mt-1"><p class="mb-2"><a href="tel:+380501001567" style="background-color: rgb(248, 249, 250); font-size: 12.8px;">тел. 050-100-15-67 (довідкова)</a><span style="font-size: 12.8px;">&nbsp;</span><br></p><p class="mb-2"><a href="mailto:gorp.apteka_43_4@ukr.net"><u>gorp.apteka_43_4@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1"><span style="font-size: 12.8px;">Пн-сб&nbsp;</span><span style="font-size: 12.8px;">07:30-19:00</span><br style="font-size: 12.8px;"><span style="font-size: 12.8px;">нд 08:00-19:00</span><br></div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №10 Аптеки №43</b>
        </div>
        <div class="col-6 col-md-4 mt-1"><span style="font-size: 12.8px;">Киівська обл., смт.</span><span style="font-size: 12.8px;">&nbsp;Дворічна,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; вул. Слобожанська, 51</span><br></div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2"><a href="tel:+380501008599">тел. 050-100-85-99</a><br></p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka_43_4@ukr.net"><u>gorp.apteka_43_4@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 08:00-16:00<br><span style="font-size: 12.8px;">нд - вихідний</span><br></div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №354</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., смт. Васищеве, вул. Орешкова, 21
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008263">тел. 050-100-82-63</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka354@ukr.net"><u>gorp.apteka354@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 08:00-18:00<br> нд 08:00-16:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>ЦРА №79</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл. м. Ізюм, вул. Соборна, 33
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008152">тел. 050-100-81-52</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka79@ukr.net"><u>gorp.apteka79@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            07:00-19:00<br> без вихідних
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №81</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., Ізюмський р-н, с. Оскіл, майдан Слобідський, 3</div><div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008914">тел. 050-100-89-14</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka81@ukr.net"><u>gorp.apteka81@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-сб 08:00 -18:00<br> нд 08:00-16:00
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №154</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., Ізюмський р-н, с. Довгеньке, Борисова, 14</div><div class="col-6 col-md-4 mt-1"><p class="mb-2"><a href="tel:+380501001567" style="font-size: 12.8px; background-color: rgb(248, 249, 250);">тел. 050-100-15-67 (довідкова)</a><span style="font-size: 12.8px;">&nbsp;</span><span style="font-size: 12.8px;">&nbsp;</span><br></p><p class="mb-2"><a href="mailto:gorp.apteka154@ukr.net" style="background-color: rgb(248, 249, 250); font-size: 12.8px;"><u>gorp.apteka154@ukr.net</u></a><br></p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:00 -15:00<br> сб 08:30-13:00<br> нд - вихідний
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №157</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., Краснокутський р-н, сел. Козіївка, вул. Слобожанська, 91-А
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008116">тел. 050-100-81-16</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka157@ukr.net"><u>gorp.apteka157@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:00 -16:00<br> сб 08:00-14:00<br> нд - вихідний
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптека №346</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., смт. Нова Водолага,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; вул. Пушкіна, 16
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008296">тел. 050-100-82-96</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka346@ukr.net"><u>gorp.apteka346@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            07:00-19:00<br> без вихідних
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №1 Аптеки №346</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., Нововодолазький р-н,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;с. Староверівка, вул. Центральна, 72</div><div class="col-6 col-md-4 mt-1"><p class="mb-2"><a href="tel:+380501001567" style="background-color: rgb(248, 249, 250); font-size: 12.8px;">тел. 050-100-15-67 (довідкова)</a><span style="font-size: 12.8px;">&nbsp;</span><br></p><p class="mb-2"><a href="mailto:gorp.apteka60@ukr.net"><u>gorp.apteka60@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:00-16:00<br> сб 08:00-14:00<br> нд - вихідний
        </div>
    </div>
    
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>Аптечний пункт №2 Аптеки №346</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., Нововодолазький р-н,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;с. Новоселівка, вул. Воскресінська, 254</div><div class="col-6 col-md-4 mt-1"><p class="mb-2"><a href="tel:+380501001567" style="background-color: rgb(248, 249, 250); font-size: 12.8px;">тел. 050-100-15-67 (довідкова)</a><span style="font-size: 12.8px;">&nbsp;</span><br></p><p class="mb-2"><a href="mailto:gorp.apteka60@ukr.net"><u>gorp.apteka60@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            Пн-пт 08:00-14:30<br> сб 08:00-13:00<br> нд - вихідний
        </div>
    </div>
	
    <div class="row bg-light p-2 mt-2 align-items-center small text-break border-bottom">
        <div class="col-6 col-md-2 mt-1 text-uppercase">
            <b>ЦРА №113</b>
        </div>
        <div class="col-6 col-md-4 mt-1">
            Киівська обл., смт.&nbsp; Зачепилівка,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; вул. Центральна, 47
        </div>
        <div class="col-6 col-md-4 mt-1">
            <p class="mb-2">
                <a href="tel:+380501008226">тел. 050-100-82-26</a>
            </p>
            <p class="mb-2">
                <a href="mailto:gorp.apteka113@ukr.net"><u>gorp.apteka113@ukr.net</u></a>
            </p>
        </div>
        <div class="col-6 col-md-2 mt-1">
            08:00-18:00<br> без вихідних
        </div>
    </div>
    
</div>
