<h2 class="h3 text-blue">Компанія «SANIGO</h2>
<p>Адреса: <u><a href="http://goo.gl/maps/tVTarQFWGqi8BTMp7" target="_blank"> м. Киів, вул. Рокосовського, будинок 7</a></u></p>

<p class="text-uppercase mt-4">
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#f39200" class="bi bi-envelope-fill mb-1 mr-2" viewBox="0 0 16 16">
        <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555zM0 4.697v7.104l5.803-3.558L0 4.697zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757zm3.436-.586L16 11.801V4.697l-5.803 3.546z"></path>
    </svg>
    <b>Електронна пошта:</b>
</p>
<p>
    <a href="mailto:info@sanigo.1gb.ua">info@sanigo.1gb.ua</a>
</p>


<p class="text-uppercase mt-4">
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#f39200" class="bi bi-telephone-fill mb-1 mr-2" viewBox="0 0 16 16">
        <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"></path>
    </svg>
    <b>Телефони:</b>
</p>
<p><a href="tel:800 333 33 33">800 333 33 33<a></p>

<p>Дзвінок безкоштовний</p>
<p>Зв'яжіться з нами в месенджерах:</p>

            <a href="viber://chat?number=%2B380500000001" class="mx-1 mt-2 mb-2">
    <img src="images/icons/tg.webp" width="32" height="32"></a>
    <a href="viber://chat?number=%2B380500000002" class="mx-1 mt-2 mb-2">
    <img src="images/icons/wp.webp" width="32" height="32"></a>
   <a href="viber://chat?number=%2B380500000003" class="mx-1 mt-2 mb-2">
    <img src="images/icons/vi.webp" width="32" height="32"></a>
 
<p class="mt-3">Режим роботи: щоденно з 9:00 до 19:00</p>
<br>
<p>Крім неділі та святкових днів</p>