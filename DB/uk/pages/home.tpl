<div class="container pt-5 pb-5">
    
    <div class="row">
 
        <div class="col-md-12 mt-3">
        
        <div id="m-about">    
            <h2 class="text-uppercase text-center mb-5">Про компанію</h2>
          
            <p class="text-sanigo-main text-center text-uppercase">
            <img src="images/logo/logo.webp" loading="lazy" width="177" height="91" class="mb-5"><br>
            компанія «Sanigo» є фінансовою установою, включеною до Державного реєстру фінансових
            установ та мають діючу ліцензію на надання послуг з факторингу. Основним напрямком
             діяльності є фінансування під відступлення права вимоги, у тому числі з фінансових кредитів.
              Наша мета допомогти бізнесу позбутися ризикових, а іноді не ліквідних активів. Ми беремо всі ваші ризики
               виникнення дебіторської заборгованості на себе, надаючи при цьому гнучкі та конкурентоспроможні умови фінансування.  
    </p>
    </div> 
    </div></div></div>


 

    <div class="pt-5 pb-5 bg-sanigo-2" >
    <div class="container mt-5 mb-5">
    <div class="row">
  
    <div class="col-6 col-md-2 text-center text-white">
        <img class="mb-3" src="images/pages/doc1.webp" loading="lazy" alt="Завантажити документи SANIGO">
        <a href="files/doc1.pdf"><p class=text-uppercase>документ</p>
       <img class="" src="images/pages/down.webp" loading="lazy" alt="Завантажити документи SANIGO">
        <p class="text-uppercase text-white"><small>завантажити</small></a></p>
    </div>

  
    <div class="col-6 col-md-2 text-center text-white">
        <img class="mb-3" src="images/pages/doc2.webp" loading="lazy" alt="Завантажити документи SANIGO">
        <a href="files/doc1.pdf"><p class=text-uppercase>документ</p>
       <img class="" src="images/pages/down.webp" loading="lazy" alt="Завантажити документи SANIGO">
        <p class="text-uppercase text-white"><small>завантажити</small></a></p>
    </div>
      
    <div class="col-6 col-md-2 text-center text-white">
        <img class="mb-3" src="images/pages/doc3.webp" loading="lazy" alt="Завантажити документи SANIGO">
        <a href="files/doc1.pdf"><p class=text-uppercase>документ</p>
       <img class="" src="images/pages/down.webp" loading="lazy" alt="Завантажити документи SANIGO">
        <p class="text-uppercase text-white"><small>завантажити</small></a></p>
    </div>

      
    <div class="col-6 col-md-2 text-center text-white">
        <img class="mb-3" src="images/pages/doc4.webp" loading="lazy" alt="Завантажити документи SANIGO">
        <a href="files/doc1.pdf"><p class=text-uppercase>документ</p>
       <img class="" src="images/pages/down.webp" loading="lazy" alt="Завантажити документи SANIGO">
        <p class="text-uppercase text-white"><small>завантажити</small></a></p>
    </div>
      
    <div class="col-6 col-md-2 text-center text-white">
        <img class="mb-3" src="images/pages/doc5.webp" loading="lazy" alt="Завантажити документи SANIGO">
        <a href="files/doc1.pdf"><p class=text-uppercase>документ</p>
       <img class="" src="images/pages/down.webp" loading="lazy" alt="Завантажити документи SANIGO">
        <p class="text-uppercase text-white"><small>завантажити</small></a></p>
    </div>
    <div class="col-6 col-md-2 text-center text-white">
        <img class="mb-3" src="images/pages/doc6.webp" loading="lazy" alt="Завантажити документи SANIGO">
        <a href="files/doc1.pdf"><p class=text-uppercase>документ</p>
       <img class="" src="images/pages/down.webp" loading="lazy" alt="Завантажити документи SANIGO">
        <p class="text-uppercase text-white"><small>завантажити</small></a></p>
    </div>

    </div>
    </div>
    </div>

    

<!--          ****************************************************************************************************************                    -->
<div id="m-services">
<div class="container pt-5 pb-5">
    <div class="row">
        <div class="col-md-12 mt-3">
                <h2 class="text-uppercase text-center mb-5">Послуги</h2>
                    <p class="text-sanigo-main text-center text-uppercase">
            <img src="images/icons/factoring.webp" loading="lazy" class="mt-2"><br>
             <h3 class="text-center text-uppercase">Факторинг</h3>
          
            <hr color="orange" width="300">
          <!--  <hr color="f39200" width="300"> -->
          <p>Компанія Sanigo виступає активним учасником на ринку купівлі заставних
          та беззаставних боргових портфелів (згідно з главою 73 ЦК України).</p>
          
          <p>Співпраця з компанією гарантує клієнту:</p>
          <ul><b>
          <li>вивільнення резервів та покращення фінансових показників;</li>
          <li>зменшення витрат на роботу з проблемними боргами;</li>
          <li>збереження позитивного стилю.</li>
</ul></b>
        </div>
    </div>
</div>
</div>
<!--          ****************************************************************************************************************                    -->
<!--          ****************************************************************************************************************                    -->
<div class="container pt-5 pb-5">
    <div class="row">
        <div class="col-md-12 mt-3">
         
                    <p class="text-sanigo-main text-center text-uppercase">
            <img src="images/icons/dosudebnoe.webp" loading="lazy" class="mt-2"><br>
             <h3 class="text-center text-uppercase">Досудове врегулювання заборгованості</h3>
          
            <hr color="orange" width="300">
          <!--  <hr color="f39200" width="300"> -->
          <p>Включає консультування боржників щодо актуального стану заборгованості, а також юридичних
          аспектів та наслідків несплати боргу. У компанії працює понад 50 операторів контакт-центру, а також штат
           юристів, до яких можна звернутися за консультацією щодо проблемного боргу.</p>
          <p>Зазвичай досудове врегулювання відбувається у два етапи:
                    soft-collection – дистанційна робота із боржником;
                    hard-collection – безпосередній контакт із боржником.</p>
          <p>Компанія спирається на сучасні системні технології та досвід кращих європейських колекторських та
          факторингових компаній. Завдяки продуманій системі навчання співробітників контакт-центру та ефективному
           механізму розподілу та управління дзвінками компанія забезпечує високий результат у кожній справі.
    </p>
        </div>
    </div>
</div>
<!--          ****************************************************************************************************************                    -->
<!--          ****************************************************************************************************************                    -->
<div class="container pt-5 pb-5">
    <div class="row">
        <div class="col-md-12 mt-3">
          
                    <p class="text-sanigo-main text-center text-uppercase">
            <img src="images/icons/sudebnoe.webp" loading="lazy" class="mt-2"><br>
             <h3 class="text-center text-uppercase">Судове врегулювання заборгованості</h3>
          
            <hr color="orange" width="300">
          <!--  <hr color="f39200" width="300"> -->
          <p>У компанії «Sanigo» працюють висококваліфіковані юристи з великим досвідом у вирішенні питань урегулювання
          заборгованості у судовому порядку. Вони забезпечують професійний юридичний супровід виконавчого
          виробництва та чітку координацію дій виконавців у регіонах.</p>
        
        <p>Судове врегулювання боргу включає:
        формування пакету документів для судового провадження;
        перевірку дій боржника щодо шахрайства;
        комплексний супровід виконавчого провадження.</p>
    </p>
        </div>
    </div>
</div>
<!--          ****************************************************************************************************************                    -->

<div class="bg-sanigo-3 pt-5 pb-5">
<div class="row">
<div class="container">


<div class="text-center text-white text-uppercase">
<img src="images/logo/logo.webp" loading="lazy" width="177" height="91" class="mb-5"><br>
Співпраця з компанією «Sanigo» – це реальний вихід зі складної ситуації
що гарантує позбавлення боргу.
<hr color="orange" width="300"></div>

<p class="text-white text-center">вимоги, що здійснюються професійними учасниками ринку фінансових послуг. Звертаємо увагу,
що поступка права (переведення боргу) відбувається без згоди боржника. Якщо раптом Ви стали нашим боржником,
  ми готові запропонувати Вам лояльні та гнучкі умови погашення боргу. Ми завжди готові піти на компроміс
  на вирішення проблем погашення боргу без репутаційних втрат для боржника. Ми індивідуально підходимо
  до кожної ситуації. Для отримання консультацій щодо врегулювання заборгованості звертайтесь за телефонами гарячої лінії.</p>

</div>
</div>
</div>


<!-- ******************************************************** ЦЕЛЬ ****************************************************************************************-->

<div class="container pt-5 pb-5">
    <div class="row">
        <div class="col-md-12 mt-3">
          
                    <p class="text-sanigo-main text-center text-uppercase">
            <img src="images/icons/target.webp" loading="lazy" class="mt-2"><br>
             <h3 class="text-center text-uppercase">Наша ціль</h3>
          
            <hr color="orange" width="300">
          <!--  <hr color="f39200" width="300"> -->
         <p>Завдання компанії – побудувати взаємодію Космосу з боржником те щоб і кредитор, і боржник виконували свої зобов'язання і зберігали власну гідність і репутацію.
         Компанія не зацікавлена зробити вас банкрутом. При цьому компанія керується принципом «заборгованість має бути погашена» і використовуватиме для нього
         реалізації всі передбачені законом методи. Ми рекомендуємо в найкоротші терміни зв'язатися з представником компанії Sanigo і погодити графік погашення боргу.
    </p>
<div class="border border-warning text-center pt-5 pb-5 px-5 mb-5 mt-5 mx-5">
    <div class="container">
        <div class="row">
        <div class="col-12 col-md-2 mb-3"><img src="images/icons/atten.webp" loading="lazy"></div>
            <div class="col-12 col-md-10"><b>Зміна інформації про заборгованість: </b>якщо заборгованість погашена повністю або частково або якщо на вашу адресу або номер телефону немає особи
            з наявною заборгованістю по кредиту, а ми зв'язуємося за цими контактними даними, а також в інших випадках невідповідності особистої інформації, просимо вас повідомити нас про це.
                    </div>
    </div></div></div>

        </div>
    </div>
</div>

<!-- ***********************************************************      FAQ     *******************************************************************************************-->
<div class="w-100 bg-grey-faq pt-5 pb-5">
    <h2 class="text-sanigo-main text-center text-uppercase">Часто задавані питання</h2>
   
    <div class="container">
            <div class="row">
                <div class="col-4 col-md-2 mt-3">
                     <img src="images/icons/quest.webp" loading="lazy" class="mt-2"><br>
                     <b>ПИТАННЯ</b><br>
                </div>
            <div class="col-8 col-md-10 mt-3">
            <h3 class="pt-3 bp-1">Як мій кредит потрапив до колекторної компанії?</h3>
            </div>
                <div class="col-4 col-md-2 mt-3 text-center">
                    <img src="images/icons/answ.webp" loading="lazy" class="mt-2"><br>
                    ВІДПОВІДЬ
                </div>

                <div class="col-12 col-md-10 mt-3">
                Фінансова установа передала нам право вимоги щодо вашого боргу відповідно до договору факторингу (цесії) згідно зі ст. 1077, 1078,
                1080 ЦК України. Причина такого рішення полягає у порушенні умов кредитного договору. В даний час фінансова установа вже не
                 є кредитором, про що ви отримали відповідне повідомлення.
                </div>
        <hr align="left" color="orange" width="300">
        </div>
    </div>

 <!-- * -->   
 <div class="container">
 <div class="row">
     <div class="col-4 col-md-2 mt-3">
          <img src="images/icons/quest.webp" loading="lazy" class="mt-2"><br>
          <b>ПИТАННЯ</b><br>
     </div>
 <div class="col-8 col-md-10 mt-3">
 <h3 class="pt-3 bp-1">Чому мені ніхто не повідомив, що борг викуплено?</h3>
 </div>
     <div class="col-4 col-md-2 mt-3 text-center">
         <img src="images/icons/answ.webp" loading="lazy" class="mt-2"><br>
         ВІДПОВІДЬ
     </div>

     <div class="col-12 col-md-10 mt-3">
     На адресу, вказану в кредитному договорі, було надіслано письмове повідомлення від нового
      кредитора (чинника). У листі вас поінформували про факт передачі грошового права
      вимоги за вашим договором. У листі вказано поточну суму вашого боргу та контактні дані нового кредитора.
     </div>
<hr align="left" color="orange" width="300">
</div>
</div>

 <!-- * -->   

 <div class="container">
 <div class="row">
     <div class="col-4 col-md-2 mt-3">
          <img src="images/icons/quest.webp" loading="lazy" class="mt-2"><br>
          <b>ПИТАННЯ</b><br>
                </div>
            <div class="col-8 col-md-10 mt-3">
            <h3 class="pt-3 bp-1">Як мій кредит потрапив до колекторної компанії?</h3>
            </div>
                <div class="col-4 col-md-2 mt-3 text-center">
                    <img src="images/icons/answ.webp" loading="lazy" class="mt-2"><br>
                    ВІДПОВІДЬ
                </div>

                <div class="col-12 col-md-10 mt-3">
                Фінансова установа передала нам право вимоги щодо вашого боргу відповідно до договору факторингу (цесії) згідно зі ст. 1077, 1078,
                1080 ЦК України. Причина такого рішення полягає у порушенні умов кредитного договору. В даний час фінансова установа вже не
                 є кредитором, про що ви отримали відповідне повідомлення.
     </div>
<hr align="left" color="orange" width="300">
</div>
</div>

<!-- * -->   
<div class="container">
<div class="row">
<div class="col-4 col-md-2 mt-3">
<img src="images/icons/quest.webp" class="mt-2"><br>
<b>ПИТАННЯ</b><br>
     </div>
 <div class="col-8 col-md-10 mt-3">
 <h3 class="pt-3 bp-1">Чому мені ніхто не повідомив, що борг викуплено?</h3>
 </div>
     <div class="col-4 col-md-2 mt-3 text-center">
         <img src="images/icons/answ.webp" loading="lazy" class="mt-2"><br>
         ВІДПОВІДЬ
     </div>

     <div class="col-12 col-md-10 mt-3">
     На адресу, вказану в кредитному договорі, було надіслано письмове повідомлення від нового
      кредитора (чинника). У листі вас поінформували про факт передачі грошового права
      вимоги за вашим договором. У листі вказано поточну суму вашого боргу та контактні дані нового кредитора.
</div>
<hr align="left" color="orange" width="300">
</div>
</div>

</div>   


<!--**********************************************************************************************************************************************************************************-->

<!-- ***********************************************************      ІНШІ ПИТАННЯ    *******************************************************************************************-->
<div class="container pt-5 pb-5">
    <h2 class="text-center text-uppercase">Інші питання</h2>
    </div>   


    <div class="container">
    <div class="row border border-warning">
         <div class="col-12 col-md-2 mt-3 text-center">
             <img src="images/icons/krest.webp" loading="lazy" class="mt-5"><br>
       
        </div>
        <div class="col-12 col-md-10 mt-1">

        <h3 class="pt-3 bp-1">Це правда, що моє зобов'язання можуть відступати комусь багато разів без моєї згоди?</h3>

<p>Відступлення права грошової вимоги здійснюється за умови наявності у фактора свідоцтва про реєстрацію фінансової установи, виданого Нацкомфінпослуг та відповідної ліцензії на надання послуг з факторингу. Наступне відступлення права грошової вимоги третій особі допускається, якщо це встановлено договором факторингу.</p>
        </div>

</div>
</div>



    <div class="container">
    <div class="row border border-warning">
         <div class="col-12 col-md-2 mt-3 text-center">
             <img src="images/icons/plus.webp" loading="lazy" class="mt-2"><br>
       
        </div>
        <div class="col-12 col-md-10 mt-1">

        <h3 class="pt-3 bp-1">Чи має бути ліцензія у факторингових компаній на здійснення фінансових операцій?</h3>

<p>Чи має бути ліцензія у факторингових компаній на здійснення фінансових операцій?</p>
        </div>

</div>
</div>



<div class="container">
<div class="row border border-warning">
     <div class="col-12 col-md-2 mt-3 text-center">
         <img src="images/icons/plus.webp" loading="lazy" class="mt-2"><br>
   
    </div>
    <div class="col-12 col-md-10 mt-1">

    <h3 class="pt-3 bp-1">На яких підставах фінансова установа передала третій особі (фактору) конфіденційну інформацію про мій борг?</h3>

    <p>На яких підставах фінансова установа передала третій особі (фактору) конфіденційну інформацію про мій борг?</p>>
    </div>

</div>
</div>

<div class="container">
<div class="row border border-warning">
     <div class="col-12 col-md-2 mt-3 text-center">
         <img src="images/icons/plus.webp" loading="lazy" class="mt-2"><br>
   
    </div>
    <div class="col-12 col-md-10 mt-1">

    <h3 class="pt-3 bp-1">Чи має бути ліцензія у факторингових компаній на здійснення фінансових операцій?</h3>

    <p>Чи має бути ліцензія у факторингових компаній на здійснення фінансових операцій?</p>
    </div>

</div>
</div>

<div class="container">
<div class="row border border-warning">
     <div class="col-12 col-md-2 mt-3 text-center">
         <img src="images/icons/plus.webp" loading="lazy" class="mt-2"><br>
   
    </div>
    <div class="col-12 col-md-10 mt-1">

    <h3 class="pt-3 bp-1">На яких підставах фінансова установа передала третій особі (фактору) конфіденційну інформацію про мій борг?</h3>

    <p>На яких підставах фінансова установа передала третій особі (фактору) конфіденційну інформацію про мій борг?</p>
    </div>

</div>
</div>



<!--**********************************************************************************************************************************************************************************-->
    


<!-- ***********************************************************      Інформація про фінансову установу    *******************************************************************************************-->
<div class="container pt-5 pb-5">
    <h2 class="text-center text-uppercase">Інформація про фінансову установу</h2>
    </div>   


    <div class="container">
            <div class="row">




    <div class="col-12 col-md-4 border border-warning">
    <h4 class="pt-3 bp-1"><b>Відомості про фінансову установу</b></h4>
    <hr align="left" color="orange" width="200">
<p>Повне найменування:
ТОВАРИСТВО З ОБМЕЖЕНОЮ ВІДПОВІДАЛЬНІСТЮ "ТАЛІОН ПЛЮС"</p>
<p>Скорочене найменування:
ТОВ "ТАЛІОН ПЛЮС"</p>
<p>Ідентифікаційний код ЄДРПОУ:
39700642</p>
</div>



 
    <div class="col-12 col-md-4 border border-warning">
    <h4 class="pt-3 bp-1"><b>Відомості про виконавчий орган фінансової установи:</b></h4>
    <hr align="left" color="orange" width="200">
    <p>Початок: 23.02.2017
    Кінець: 06.09.2018
    Підписант: Директор
    ПІБ: Дубовий В.М.</p>
    
    <p>Початок: 07.09.2018
    Кінець: по теперішній час
    Підписант: Директор
    ПІБ: Патіюк В.В.</p>
    </div>

<div class="col-12 col-md-4 border border-warning">
<h4 class="pt-3 bp-1"><b>Местонахождение:</b></h4>
<hr align="left" color="orange" width="200">
<p>Код території за КОАТУУ: 7410100000</p>
<p>Адреса: Україна, 14027, Киівська обл., місто Киів, вул.Рокосовського, будинок 7, перший поверх</p>
</div>




    <div class="col-12 col-md-4 border border-warning">
    <h4 class="pt-3 bp-1"><b>Список фінансових послуг</b></h4>
    <hr align="left" color="orange" width="200">
<p>Надання факторингових факторів</p>
</div>



    <div class="col-12 col-md-4 border border-warning">
    <h4 class="pt-3 bp-1"><b>Вартість фінансових послуг</b></h4>
    <hr align="left" color="orange" width="200">
<p>Вартість фінансових послуг визначається відповідно до
 обраними клієнтом програмами, згідно з якими останній бажає отримати фінансові послуги.</p>
</div>

<div class="col-12 col-md-4 border border-warning">
<h4 class="pt-3 bp-1"><b>Контактні дані</b></h4>
<hr align="left" color="orange" width="200">
<p>Контактні телефони:
+380462941331,
0 800 215 017 – безкоштовно з усіх операторів України</p>

<p>Адреса електронної пошти:
info@sanigo.com.ua</p>

<p>Факс: -</p>
</div>


</div>
</div>
<!--********************************************************************************************************************************************************************************** -->
<div class="container pt-5 pb-5">
    <h2 class="text-center text-uppercase">Контакти</h2>
    </div>

<div class="container mb-5">
<div class="row text-uppercase">

<div class="col-12 col-md-6 sec">
Безкоштовний дзвінок: <a href="tel:8003333333">800-333-33-33<a><br>
e-mail: <b><a href="mailto:info@sanigo.1gb.ua">info@sanigo.1gb.ua</a></b><br>

Зв'яжіться з нами у месенджерах:
<a href="viber://chat?number=%2B380500000001" class="mx-1 mt-2 mb-2">
<img src="images/icons/tg.webp" width="32" height="32"></a>
<a href="viber://chat?number=%2B380500000002" class="mx-1 mt-2 mb-2">
<img src="images/icons/wp.webp" width="32" height="32"></a>
<a href="viber://chat?number=%2B380500000003" class="mx-1 mt-2 mb-2">
<img src="images/icons/vi.webp" width="32" height="32"></a><br>

Режим роботи: <b>щоденно з 9:00 до 19:00</b><br>
Окрім неділі та святкових днів<br>
<br>

Компанія "Sanigo"<br>
14027, м. Киів, <br>
вул. Рокосовського,<br>
будинок 7, перший поверх
</div>
<div class="col-12 col-md-6">
{feedback-form-1}
</div>


</div>
</div>