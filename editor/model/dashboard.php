<?php

// Проверка доступных для редактирования языков сайта, язык по умолчанию
include 'components/lang_site.php';

// На каком языке будет редактироваться контент сайта
if (isset($_GET['elang'])) {
    $elang = $_GET['elang'];
    //Какой язык по умолчанию
    foreach ($db_lang_site as $line => $column) {
        if ($elang == $column['lang_site_code']) {
            $elang_name = $column['lang_site_name'];
            break;
        }
    }
}
else {
    $elang = $lang_site_default;
    $elang_name = $lang_site_name;
}

// Проверяем, есть ли данные на редактирование определенной страницы или записи
if (isset($_GET['epage'])) {
    $epage = $_GET['epage'];
}
else {
    $epage = null;
}

$number_of_pages = 0;
$number_of_blog_posts = 0;
$number_of_products = 0;
$number_of_orders = 0;

$pages = fopen("../DB/$elang/pages.csv", "rt") or die("Unable to open file (pages.csv)...");

if($pages) {
    
    flock($pages, LOCK_EX); // блокируем файл
    
    for ($i = 0; $data = fgetcsv($pages, 0, ","); $i++) {
        
        if ($data[0] == '#') {
            
            ++$number_of_pages;
            
        }
    }
    
    flock($pages, LOCK_UN); // снимаем блокировку
}

fclose($pages);

// ======================

$blog = fopen("../DB/$elang/blog.csv", "rt") or die("Unable to open file (blog.csv)...");

if($blog) {
    
    flock($blog, LOCK_EX); // блокируем файл
    
    for ($i = 0; $data = fgetcsv($blog, 0, ","); $i++) {
        
        ++$number_of_blog_posts;
        
    }
    
    flock($blog, LOCK_UN); // снимаем блокировку
}

fclose($blog);

// ======================

$items = fopen("../DB/$elang/items.csv", "rt") or die("Unable to open file (items.csv)...");

if($items) {
    
    flock($items, LOCK_EX); // блокируем файл
    
    for ($i = 0; $data = fgetcsv($items, 0, ","); $i++) {
        
        ++$number_of_products;
        
    }
    
    flock($items, LOCK_UN); // снимаем блокировку
}

fclose($items);

// ======================

$orders = fopen("../DB/orders.csv", "rt") or die("Unable to open file (orders.csv)...");

if($orders) {
    
    flock($orders, LOCK_EX); // блокируем файл
    
    for ($i = 0; $data = fgetcsv($orders, 0, ","); $i++) {
        
        ++$number_of_orders;
        
    }
    
    flock($orders, LOCK_UN); // снимаем блокировку
}

fclose($orders);
