<?php
$lang = fopen("DB/lang.csv", "rt") or die("Unable to open file (lang.csv)...");

if($lang) {
    
    for ($i = 0; $data = fgetcsv($lang, 1000, ","); $i++) {
        $db_lang[] = [
            'lang_code' => $data[0], // Код языка
            'lang_name' => $data[1], // Название языка
            'lang_default' => $data[2], // Язык по умолчанию
        ];
    }
}

fclose($lang);
