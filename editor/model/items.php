<?php
$items = fopen('../' . DB_DIR_PATH . $elang . '/items.csv', "rt") or die("Error Establishing a Database Connection...");

if($items) {
    
    flock($items, LOCK_EX); // блокируем файл
    
    for ($i = 0; $data = fgetcsv($items, 0, ","); $i++) {
        
        $db_items[] = [
            'id' => $data[0],
            'page_layout' => $data[1],
            'url' => $data[2],
            'title' => $data[3],
            'keywords' => $data[4],
            'description' => $data[5],
            'h1' => $data[6],
            'category_id' => $data[7],
            'record_img_1x1' => $data[8],
            'record_img_4x3' => $data[9],
            'record_img_16x9' => $data[10],
            'record_img_preview' => $data[11],
            'record_img_alt' => $data[12],
            'record_all_images' => $data[13],
            'item_short_description' => $data[14],
            'item_price' => $data[15],
            'item_new_price' => $data[16], // sale - новая цена со скидкой
            'item_sku' => $data[17],
            'item_availability' => $data[18],
            'item_brand_name' => $data[19]
        ];
        
        $final_id = $data[0];
    }
    
    flock($items, LOCK_UN); // снимаем блокировку

}
fclose($items);

// Если товары есть, перезаписываем их в обратном порядке
if ($db_items) {
    
    $db_items = array_reverse($db_items);
}

// Узнаем id категорий, seo-url категорий и названия категорий для списка формы
function getCategories($elang) {
    
    $pages = fopen('../' . DB_DIR_PATH . $elang . '/pages.csv', "rt") or die("Error Establishing a Database Connection...");

    if($pages) {
        
        $db_categories = null;

        flock($pages, LOCK_EX); // блокируем файл

        for ($i = 0; $data = fgetcsv($pages, 0, ","); $i++) {

            if ($data[1] == 'category') {
                
                $db_categories[] = [
                    'id' => $data[0],
                    'url' => $data[2],
                    'title' => $data[3],
                ];
            }
        }

        flock($pages, LOCK_UN); // снимаем блокировку
    }

    fclose($pages);
    return $db_categories;
}

// Узнаем id, категорию (seo-url) и название
function getCategory($item_id, $elang) {
    
    $pages = fopen('../' . DB_DIR_PATH . $elang . '/pages.csv', "rt") or die("Error Establishing a Database Connection...");

    if($pages) {

        flock($pages, LOCK_EX); // блокируем файл

        for ($i = 0; $data = fgetcsv($pages, 0, ","); $i++) {

            if ($data[0] == $item_id) {
                
                $category['id'] = $data[0];
                $category['url'] = $data[2];
                $category['title'] = $data[3];
                break;
            }
            else {
                $category = null;
            }
        }

        flock($pages, LOCK_UN); // снимаем блокировку
    }

    fclose($pages);
    return $category;
}
