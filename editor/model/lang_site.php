<?php
$lang_site = fopen("../DB/lang.csv", "rt") or die("Unable to open file (lang.csv)...");

if($lang_site) {
    
    for ($i = 0; $data = fgetcsv($lang_site, 1000, ","); $i++) {
        $db_lang_site[] = [
            'lang_site_code' => $data[0], // Код языка
            'lang_site_name' => $data[1], // Название языка
            'lang_site_default' => $data[2], // Язык по умолчанию
        ];
    }
}

fclose($lang_site);
