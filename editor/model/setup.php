<?php
$setup = fopen("DB/setup.csv", "rt") or die("Unable to open file (setup.csv)...");

if($setup) {
    
    flock($setup, LOCK_EX); // блокируем файл
    
    for ($i = 0; $data = fgetcsv($setup, 1000, ","); $i++) {
        $db_setup[] = $data[0]; /*option*/
    }
    
    flock($setup, LOCK_UN); // снимаем блокировку
    
    fclose($setup);
}
