<?php
$records = fopen('../' . DB_DIR_PATH . $elang . '/blog.csv', "rt") or die("Error Establishing a Database Connection...");

if($records) {
    
    flock($records, LOCK_EX); // блокируем файл
    
    for ($i = 0; $data = fgetcsv($records, 0, ","); $i++) {
        
         $db_records[] = [
            'id' => $data[0],
            'page_layout' => $data[1],
            'url' => $data[2],
            'title' => $data[3],
            'keywords' => $data[4],
            'description' => $data[5],
            'h1' => $data[6],
            'record_date' => $data[7],
            'record_mod_date' => $data[8],
            'record_img_1x1' => $data[9],
            'record_img_4x3' => $data[10],
            'record_img_16x9' => $data[11],
            'record_img_preview' => $data[12],
            'record_img_alt' => $data[13],
            'record_author' => $data[14],
            'record_author_type' => $data[15] // Person или Organization для JSON-LD
        ];
        
        $final_id = $data[0];
    }
    
    flock($records, LOCK_UN); // снимаем блокировку

}
fclose($records);

// Если записи есть, перезаписываем их в обратном порядке
if ($db_records) {
    
    $db_records = array_reverse($db_records);
}
