<?php

$settings = fopen('../' . DB_DIR_PATH . 'settings.csv', "rt") or die("Error Establishing a Database Connection...");

if($settings)
{
    for ($i = 0; $data = fgetcsv($settings, 1000, ","); $i++)
    {
        $db_settings[] = $data[0]; /*option*/
    }
}
fclose($settings);

$http_protocol = $db_settings[0]; // ex: http://
$domain_name = trim($db_settings[1]); // ex: web-platinum.com
$page_default = trim($db_settings[2]); // страница по умолчанию
$main_send_email = trim($db_settings[3]); // Основной Email для отправки сообщений или заказов
$img_default = trim($db_settings[4]); // ссылка на картинку по умолчанию для OpenGraph
$time_zone = $db_settings[5]; // TimeZone (ex: +03:00)
$google_id = trim($db_settings[6]); // Google ID для Google Analytics


$db_timezone = [
    '-12:00',
    '-11:00',
    '-10:00',
    '-9:30',
    '-9:00',
    '-8:00',
    '-7:00',
    '-6:00',
    '-5:00',
    '-4:00',
    '-3:30',
    '-3:00',
    '-2:00',
    '-1:00',
    '+0:00',
    '+1:00',
    '+2:00',
    '+3:00',
    '+3:30',
    '+4:00',
    '+4:30',
    '+5:00',
    '+5:30',
    '+5:45',
    '+6:00',
    '+6:30',
    '+7:00',
    '+8:00',
    '+8:45',
    '+9:00',
    '+9:30',
    '+10:00',
    '+10:30',
    '+11:00',
    '+12:00',
    '+12:45',
    '+13:00',
    '+14:00',
];
