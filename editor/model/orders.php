<?php
$orders = fopen('../' . DB_DIR_PATH . 'orders.csv', "rt") or die("Error Establishing a Database Connection...");

if($orders) {
    
    flock($orders, LOCK_EX); // блокируем файл
    
    for ($i = 0; $data = fgetcsv($orders, 0, ","); $i++) {
            
        $db_orders[] = [
            'order_number' => $data[0],
            'date' => $data[1],
            'first_name' => $data[2],
            'last_name' => $data[3],
            'phone' => $data[4],
            'email' => $data[5],
            'subscription' => $data[6],
            'region' => $data[7],
            'city' => $data[8],
            'address' => $data[9],
            'delivery' => $data[10],
            'payment' => $data[11],
            'total' => $data[12],
            'comment' => $data[13],
            'orders' => $data[14]
        ];
    }
    
    flock($orders, LOCK_UN); // снимаем блокировку
}

fclose($orders);

$db_orders = array_reverse($db_orders);
