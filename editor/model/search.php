<?php

// Основной файл конфигурации сайта
require_once '../../config.php';

$search_result = null;
$number_of_links = 50; // количесво элементов для вывода в результатах поиска

//Открытие списка категорий
$category = fopen('../../' . DB_DIR_PATH . $elang . '/pages.csv', "rt") or die("Error Establishing a Database Connection...");

if($category) {
    
    flock($category, LOCK_EX); // блокируем файл
    
    for ($i = 0; $data = fgetcsv($category, 0, ","); $i++) {

        if ($data[1] == 'category') {

            $db_category[] = [
                'category_id' => $data[0],
                'category_url' => $data[2]
            ];
        }
    }
    
    flock($category, LOCK_UN); // снимаем блокировку
    
}
fclose($category);

// Открытие БД
if ($page == 'pages') {
    
    $count_pages = 0;
    
    $pages_for_search = fopen('../../' . DB_DIR_PATH . $elang . '/pages.csv', "rt") or die("Error Establishing a Database Connection...");

    flock($pages_for_search, LOCK_EX); // блокируем файл
    
        for ($i = 0; $data = fgetcsv($pages_for_search, 0, ","); $i++) {
            
            if ($data[0] == '#') {
            
                $item_image_link = null;

                $page_id = $data[0];
                $page_seo_url = $data[2];
                $page_title = $data[3];
                $page_description = $data[5];
                $page_h1 = $data[6];

                $db_title = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($page_title)))), MB_CASE_LOWER, "UTF-8");
                $db_description = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($page_description)))), MB_CASE_LOWER, "UTF-8");
                $db_h1 = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($page_h1)))), MB_CASE_LOWER, "UTF-8");

                // Поиск подстроки в строке
                $pos_title = strpos($db_title, $search_request);
                $pos_description = strpos($db_description, $search_request);
                $pos_h1 = strpos($db_h1, $search_request);

                if ($pos_title !== false or $pos_description !== false or $pos_h1 !== false) {

                    $search_result[] = $item_image_link . '|*|' . $page_seo_url . '|*|' . $page_h1;

                    ++$count_pages;
                }

                if ($count_pages > $number_of_links) {
                    break;
                }
                
            }
        }
    
    flock($pages_for_search, LOCK_UN); // снимаем блокировку
    
    fclose($pages_for_search);
    
}
elseif ($page == 'categories') {
    
    $count_pages = 0;
    
    $pages_for_search = fopen('../../' . DB_DIR_PATH . $elang . '/pages.csv', "rt") or die("Error Establishing a Database Connection...");

    flock($pages_for_search, LOCK_EX); // блокируем файл
    
        for ($i = 0; $data = fgetcsv($pages_for_search, 0, ","); $i++) {
            
            if ($data[0] != '#') {
                
                $item_image_link = null;

                $page_id = $data[0];
                $page_seo_url = $data[2];
                $page_title = $data[3];
                $page_description = $data[5];
                $page_h1 = $data[6];

                $db_title = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($page_title)))), MB_CASE_LOWER, "UTF-8");
                $db_description = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($page_description)))), MB_CASE_LOWER, "UTF-8");
                $db_h1 = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($page_h1)))), MB_CASE_LOWER, "UTF-8");

                // Поиск подстроки в строке
                $pos_title = strpos($db_title, $search_request);
                $pos_description = strpos($db_description, $search_request);
                $pos_h1 = strpos($db_h1, $search_request);

                if ($pos_title !== false or $pos_description !== false or $pos_h1 !== false) {

                    $search_result[] = $item_image_link . '|*|' . $page_seo_url . '|*|' . $page_h1;

                    ++$count_pages;
                }

                if ($count_pages > $number_of_links) {
                    break;
                }
                
            }
        }
    
    flock($pages_for_search, LOCK_UN); // снимаем блокировку
    
    fclose($pages_for_search);
    
}
elseif (($page == 'items')) {
    
    $count_pages = 0;
    
    $pages_for_search = fopen('../../' . DB_DIR_PATH . $elang . '/items.csv', "rt") or die("Error Establishing a Database Connection...");

    flock($pages_for_search, LOCK_EX); // блокируем файл
    
        for ($i = 0; $data = fgetcsv($pages_for_search, 0, ","); $i++) {
            
            $page_id = $data[0];
            $page_seo_url = $data[2];
            $page_title = $data[3];
            $page_description = $data[5];
            $page_h1 = $data[6];
            $category_id = $data[7];
            $record_img_1x1 = $data[8];
            $item_short_description = $data[14];
            $item_sku = $data[17];
            
            foreach ($db_category as $key => $value) {
                
                if ($value['category_id'] == $category_id) {
                    
                    $item_image_link = $record_img_1x1;
                    break;
                }
                else {
                    $item_image_link = 'images/no_image.png';
                }
            }

            $db_title = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($page_title)))), MB_CASE_LOWER, "UTF-8");
            $db_description = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($page_description)))), MB_CASE_LOWER, "UTF-8");
            $db_h1 = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($page_h1)))), MB_CASE_LOWER, "UTF-8");
            $db_short_description = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($item_short_description)))), MB_CASE_LOWER, "UTF-8");
            $db_sku = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($item_sku)))), MB_CASE_LOWER, "UTF-8");
        
            // Поиск подстроки в строке
            $pos_title = strpos($db_title, $search_request);
            $pos_description = strpos($db_description, $search_request);
            $pos_h1 = strpos($db_h1, $search_request);
            $pos_short_description = strpos($db_short_description, $search_request);
            $pos_sku = strpos($db_sku, $search_request);

            if ($pos_title !== false or $pos_description !== false or $pos_h1 !== false or $pos_short_description !== false or $pos_sku !== false) {

                $search_result[] = $item_image_link . '|*|' . $page_seo_url . '|*|' . $page_h1;

                ++$count_pages;
            }

            if ($count_pages > $number_of_links) {
                break;
            }

        }
    
    flock($pages_for_search, LOCK_UN); // снимаем блокировку
    
    fclose($pages_for_search);
    
}
elseif (($page == 'orders')) {
    
    $count_pages = 0;
    
    $pages_for_search = fopen('../../' . DB_DIR_PATH . 'orders.csv', "rt") or die("Error Establishing a Database Connection...");

    flock($pages_for_search, LOCK_EX); // блокируем файл
    
        for ($i = 0; $data = fgetcsv($pages_for_search, 0, ","); $i++) {
            
            $item_image_link = null;

            $order_number = $data[0];
            $first_name = $data[2];
            $last_name = $data[3];
            $phone = $data[4];
            $email = $data[5];
            $total = $data[12];

            $db_order_number = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($order_number)))), MB_CASE_LOWER, "UTF-8");
            $db_first_name = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($first_name)))), MB_CASE_LOWER, "UTF-8");
            $db_last_name = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($last_name)))), MB_CASE_LOWER, "UTF-8");
            $db_phone = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($phone)))), MB_CASE_LOWER, "UTF-8");
            
            // Поиск подстроки в строке
            $pos_order_number = strpos($db_order_number, $search_request);
            $pos_first_name = strpos($db_first_name, $search_request);
            $pos_last_name = strpos($db_last_name, $search_request);
            $pos_phone = strpos($db_phone, $search_request);

            if ($pos_order_number !== false or $pos_first_name !== false or $pos_last_name !== false or $pos_phone !== false) {

                $search_result[] = $item_image_link . '|*|' . $order_number . '|*|' . '<b>#' . $order_number . '</b>: ' . $first_name . ' ' . $last_name . ' (' . $phone . ' | ' . $email . ' | ' . $total . ')';

                ++$count_pages;
            }

            if ($count_pages > $number_of_links) {
                break;
            }
            
        }
    
    flock($pages_for_search, LOCK_UN); // снимаем блокировку
    
    fclose($pages_for_search);
    
}
elseif (($page == 'blog')) {
    
    $count_pages = 0;
    
    $pages_for_search = fopen('../../' . DB_DIR_PATH . $elang . '/blog.csv', "rt") or die("Error Establishing a Database Connection...");

    flock($pages_for_search, LOCK_EX); // блокируем файл
    
        for ($i = 0; $data = fgetcsv($pages_for_search, 0, ","); $i++) {
            
            $item_image_link = null;
            
            $page_id = $data[0];
            $page_seo_url = $data[2];
            $page_title = $data[3];
            $page_description = $data[5];
            $page_h1 = $data[6];

            $db_title = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($page_title)))), MB_CASE_LOWER, "UTF-8");
            $db_description = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($page_description)))), MB_CASE_LOWER, "UTF-8");
            $db_h1 = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($page_h1)))), MB_CASE_LOWER, "UTF-8");

            // Поиск подстроки в строке
            $pos_title = strpos($db_title, $search_request);
            $pos_description = strpos($db_description, $search_request);
            $pos_h1 = strpos($db_h1, $search_request);

            if ($pos_title !== false or $pos_description !== false or $pos_h1 !== false) {

                $search_result[] = $item_image_link . '|*|' . $page_seo_url . '|*|' . $page_h1;

                ++$count_pages;
            }

            if ($count_pages > $number_of_links) {
                break;
            }

        }
    
    flock($pages_for_search, LOCK_UN); // снимаем блокировку
    
    fclose($pages_for_search);
}
