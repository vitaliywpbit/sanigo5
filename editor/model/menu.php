<?php

// ОСНОВНОЕ МЕНЮ
$menu = fopen("DB/$lang/menu.csv", "rt") or die("Unable to open file (menu.csv)...");

if($menu) {
    for ($i = 0; $data = fgetcsv($menu, 1000, ","); $i++) {
        $db_menu[] = [
            'menu_id' => $data[0], // ID
            'menu_item' => $data[1], // Пункт меню
            'menu_link' => $data[2] // Ссылка. Если ## - открытие подменю
        ];
    }
}

fclose($menu);

// ПОДМЕНЮ 1
$submenu_01 = fopen("DB/$lang/submenu_01.csv", "rt") or die("Unable to open file (submenu_01.csv)...");

if($submenu_01) {
    for ($i = 0; $data = fgetcsv($submenu_01, 1000, ","); $i++) {
        $db_submenu_01[] = [
            'submenu_01_id' => $data[0], // ID
            'submenu_01_menu_id' => $data[1], // ID главного меню
            'submenu_01_item' => $data[2], // Пункт меню
            'submenu_01_link' => $data[3] // Ссылка
        ];
    }
}

fclose($submenu_01);
