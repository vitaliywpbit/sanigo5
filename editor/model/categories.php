<?php
$pages = fopen('../' . DB_DIR_PATH . $elang . '/pages.csv', "rt") or die("Error Establishing a Database Connection...");

if($pages) {
    
    flock($pages, LOCK_EX); // блокируем файл
    
    for ($i = 0; $data = fgetcsv($pages, 0, ","); $i++) {
        
        if ($data[1] == 'category') {
            
            $db_categories[] = [
                'id' => $data[0],
                'page_layout' => $data[1],
                'url' => $data[2],
                'title' => $data[3],
                'keywords' => $data[4],
                'description' => $data[5],
                'h1' => $data[6]
            ];
            
        }
    }
    
    flock($pages, LOCK_UN); // снимаем блокировку
}

fclose($pages);
