<?php include 'model/menu.php'; ?>

<ul class="navbar-nav mr-auto">
    <?php 
    if (isset($_SESSION['loggedIn']) == true) {
        $count_for_id = 0;
    
        foreach ($db_menu as $line => $column) {
            if($column['menu_link'] != '##' or $column['menu_link'] == null) {

                // Выделение активной ссылке в меню
                if($page == $column['menu_link']) {
                    $menu_active = 'active';
                }
                else {
                    $menu_active = 'no-active';
                }
                // Конец кода для активной ссылке ?>

               <li class="nav-item <?php echo $menu_active; ?>">
                    <a class="nav-link" href="editor/index.php?page=<?php echo $column['menu_link']; ?>&lang=<?php echo $lang; ?>"><?php echo $column['menu_item']; ?></a>
               </li>
        <?php }
            elseif ($column['menu_link'] == '##') {
            ++$count_for_id; ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="Submenu<?php echo $count_for_id; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $column['menu_item']; ?></a>
                    <div class="dropdown-menu" aria-labelledby="Submenu<?php echo $count_for_id; ?>">
                        <?php
                            foreach ($db_submenu_01 as $line_submenu_01 => $column_submenu_01) {
                                if($column_submenu_01['submenu_01_menu_id'] == $column['menu_id']) {
                                    // Выделение активной ссылке в подменю
                                    if($page == $column_submenu_01['submenu_01_link']) {
                                            $menu_active = 'active';
                                        }
                                        else {
                                            $menu_active = 'no-active';
                                        } ?>
                                    <a class="dropdown-item <?php echo $menu_active; ?>" href="editor/index.php?page=<?php echo $column_submenu_01['submenu_01_link']; ?>&lang=<?php echo $lang; ?>">
                                        <?php echo $column_submenu_01['submenu_01_item']; ?>
                                    </a>
                        <?php } } ?>
                    </div>
                </li>
      <?php }
        } 
    } ?>
</ul>

<ul class="navbar-nav my-2 my-lg-0">
    <li class="nav-item">
        <span class="authorization-info">
            <i><?php if (isset($_SESSION['loggedIn']) == true) { ?>
                <form id="logout" action="" method="post">
                    <input type="hidden" name="action" value="logout">
                    <input type="hidden" name="lang" value="<?php echo $lang; ?>">
                </form>
                <button type="submit" form="logout" class="btn btn-danger"><?php echo $log_out; ?></button>
                <?php } 
                else {
                     echo $not_log_in;
                } ?></i>
        </span>
    </li>
</ul>
