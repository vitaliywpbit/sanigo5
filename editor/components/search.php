<?php
//Совместимость с Bootstrap 4.x.x
include 'language/' . $lang . '/search.php'; //Загрузка переменных на нужном языке
?>

<div class="dropdown">
    <form class="form-inline justify-content-end">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text rounded-pill">
                    <svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M12.442 12.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z" clip-rule="evenodd"/>
                        <path fill-rule="evenodd" d="M8.5 14a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM15 8.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z" clip-rule="evenodd"/>
                    </svg>
                </div>
            </div>
            <input type="text" value="" class="form-control search_input rounded-pill" id="tourSearch" placeholder="<?php echo $search_placeholder; ?>" aria-label="Search form" autocomplete="off">
        </div>
    </form>
    <div id="search_result" aria-labelledby="dropdownMenuButton"></div>
</div>

<script>
$ (document).ready(function() {
    
    //Живой поиск
    $('.search_input').on("change keyup input click", function() {
        if(this.value.length >= 2){
            $.ajax({
                type: 'POST',
                url: "editor/modules/search.php", //Путь к обработчику
                data: { page: '<?php echo $page; ?>', lang: '<?php echo $lang; ?>', elang: '<?php echo $elang; ?>', search_request: this.value },
                success: function(data){
                    $("#search_result").html(data).fadeIn(); //Выводим полученые данные в списке
               }
           })
        }
        else {
            $("#search_result").fadeOut();
        }
    })

    $("#search_result").hover(function(){
        $(".search_input").blur(); //Убираем фокус с input
    })

    //При выборе результата поиска, прячем список и заносим выбранный результат в input
    $("#search_result").on("click", "a", function(){
        s_user = $(this).text();
        //$(".search_input").val(s_user).attr('disabled', 'disabled'); //деактивируем input, если нужно
        $("#search_result").fadeOut();
    })
    
    $(document).on('click', function(e) { // событие клика по веб-документу
        $("#search_result").fadeOut();
    })
    
})
</script>
