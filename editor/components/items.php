<?php
// Работает на Bootstrap 4.x.x
// ===========================

// Подключение к БД
include 'model/items.php';

// Pagination function
// ====================
function pagePagination($page, $lang, $elang, $page_num, $how_many_pages, $how_many_links, $how_many_links_r_l) {

    if ($how_many_pages > 1) {
        echo '<nav aria-label="Page navigation"><ul class="pagination pagination-sm justify-content-start">'; // стили для блока ссылок по страницам
        if ($how_many_pages <= $how_many_links) {
            for ($i = 1; $i <= $how_many_pages; $i++) {
                if ($i == 1) {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang'>$i</a>
                            </li>";
                    }
                }
                else {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                    }
                }
            }
        }
        elseif ($how_many_pages > $how_many_links && $page_num < $how_many_links) {
            for ($i = 1; $i <= $how_many_pages; $i++) {
                if ($i == 1) {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang'>$i</a>
                            </li>";
                    }
                }
                else {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                    }
                }
                if ($i == $how_many_links) {
                    break;
                }
            }
            echo "<li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>
                   <li class='page-item'>
                        <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$how_many_pages'>$how_many_pages</a>
                   </li>";
        }
        elseif ($how_many_pages > $how_many_links && $page_num >= $how_many_links && $how_many_pages >= $page_num + $how_many_links_r_l) {
            $new_i = $page_num - $how_many_links_r_l;
            $count_page_num = 1;
            $end_of_links = false;
            echo "<li class='page-item'>
                        <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang'>1</a>
                   </li>
                    <li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>";
            for ($i = $new_i; $i <= $how_many_pages; $i++) {
                if ($i == $page_num) {
                    echo "<li class='page-item active'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                }
                else {
                     echo "<li class='page-item'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                }
                if ($i == $how_many_pages) {
                    $end_of_links = true;
                }
                if ($count_page_num == $how_many_links) {
                    break;
                }
                ++$count_page_num;
            }
            if (!$end_of_links) {
                echo "<li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>
                   <li class='page-item'>
                        <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$how_many_pages'>$how_many_pages</a>
                   </li>";
            }
        }
        elseif ($how_many_pages > $how_many_links && $page_num >= $how_many_links && $how_many_pages < $page_num + $how_many_links_r_l) {
            $new_i = $how_many_pages - ($how_many_links - 1);
            $count_page_num = 1;
            echo "<li class='page-item'>
                        <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang'>1</a>
                   </li>
                    <li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>";
            for ($i = $new_i; $i <= $how_many_pages; $i++) {
                if ($i == $page_num) {
                   echo "<li class='page-item active'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                }
                else {
                     echo "<li class='page-item'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                }
                if ($i == $how_many_pages) {
                    $end_of_links = true;
                }
                if ($count_page_num == $how_many_links) {
                    break;
                }
                ++$count_page_num;
            }
        }
        echo '</ul></nav>'; // стили для блока ссылок по страницам
    }
}
// End Pagination function
// ========================

if ($db_items) {
    
    $show_records = 25; // Сколько записей на 1 странице показывать
    $how_many_records = 0; // сколько всего записей по умолчанию
    $how_many_pages = 1; // сколько страниц с записями по умолчанию
    $how_many_links = 5; // Сколько ссылок на страницы в одном блоке
    $how_many_links_r_l = floor($how_many_links / 2); //Сколько ссылок слева от активного номера страницы
    if(isset($db_items)) {
        $how_many_records = count($db_items); // сколько всего записей
        $how_many_pages = ceil($how_many_records / $show_records); // сколько страниц с записями
    } ?>

    <div class="row">
        <div class="col-md-8 mt-3">
            <?php pagePagination($page, $lang, $elang, $page_num, $how_many_pages, $how_many_links, $how_many_links_r_l); ?>
        </div>
        <div class="col-md-4 mt-3">
            <a href="editor/index.php?page=items&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>&epage=newitem" role="button" class="btn btn-sm btn-outline-info btn-block">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                    <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                </svg>
                <?php echo $add_new_item_text; ?>
            </a>
        </div>
    </div>

    <br>
    
    <?php
    // Показ списка статей, товаров, новостей или записей
    // ====================================================
    $start_show_num = $show_records * ($page_num - 1);
    $count_record_by_category = 0;
    $stop_count_show_record = 0;

        foreach ($db_items as $line => $column) { 
            
            if($count_record_by_category >= $start_show_num) {
                
                //Код проверки наличия изображения для записи
                if(isset($column['record_img_1x1'])) {

                    $record_image_check = trim($column['record_img_1x1']);

                    if($record_image_check != null && file_exists('../' . $record_image_check)) {
                        $record_image = $record_image_check;
                    }
                    else {
                        $record_image = 'images/no_image.png';
                    }
                }
                else {
                    $record_image = 'images/no_image.png';
                }
            ?>
                
            <div id="resultsDelete<?php echo $column['id']; ?>">
                
                <div class="row align-items-center border p-2 mt-4">
                    <div class="col-2 col-sm-2 col-md-1 mb-1">
                        <img src="<?php echo $record_image; ?>" class="img-fluid" alt="<?php echo $column['record_img_alt']; ?>">
                    </div>

                    <div class="col-10 col-sm-10 col-md-8 mb-1">
                        <p class="mb-0">
                            <?php echo $column['title']; ?>
                        </p>
                        <p class="mb-0 text-muted small">
                            <?php
                            if ($column['item_availability'] == 'InStock') { ?>
                                <span class="text-success border-right border-dark pl-1 pr-1"><b><?php echo $instock_text; ?></b></span>
                            <?php }
                            elseif ($column['item_availability'] == 'OutOfStock') { ?>
                                <span class="text-danger border-right border-dark pl-1 pr-1"><b><?php echo $outofstock_text; ?></b></span>
                            <?php }
                            elseif ($column['item_availability'] == 'PreOrder') { ?>
                                <span class="text-warning border-right border-dark pl-1 pr-1"><b><?php echo $preorder_text; ?></b></span>
                            <?php }
                            else { ?>
                                <span class="text-muted border-right border-dark pl-1 pr-1"><b><?php echo $column['item_availability']; ?></b></span>
                            <?php }

                            if ($column['item_new_price'] && $column['item_new_price'] != "") { ?>
                                <span class="border-right border-dark pl-1 pr-1"><b><?php echo $price_text; ?></b> <?php echo '<span class="text-danger">' . $column['item_new_price'] . '</span> <s>' . $column['item_price'] . '</s>'; ?></span>
                            <?php }
                            else { ?>
                                <span class="border-right border-dark pl-1 pr-1"><b><?php echo $price_text; ?></b> <?php echo $column['item_price']; ?></span>
                            <?php } ?>
                            
                            <span class="pl-1 pr-1"><b><?php echo $sku_text; ?></b> <?php echo $column['item_sku']; ?></span>
                        </p>
                    </div>

                    <div class="col-12 col-sm-12 col-md-3 mb-1 text-right">
                        
                        <ul class="nav justify-content-end">
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#" id="itemClone<?php echo $column['id']; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="<?php echo $clone_text; ?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-files" viewBox="0 0 16 16">
                                        <path d="M13 0H6a2 2 0 0 0-2 2 2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2 2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zm0 13V4a2 2 0 0 0-2-2H5a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1zM3 4a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4z"/>
                                    </svg>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right text-center border-info p-3" aria-labelledby="itemClone<?php echo $column['id']; ?>">

                                    <p><?php echo $clone_item_yes_no_text; ?></p>

                                    <button type="submit" class="btn btn-outline-info text-uppercase" onclick="cloneItem('<?php echo $column['id']; ?>', '<?php echo $column['category_id']; ?>', '<?php echo $lang; ?>', '<?php echo $elang; ?>');">
                                        <?php echo $clone_text; ?>
                                    </button>
                                    
                                </div>
                            </li>

                            <?php
                            if ($page_num == 1) { ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="editor/index.php?page=<?php echo $page; ?>&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>&epage=<?php echo $column['url']; ?>" title="<?php echo $edit; ?>">
                                        <svg width="24" height="24" viewBox="0 0 16 16" class="bi bi-gear-fill" fill="#175496" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 0 0-5.86 2.929 2.929 0 0 0 0 5.858z"/>
                                        </svg>
                                    </a>
                                </li>
                            <?php }
                            else { ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="editor/index.php?page=<?php echo $page; ?>&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>&epage=<?php echo $column['url']; ?>&p=<?php echo $page_num; ?>" title="<?php echo $edit; ?>">
                                        <svg width="24" height="24" viewBox="0 0 16 16" class="bi bi-gear-fill" fill="#175496" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 0 0-5.86 2.929 2.929 0 0 0 0 5.858z"/>
                                        </svg>
                                    </a>
                                </li>
                            <?php } ?>                            

                            <li class="nav-item dropdown">
                                <a class="nav-link disabled--" href="#" id="itemDelete<?php echo $column['id']; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="<?php echo $delete; ?>">
                                    <svg width="24" height="24" viewBox="0 0 16 16" class="bi bi-trash-fill" fill="#ff0000" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                                    </svg>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right text-center border-danger p-3" aria-labelledby="itemDelete<?php echo $column['id']; ?>">

                                    <p><?php echo $delete_item_yes_no_text; ?></p>
                                    <button type="button" class="btn btn-outline-dark text-uppercase" onclick="deleteItem('<?php echo $column['id']; ?>', '<?php echo $column['category_id']; ?>', '<?php echo $lang; ?>', '<?php echo $elang; ?>');">
                                        <?php echo $delete; ?>
                                    </button>
                                    
                                </div>
                            </li>
                            
                        </ul>

                    </div>
                </div>
                    
            </div>
    
        <?php 
        $stop_count_show_record++;

        if($stop_count_show_record == $show_records) {
            break;
            }
        }
        $count_record_by_category++;
    }
}
else { ?>
    
    <div class="row">
        <div class="col-md-8 mt-3"></div>
        <div class="col-md-4 mt-3">
            <a href="editor/index.php?page=items&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>&epage=newitem" role="button" class="btn btn-sm btn-outline-info btn-block">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                    <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                </svg>
                <?php echo $add_new_item_text; ?>
            </a>
        </div>
    </div>
    
<?php }

echo '<br><br>';

pagePagination($page, $lang, $elang, $page_num, $how_many_pages, $how_many_links, $how_many_links_r_l);

?>

<script>
    function cloneItem(ID, category_id, lang, elang) {
        
        $.ajax({
            
            type: 'POST',
            url: 'editor/modules/clone_item.php',
            data: {'id': ID, 'category_id': category_id, 'lang': lang, 'elang': elang},
          
        }).done(function(html) {

            $('body').prepend(html);

        });

    }
    
    function deleteItem(ID, category_id, lang, elang) {
        
        $.ajax({
            
            type: 'POST',
            url: 'editor/modules/delete_item.php',
            data: {'id': ID, 'category_id': category_id, 'lang': lang, 'elang': elang},
          
        }).done(function(html) {

            $('body').prepend(html);

        });

    }
</script>
