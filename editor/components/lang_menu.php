<?php
//Работает на Bootstrap 4
//БД с языками подключена в файле index.php

foreach ($db_lang as $line => $column) {
    
    // Какой язык Активен
    if ($lang == $column['lang_code']) {
        
        $active_lang = 'active';
        $link_disabled = 'disabled';
    }
    else {
        $active_lang = null;
        $link_disabled = null;
    } ?>
    <a class="dropdown-item <?php echo $active_lang; ?>"
       href="editor/index.php?lang=<?php echo  $column['lang_code'] ?>"><?php echo $column['lang_name']; ?></a>
<?php }
