<?php
include 'model/lang_site.php';

//Какой язык по умолчанию
foreach ($db_lang_site as $line => $column) {
    
    if ($column['lang_site_default'] == 'd') {
        
        $lang_site_default = $column['lang_site_code'];
        $lang_site_name = $column['lang_site_name'];
        
        break;
    }
}
