<?php
// Работает на Bootstrap 4.x.x
// ===========================

// Подключение к БД
include 'model/orders.php';

// Pagination function
// ====================
function pagePagination($page, $lang, $elang, $page_num, $how_many_pages, $how_many_links, $how_many_links_r_l) {

    if ($how_many_pages > 1) {
        echo '<nav aria-label="Page navigation"><ul class="pagination pagination-sm justify-content-start">'; // стили для блока ссылок по страницам
        if ($how_many_pages <= $how_many_links) {
            for ($i = 1; $i <= $how_many_pages; $i++) {
                if ($i == 1) {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang'>$i</a>
                            </li>";
                    }
                }
                else {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                    }
                }
            }
        }
        elseif ($how_many_pages > $how_many_links && $page_num < $how_many_links) {
            for ($i = 1; $i <= $how_many_pages; $i++) {
                if ($i == 1) {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang'>$i</a>
                            </li>";
                    }
                }
                else {
                    if ($i == $page_num) {
                        echo "<li class='page-item active'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                    }
                    else {
                        echo "<li class='page-item'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                    }
                }
                if ($i == $how_many_links) {
                    break;
                }
            }
            echo "<li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>
                   <li class='page-item'>
                        <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$how_many_pages'>$how_many_pages</a>
                   </li>";
        }
        elseif ($how_many_pages > $how_many_links && $page_num >= $how_many_links && $how_many_pages >= $page_num + $how_many_links_r_l) {
            $new_i = $page_num - $how_many_links_r_l;
            $count_page_num = 1;
            $end_of_links = false;
            echo "<li class='page-item'>
                        <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang'>1</a>
                   </li>
                    <li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>";
            for ($i = $new_i; $i <= $how_many_pages; $i++) {
                if ($i == $page_num) {
                    echo "<li class='page-item active'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                }
                else {
                     echo "<li class='page-item'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                }
                if ($i == $how_many_pages) {
                    $end_of_links = true;
                }
                if ($count_page_num == $how_many_links) {
                    break;
                }
                ++$count_page_num;
            }
            if (!$end_of_links) {
                echo "<li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>
                   <li class='page-item'>
                        <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$how_many_pages'>$how_many_pages</a>
                   </li>";
            }
        }
        elseif ($how_many_pages > $how_many_links && $page_num >= $how_many_links && $how_many_pages < $page_num + $how_many_links_r_l) {
            $new_i = $how_many_pages - ($how_many_links - 1);
            $count_page_num = 1;
            echo "<li class='page-item'>
                        <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang'>1</a>
                   </li>
                    <li class='page-item disabled'>
                      <span class='page-link'>...</span>
                   </li>";
            for ($i = $new_i; $i <= $how_many_pages; $i++) {
                if ($i == $page_num) {
                   echo "<li class='page-item active'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                }
                else {
                     echo "<li class='page-item'>
                                <a class='page-link' href='editor/index.php?page=$page&lang=$lang&elang=$elang&p=$i'>$i</a>
                            </li>";
                }
                if ($i == $how_many_pages) {
                    $end_of_links = true;
                }
                if ($count_page_num == $how_many_links) {
                    break;
                }
                ++$count_page_num;
            }
        }
        echo '</ul></nav>'; // стили для блока ссылок по страницам
    }
}
// End Pagination function
// ========================

if ($db_orders) {
    
    $show_records = 50; // Сколько записей на 1 странице показывать
    $how_many_records = 0; // сколько всего записей по умолчанию
    $how_many_pages = 1; // сколько страниц с записями по умолчанию
    $how_many_links = 5; // Сколько ссылок на страницы в одном блоке
    $how_many_links_r_l = floor($how_many_links / 2); //Сколько ссылок слева от активного номера страницы
    if(isset($db_orders)) {
        $how_many_records = count($db_orders); // сколько всего записей
        $how_many_pages = ceil($how_many_records / $show_records); // сколько страниц с записями
    } ?>

    <div class="row">
        <div class="col-md-12 mt-3">
            <?php pagePagination($page, $lang, $elang, $page_num, $how_many_pages, $how_many_links, $how_many_links_r_l); ?>
        </div>
    </div>

    <br>
    
    <?php
    // Показ списка статей, товаров, новостей или записей
    // ====================================================
    $start_show_num = $show_records * ($page_num - 1);
    $count_record_by_category = 0;
    $stop_count_show_record = 0;

        foreach ($db_orders as $line => $column) { 
            
            if($count_record_by_category >= $start_show_num) { ?>
                
            <div class="row border p-2 mt-4">

                <div class="col-2 col-sm-2 col-md-1 mb-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="#039394" class="bi bi-bag-check" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                        <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z"/>
                    </svg>
                </div>

                <div class="col-10 col-sm-7 col-md-8 mb-1">
                    <p class="mb-0">
                        <?php echo $column['date'] . ' | ' . $order_text . $column['order_number']; ?>
                    </p>
                    <p class="mb-0 text-muted small">
                        <?php echo '<b>' . $customer_text . '</b>' . $column['first_name'] . ' ' . $column['last_name'] . ' | '; ?>
                        <?php echo '<b>' . $total_text . '</b>' . $column['total'] . $currency; ?>
                    </p>
                </div>

                <div class="col-12 col-sm-3 col-md-3 mb-1 text-right">
                    
                    <ul class="nav justify-content-end">

                        <?php
                        if ($page_num == 1) { ?>
                            <li class="nav-item">
                                <a class="nav-link" href="editor/index.php?page=<?php echo $page; ?>&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>&epage=<?php echo $column['order_number']; ?>" title="<?php echo $edit; ?>">
                                    <svg width="24" height="24" viewBox="0 0 16 16" class="bi bi-gear-fill" fill="#175496" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 0 0-5.86 2.929 2.929 0 0 0 0 5.858z"/>
                                    </svg>
                                </a>
                            </li>
                        <?php }
                        else { ?>
                            <li class="nav-item">
                                <a class="nav-link" href="editor/index.php?page=<?php echo $page; ?>&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>&epage=<?php echo $column['order_number']; ?>&p=<?php echo $page_num; ?>" title="<?php echo $edit; ?>">
                                    <svg width="24" height="24" viewBox="0 0 16 16" class="bi bi-gear-fill" fill="#175496" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 0 0-5.86 2.929 2.929 0 0 0 0 5.858z"/>
                                    </svg>
                                </a>
                            </li>
                        <?php } ?>
                        
                    </ul>

                </div>
            </div>
    
            <?php 
            $stop_count_show_record++;

            if($stop_count_show_record == $show_records) {
                break;
            }
        }
        $count_record_by_category++;
    }
}

echo '<br><br>';

pagePagination($page, $lang, $elang, $page_num, $how_many_pages, $how_many_links, $how_many_links_r_l);
