<?php
include 'model/lang.php';

// Какой язык по умолчанию
foreach ($db_lang as $line => $column) {
    
    if ($column['lang_default'] == 'd') {
        
        $lang_default = $column['lang_code'];
        
        break;
    }
}
