<!-- Модальное окно для загрузки изображений -->
<div class="modal fade" id="filemanagerModal" tabindex="-1" aria-labelledby="filemanagerModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header bg-blue text-white">
                <p class="modal-title h-5" id="filemanagerModalLabel">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-images mr-2" viewBox="0 0 16 16">
                        <path d="M4.502 9a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"/>
                        <path d="M14.002 13a2 2 0 0 1-2 2h-10a2 2 0 0 1-2-2V5A2 2 0 0 1 2 3a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2v8a2 2 0 0 1-1.998 2zM14 2H4a1 1 0 0 0-1 1h9.002a2 2 0 0 1 2 2v7A1 1 0 0 0 15 11V3a1 1 0 0 0-1-1zM2.002 4a1 1 0 0 0-1 1v8l2.646-2.354a.5.5 0 0 1 .63-.062l2.66 1.773 3.71-3.71a.5.5 0 0 1 .577-.094l1.777 1.947V5a1 1 0 0 0-1-1h-10z"/>
                    </svg>
                    <?php echo $image_manager_text; ?>
                </p>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <div id="filemanagerResault"></div>
                
            </div>
            <div class="modal-footer justify-content-center">
                
                <div id="fileSaveDeleteResault"></div>
                
                <div class="row">
                    
                    <div class="col-md-6 mt-2">
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" id="sortImage" class="custom-file-input" id="customFileLangHTML">
                                <label class="custom-file-label" for="customFileLangHTML" data-browse="..."><?php echo $select_file_text; ?></label>
                            </div>
                            <div class="input-group-append">
                                <button id="uploadImages" class="btn btn-outline-secondary" type="button">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-upload" viewBox="0 0 16 16">
                                        <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                                        <path d="M7.646 1.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 2.707V11.5a.5.5 0 0 1-1 0V2.707L5.354 4.854a.5.5 0 1 1-.708-.708l3-3z"/>
                                    </svg>
                                    <?php ## echo $upload_image_text; ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 mt-2">
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo $back_to_editing_text; ?></button>
                    </div>
                
                </div>
                
            </div>
        </div>
    </div>
</div>

<script>
    function workFileManager(ID, type, folder) {
        $('#filemanagerResault').load('editor/modules/filemanager.php', 
            { 'id': ID, 'type': type, 'folder': folder } );
    }
    
    function deleteImages(ID, img_path) {
        $('#fileSaveDeleteResault').load('editor/modules/delete_images.php', 
            { 'id': ID, 'img_path': img_path } );
    }

    function putFileLink(ID, link) {

        $("#mainImage" + ID).attr("src", link);
        $("#mainImageInput" + ID).attr("value", link);

        $('#filemanagerModal').modal('hide');

    }

    $('#uploadImages').on('click', function() {
        
        var server_dir = '<?php echo SERVER_DIR; ?>';
        var path = $('#pathToFile').text();
        var id = $('#idFileToPut').text();
        
        var file_data = $('#sortImage').prop('files')[0];
        var form_data = new FormData();
        form_data.append('server_dir', server_dir);
        form_data.append('file', file_data);
        form_data.append('path', path);
        form_data.append('id', id);
        // alert(form_data);
        $.ajax({
            type: 'POST',
            url: 'editor/modules/upload_images.php',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data
        }).done(function( html ) {
            $("#imagePrepend").prepend(html);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert('Error file upload');
        });
    });
    
    $(document).ready(function () {
        bsCustomFileInput.init(); 
    });
</script>
