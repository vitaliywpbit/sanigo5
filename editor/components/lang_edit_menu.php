<?php
// Работает на Bootstrap 4
// БД подключена в шаблоне страницы

foreach ($db_lang_site as $line => $column) {
    
    // Какой язык Активен
    if ($elang == $column['lang_site_code']) {
        $active_lang_site = 'active';
    }
    else {
        $active_lang_site = null;
    }
    
    // Выбор языка для всего списка страниц или одной редактируемой
    if ($epage) {
        if ($page_num == 1) { ?>
            <a class="dropdown-item <?php echo $active_lang_site; ?>"
                href="editor/index.php?page=<?php echo $page; ?>&lang=<?php echo $lang; ?>&elang=<?php echo  $column['lang_site_code'] ?>&epage=<?php echo $epage; ?>"><?php echo $column['lang_site_name']; ?></a>
  <?php }
        else { ?>
            <a class="dropdown-item <?php echo $active_lang_site; ?>"
                href="editor/index.php?page=<?php echo $page; ?>&lang=<?php echo $lang; ?>&elang=<?php echo  $column['lang_site_code'] ?>&epage=<?php echo $epage; ?>&p=<?php echo $page_num; ?>"><?php echo $column['lang_site_name']; ?></a>
  <?php }
    }
    else {
        if ($page_num == 1) { ?>
            <a class="dropdown-item <?php echo $active_lang_site; ?>"
                href="editor/index.php?page=<?php echo $page; ?>&lang=<?php echo $lang; ?>&elang=<?php echo  $column['lang_site_code'] ?>"><?php echo $column['lang_site_name']; ?></a>
  <?php }
        else { ?>
            <a class="dropdown-item <?php echo $active_lang_site; ?>"
                href="editor/index.php?page=<?php echo $page; ?>&lang=<?php echo $lang; ?>&elang=<?php echo  $column['lang_site_code'] ?>&p=<?php echo $page_num; ?>"><?php echo $column['lang_site_name']; ?></a>
  <?php }
    }
}
