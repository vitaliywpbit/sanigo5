<?php

include_once '../../config.php';

if (isset($_FILES['file'])) {

    if ($_FILES['file']['error'] > 0) {

        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else {

        if (isset($_POST['path'])) {
            
            $path = $_POST['path'];
            
            if (isset($_POST['server_dir'])) {
                $server_path = $_POST['server_dir'] . 'images/' . $path;
            }
            else {
                $server_path = null;
            }
            
        }
        else {
            $path = null;
            $server_path = null;
        }
        
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
        }
        else {
            $id = null;
        }
        
        $file_name = $_FILES['file']['name'];
        $file_path = SERVER_DIR . 'images/'. $path . $file_name;

        if (!file_exists($file_path)) {
        
            move_uploaded_file($_FILES['file']['tmp_name'], $file_path); ?>

            <div class="col-6 col-sm-4 col-md-3 text-center mt-3 position-relative" data-file-name="<?php echo $file_name; ?>">
                <div class="dropdown position-absolute mt-n2 ml-n2">
                    <a class="btn btn-sm btn-danger rounded" role="button" id="dropdownDeleteImage<?php echo $file_name; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#ffffff" class="bi bi-x" viewBox="0 0 16 16">
                            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                        </svg>
                    </a>
                    <div class="dropdown-menu border-danger" aria-labelledby="dropdownDeleteImage<?php echo $file_name; ?>">
                        <a class="dropdown-item" role="button" onclick="deleteImages('<?php echo $file_name; ?>', '<?php echo $server_path . $file_name; ?>')">
                            Удалить
                        </a>
                    </div>
                </div>

                <a role="button" onclick="putFileLink('<?php echo $id; ?>', 'images/<?php echo $path . $file_name; ?>');">
                    <img src="images/<?php echo $path . $file_name; ?>" loading="lazy" class="img-fluid">
                </a>
                <br>
                <span class="small text-break"><b><?php echo $file_name; ?></b></span>
            </div>

            <script>alert("Файл успешно загружен");</script>
            
        <?php
        }
        else {
            echo '<script>alert("Такой файл уже существует");</script>';
        }  
    }
}
else {
    echo '<script>alert("Вы не выбрали файл");</script>';
}
