<?php

if (isset($_POST['id']) && isset($_POST['img_path'])) {
    
    // Удаление файла изображения
    $image_file = $_POST['img_path'];

    if (file_exists($image_file)) {
        
        unlink($image_file);
        
        echo '<script>alert("Файл успешно удален"); var deletefile = $(\'div[data-file-name|="' . $_POST['id'] . '"]\'); deletefile.remove();</script>';
    }
    else {
        echo '<script>alert("Такого файла не существует");</script>';
    }
}
else {
    echo '<script>alert("Ошибка при удалении");</script>';
}

?>
