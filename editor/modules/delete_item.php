<?php

if (isset($_POST['id']) && isset($_POST['category_id']) && isset($_POST['lang']) && isset($_POST['elang'])) {
    
    include_once '../controllers/items.php';
    
    if (itemDelete($_POST['id'], $_POST['category_id'], $_POST['lang'], $_POST['elang'])) {
        
        // Перезагрузка страницы с товарами
        echo '<script>location.reload();</script>';
    }
    else {
        
        // Перезагрузка страницы с товарами
        echo '<script>location.reload();</script>';
    }
}
else {
    
    // Перезагрузка страницы с товарами
    echo '<script>location.reload();</script>';
}
