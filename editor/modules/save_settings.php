<?php

include_once '../controllers/settings.php'; // подключение функций для работы с Настройками сайта

if (isset($_POST['http_protocol']) && 
    isset($_POST['default_page']) && 
    isset($_POST['email']) && 
    isset($_POST['og_image']) && 
    isset($_POST['time_zone']) && 
    isset($_POST['domain'])) {
    
    if (isset($_POST['gtag'])) {
        $gtag = $_POST['gtag'];
    }
    else {
        $gtag = null;
    }
    
    if (isset($_POST['lang'])) {
        $lang = $_POST['lang'];
    }
    else {
        $lang = 'ru';
    }
    
    include '../language/' . $lang . '/all_content.php'; // Загрузка переменных на нужном языке
    
    // Функция сохранения страницы
    if (settingsSave($_POST['http_protocol'], 
            $_POST['domain'], 
            $_POST['default_page'], 
            $_POST['email'], 
            $_POST['og_image'],
            $_POST['time_zone'],
            $gtag,
        )) { ?>
        
        <div class="text-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="green" class="bi bi-check-circle-fill" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
            </svg>
            <p class="mt-3"><?php echo $saved_text; ?></p>
        </div>

    <?php }
    else { ?>

        <div class="text-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="red" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
            </svg>
            <p class="mt-3"><?php echo $save_error_text; ?></p>
        </div>
        <div class="alert alert-danger" role="alert">
            <?php echo $global_save_page_err; ?>
        </div>
        
<?php }
    
}
