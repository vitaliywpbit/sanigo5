<?php

// Подключение к БД
include 'model/items.php';

if(isset($_GET['epage'])) {
    
    $page_available = false;
    $new_item = false;
    
    if($_GET['epage'] == 'newitem') {
        
        $page_available = true;
        $new_item = true;
        $save_module = 'editor/modules/add_item.php';

        $page_id = $final_id + 1;
        $page_page_layout = 'catalog';
        $page_url = 'new-item';
        $page_title = '';
        $page_keywords = '';
        $page_description = '';
        $page_h1 = '';
        $category_id = '';
        $record_img_1x1 = 'images/no_image.png';
        $record_img_4x3 = 'images/no_image.png';
        $record_img_16x9 = 'images/no_image.png';
        $record_img_preview = 'images/no_image.png';
        $record_img_alt = '';
        $record_all_images = '';
        $item_short_description = '';
        $item_price = '0.00';
        $item_new_price = '0';
        $item_sku = '';
        $item_availability = 'InStock';
        $item_brand_name = 'Brand';
        
    }
    else {
        
        foreach ($db_items as $line => $column) {

            if($_GET['epage'] == $column['url']) {

                $page_available = true;
                $save_module = 'editor/modules/save_item.php';

                $page_id = $column['id'];
                $page_page_layout = $column['page_layout'];
                $page_url = $column['url'];
                $page_title = $column['title'];
                $page_keywords = $column['keywords'];
                $page_description = $column['description'];
                $page_h1 = $column['h1'];
                $category_id = $column['category_id'];
                $record_img_1x1 = $column['record_img_1x1'];
                $record_img_4x3 = $column['record_img_4x3'];
                $record_img_16x9 = $column['record_img_16x9'];
                $record_img_preview = $column['record_img_preview'];
                $record_img_alt = $column['record_img_alt'];
                $record_all_images = $column['record_all_images'];
                $item_short_description = $column['item_short_description'];
                $item_price = $column['item_price'];
                $item_new_price = $column['item_new_price'];
                $item_sku = $column['item_sku'];
                $item_availability = $column['item_availability'];
                $item_brand_name = $column['item_brand_name'];
                break;
            }
        }
    }
}

if ($page_available) {

    if (!$new_item) {
        // Блок загрузки контента для товара
        $category = getCategory($category_id, $elang);

        $path_to_content = '../' . DB_DIR_PATH . $elang . '/catalog/'. $category['url'] . '/' . $page_url . '.tpl';

        if (file_exists($path_to_content)) {
            $content = file_get_contents($path_to_content);
        }
         else {
            $content = 'no data...';
        }
    }
    else {
        
        $category['id'] = null;
        $category['url'] = null;
        $category['title'] = null;
        $content = 'no data...';
    }
    
    include_once 'components/item_delete.php'; // модальное  окно для подтверждения удаления
    ?>

    <form class="mt-5" method="POST" id="itemSaveForm" action="javascript:void(null);" onsubmit="saveItem()">

        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-link active" id="nav-main-tab" data-toggle="tab" href="#nav-main" role="tab" aria-controls="nav-main" aria-selected="true">
                    <?php echo $main_tab_text; ?>
                </a>
                <a class="nav-link" id="nav-data-tab" data-toggle="tab" href="#nav-data" role="tab" aria-controls="nav-data" aria-selected="false">
                    <?php echo $data_tab_text; ?>
                </a>
                <a class="nav-link" id="nav-images-tab" data-toggle="tab" href="#nav-images" role="tab" aria-controls="nav-images" aria-selected="false">
                    <?php echo $images_tab; ?>
                </a>
            </div>
        </nav>

        <div class="tab-content" id="nav-tabContent">

            <div class="tab-pane fade pt-5 show active" id="nav-main" role="tabpanel" aria-labelledby="nav-main-tab">

                <div class="form-group mb-4">
                    <label for="inputTitle"><b><?php echo $input_title; ?></b></label>
                    <input type="text" class="form-control" name="inputtitle" id="inputTitle" placeholder="<?php echo $input_title_placeholder; ?>" value="<?php echo $page_title; ?>" required>
                    <small id="passwordHelpBlock" class="form-text text-muted">
                        <?php echo $title_recommended; ?>
                    </small>
                </div>

                <div class="form-group mb-4">
                    <label for="inputH1"><b><?php echo $input_h1; ?></b></label>
                    <input type="text" class="form-control" name="inputh1" id="inputH1" placeholder="<?php echo $input_h1_placeholder; ?>" value="<?php echo $page_h1; ?>" required>
                </div>

                <div class="form-row mb-4">
                    <div class="form-group col-md-8">
                        <label for="inputURL"><b><?php echo $input_url; ?></b></label>
                        <input type="text" class="form-control" name="inputurl" id="inputURL" placeholder="<?php $input_url_placeholder; ?>" value="<?php echo $page_url; ?>" required>
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            <?php echo $input_url_recommended; ?>
                        </small>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputPageLayout"><b><?php echo $input_page_layout; ?></b></label>
                        <select id="inputPageLayout" class="form-control" name="inputpagelayout">
                            <option selected><?php echo $page_page_layout; ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="KeywordsTextarea"><b><?php echo $keywords_textarea; ?></b></label>
                        <textarea class="form-control" id="KeywordsTextarea" name="keywordstextarea" placeholder="<?php echo $keywords_textarea_placeholder; ?>" rows="3" required><?php echo $page_keywords; ?></textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="DescriptionTextarea"><b><?php echo $description_textarea; ?></b></label>
                        <textarea class="form-control" id="DescriptionTextarea" name="descriptiontextarea" placeholder="<?php echo $description_textarea_placeholder; ?>" rows="3" required><?php echo $page_description; ?></textarea>
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            <?php echo $description_textarea_recommended; ?>
                        </small>
                    </div>
                </div>
                <br>
                <br>

                <div id="editorWYSIWYG">
                    <textarea id="summernote" name="editordata"><?php echo $content; ?></textarea>
                </div>

            </div>

            <div class="tab-pane fade pt-5" id="nav-data" role="tabpanel" aria-labelledby="nav-data-tab">

                <div class="form-group row">
                    <label for="selectCategory" class="col-sm-2 col-form-label"><?php echo $item_category_text; ?></label>
                    <div class="col-sm-10">
                        <select name="category_id" class="form-control" id="selectCategory" required>
                            <option value="<?php echo $category['id']; ?>"><?php echo $category['title']; ?></option>
                            <?php
                            $db_categories = getCategories($elang);

                            if ($db_categories) {

                                foreach ($db_categories as $key => $value) {

                                    if ($value['id'] != $category['id']) { ?>

                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['title']; ?></option>
                            <?php }
                                } 
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputBrand" class="col-sm-2 col-form-label"><?php echo $brand_text; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="brand" class="form-control" id="inputBrand" value="<?php echo $item_brand_name; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputSKU" class="col-sm-2 col-form-label"><?php echo $sku_text; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="sku" class="form-control" id="inputSKU" value="<?php echo $item_sku; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPrice" class="col-sm-2 col-form-label"><?php echo $price_text; ?></label>
                    <div class="col-sm-10">
                        <input type="number" name="price" class="form-control" id="inputPrice" step="0.01" value="<?php echo $item_price; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputSalePrice" class="col-sm-2 col-form-label"><?php echo $discount_price_text; ?></label>
                    <div class="col-sm-10">
                        <input type="number" name="discountprice" class="form-control" id="inputSalePrice" step="0.01" value="<?php echo $item_new_price; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="selectStatus" class="col-sm-2 col-form-label"><?php echo $availability_status_text; ?></label>
                    <div class="col-sm-10">
                        <select name="availability" class="form-control" id="selectStatus" required>                        
                            <?php
                             switch ($item_availability) {
                                 case 'InStock':
                                     echo '<option value="InStock">' . $instock_text . '</option>';
                                     echo '<option value="OutOfStock">' . $outofstock_text . '</option>';
                                     echo '<option value="PreOrder">' . $preorder_text . '</option>';
                                     break;
                                 case 'OutOfStock':
                                     echo '<option value="OutOfStock">' . $outofstock_text . '</option>';
                                     echo '<option value="InStock">' . $instock_text . '</option>';
                                     echo '<option value="PreOrder">' . $preorder_text . '</option>';
                                     break;
                                 case 'PreOrder':
                                     echo '<option value="PreOrder">' . $preorder_text . '</option>';
                                     echo '<option value="InStock">' . $instock_text . '</option>';
                                     echo '<option value="OutOfStock">' . $outofstock_text . '</option>';
                                     break;
                                 default:
                                     echo '<option value="InStock">' . $instock_text . '</option>';
                                     echo '<option value="OutOfStock">' . $outofstock_text . '</option>';
                                     echo '<option value="PreOrder">' . $preorder_text . '</option>';
                                     break;
                             } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group mt-5">
                    <label for="ShortDescriptionTextarea"><b><?php echo $short_description_textarea; ?></b></label>
                    <textarea class="form-control" id="ShortDescriptionTextarea" name="shortdescriptiontextarea" rows="3"><?php echo $item_short_description; ?></textarea>
                    <small class="form-text text-muted">
                        <?php echo $short_description_textarea_recommended; ?>
                    </small>
                </div>

            </div>

            <div class="tab-pane fade pt-5" id="nav-images" role="tabpanel" aria-labelledby="nav-images-tab">
                <p class="bg-light p-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-image mb-1 mr-2" viewBox="0 0 16 16">
                        <path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
                        <path d="M2.002 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2h-12zm12 1a1 1 0 0 1 1 1v6.5l-3.777-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12V3a1 1 0 0 1 1-1h12z"/>
                    </svg>
                    <b><?php echo $main_image_text; ?></b>
                </p>

                <div class="row align-items-center">

                    <div class="col-md-2 text-center mt-3">

                        <?php                        
                        if (file_exists('../' . $record_img_1x1)) { ?>

                            <img id="mainImage1x1" src="<?php echo $record_img_1x1; ?>" class="img-fluid img-thumbnail" alt="<?php echo $record_img_alt; ?>">
                            <input id="mainImageInput1x1" type="hidden" name="main_image_1x1" value="<?php echo $record_img_1x1; ?>">

                        <?php }
                        else { ?>

                            <img id="mainImage1x1" src="images/no_image.png" class="img-fluid img-thumbnail" alt="No image">
                            <input id="mainImageInput1x1" type="hidden" name="main_image_1x1" value="images/no_image.png">

                        <?php } ?>

                    </div>
                    <div class="col-md-4 mt-3">

                        <button class="btn btn-primary mr-1" type="button" title="<?php echo $edit_btn_title; ?>" onclick="openFileManager('<?php echo $lang; ?>', '1x1');">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                            </svg>
                        </button>
                        <button id="delMainImage1x1" class="btn btn-danger" type="button" title="<?php echo $clear_btn_title; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eraser-fill" viewBox="0 0 16 16">
                                <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm.66 11.34L3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"/>
                            </svg>
                        </button>
                        <span class="ml-3 h5"><mark>1x1</mark></span>

                    </div>

                    <div class="col-md-2 text-center mt-4">

                        <?php                        
                        if (file_exists('../' . $record_img_4x3)) { ?>

                            <img id="mainImage4x3" src="<?php echo $record_img_4x3; ?>" class="img-fluid img-thumbnail" alt="<?php echo $record_img_alt; ?>">
                            <input id="mainImageInput4x3" type="hidden" name="main_image_4x3" value="<?php echo $record_img_4x3; ?>">

                        <?php }
                        else { ?>

                            <img id="mainImage4x3" src="images/no_image.png" class="img-fluid img-thumbnail" alt="No image">
                            <input id="mainImageInput4x3" type="hidden" name="main_image_4x3" value="images/no_image.png">

                        <?php } ?>

                    </div>
                    <div class="col-md-4 mt-3">

                        <button class="btn btn-primary mr-1" type="button" title="<?php echo $edit_btn_title; ?>" onclick="openFileManager('<?php echo $lang; ?>', '4x3');">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                            </svg>
                        </button>
                        <button id="delMainImage4x3" class="btn btn-danger" type="button" title="<?php echo $clear_btn_title; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eraser-fill" viewBox="0 0 16 16">
                                <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm.66 11.34L3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"/>
                            </svg>
                        </button>
                        <span class="ml-3 h5"><mark>4x3</mark></span>

                    </div>

                    <div class="col-md-2 text-center mt-4">

                        <?php                        
                        if (file_exists('../' . $record_img_16x9)) { ?>

                            <img id="mainImage16x9" src="<?php echo $record_img_16x9; ?>" class="img-fluid img-thumbnail" alt="<?php echo $record_img_alt; ?>">
                            <input id="mainImageInput16x9" type="hidden" name="main_image_16x9" value="<?php echo $record_img_16x9; ?>">

                        <?php }
                        else { ?>

                            <img id="mainImage16x9" src="images/no_image.png" class="img-fluid img-thumbnail" alt="No image">
                            <input id="mainImageInput16x9" type="hidden" name="main_image_16x9" value="images/no_image.png">

                        <?php } ?>

                    </div>
                    <div class="col-md-4 mt-3">

                        <button class="btn btn-primary mr-1" type="button" title="<?php echo $edit_btn_title; ?>" onclick="openFileManager('<?php echo $lang; ?>', '16x9');">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                            </svg>
                        </button>
                        <button id="delMainImage16x9" class="btn btn-danger" type="button" title="<?php echo $clear_btn_title; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eraser-fill" viewBox="0 0 16 16">
                                <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm.66 11.34L3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"/>
                            </svg>
                        </button>
                        <span class="ml-3 h5"><mark>16x9</mark></span>

                    </div>

                    <div class="col-md-2 text-center mt-4">

                        <?php                        
                        if (file_exists('../' . $record_img_preview)) { ?>

                            <img id="mainImagepreview" src="<?php echo $record_img_preview; ?>" class="img-fluid img-thumbnail" alt="<?php echo $record_img_alt; ?>">
                            <input id="mainImageInputpreview" type="hidden" name="main_image_preview" value="<?php echo $record_img_preview; ?>">

                        <?php }
                        else { ?>

                            <img id="mainImagepreview" src="images/no_image.png" class="img-fluid img-thumbnail" alt="No image">
                            <input id="mainImageInputpreview" type="hidden" name="main_image_preview" value="images/no_image.png">

                        <?php } ?>

                    </div>
                    <div class="col-md-4 mt-3">

                        <button class="btn btn-primary mr-1" type="button" title="<?php echo $edit_btn_title; ?>" onclick="openFileManager('<?php echo $lang; ?>', 'preview');">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                            </svg>
                        </button>
                        <button id="delMainImagepreview" class="btn btn-danger" type="button" title="<?php echo $clear_btn_title; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eraser-fill" viewBox="0 0 16 16">
                                <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm.66 11.34L3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"/>
                            </svg>
                        </button>
                        <span class="ml-3 h5"><mark>Preview</mark></span>

                    </div>

                </div>

                <div class="form-group mt-4 mb-3">
                    <label for="inputMainImageAlt"><span class="small"><b><?php echo $item_image_alt_text; ?></b></span></label>
                    <input type="text" class="form-control" name="main_image_alt" id="inputMainImageAlt" value="<?php echo $record_img_alt; ?>" required>
                </div>

                <p class="bg-light p-3 mt-5">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-images mb-1 mr-2" viewBox="0 0 16 16">
                        <path d="M4.502 9a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"/>
                        <path d="M14.002 13a2 2 0 0 1-2 2h-10a2 2 0 0 1-2-2V5A2 2 0 0 1 2 3a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2v8a2 2 0 0 1-1.998 2zM14 2H4a1 1 0 0 0-1 1h9.002a2 2 0 0 1 2 2v7A1 1 0 0 0 15 11V3a1 1 0 0 0-1-1zM2.002 4a1 1 0 0 0-1 1v8l2.646-2.354a.5.5 0 0 1 .63-.062l2.66 1.773 3.71-3.71a.5.5 0 0 1 .577-.094l1.777 1.947V5a1 1 0 0 0-1-1h-10z"/>
                    </svg>
                    <b><?php echo $additional_images; ?></b>

                    <input type="hidden" name="additional_images" value="images_data">
                </p>

            </div>

        </div>

        <input type="hidden" name="lang" value="<?php echo $lang; ?>">
        <input type="hidden" name="elang" value="<?php echo $elang; ?>">
        <input type="hidden" name="page_id" value="<?php echo $page_id; ?>">
        <input id="origCategory" type="hidden" name="orig_category" value="<?php echo $category['url']; ?>">
        <input id="origUrl" type="hidden" name="orig_url" value="<?php echo $page_url; ?>">

        <div class="text-center mt-5">
            <button type="submit" class="btn btn-primary mt-3 mr-2"><?php echo $save_button; ?></button>
            <?php
            if($_GET['epage'] != 'newitem') { ?>
                <button type="button" class="btn btn-danger mt-3" data-toggle="modal" data-target="#pageDelete"><?php echo $delete; ?></button>
            <?php }
            ?>
        </div>

    </form>

    <script>
        function openFileManager(lang, ID) {
            $('#filemanagerResault').load('editor/modules/filemanager.php', 
                { 'lang': lang, 'id': ID, 'type': 'catalog', 'folder': '' } );
            
            $('#filemanagerModal').modal('show');
        }
    </script>

    <script>
        function saveItem() {
            
            var msg   = $('#itemSaveForm').serialize();
            
            $.ajax({
                 
                type: 'POST',
                url: '<?php echo $save_module; ?>',
                data: msg,
                
            }).done(function(html) {
                  
                $('#pageSaveResault').html(html);
                $('#pageSaveInfo').modal('show');
                
            });
            
        }
        
        function deleteItem(ID, category_id, lang, elang) {
        
            $.ajax({
                 
                type: 'POST',
                url: 'editor/modules/delete_item.php',
                data: {'id': ID, 'category_id': category_id, 'lang': lang, 'elang': elang},
                
            }).done(function(html) {
                  
                $('body').prepend(html);
                
            });

        }
    </script>

    <script>
    $("#delMainImage1x1").bind("click", function() {
        $("#mainImage1x1").attr("src","images/no_image.png");
        $("#mainImageInput1x1").attr("value","images/no_image.png");
    });
    $("#delMainImage4x3").bind("click", function() {
        $("#mainImage4x3").attr("src","images/no_image.png");
        $("#mainImageInput4x3").attr("value","images/no_image.png");
    });
    $("#delMainImage16x9").bind("click", function() {
        $("#mainImage16x9").attr("src","images/no_image.png");
        $("#mainImageInput16x9").attr("value","images/no_image.png");
    });
    $("#delMainImagepreview").bind("click", function() {
        $("#mainImagepreview").attr("src","images/no_image.png");
        $("#mainImageInputpreview").attr("value","images/no_image.png");
    });
    </script>

    <!-- Run summernote
    Run the script below when document is ready! -->
    <script>
        $('#summernote').summernote( {
            height: 450,
            codemirror: { // codemirror options
                mode: 'text/html',
                htmlMode: true,
                lineNumbers: true,
                theme: 'monokai'
                },
            <?php
            if ($lang == 'ru') { ?>
                lang: 'ru-RU' // По умолчанию en-US
            <?php } ?>
        } );
    </script>
<?php }
else { ?>
    <p class="mt-5">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-circle-fill mb-1 mr-2" viewBox="0 0 16 16">
            <circle cx="8" cy="8" r="8"/>
        </svg>
        <?php echo $page_not_available; ?>
    </p>
<?php }
