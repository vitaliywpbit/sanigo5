<?php

include_once '../../config.php';

// Для создания новой директории в images/ с актуальным годом (только для Блога и др. Записей)
$folde_preview = 'yyyy';
$folder_16x9 = 'yyyy';
$folder_4x3 = 'yyyy';
$folder_1x1 = 'yyyy';
$folder_all = 'all';

if (isset($_POST['id']) && isset($_POST['type'])) {
    
    if ($_POST['folder'] != '') {
        
        $folde_preview = $_POST['type'] . '/preview';
        $folder_16x9 = $_POST['type'] . '/16x9';
        $folder_4x3 = $_POST['type'] . '/4x3';
        $folder_1x1 = $_POST['type'] . '/1x1';
        $folder_all = $_POST['type'] . '/all';
        
        $path = $_POST['folder'] . '/';
        $path_for_back = $_POST['folder'];
        
    }
    else {
        $path = $_POST['type'] . '/';
        $path_for_back = $_POST['type'];
    }
    
    $server_path = SERVER_DIR . 'images/' . $path; // полный путь на сервере
    
    // Проверка, существует ли папка с годом, актуальным на данный момент
    // - если ее нет, то создаем папку с текущем годом
    if ($_POST['folder'] == $folde_preview or 
        $_POST['folder'] == $folder_16x9 or 
        $_POST['folder'] == $folder_4x3 or
        $_POST['folder'] == $folder_1x1 or
        $_POST['folder'] == $folder_all ) {
        
        $dir = $server_path . idate('Y');
        
        if (!file_exists($dir)) { 
            
            mkdir($dir, 0777, true); // создание директории
        }
        
    }
    
    // Получаем массив из пути к файлам и удаляем последний элемент массива
    // для перехода к предыдущей директории
    $path_back = explode('/', $path_for_back);
    $final_path = array_pop($path_back);
    $path_back = implode('/', $path_back);
    ?>

    <p>
        <span id="idFileToPut" class="d-none"><?php echo $_POST["id"]; ?></span>
        <mark>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-folder2-open mr-1" viewBox="0 0 16 16">
                <path d="M1 3.5A1.5 1.5 0 0 1 2.5 2h2.764c.958 0 1.76.56 2.311 1.184C7.985 3.648 8.48 4 9 4h4.5A1.5 1.5 0 0 1 15 5.5v.64c.57.265.94.876.856 1.546l-.64 5.124A2.5 2.5 0 0 1 12.733 15H3.266a2.5 2.5 0 0 1-2.481-2.19l-.64-5.124A1.5 1.5 0 0 1 1 6.14V3.5zM2 6h12v-.5a.5.5 0 0 0-.5-.5H9c-.964 0-1.71-.629-2.174-1.154C6.374 3.334 5.82 3 5.264 3H2.5a.5.5 0 0 0-.5.5V6zm-.367 1a.5.5 0 0 0-.496.562l.64 5.124A1.5 1.5 0 0 0 3.266 14h9.468a1.5 1.5 0 0 0 1.489-1.314l.64-5.124A.5.5 0 0 0 14.367 7H1.633z"/>
            </svg>:
            <span class="ml-2 small">images/</span><span id="pathToFile" class="small"><?php echo trim($path); ?></span>
        </mark>
    </p>

    <hr>
    
    <?php
    if (is_dir($server_path) === true) {
        
        $files = scandir($server_path);

        echo '<div id="imagePrepend" class="row">'; ?>
        
        <div class="col-6 col-sm-4 col-md-3 order-first text-center mt-3">
            <a role="button" onclick="workFileManager('<?php echo $_POST['id']; ?>', '<?php echo $_POST['type']; ?>', '<?php echo $path_back; ?>');">
                <svg xmlns="http://www.w3.org/2000/svg" fill="#999999" class="bi bi-folder img-fluid" viewBox="0 0 16 16">
                    <path d="M.54 3.87.5 3a2 2 0 0 1 2-2h3.672a2 2 0 0 1 1.414.586l.828.828A2 2 0 0 0 9.828 3h3.982a2 2 0 0 1 1.992 2.181l-.637 7A2 2 0 0 1 13.174 14H2.826a2 2 0 0 1-1.991-1.819l-.637-7a1.99 1.99 0 0 1 .342-1.31zM2.19 4a1 1 0 0 0-.996 1.09l.637 7a1 1 0 0 0 .995.91h10.348a1 1 0 0 0 .995-.91l.637-7A1 1 0 0 0 13.81 4H2.19zm4.69-1.707A1 1 0 0 0 6.172 2H2.5a1 1 0 0 0-1 .981l.006.139C1.72 3.042 1.95 3 2.19 3h5.396l-.707-.707z"/>
                </svg>
                <br>
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-three-dots" viewBox="0 0 16 16">
                    <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"/>
                </svg>
            </a>
        </div>

    <?php 
        foreach($files as $file_name) {
            
            if ($file_name != '.' && $file_name != '..' && $file_name != '...') {

                if(is_dir($server_path . $file_name) === true) { ?>

                    <div class="col-6 col-sm-4 col-md-3 text-center mt-3">
                        <a role="button" onclick="workFileManager('<?php echo $_POST['id']; ?>', '<?php echo $_POST['type']; ?>', '<?php echo $path . $file_name; ?>');">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="#999999" class="bi bi-folder-fill img-fluid" viewBox="0 0 16 16">
                                <path d="M9.828 3h3.982a2 2 0 0 1 1.992 2.181l-.637 7A2 2 0 0 1 13.174 14H2.825a2 2 0 0 1-1.991-1.819l-.637-7a1.99 1.99 0 0 1 .342-1.31L.5 3a2 2 0 0 1 2-2h3.672a2 2 0 0 1 1.414.586l.828.828A2 2 0 0 0 9.828 3zm-8.322.12C1.72 3.042 1.95 3 2.19 3h5.396l-.707-.707A1 1 0 0 0 6.172 2H2.5a1 1 0 0 0-1 .981l.006.139z"/>
                            </svg>
                            <br>
                            <?php echo $file_name; ?>
                        </a>
                    </div>
            
            <?php }
                else { ?>

                <div class="col-6 col-sm-4 col-md-3 text-center mt-3 position-relative" data-file-name="<?php echo $file_name; ?>">
                    <div class="dropdown position-absolute mt-n2 ml-n2">
                        <a class="btn btn-sm btn-danger rounded" role="button" id="dropdownDeleteImage<?php echo $file_name; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#ffffff" class="bi bi-x" viewBox="0 0 16 16">
                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                            </svg>
                        </a>
                        <div class="dropdown-menu border-danger" aria-labelledby="dropdownDeleteImage<?php echo $file_name; ?>">
                            <a class="dropdown-item" role="button" onclick="deleteImages('<?php echo $file_name; ?>', '<?php echo $server_path . $file_name; ?>')">
                                Удалить
                            </a>
                        </div>
                    </div>
                    
                    <a role="button" onclick="putFileLink('<?php echo $_POST["id"]; ?>', '<?php echo "images/" . $path . $file_name; ?>');">
                        <img src="<?php echo 'images/' . $path . $file_name; ?>" loading="lazy" class="img-fluid">
                    </a>
                    <br>
                    <span class="small text-break"><b><?php echo $file_name; ?></b></span>
                </div>

            <?php }
            } 
        }
        echo '</div>';
    }
    else {
        echo '<p class="text-center">no such file or directory</p>';
    }
}
 else {
    echo '<p class="text-center">no such file or directory</p>';
}
