<?php

include_once '../controllers/records.php'; // подключение функций для работы с товарами

if (isset($_POST['elang']) && 
        isset($_POST['page_id']) && 
        isset($_POST['inputpagelayout']) && 
        isset($_POST['inputurl']) && 
        isset($_POST['inputtitle']) && 
        isset($_POST['keywordstextarea']) && 
        isset($_POST['descriptiontextarea']) && 
        isset($_POST['inputh1']) && 
        // isset($_POST['record_date']) && 
        // isset($_POST['record_mod_date']) && 
        isset($_POST['main_image_1x1']) && 
        isset($_POST['main_image_4x3']) && 
        isset($_POST['main_image_16x9']) && 
        isset($_POST['main_image_preview']) && 
        isset($_POST['main_image_alt']) && 
        isset($_POST['record_author']) && 
        isset($_POST['record_author_type'])
    ) {
    
    if (isset($_POST['editordata'])) {
        $editordata = $_POST['editordata'];
    }
    else {
        $editordata = null;
    }
    
    if (isset($_POST['record_date'])) {
        $record_date = $_POST['record_date'];
    }
    else {
        $record_date = date('Y-m-d\TH:i:s');
    }
    
    // $record_mod_date = date('Y-m-d\TH:i:s');
    
    if (isset($_POST['lang'])) {
        $lang = $_POST['lang'];
    }
    else {
        $lang = 'ru';
    }
    
    include '../language/' . $lang . '/all_content.php'; // Загрузка переменных на нужном языке
    
    // Функция сохранения страницы
    if (recordAdd($_POST['elang'], 
            $_POST['page_id'], 
            $_POST['inputpagelayout'], 
            $_POST['inputurl'], 
            $_POST['inputtitle'], 
            $_POST['keywordstextarea'], 
            $_POST['descriptiontextarea'], 
            $_POST['inputh1'],
            $editordata,
            $record_date,
            // $record_mod_date,
            $_POST['main_image_1x1'],
            $_POST['main_image_4x3'],
            $_POST['main_image_16x9'],
            $_POST['main_image_preview'],
            $_POST['main_image_alt'],
            $_POST['record_author'],
            $_POST['record_author_type']
        )) { ?>

        <div class="text-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="green" class="bi bi-check-circle-fill" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
            </svg>
            <p class="mt-3"><?php echo $saved_text; ?></p>
        </div>
        <script>
            self.location="editor/index.php?page=blog&lang=<?php echo $lang; ?>&elang=<?php echo $_POST['elang']; ?>";
        </script>

    <?php }
    else { ?>

        <div class="text-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="red" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
            </svg>
            <p class="mt-3"><?php echo $save_error_text; ?></p>
        </div>
        <div class="alert alert-danger" role="alert">
            <?php echo $global_save_page_err; ?>
        </div>
        <script>
            $('#pageSaveInfo').modal('show');
        </script>
        
<?php }
    
}
