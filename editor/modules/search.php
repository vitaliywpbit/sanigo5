<?php

if (isset($_POST['page']) && 
    isset($_POST['lang']) && 
    isset($_POST['elang']) && 
    isset($_POST['search_request'])) {
    
    $page = $_POST['page'];
    $lang = $_POST['lang'];
    $elang = $_POST['elang'];
    
    $search_request = mb_convert_case(trim(strip_tags(stripcslashes(htmlspecialchars($_POST['search_request'])))), MB_CASE_LOWER, "UTF-8");
    
    include '../model/search.php'; // Подключение к БД
    include '../language/' . $lang . '/search.php'; // Загрузка переменных на нужном языке
    
}
else {
    $page = null;
    $lang = 'ru';
    $search_request = null;
}

if ($search_result) {
    
    foreach ($search_result as $value) {
        
        $db_page_info = explode('|*|', $value);

        $page_url = $db_page_info[1];
        $page_h1 = $db_page_info[2];
        
        // Проверяем, есть ли изображение
        if ($db_page_info[0]) {
            
            if (file_exists('../../' . $db_page_info[0])) {

                $page_image_link = $db_page_info[0];
            }
            else {
                $page_image_link = 'images/no_image.png';
            }
        }
        else {
            $page_image_link = null;
        }
        ?>

        <a class="dropdown-item" href="editor/index.php?page=<?php echo $page; ?>&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>&epage=<?php echo $page_url; ?>">
            <div class="row align-items-center">
                <div class="col-3 col-sm-3 col-md-2 text-center">
                    <?php
                    if ($page_image_link) { ?>
                    
                        <img src="<?php echo $page_image_link; ?>" class="img-fluid img-thumbnail">
                        
                    <?php }
                    elseif ($page == 'categories') { ?>
                        
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#039394" class="bi bi-list-nested" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M4.5 11.5A.5.5 0 0 1 5 11h10a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zm-2-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm-2-4A.5.5 0 0 1 1 3h10a.5.5 0 0 1 0 1H1a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                            
                    <?php }
                    elseif ($page == 'orders') { ?>
                        
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#039394" class="bi bi-bag-check" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                            <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z"/>
                        </svg>
                            
                    <?php }
                    else { ?>
                        
                        <svg width="16" height="16" viewBox="0 0 16 16" class="bi bi-file-richtext" fill="#039394" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M4 1h8a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H4z"/>
                            <path fill-rule="evenodd" d="M4.5 11.5A.5.5 0 0 1 5 11h3a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zm0-2A.5.5 0 0 1 5 9h6a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zm1.639-3.708l1.33.886 1.854-1.855a.25.25 0 0 1 .289-.047l1.888.974V7.5a.5.5 0 0 1-.5.5H5a.5.5 0 0 1-.5-.5V7s1.54-1.274 1.639-1.208zM6.25 5a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5z"/>
                        </svg>
                        
                   <?php } ?>
                </div>
                <div class="col-9 col-sm-9 col-md-10">
                    <?php echo $page_h1; ?>
                </div>
            </div>
        </a>
<?php }
}
else {
    echo '<p class="text-center">' . $search_not_found . '</p>';
}
