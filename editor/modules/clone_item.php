<?php

if (isset($_POST['id']) && isset($_POST['category_id']) && isset($_POST['lang']) && isset($_POST['elang'])) {
    
    include_once '../controllers/items.php';
    
    if (itemClone($_POST['id'], $_POST['category_id'], $_POST['lang'], $_POST['elang'])) {
        
        // Переадресация на первую страницу со списком категорий
        echo '<script>self.location="editor/index.php?page=items&lang=' . $_POST['lang'] . '&elang=' . $_POST['elang'] . '";</script>';
    }
    else {
        
        // Перезагрузка страницы с товарами
        echo '<script>location.reload();</script>';
    }
}
else {
    
    // Перезагрузка страницы с товарами
    echo '<script>location.reload();</script>';
}

//echo "<script>document.location.href='http://example.com/final.php';</script>";
//echo "<script>window.location.href='http://example.com/final.php';</script>";
//echo "<script>window.location.replace('http://example.com/final.php');</script>";
