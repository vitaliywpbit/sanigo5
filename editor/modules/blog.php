<?php

// Подключение к БД
include 'model/blog.php';

if(isset($_GET['epage'])) {
    
    $page_available = false;
    $new_record = false;
    
    if($_GET['epage'] == 'newrecord') {
        
        $page_available = true;
        $new_record = true;
        $save_module = 'editor/modules/add_blog_post.php';

        $page_id = $final_id + 1;
        $page_layout = 'blog';
        $page_url = 'new-blog-post';
        $page_title = '';
        $page_keywords = '';
        $page_description = '';
        $page_h1 = '';
        $record_date = date('Y-m-d\TH:i:s');
        $record_mod_date = $record_date;
        $record_img_1x1 = 'images/no_image.png';
        $record_img_4x3 = 'images/no_image.png';
        $record_img_16x9 = 'images/no_image.png';
        $record_img_preview = 'images/no_image.png';
        $record_img_alt = '';
        $record_author = '';
        $record_author_type = '';
        
    }
    else {
        
        foreach ($db_records as $line => $column) {

            if($_GET['epage'] == $column['url']) {

                $page_available = true;
                $save_module = 'editor/modules/save_blog_post.php';

                $page_id = $column['id'];
                $page_layout = $column['page_layout'];
                $page_url = $column['url'];
                $page_title = $column['title'];
                $page_keywords = $column['keywords'];
                $page_description = $column['description'];
                $page_h1 = $column['h1'];
                $record_date = $column['record_date'];
                $record_mod_date = $column['record_mod_date'];
                $record_img_1x1 = $column['record_img_1x1'];
                $record_img_4x3 = $column['record_img_4x3'];
                $record_img_16x9 = $column['record_img_16x9'];
                $record_img_preview = $column['record_img_preview'];
                $record_img_alt = $column['record_img_alt'];
                $record_author = $column['record_author'];
                $record_author_type = $column['record_author_type'];
                break;
            }
        }
    }
}

if ($page_available) {

    if (!$new_record) {
        
        $page_layout = 'blog';
        
        // Блок загрузки контента для записи
        $path_to_content = '../' . DB_DIR_PATH . $elang . '/' . $page_layout . '/' . $page_url . '.tpl';

        if (file_exists($path_to_content)) {
            $content = file_get_contents($path_to_content);
        }
         else {
            $content = 'no data...';
        }
    }
    else {
        $content = 'no data...';
    }
    
    include_once 'components/record_delete.php'; // модальное  окно для подтверждения удаления
    ?>

    <form class="mt-5" method="POST" id="recordSaveForm" action="javascript:void(null);" onsubmit="saveRecord()">

        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-link active" id="nav-main-tab" data-toggle="tab" href="#nav-main" role="tab" aria-controls="nav-main" aria-selected="true">
                    <?php echo $main_tab_text; ?>
                </a>
                <a class="nav-link" id="nav-data-tab" data-toggle="tab" href="#nav-data" role="tab" aria-controls="nav-data" aria-selected="false">
                    <?php echo $data_tab_text; ?>
                </a>
                <a class="nav-link" id="nav-images-tab" data-toggle="tab" href="#nav-images" role="tab" aria-controls="nav-images" aria-selected="false">
                    <?php echo $images_tab; ?>
                </a>
            </div>
        </nav>

        <div class="tab-content" id="nav-tabContent">

            <div class="tab-pane fade pt-5 show active" id="nav-main" role="tabpanel" aria-labelledby="nav-main-tab">

                <div class="form-group mb-4">
                    <label for="inputTitle"><b><?php echo $input_title; ?></b></label>
                    <input type="text" class="form-control" name="inputtitle" id="inputTitle" placeholder="<?php echo $input_title_placeholder; ?>" value="<?php echo $page_title; ?>" required>
                    <small id="passwordHelpBlock" class="form-text text-muted">
                        <?php echo $title_recommended; ?>
                    </small>
                </div>

                <div class="form-group mb-4">
                    <label for="inputH1"><b><?php echo $input_h1; ?></b></label>
                    <input type="text" class="form-control" name="inputh1" id="inputH1" placeholder="<?php echo $input_h1_placeholder; ?>" value="<?php echo $page_h1; ?>" required>
                </div>

                <div class="form-row mb-4">
                    <div class="form-group col-md-8">
                        <label for="inputURL"><b><?php echo $input_url; ?></b></label>
                        <input type="text" class="form-control" name="inputurl" id="inputURL" placeholder="<?php $input_url_placeholder; ?>" value="<?php echo $page_url; ?>" required>
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            <?php echo $input_url_recommended; ?>
                        </small>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputPageLayout"><b><?php echo $input_page_layout; ?></b></label>
                        <select id="inputPageLayout" class="form-control" name="inputpagelayout">
                            <option selected><?php echo $page_layout; ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="KeywordsTextarea"><b><?php echo $keywords_textarea; ?></b></label>
                        <textarea class="form-control" id="KeywordsTextarea" name="keywordstextarea" placeholder="<?php echo $keywords_textarea_placeholder; ?>" rows="3" required><?php echo $page_keywords; ?></textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="DescriptionTextarea"><b><?php echo $description_textarea; ?></b></label>
                        <textarea class="form-control" id="DescriptionTextarea" name="descriptiontextarea" placeholder="<?php echo $description_textarea_placeholder; ?>" rows="3" required><?php echo $page_description; ?></textarea>
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            <?php echo $description_textarea_recommended; ?>
                        </small>
                    </div>
                </div>
                <br>
                <br>

                <div id="editorWYSIWYG">
                    <textarea id="summernote" name="editordata"><?php echo $content; ?></textarea>
                </div>

            </div>

            <div class="tab-pane fade pt-5" id="nav-data" role="tabpanel" aria-labelledby="nav-data-tab">

                <div class="form-group row">
                    <label for="selectAuthorType" class="col-sm-2 col-form-label"><?php echo $record_author_type_text; ?></label>
                    <div class="col-sm-10">
                        <select name="record_author_type" class="form-control" id="selectAuthorType" required>                        
                            <?php
                             switch ($record_author_type) {
                                 case 'Organization':
                                     echo '<option value="Organization">' . $organization_text . '</option>';
                                     echo '<option value="Person">' . $person_text . '</option>';
                                     break;
                                 case 'Person':
                                     echo '<option value="Person">' . $person_text . '</option>';
                                     echo '<option value="Organization">' . $organization_text . '</option>';
                                     break;
                                 default:
                                     echo '<option value="Organization">' . $organization_text . '</option>';
                                     echo '<option value="Person">' . $person_text . '</option>';
                                     break;
                             } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputRecordAuthor" class="col-sm-2 col-form-label"><?php echo $record_author_text; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="record_author" class="form-control" id="inputRecordAuthor" value="<?php echo $record_author; ?>" required>
                    </div>
                </div>
                <br>
                <br>
                <div class="form-group row">
                    <label for="inputRecordDate" class="col-sm-2 col-form-label"><?php echo $record_date_text; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="record_date" class="form-control" id="inputRecordDate" value="<?php echo $record_date; ?>" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputRecordModDate" class="col-sm-2 col-form-label"><?php echo $record_mod_date_text; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="record_mod_date" class="form-control" id="inputRecordModDate" value="<?php echo $record_mod_date; ?>" readonly>
                    </div>
                </div>

            </div>

            <div class="tab-pane fade pt-5" id="nav-images" role="tabpanel" aria-labelledby="nav-images-tab">
                <p class="bg-light p-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-image mb-1 mr-2" viewBox="0 0 16 16">
                        <path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
                        <path d="M2.002 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2h-12zm12 1a1 1 0 0 1 1 1v6.5l-3.777-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12V3a1 1 0 0 1 1-1h12z"/>
                    </svg>
                    <b><?php echo $main_image_text; ?></b>
                </p>

                <div class="row align-items-center">

                    <div class="col-md-2 text-center mt-3">

                        <?php                        
                        if (file_exists('../' . $record_img_1x1)) { ?>

                            <img id="mainImage1x1" src="<?php echo $record_img_1x1; ?>" class="img-fluid img-thumbnail" alt="<?php echo $record_img_alt; ?>">
                            <input id="mainImageInput1x1" type="hidden" name="main_image_1x1" value="<?php echo $record_img_1x1; ?>">

                        <?php }
                        else { ?>

                            <img id="mainImage1x1" src="images/no_image.png" class="img-fluid img-thumbnail" alt="No image">
                            <input id="mainImageInput1x1" type="hidden" name="main_image_1x1" value="images/no_image.png">

                        <?php } ?>

                    </div>
                    <div class="col-md-4 mt-3">

                        <button class="btn btn-primary mr-1" type="button" title="<?php echo $edit_btn_title; ?>" onclick="openFileManager('<?php echo $lang; ?>', '1x1');">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                            </svg>
                        </button>
                        <button id="delMainImage1x1" class="btn btn-danger" type="button" title="<?php echo $clear_btn_title; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eraser-fill" viewBox="0 0 16 16">
                                <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm.66 11.34L3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"/>
                            </svg>
                        </button>
                        <span class="ml-3 h5"><mark>1x1</mark></span>

                    </div>

                    <div class="col-md-2 text-center mt-4">

                        <?php                        
                        if (file_exists('../' . $record_img_4x3)) { ?>

                            <img id="mainImage4x3" src="<?php echo $record_img_4x3; ?>" class="img-fluid img-thumbnail" alt="<?php echo $record_img_alt; ?>">
                            <input id="mainImageInput4x3" type="hidden" name="main_image_4x3" value="<?php echo $record_img_4x3; ?>">

                        <?php }
                        else { ?>

                            <img id="mainImage4x3" src="images/no_image.png" class="img-fluid img-thumbnail" alt="No image">
                            <input id="mainImageInput4x3" type="hidden" name="main_image_4x3" value="images/no_image.png">

                        <?php } ?>

                    </div>
                    <div class="col-md-4 mt-3">

                        <button class="btn btn-primary mr-1" type="button" title="<?php echo $edit_btn_title; ?>" onclick="openFileManager('<?php echo $lang; ?>', '4x3');">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                            </svg>
                        </button>
                        <button id="delMainImage4x3" class="btn btn-danger" type="button" title="<?php echo $clear_btn_title; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eraser-fill" viewBox="0 0 16 16">
                                <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm.66 11.34L3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"/>
                            </svg>
                        </button>
                        <span class="ml-3 h5"><mark>4x3</mark></span>

                    </div>

                    <div class="col-md-2 text-center mt-4">

                        <?php                        
                        if (file_exists('../' . $record_img_16x9)) { ?>

                            <img id="mainImage16x9" src="<?php echo $record_img_16x9; ?>" class="img-fluid img-thumbnail" alt="<?php echo $record_img_alt; ?>">
                            <input id="mainImageInput16x9" type="hidden" name="main_image_16x9" value="<?php echo $record_img_16x9; ?>">

                        <?php }
                        else { ?>

                            <img id="mainImage16x9" src="images/no_image.png" class="img-fluid img-thumbnail" alt="No image">
                            <input id="mainImageInput16x9" type="hidden" name="main_image_16x9" value="images/no_image.png">

                        <?php } ?>

                    </div>
                    <div class="col-md-4 mt-3">

                        <button class="btn btn-primary mr-1" type="button" title="<?php echo $edit_btn_title; ?>" onclick="openFileManager('<?php echo $lang; ?>', '16x9');">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                            </svg>
                        </button>
                        <button id="delMainImage16x9" class="btn btn-danger" type="button" title="<?php echo $clear_btn_title; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eraser-fill" viewBox="0 0 16 16">
                                <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm.66 11.34L3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"/>
                            </svg>
                        </button>
                        <span class="ml-3 h5"><mark>16x9</mark></span>

                    </div>

                    <div class="col-md-2 text-center mt-4">

                        <?php                        
                        if (file_exists('../' . $record_img_preview)) { ?>

                            <img id="mainImagepreview" src="<?php echo $record_img_preview; ?>" class="img-fluid img-thumbnail" alt="<?php echo $record_img_alt; ?>">
                            <input id="mainImageInputpreview" type="hidden" name="main_image_preview" value="<?php echo $record_img_preview; ?>">

                        <?php }
                        else { ?>

                            <img id="mainImagepreview" src="images/no_image.png" class="img-fluid img-thumbnail" alt="No image">
                            <input id="mainImageInputpreview" type="hidden" name="main_image_preview" value="images/no_image.png">

                        <?php } ?>

                    </div>
                    <div class="col-md-4 mt-3">

                        <button class="btn btn-primary mr-1" type="button" title="<?php echo $edit_btn_title; ?>" onclick="openFileManager('<?php echo $lang; ?>', 'preview');">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                            </svg>
                        </button>
                        <button id="delMainImagepreview" class="btn btn-danger" type="button" title="<?php echo $clear_btn_title; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eraser-fill" viewBox="0 0 16 16">
                                <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm.66 11.34L3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"/>
                            </svg>
                        </button>
                        <span class="ml-3 h5"><mark>Preview</mark></span>

                    </div>

                </div>

                <div class="form-group mt-4 mb-3">
                    <label for="inputMainImageAlt"><span class="small"><b><?php echo $item_image_alt_text; ?></b></span></label>
                    <input type="text" class="form-control" name="main_image_alt" id="inputMainImageAlt" value="<?php echo $record_img_alt; ?>" required>
                </div>

            </div>

        </div>

        <input type="hidden" name="lang" value="<?php echo $lang; ?>">
        <input type="hidden" name="elang" value="<?php echo $elang; ?>">
        <input type="hidden" name="page_id" value="<?php echo $page_id; ?>">
        <input id="origUrl" type="hidden" name="orig_url" value="<?php echo $page_url; ?>">

        <div class="text-center mt-5">
            <button type="submit" class="btn btn-primary mt-3 mr-2"><?php echo $save_button; ?></button>
            <?php
            if($_GET['epage'] != 'newitem') { ?>
                <button type="button" class="btn btn-danger mt-3" data-toggle="modal" data-target="#pageDelete"><?php echo $delete; ?></button>
            <?php }
            ?>
        </div>

    </form>

    <script>
        function openFileManager(lang, ID) {
            $('#filemanagerResault').load('editor/modules/filemanager.php', 
                { 'lang': lang, 'id': ID, 'type': 'blog', 'folder': '' } );
            
            $('#filemanagerModal').modal('show');
        }
    </script>

    <script>
        function saveRecord() {
            var msg   = $('#recordSaveForm').serialize();
            $.ajax({
                type: 'POST',
                url: '<?php echo $save_module; ?>',
                data: msg,
                success: function(data) {
                    $('#pageSaveResault').html(data);
                },
                error:  function(xhr, str){
                    alert('Error: ' + xhr.responseCode);
                }
            });
        }
        
        function deleteRecord(ID, page_layout, lang, elang) {
          
            $.ajax({
            type: 'POST',
            url: 'editor/modules/delete_blog_post.php',
            data: {'id': ID, 'page_layout': page_layout, 'lang': lang, 'elang': elang},
            success: function(data) {
              $('body').html(data);
              // $('#pageSaveInfo').modal('show');
            },
            error:  function(xhr, str){
              alert('Error: ' + xhr.responseCode);
            }
          });

      }
    </script>

    <script>
    $("#delMainImage1x1").bind("click", function() {
        $("#mainImage1x1").attr("src","images/no_image.png");
        $("#mainImageInput1x1").attr("value","images/no_image.png");
    });
    $("#delMainImage4x3").bind("click", function() {
        $("#mainImage4x3").attr("src","images/no_image.png");
        $("#mainImageInput4x3").attr("value","images/no_image.png");
    });
    $("#delMainImage16x9").bind("click", function() {
        $("#mainImage16x9").attr("src","images/no_image.png");
        $("#mainImageInput16x9").attr("value","images/no_image.png");
    });
    $("#delMainImagepreview").bind("click", function() {
        $("#mainImagepreview").attr("src","images/no_image.png");
        $("#mainImageInputpreview").attr("value","images/no_image.png");
    });
    </script>

    <!-- Run summernote
    Run the script below when document is ready! -->
    <script>
        $('#summernote').summernote( {
            height: 450,
            codemirror: { // codemirror options
                mode: 'text/html',
                htmlMode: true,
                lineNumbers: true,
                theme: 'monokai'
                },
            <?php
            if ($lang == 'ru') { ?>
                lang: 'ru-RU' // По умолчанию en-US
            <?php } ?>
        } );
    </script>
<?php }
else { ?>
    <p class="mt-5">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-circle-fill mb-1 mr-2" viewBox="0 0 16 16">
            <circle cx="8" cy="8" r="8"/>
        </svg>
        <?php echo $page_not_available; ?>
    </p>
<?php }
