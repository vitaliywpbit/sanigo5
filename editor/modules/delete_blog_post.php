<?php

if (isset($_POST['id']) && isset($_POST['lang']) && isset($_POST['elang'])) {
    
    include_once '../controllers/records.php';
    
    if (recordDelete($_POST['id'], 'blog', $_POST['lang'], $_POST['elang'])) {
        
        // Перезагрузка страницы с записями
        echo '<script>location.reload();</script>';
    }
    else {
        
        // Перезагрузка страницы с записями
        echo '<script>location.reload();</script>';
    }
}
else {
    
    // Перезагрузка страницы с записями
    echo '<script>location.reload();</script>';
}
