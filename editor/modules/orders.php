<?php

// Подключение к БД
include 'model/orders.php';

if(isset($_GET['epage'])) {
    
    foreach ($db_orders as $line => $column) {
        
        if($_GET['epage'] == $column['order_number']) {
            
            $order_number = $column['order_number'];
            $date = $column['date'];
            $first_name = $column['first_name'];
            $last_name = $column['last_name'];
            $phone = $column['phone'];
            $email = $column['email'];
            $subscription = $column['subscription'];
            $region = $column['region'];
            $city = $column['city'];
            $address = $column['address'];
            $delivery = $column['delivery'];
            $payment = $column['payment'];
            $comment = $column['comment'];
            $orders = $column['orders'];
            $total = $column['total'];
            
            break;
        } 
    }
}
?>

<h5 class="mt-5 mb-1"><ins><?php echo $order_text . ' <b>' . $order_number . '</b>'; ?></ins></h5>
<p class="text-muted"><?php echo $date; ?></p>

<div class="bg-light mt-4 p-3 text-uppercase">
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-person mb-1 mr-2" viewBox="0 0 16 16">
        <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
    </svg>
    <?php echo $personal_data_text; ?>
</div>

<div class="row">
    <div class="col-md-6 mt-3">
        <div class="p-3 border">
            <p><b><?php echo $first_name_text; ?></b> <?php echo $first_name; ?></p>
            <p><b><?php echo $last_name_text; ?></b> <?php echo $last_name; ?></p>
            <p><b><?php echo $phone_text; ?></b> <?php echo $phone; ?></p>
            <p><b><?php echo $email_text; ?></b> <?php echo $email; ?></p>
            <?php
            if ($subscription == 'y') { ?>
                <p>
                    <b><?php echo $subscription_text; ?></b>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="green" class="bi bi-circle-fill mb-1 ml-2" viewBox="0 0 16 16">
                        <circle cx="8" cy="8" r="8"/>
                    </svg>
                </p>
            <?php }
            else { ?>
                <p>
                    <b><?php echo $subscription_text; ?></b>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-circle-fill mb-1 ml-2" viewBox="0 0 16 16">
                        <circle cx="8" cy="8" r="8"/>
                    </svg>
                </p>
            <?php } ?>
        </div>
    </div>
    <div class="col-md-6 mt-3">
        <div class="p-3 border">
            <p><b><?php echo $region_text; ?></b> <?php echo $region; ?></p>
            <p><b><?php echo $city_text; ?></b> <?php echo $city; ?></p>
            <p><b><?php echo $address_text; ?></b> <?php echo $address; ?></p>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-md-6 mt-3">
        <div class="bg-light p-3 text-uppercase">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-truck mb-1 mr-2" viewBox="0 0 16 16">
                <path d="M0 3.5A1.5 1.5 0 0 1 1.5 2h9A1.5 1.5 0 0 1 12 3.5V5h1.02a1.5 1.5 0 0 1 1.17.563l1.481 1.85a1.5 1.5 0 0 1 .329.938V10.5a1.5 1.5 0 0 1-1.5 1.5H14a2 2 0 1 1-4 0H5a2 2 0 1 1-3.998-.085A1.5 1.5 0 0 1 0 10.5v-7zm1.294 7.456A1.999 1.999 0 0 1 4.732 11h5.536a2.01 2.01 0 0 1 .732-.732V3.5a.5.5 0 0 0-.5-.5h-9a.5.5 0 0 0-.5.5v7a.5.5 0 0 0 .294.456zM12 10a2 2 0 0 1 1.732 1h.768a.5.5 0 0 0 .5-.5V8.35a.5.5 0 0 0-.11-.312l-1.48-1.85A.5.5 0 0 0 13.02 6H12v4zm-9 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm9 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
            </svg>
            <?php echo $delivery_text; ?>
        </div>
        <div class="p-3 border mt-3">
            <p><?php echo $delivery; ?></p>
        </div>
    </div>
    <div class="col-md-6 mt-3">
        <div class="bg-light p-3 text-uppercase">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-credit-card mb-1 mr-2" viewBox="0 0 16 16">
                <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v1h14V4a1 1 0 0 0-1-1H2zm13 4H1v5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V7z"/>
                <path d="M2 10a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1z"/>
            </svg>
            <?php echo $payment_text; ?>
        </div>
        <div class="p-3 border mt-3">
            <p><?php echo $payment; ?></p>
        </div>
    </div>
</div>

<div class="mt-5 bg-light p-3 text-uppercase">
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-chat-square-text mb-1 mr-2" viewBox="0 0 16 16">
        <path d="M14 1a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1h-2.5a2 2 0 0 0-1.6.8L8 14.333 6.1 11.8a2 2 0 0 0-1.6-.8H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h2.5a1 1 0 0 1 .8.4l1.9 2.533a1 1 0 0 0 1.6 0l1.9-2.533a1 1 0 0 1 .8-.4H14a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
        <path d="M3 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 6a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 6zm0 2.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z"/>
    </svg>
    <?php echo $comment_text; ?>
</div>

<div class="row">
    <div class="col-md-12 mt-3">
        <div class="p-3 border">
            <?php echo $comment; ?></p>
        </div>
    </div>
</div>

<div class="mt-5 bg-light p-3 text-uppercase">
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-cart-check mb-1 mr-2" viewBox="0 0 16 16">
        <path d="M11.354 6.354a.5.5 0 0 0-.708-.708L8 8.293 6.854 7.146a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"/>
        <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
    </svg>
    <?php echo $orders_text; ?>
</div>

<div class="table-responsive mt-3">
    <?php echo $orders; ?>
</div>
<br>

<hr>
    
<p>
    <?php echo '<b>' . $total_text . '</b> ' . $total . $currency; ?>
</p>
