<?php

if (isset($_POST['id']) && isset($_POST['lang']) && isset($_POST['elang'])) {
    
    include_once '../controllers/records.php';
    
    if (recordClone($_POST['id'], 'blog', $_POST['lang'], $_POST['elang'])) {
        
        // Переадресация на первую страницу со списком записей
        echo '<script>self.location="editor/index.php?page=blog&lang=' . $_POST['lang'] . '&elang=' . $_POST['elang'] . '";</script>';
    }
    else {
        
        // Перезагрузка страницы с записями
        echo '<script>location.reload();</script>';
    }
}
else {
    
    // Перезагрузка страницы с записями
    echo '<script>location.reload();</script>';
}
