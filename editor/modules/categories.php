<?php

// Подключение к БД
include 'model/categories.php';

if(isset($_GET['epage'])) {
    
    foreach ($db_categories as $line => $column) {
        
        if($_GET['epage'] == $column['url']) {
            
            $page_id = $column['id'];
            $page_page_layout = $column['page_layout'];
            $page_url = $column['url'];
            $page_title = $column['title'];
            $page_keywords = $column['keywords'];
            $page_description = $column['description'];
            $page_h1 = $column['h1'];
            break;
        } 
    }
}

// Блок загрузки контента страниц (Верхняя часть от списка)
$path_to_content = '../' . DB_DIR_PATH . $elang . '/pages/' . $page_url . '.tpl';

if (file_exists($path_to_content)) {
    $content = file_get_contents($path_to_content);
}
 else {
    $content = 'no data...';
}

// Блок загрузки контента страниц (Нижняя часть от списка)
$path_to_content = '../' . DB_DIR_PATH . $elang . '/categories/' . $page_url . '.tpl';

if (file_exists($path_to_content)) {
    $content2 = file_get_contents($path_to_content);
}
 else {
    $content2 = 'no data...';
}
?>

<form class="mt-5" method="POST" id="pageSaveForm" action="javascript:void(null);" onsubmit="savePage()">

    <div class="form-group mb-4">
        <label for="inputTitle"><b><?php echo $input_title; ?></b></label>
        <input type="text" class="form-control" name="inputtitle" id="inputTitle" placeholder="<?php echo $input_title_placeholder; ?>" value="<?php echo $page_title; ?>" required>
        <small id="passwordHelpBlock" class="form-text text-muted">
            <?php echo $title_recommended; ?>
        </small>
    </div>

    <div class="form-group mb-4">
        <label for="inputH1"><b><?php echo $input_h1; ?></b></label>
        <input type="text" class="form-control" name="inputh1" id="inputH1" placeholder="<?php echo $input_h1_placeholder; ?>" value="<?php echo $page_h1; ?>" required>
    </div>

    <div class="form-row mb-4">
        <div class="form-group col-md-8">
            <label for="inputURL"><b><?php echo $input_url; ?></b></label>
            <input type="text" class="form-control" name="inputurl" id="inputURL" placeholder="<?php $input_url_placeholder; ?>" value="<?php echo $page_url; ?>" readonly>
            <small id="passwordHelpBlock" class="form-text text-muted">
                <?php echo $input_url_recommended; ?>
            </small>
        </div>
        <div class="form-group col-md-4">
            <label for="inputPageLayout"><b><?php echo $input_page_layout; ?></b></label>
            <select id="inputPageLayout" class="form-control" name="inputpagelayout">
                <option selected><?php echo $page_page_layout; ?></option>
            </select>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="KeywordsTextarea"><b><?php echo $keywords_textarea; ?></b></label>
            <textarea class="form-control" id="KeywordsTextarea" name="keywordstextarea" placeholder="<?php echo $keywords_textarea_placeholder; ?>" rows="3" required><?php echo $page_keywords; ?></textarea>
        </div>
        <div class="form-group col-md-6">
            <label for="DescriptionTextarea"><b><?php echo $description_textarea; ?></b></label>
            <textarea class="form-control" id="DescriptionTextarea" name="descriptiontextarea" placeholder="<?php echo $description_textarea_placeholder; ?>" rows="3" required><?php echo $page_description; ?></textarea>
            <small id="passwordHelpBlock" class="form-text text-muted">
                <?php echo $description_textarea_recommended; ?>
            </small>
        </div>
    </div>
    <br>
    <br>
    
    <div id="editorWYSIWYG">
        <p><b><?php echo $content_textarea; ?></b></p>
        <textarea class="summernote" name="editordata"><?php echo $content; ?></textarea>
        <br>
        <br>
        <p><b><?php echo $content_textarea_2; ?></b></p>
        <textarea class="summernote" name="editordata2"><?php echo $content2; ?></textarea>
    </div>
    
    <input type="hidden" name="id" value="<?php echo $page_id; ?>">
    <input type="hidden" name="lang" value="<?php echo $lang; ?>">
    <input type="hidden" name="elang" value="<?php echo $elang; ?>">
    
    <div class="text-center mt-5">        
        <button type="submit" class="btn btn-primary mt-3 mr-2"><?php echo $save_button; ?></button>
        <button type="button" class="btn btn-danger mt-3" data-toggle="modal" data-target="#pageDelete"><?php echo $delete; ?></button>
    </div>

</form>

<script>
    function savePage() {
        var msg   = $('#pageSaveForm').serialize();
        $.ajax({
            type: 'POST',
            url: 'editor/modules/save_category.php',
            data: msg,
            success: function(data) {
                $('#pageSaveResault').html(data);
                $('#pageSaveInfo').modal('show');
            },
            error:  function(xhr, str){
                alert('Error: ' + xhr.responseCode);
            }
        });
    }
</script>

<!-- Run summernote
Run the script below when document is ready! -->
<script>
    $('.summernote').summernote( {
        height: 450,
        codemirror: { // codemirror options
            mode: 'text/html',
            htmlMode: true,
            lineNumbers: true,
            theme: 'monokai'
            },
        <?php
        if ($lang == 'ru') { ?>
            lang: 'ru-RU' // По умолчанию en-US
        <?php } ?>
    } );
</script>
