<?php
// SEO
$title = 'Blog post editor | Web Platinum Studio';
$h1 = 'Blog';
$description = 'Create, edit and delete blog posts.';

// Main text
$add_new_record_text = 'Add new Record';
$delete_record_yes_no_text = 'Are you sure you want to delete the record?';
$clone_record_yes_no_text = 'A copy of the record will be created.';

// Head, meta tags
$input_title = 'Title Page';
$input_title_placeholder = 'Write the Title of the page';
$title_recommended = 'Recommended characters: 50-70.';

$input_h1 = 'Heading level 1 (H1)';
$input_h1_placeholder = 'Write H1 Headline';

$input_url = 'Webpage address (URL)';
$input_url_placeholder = 'example-page';
$input_url_recommended = 'Use only the letters "abc" and the delimiter character "-". Without spaces.';

$input_page_layout = 'Page Layout';

$keywords_textarea = 'Keywords';
$keywords_textarea_placeholder = 'keyword1, keyword2...';

$description_textarea = 'Description of the page';
$description_textarea_placeholder = 'Description...';
$description_textarea_recommended = 'Recommended characters: 160-260.';


// Record
$main_tab_text = 'Main';
$data_tab_text = 'Data';
$images_tab = 'Images';

$record_author_text = 'Author Name';
$record_author_type_text = 'Author';
$organization_text = 'Organization';
$person_text = 'Person';
$record_date_text = 'Время и дата первой публикации';
$record_mod_date_text = 'Время и дата последних изменений';

$main_image_text = 'Main image';
$edit_btn_title = 'Edit';
$clear_btn_title = 'Clear';
$item_image_alt_text = 'Main Image Alt';
$additional_images = 'Additional images';


// Content
$content = '
        <p>content...</p>
    ';
