<?php
// SEO
$title = 'WPbit Editor - Dashboard';
$h1 = 'WPbit Editor (Dashboard)';
$description = 'Content management system and content editing for the site from Web Platinum Studio';

// Main text
$total_main_pages_text = 'Total main pages';
$total_blog_posts_text = 'Total Blog Posts';
$total_items_text = 'Total products';
$total_orders_text = 'Total orders';
$project_support_text = 'Project support: ';
$quick_links_text = 'Quick links';

// Buttons
$add_new_page_text = 'Edit page';
$add_new_blog_post_text = 'Add blog post';
$add_new_product_text = 'Add product';

// Content
$content = '
        <p>content...</p>
    ';
