<?php
// SEO
$title = 'Main page editor | Web Platinum Studio';
$h1 = 'Main pages';
$description = 'Creating, editing and deleting the main pages of the site';

// Main text
$add_new_page_text = 'Add new page';
$delete_page_yes_no_text = 'Are you sure you want to delete the page?';

// Head, meta tags
$input_title = 'Title Page';
$input_title_placeholder = 'Write the Title of the page';
$title_recommended = 'Recommended characters: 50-70.';

$input_h1 = 'Heading level 1 (H1)';
$input_h1_placeholder = 'Write H1 Headline';

$input_url = 'Webpage address (URL)';
$input_url_placeholder = 'example-page';
$input_url_recommended = 'Use only the letters "abc" and the delimiter character "-". Without spaces.';

$input_page_layout = 'Page Layout';

$keywords_textarea = 'Keywords';
$keywords_textarea_placeholder = 'keyword1, keyword2...';

$description_textarea = 'Description of the page';
$description_textarea_placeholder = 'Description...';
$description_textarea_recommended = 'Recommended characters: 160-260.';

// Content
$content = '
        <p>content...</p>
    ';
