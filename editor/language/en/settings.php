<?php
// SEO
$title = 'Main settings | Web Platinum Studio';
$h1 = 'Settings';
$description = 'Main CMS parameters and site settings';

// Main text
$http_protocol_text = 'HTTP data transfer protocol';
$domain_name_text = 'Domain (site.com or www.site.com)';
$home_page_text = 'Home page (SEO URL)';
$main_email_text = 'Main Email';
$og_image_text = 'Image (url) for OpenGraph';
$time_zone_text = 'Time Zone';
$gtag_id_text = 'ID Google Analytics';


//Content
$content = '
        <p>content...</p>
    ';
