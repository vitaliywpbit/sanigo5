<?php
// SEO
$title = 'Order editor | Web Platinum Studio';
$h1 = 'Orders';
$description = 'Create, edit and delete orders.';

// Main text
$order_text = 'Order #: ';
$customer_text = 'Customer: ';
$total_text = 'Total: ';
$currency = ' UAH.';

$personal_data_text = 'Personal Data';
$first_name_text = 'First Name: ';
$last_name_text = 'Last Name: ';
$phone_text = 'Phone: ';
$email_text = 'E-mail: ';
$subscription_text = 'Subscription: ';
$region_text = 'Region: ';
$city_text = 'City: ';
$address_text = 'Address: ';
$delivery_text = 'Delivery';
$payment_text = 'Payment';
$comment_text = 'Comment';
$orders_text = 'Orders';

//Content
$content = '
        <p>content...</p>
    ';
