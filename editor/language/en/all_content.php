<?php

// Top nav
$brand = 'WPbit';
$log_out = 'Log out';
$not_log_in = 'Please sign in';

// Content
$lang_text = 'Language';
$back_to_list_text = 'Back to list';
$back_to_editing_text = 'Back to editing';
$delete_text = 'Deleting';
$delete = 'Delete';
$edit = 'Edit';
$save_button = 'Save';
$saved_text = 'Saved successfully';
$save_error_text = 'Save Error';
$saving_process_text = 'Saving process...';
$image_manager_text = 'Image manager';
$upload_image_text = 'Upload New Image';
$clone_text = 'Clone';
$select_file_text = 'Browse file';

// Errors
$global_save_page_err = '
    <ul class="p-2 mb-0 small">
        <li>make sure the fields are filled in correctly</li>
        <li>it is possible that one of the fields is empty</li>
    </ul>
';
$page_not_available = 'The page has been removed or the URL has been changed!';
$page_off = 'Module not connected';

// Версия редактора
$editor_ver = '2021 WPbit-Editor';
