<?php

// Errors
$field_error = '<strong> Error!</strong> One of the fields is incorrect.';
$wrong_login_or_password = '<strong> Login failed!</strong> Incorrect username or password.';
