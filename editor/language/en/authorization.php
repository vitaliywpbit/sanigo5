<?php
//SEO
$title = 'Sign in | WPbit Editor';
$h1 = null;
$description = 'Authorization in the content management system WPbit Editor';

//Form
$sign_in = 'Login form';
$sign_in_btn = 'Sign in';
$email_placeholder = 'Email';
$pass_placeholder = 'Password';

//Content
$content = null;
