<?php
// SEO
$title = 'Редактирование основных страниц сайта | Web Platinum Studio';
$h1 = 'Основные страницы';
$description = 'Создание, редактирование и удаление основных страниц сайта';

// Main text
$add_new_page_text = 'Добавить страницу';
$delete_page_yes_no_text = 'Вы уверены, что хотите удалить страницу?';

// Head, meta tags
$input_title = 'Заголовок страницы (Title)';
$input_title_placeholder = 'Напишите заголовок страницы (Title)';
$title_recommended = 'Рекомендуемое количество символов: 50-70.';

$input_h1 = 'Заголовок первого уровня (H1)';
$input_h1_placeholder = 'Напишите заголовок H1';

$input_url = 'ЧПУ (адрес страницы URL)';
$input_url_placeholder = 'example-page';
$input_url_recommended = 'Используйте только буквы латинского алфавита (abc) и символ резделительной черты "-". Без пробелов.';

$input_page_layout = 'Макет страницы';

$keywords_textarea = 'Ключевые слова (Keywords)';
$keywords_textarea_placeholder = 'keyword1, keyword2...';

$description_textarea = 'Описание страницы (Description)';
$description_textarea_placeholder = 'Описание...';
$description_textarea_recommended = 'Рекомендуемое количество символов: 160-260.';

//Content
$content = '
        <p>content...</p>
    ';
