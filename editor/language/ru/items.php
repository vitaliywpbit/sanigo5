<?php
// SEO
$title = 'Редактор товаров | Web Platinum Studio';
$h1 = 'Товары';
$description = 'Создание, редактирование и удаление товаров.';

// Main text
$add_new_item_text = 'Добавить товар';
$delete_item_yes_no_text = 'Вы уверены, что хотите удалить товар?';
$clone_item_yes_no_text = 'Будет создана копия карточки товара.';

// Head, meta tags
$input_title = 'Заголовок страницы (Title)';
$input_title_placeholder = 'Напишите заголовок страницы (Title)';
$title_recommended = 'Рекомендуемое количество символов: 50-70.';

$input_h1 = 'Заголовок первого уровня (H1)';
$input_h1_placeholder = 'Напишите заголовок H1';

$input_url = 'ЧПУ (адрес страницы URL)';
$input_url_placeholder = 'example-page';
$input_url_recommended = 'Используйте только буквы латинского алфавита (abc) и символ резделительной черты "-". Без пробелов.';

$input_page_layout = 'Макет страницы';

$keywords_textarea = 'Ключевые слова (Keywords)';
$keywords_textarea_placeholder = 'keyword1, keyword2...';

$description_textarea = 'Описание страницы (Description)';
$description_textarea_placeholder = 'Описание...';
$description_textarea_recommended = 'Рекомендуемое количество символов: 160-260.';

$short_description_textarea = 'Короткое описание';
$short_description_textarea_recommended = 'Это скрытое описание или набор ключевых фраз для живого поиска по сайту';

// Store
$instock_text = 'В наличии';
$outofstock_text = 'Нет в наличии';
$preorder_text = 'Предзаказ';

$main_tab_text = 'Основное';
$data_tab_text = 'Данные';
$images_tab = 'Изображения';

$item_category_text = 'Категория товара';
$brand_text = 'Бренд: ';
$sku_text = 'Код (SKU): ';
$price_text = 'Цена: ';
$discount_price_text = 'Цена со скидкой';
$availability_status_text = 'Статус на складе';
$main_image_text = 'Основное изображение';
$edit_btn_title = 'Редактировать';
$clear_btn_title = 'Очистить';
$item_image_alt_text = 'Атрибут Alt для основного изображения';
$additional_images = 'Дополнительные изображения';

//Content
$content = '
        <p>content...</p>
    ';
