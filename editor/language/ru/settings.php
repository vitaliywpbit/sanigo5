<?php
// SEO
$title = 'Основные настройки | Web Platinum Studio';
$h1 = 'Настройки';
$description = 'Основные параметры CMS  и настройки сайта';

// Main text
$http_protocol_text = 'Протокол передачи данных';
$domain_name_text = 'Домен (site.com или www.site.com)';
$home_page_text = 'Домашняя страница (SEO URL)';
$main_email_text = 'Основной Email';
$og_image_text = 'Изображение (url) для OpenGraph';
$time_zone_text = 'Часовой пояс';
$gtag_id_text = 'Код Google Analytics';


//Content
$content = '
        <p>content...</p>
    ';
