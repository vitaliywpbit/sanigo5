<?php
// SEO
$title = 'Редактор заказов | Web Platinum Studio';
$h1 = 'Заказы';
$description = 'Создание, редактирование и удаление заказов.';

// Main text
$order_text = 'Заказ #: ';
$customer_text = 'Покупатель: ';
$total_text = 'Итого: ';
$currency = ' грн.';

$personal_data_text = 'Данные клиента';
$first_name_text = 'Имя: ';
$last_name_text = 'Фамилия: ';
$phone_text = 'Телефон: ';
$email_text = 'E-mail: ';
$subscription_text = 'Подписка: ';
$region_text = 'Регион: ';
$city_text = 'Город: ';
$address_text = 'Адрес: ';
$delivery_text = 'Доставка';
$payment_text = 'Оплата';
$comment_text = 'Комментарий';
$orders_text = 'Покупки';

//Content
$content = '
        <p>content...</p>
    ';
