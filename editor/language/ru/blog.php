<?php
// SEO
$title = 'Редактор записей в блоге | Web Platinum Studio';
$h1 = 'Блог';
$description = 'Создание, редактирование и удаление записей в блоге.';

// Main text
$add_new_record_text = 'Добавить запись';
$delete_record_yes_no_text = 'Вы уверены, что хотите удалить запись?';
$clone_record_yes_no_text = 'Будет создана копия записи.';

// Head, meta tags
$input_title = 'Заголовок страницы (Title)';
$input_title_placeholder = 'Напишите заголовок страницы (Title)';
$title_recommended = 'Рекомендуемое количество символов: 50-70.';

$input_h1 = 'Заголовок первого уровня (H1)';
$input_h1_placeholder = 'Напишите заголовок H1';

$input_url = 'ЧПУ (адрес страницы URL)';
$input_url_placeholder = 'example-page';
$input_url_recommended = 'Используйте только буквы латинского алфавита (abc) и символ резделительной черты "-". Без пробелов.';

$input_page_layout = 'Макет страницы';

$keywords_textarea = 'Ключевые слова (Keywords)';
$keywords_textarea_placeholder = 'keyword1, keyword2...';

$description_textarea = 'Описание страницы (Description)';
$description_textarea_placeholder = 'Описание...';
$description_textarea_recommended = 'Рекомендуемое количество символов: 160-260.';


// Record
$main_tab_text = 'Основное';
$data_tab_text = 'Данные';
$images_tab = 'Изображения';

$record_author_text = 'Имя автора';
$record_author_type_text = 'Автор статьи';
$organization_text = 'Организация';
$person_text = 'Человек';
$record_date_text = 'Время и дата первой публикации';
$record_mod_date_text = 'Время и дата последних изменений';

$main_image_text = 'Основное изображение';
$edit_btn_title = 'Редактировать';
$clear_btn_title = 'Очистить';
$item_image_alt_text = 'Атрибут Alt для основного изображения';
$additional_images = 'Дополнительные изображения';

//Content
$content = '
        <p>content...</p>
    ';
