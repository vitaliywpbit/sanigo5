<?php

// Top nav
$brand = 'WPbit';
$log_out = 'Выйти';
$not_log_in = 'Вам необходимо авторизоваться';

// Content
$lang_text = 'Язык';
$back_to_list_text = 'Назад к списку';
$back_to_editing_text = 'Вернуться к редактированию';
$delete_text = 'Удаление';
$delete = 'Удалить';
$edit = 'Редактировать';
$save_button = 'Сохранить';
$saved_text = 'Страница сохранена';
$save_error_text = 'Ошибка при сохранении';
$saving_process_text = 'Сохранение...';
$image_manager_text = 'Менеджер изображений';
$upload_image_text = 'Загрузить изображение';
$clone_text = 'Клонировать';
$select_file_text = 'Выбрать файл';

// Errors
$global_save_page_err = '
    <ul class="p-2 mb-0 small">
        <li>убедитесь, что все необходимые поля заполнены верно</li>
        <li>возможно, что одно из полей осталось пустым</li>
    </ul>
';
$page_not_available = 'Страница удалена или ее URL адрес был изменен!';
$page_off = 'Модуль не подключен';

// Версия редактора
$editor_ver = '2021 WPbit-Editor';
