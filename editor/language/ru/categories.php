<?php
// SEO
$title = 'Редактирование категорий сайта | Web Platinum Studio';
$h1 = 'Категории';
$description = 'Создание, редактирование и удаление основных категорий сайта';

// Main text
$add_new_category_text = 'Добавить категорию';
$delete_category_yes_no_text = 'Вы уверены, что хотите удалить категорию?';

// Head, meta tags
$input_title = 'Заголовок страницы (Title)';
$input_title_placeholder = 'Напишите заголовок страницы (Title)';
$title_recommended = 'Рекомендуемое количество символов: 50-70.';

$input_h1 = 'Заголовок первого уровня (H1)';
$input_h1_placeholder = 'Напишите заголовок H1';

$input_url = 'ЧПУ (адрес страницы URL)';
$input_url_placeholder = 'example-page';
$input_url_recommended = 'Используйте только буквы латинского алфавита (abc) и символ резделительной черты "-". Без пробелов.';

$input_page_layout = 'Макет страницы';

$keywords_textarea = 'Ключевые слова (Keywords)';
$keywords_textarea_placeholder = 'keyword1, keyword2...';

$description_textarea = 'Описание страницы (Description)';
$description_textarea_placeholder = 'Описание...';
$description_textarea_recommended = 'Рекомендуемое количество символов: 160-260.';

$content_textarea = 'Верхняя часть контента от списка';
$content_textarea_2 = 'Нижняя часть контента от списка';

//Content
$content = '
        <p>content...</p>
    ';
