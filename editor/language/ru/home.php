<?php
// SEO
$title = 'WPbit Editor - Обзор';
$h1 = 'WPbit Editor (Обзор)';
$description = 'Панель управление и редактирование контента на сайте от Web Platinum Studio';

// Main text
$total_main_pages_text = 'Всего основных страниц';
$total_blog_posts_text = 'Всего записей в Блоге';
$total_items_text = 'Всего товаров';
$total_orders_text = 'Всего заказов';
$project_support_text = 'Поддержка проекта: ';
$quick_links_text = 'Быстрые ссылки';

// Buttons
$add_new_page_text = 'Редактировать страницу';
$add_new_blog_post_text = 'Добавить запись';
$add_new_product_text = 'Добавить товар';


// Content
$content = '
        <p>content...</p>
    ';
