<?php
// echo '<br>Пароль для входа: ' . md5(md5('xxxxxxxxx' . 'WP19&Bit')) . '<br>';

// Основной файл конфигурации сайта
require_once '../config.php';

// Чтение и обработка URL из браузерной строки
$url = explode("/", $_SERVER['REQUEST_URI']);

// Определение основного адреса (URL) для всех относительных адресов (URLs)
foreach ($url as $url_pieces) {
    if ($url_pieces == DOMAIN_NAME) {
        $base_url = '/' . DOMAIN_NAME . '/'; // base URL для localhost
        break;
    }
    else {
        $base_url = HTTP_PROTOCOL . DOMAIN_NAME . '/'; // base URL
    }
}

// Блок загрузки основных настроек для редактора
include 'components/setup.php';

// Какую по счету страницу загружать с множеством страниц
if(isset($_GET['p'])) {
    $page_num = $_GET['p'];
}
 else {
    $page_num = 1;
}

// Проверка доступных языков для редактора и язык по умолчанию
include 'components/lang.php';

// На каком языке загружать редактор
if(isset($_GET['lang'])) {
    $lang = $_GET['lang'];
}
 else {
    $lang = $lang_default;
}

// Остальной контент на нужном языке
include 'language/' . $lang . '/all_content.php';

// Имя активного языка
foreach ($db_lang as $line => $column) {
    if ($column['lang_code'] == $lang) {
        $active_lang_name = $column['lang_name'];
        break;
    }
    else {
        $active_lang_name = 'Language';
    }
}

// Подключение контроллера для Авторизации, проверки логина и пароля
include_once 'controllers/access.php';

// Получен ли доступ к страницам 
if (!userIsLoggedIn()) {
    $page = 'authorization';
}
else {
    if(isset($_GET['page'])) {
        if ($_GET['page'] == 'home') {
            $page = $page_default;
        }
        else {
            $page = $_GET['page'];
        }
    }
    else {
         $page = $page_default;
    }
}

// Проверка существующих страниц
$page_file = 'page_layout/' . $page . '.php';
if (file_exists($page_file)) {
    $page = $page;
}
else {
    $page = '404';
}

// Загрузка SEO контента для страниц редактора
include 'language/' . $lang . '/' . $page . '.php';
    if(isset($title) == null) {
        $title = 'Page title';
    }
    if(isset($title) == null) {
        $description = 'Page description';
    }
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
    <head>
        <meta charset="utf-8">
        <base href="<?php echo $base_url; ?>">
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="<?php echo $description; ?>">
        
        <link rel="icon" href="editor/images/favicon-32x32.png">

        <!-- Editor and Bootstrap CSS -->
        <link href="editor/css/styles.css" rel="stylesheet">
        <!-- Styles for editor from site -->
        <link href="css/stylesforeditor.css" rel="stylesheet">
        <!--Bootstrap / JQUERY / POPPER JS -->
        <script src="editor/js/wpbit.js"></script>
        
        <!-- codemirror -->
        <link rel="stylesheet" type="text/css" href="editor/codemirror/5.58.1/codemirror.css" />
        <link rel="stylesheet" href="editor/codemirror/5.58.1/blackboard.css">
        <link rel="stylesheet" href="editor/codemirror/5.58.1/monokai.css">
        <script type="text/javascript" src="editor/codemirror/5.58.1/codemirror.js"></script>
        <script src="editor/codemirror/5.58.1/xml.js"></script>

        <!-- include summernote css/js -->
        <link href="editor/summernote/summernote-bs4.css" rel="stylesheet">
        <script src="editor/summernote/summernote-bs4.js"></script>
        <!-- include summernote-ru-RU -->
        <script src="editor/summernote/lang/summernote-ru-RU.js"></script>
        <!-- custom file input -->
        <script src="editor/js/bs-custom-file-input.min.js"></script>
    </head>

    <body>        
        <?php include 'page_layout/' . $page . '.php'; ?>
    </body>
    
    <!--YOUTUBE OPTIMIZATION FOR VIDEO IN EDITOR-->
    <script>
    'use strict';
    function r(f){/in/.test(document.readyState)?setTimeout('r('+f+')',9):f()}
    r(function(){
        if (!document.getElementsByClassName) {
            // Поддержка IE8
            var getElementsByClassName = function(node, classname) {
                var a = [];
                var re = new RegExp('(^| )'+classname+'( |$)');
                var els = node.getElementsByTagName("*");
                for(var i=0,j=els.length; i<j; i++)
                    if(re.test(els[i].className))a.push(els[i]);
                return a;
            }
            var videos = getElementsByClassName(document.body,"youtube");
        } else {
            var videos = document.getElementsByClassName("youtube");
        }

        var nb_videos = videos.length;
        for (var i=0; i<nb_videos; i++) {
            // Находим постер для видео, зная ID нашего видео
            videos[i].style.backgroundImage = 'url(http://i.ytimg.com/vi/' + videos[i].id + '/sddefault.jpg)';

            // Размещаем над постером кнопку Play, чтобы создать эффект плеера
            var play = document.createElement("div");
            play.setAttribute("class","play");
            videos[i].appendChild(play);

            videos[i].onclick = function() {
                // Создаем iFrame и сразу начинаем проигрывать видео, т.е. атрибут autoplay у видео в значении 1
                var iframe = document.createElement("iframe");
                var iframe_url = "http://www.youtube.com/embed/" + this.id + "?rel=0&autoplay=1";
                if (this.getAttribute("data-params")) iframe_url+='&'+this.getAttribute("data-params");
                iframe.setAttribute("src",iframe_url);
                iframe.setAttribute("frameborder",'0');
                iframe.setAttribute("allowfullscreen","allowfullscreen");
                iframe.setAttribute("class",'embed-responsive-item');

                // Заменяем начальное изображение (постер) на iFrame
                this.parentNode.replaceChild(iframe, this);
            }
        }
    });
    </script>
        
    <!-- BUTTON UP -->
    <div id="toTopEditor" >^</div>
    <script>
        $(function() {
            $(window).scroll(function() {
                if($(this).scrollTop() != 0) {
                    $('#toTopEditor').fadeIn();
                } else {
                    $('#toTopEditor').fadeOut();
                }
            });
            $('#toTopEditor').click(function() {
                $('body,html').animate({scrollTop:0},800);
            });
        });
    </script>
</html>
