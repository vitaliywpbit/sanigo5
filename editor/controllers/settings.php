<?php

// Основной файл конфигурации сайта
require_once '../../config.php';

// Функция сохранения Настроек сайта
function settingsSave($http_protocol, $domain, $default_page, $email, $og_image, $time_zone, $gtag) {
    
    // Подключение к БД
    $settings_file_path = '../../' . DB_DIR_PATH . '/settings.csv';
    $settings = fopen($settings_file_path, "w") or die('Error Establishing a Database Connection...');
    
    if($settings) {
        
        $db_settings[] = ['HTTP' => $http_protocol];
        $db_settings[] = ['Domain' => trim($domain)];
        $db_settings[] = ['Default_Page' => trim($default_page)];
        $db_settings[] = ['Email' => trim($email)];
        $db_settings[] = ['OG_Image' => trim($og_image)];
        $db_settings[] = ['Time_Zone' => $time_zone];
        $db_settings[] = ['GTag_ID' => trim($gtag)];
        
        flock($settings, LOCK_EX); // блокируем файл

        foreach ($db_settings as $line => $column) {
            fputcsv($settings, $column);
        }

        flock($settings, LOCK_UN); // снимаем блокировку
        
        fclose($settings);
        return true;
    }
    else {
        fclose($pages);
        return false;
    }
}
