<?php

function userIsLoggedIn() {
    include 'language/' . $GLOBALS['lang'] . '/access.php';
  
    if (isset($_POST['action']) and $_POST['action'] == 'login') {
    
        if (!isset($_POST['email']) or $_POST['email'] == '' or 
            !isset($_POST['password']) or $_POST['password'] == '') {
                $GLOBALS['loginError'] = $field_error;
                return false;
            }

    $password = md5(md5($_POST['password'] . 'WP19&Bit'));

    if (databaseContainsUsers($_POST['email'], $password)) {
      session_start();
      $_SESSION['loggedIn'] = true;
      $_SESSION['email'] = $_POST['email'];
      $_SESSION['password'] = $password;
      return true;     
    }
    else {
      session_start();
      unset($_SESSION['loggedIn']);
      unset($_SESSION['email']);
      unset($_SESSION['password']);
      $GLOBALS['loginError'] = $wrong_login_or_password;
      return false;
    }
  }

  if (isset($_POST['action']) and $_POST['action'] == 'logout') {
    session_start();
    unset($_SESSION['loggedIn']);
    unset($_SESSION['email']);
    unset($_SESSION['password']);
    header('Location: ' . 'index.php?lang=' . $_POST['lang']);
    exit();
  }

  session_start();
  if (isset($_SESSION['loggedIn'])) {
    return databaseContainsUsers($_SESSION['email'], $_SESSION['password']);
  }
}

//Функция, есть ли в базе пользователь и пароль, которые он ввел
function databaseContainsUsers($email, $password) {
    
//Подключение файла с пользователями
    include 'DB/users.php';
    
    foreach ($users as $user_email => $user_pass) {
        if ($email == $user_email and $password == $user_pass) {
            $user_find = true;
            break;
        }
        else {
            $user_find = false;
        }
    }
    if($user_find == true) {
        return true;
    }
    else {
        return false;
    }
}