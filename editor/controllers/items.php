<?php

// Основной файл конфигурации сайта
require_once '../../config.php';

// Узнаем seo-url определенной категории
function getCategorySeoUrl($category_id, $elang) {
    
    $pages = fopen('../../' . DB_DIR_PATH . $elang . '/pages.csv', "rt") or die("Error Establishing a Database Connection...");

    if($pages) {

        flock($pages, LOCK_EX); // блокируем файл

        for ($i = 0; $data = fgetcsv($pages, 0, ","); $i++) {

            if ($data[0] == $category_id) {
                
                $category_seo_url = $data[2];
                break;
            }
            else {
                $category_seo_url = null;
            }
        }

        flock($pages, LOCK_UN); // снимаем блокировку
    }

    fclose($pages);
    return $category_seo_url;
}

// Проверка SEO URL страницы на дубликат по ID кроме самой себя
function duplicateSeoUrl($id, $seo_url, $file_name, $elang) {
    
    // Подключение к БД
    $file_path = '../../' . DB_DIR_PATH . $elang . '/' . $file_name . '.csv';
    $pages = fopen($file_path, "rt") or die('Error Establishing a Database Connection...');
    
    if($pages) {
        
        flock($pages, LOCK_EX); // блокируем файл
        
        for ($i = 0; $data = fgetcsv($pages, 0, ","); $i++) {
            
            if ($data[0] != $id && $data[2] == $seo_url) {
                
                return true;
            }
            
        }
        
        flock($pages, LOCK_UN); // снимаем блокировку
        fclose($pages);
    }
    
    return false;
}

// Функция создания нового товара
function itemAdd($elang,
        $page_id, 
        $page_layout, 
        $seo_url, 
        $title, 
        $keywords, 
        $description, 
        $h1, 
        $editordata, 
        $category_id, 
        $main_image_1x1, 
        $main_image_4x3, 
        $main_image_16x9, 
        $main_image_preview,
        $main_image_alt,
        $additional_images,
        $shortdescription,
        $price,
        $discountprice,
        $sku,
        $availability,
        $brand
    ) {
    
    // Подключение к БД
    $items_file_path = '../../' . DB_DIR_PATH . $elang . '/items.csv';
    $items = fopen($items_file_path, "rt") or die('Error Establishing a Database Connection...');
    
    if($items) {
        
        $seo_url = trim($seo_url);
                
        // Проверим на дубликат SEO URL
        if (duplicateSeoUrl($page_id, $seo_url, 'items', $elang)) {

            $new_seo_url = $seo_url . '-' . $page_id;
        }
        else {
            $new_seo_url = $seo_url;
        }
        
        flock($items, LOCK_EX); // блокируем файл
        
        for ($i = 0; $data = fgetcsv($items, 0, ","); $i++) {
                
            $db_items[] = [
                'id' => trim($data[0]),
                'page_layout' => trim($data[1]),
                'url' => trim($data[2]),
                'title' => trim($data[3]),
                'keywords' => trim($data[4]),
                'description' => trim($data[5]),
                'h1' => trim($data[6]),
                'category_id' => trim($data[7]),
                'main_image_1x1' => trim($data[8]),
                'main_image_4x3' => trim($data[9]),
                'main_image_16x9' => trim($data[10]),
                'main_image_preview' => trim($data[11]),
                'main_image_alt' => trim($data[12]),
                'additional_images' => trim($data[13]),
                'shortdescription' => trim($data[14]),
                'price' => trim($data[15]),
                'discountprice' => trim($data[16]),
                'sku' => trim($data[17]),
                'availability' => trim($data[18]),
                'brand' => trim($data[19]),
            ];
            
            $final_id = trim($data[0]);
        }
        
        flock($items, LOCK_UN); // снимаем блокировку
        fclose($items);
        
        $db_items[] = [
            'id' => $final_id + 1,
            'page_layout' => trim($page_layout),
            'url' => $new_seo_url,
            'title' => trim(str_replace('"', '&quot;', $title)),
            'keywords' => trim(str_replace('"', '&quot;', $keywords)),
            'description' => trim(str_replace('"', '&quot;', $description)),
            'h1' => trim(str_replace('"', '&quot;', $h1)),
            'category_id' => $category_id,
            'main_image_1x1' => $main_image_1x1,
            'main_image_4x3' => $main_image_4x3,
            'main_image_16x9' => $main_image_16x9,
            'main_image_preview' => $main_image_preview,
            'main_image_alt' => trim(str_replace('"', '&quot;', $main_image_alt)),
            'additional_images' => $additional_images,
            'shortdescription' => trim(str_replace('"', '&quot;', $shortdescription)),
            'price' => trim($price),
            'discountprice' => trim($discountprice),
            'sku' => trim(str_replace('"', '&quot;', $sku)),
            'availability' => trim($availability),
            'brand' => trim(str_replace('"', '&quot;', $brand)),
        ];
        
        // Запись новых данных в БД товара
        // ===============================
        $items = fopen($items_file_path, "w") or die('Error Establishing a Database Connection...');

        flock($items, LOCK_EX); // блокируем файл

        foreach ($db_items as $line => $column) {
            fputcsv($items, $column);
        }

        flock($items, LOCK_UN); // снимаем блокировку
        fclose($items);
        
        // Новый файл шаблона для записи
        $category = getCategorySeoUrl($category_id, $elang);
        
        $item_file = '../../' . DB_DIR_PATH . $elang . '/catalog/' . $category . '/' . $new_seo_url . '.tpl';
        
        // Открываем файл и если его не существует, создаем его
        $item_data = fopen($item_file, 'w');
        
        flock($item_data, LOCK_EX); // блокируем файл

            // Записываем данные
            if (fwrite($item_data, $editordata) === FALSE) {
                return false;
            }
            
        flock($item_data, LOCK_UN); // снимаем блокировку
        
        // Закрываем файл
        fclose($item_data);
        
        return true;
    }
    
}

// Функция сохранения страницы с товаром
function itemSave($elang, $orig_category, $orig_url,
        $page_id, 
        $page_layout, 
        $seo_url, 
        $title, 
        $keywords, 
        $description, 
        $h1, 
        $editordata, 
        $category_id, 
        $main_image_1x1, 
        $main_image_4x3, 
        $main_image_16x9, 
        $main_image_preview,
        $main_image_alt,
        $additional_images,
        $shortdescription,
        $price,
        $discountprice,
        $sku,
        $availability,
        $brand
    ) {
    
    // Подключение к БД
    $items_file_path = '../../' . DB_DIR_PATH . $elang . '/items.csv';
    $items = fopen($items_file_path, "rt") or die('Error Establishing a Database Connection...');
    
    if($items) {
        
        $seo_url = trim($seo_url);
                
        // Проверим на дубликат SEO URL
        if (duplicateSeoUrl($page_id, $seo_url, 'items', $elang)) {

            $new_seo_url = $seo_url . '-' . $page_id;
        }
        else {
            $new_seo_url = $seo_url;
        }
        
        flock($items, LOCK_EX); // блокируем файл
        
        for ($i = 0; $data = fgetcsv($items, 0, ","); $i++) {
                
            if ($data[0] == $page_id) {
                
                $db_items[] = [
                    'id' => $page_id,
                    'page_layout' => trim($page_layout),
                    'url' => $new_seo_url,
                    'title' => trim(str_replace('"', '&quot;', $title)),
                    'keywords' => trim(str_replace('"', '&quot;', $keywords)),
                    'description' => trim(str_replace('"', '&quot;', $description)),
                    'h1' => trim(str_replace('"', '&quot;', $h1)),
                    'category_id' => $category_id,
                    'main_image_1x1' => $main_image_1x1,
                    'main_image_4x3' => $main_image_4x3,
                    'main_image_16x9' => $main_image_16x9,
                    'main_image_preview' => $main_image_preview,
                    'main_image_alt' => trim(str_replace('"', '&quot;', $main_image_alt)),
                    'additional_images' => $additional_images,
                    'shortdescription' => trim(str_replace('"', '&quot;', $shortdescription)),
                    'price' => trim($price),
                    'discountprice' => trim($discountprice),
                    'sku' => trim(str_replace('"', '&quot;', $sku)),
                    'availability' => trim($availability),
                    'brand' => trim(str_replace('"', '&quot;', $brand)),
                ];
            }
            else {
                
                $db_items[] = [
                    'id' => trim($data[0]),
                    'page_layout' => trim($data[1]),
                    'url' => trim($data[2]),
                    'title' => trim($data[3]),
                    'keywords' => trim($data[4]),
                    'description' => trim($data[5]),
                    'h1' => trim($data[6]),
                    'category_id' => trim($data[7]),
                    'main_image_1x1' => trim($data[8]),
                    'main_image_4x3' => trim($data[9]),
                    'main_image_16x9' => trim($data[10]),
                    'main_image_preview' => trim($data[11]),
                    'main_image_alt' => trim($data[12]),
                    'additional_images' => trim($data[13]),
                    'shortdescription' => trim($data[14]),
                    'price' => trim($data[15]),
                    'discountprice' => trim($data[16]),
                    'sku' => trim($data[17]),
                    'availability' => trim($data[18]),
                    'brand' => trim($data[19]),
                ];
            }
        }
        
        flock($items, LOCK_UN); // снимаем блокировку
        fclose($items);
        
        // Запись новых данных в БД товара
        // ===============================
        $items = fopen($items_file_path, "w") or die('Error Establishing a Database Connection...');

        flock($items, LOCK_EX); // блокируем файл

        foreach ($db_items as $line => $column) {
            fputcsv($items, $column);
        }

        flock($items, LOCK_UN); // снимаем блокировку
        fclose($items);
        
        // Запись изменений в файл шаблона товара
        // ======================================
        $category = getCategorySeoUrl($category_id, $elang);
        
        // Удаление файла шаблона из старой категории, если произошла смена категории или URL адреса
        if ($category != $orig_category or trim($seo_url) != trim($orig_url)) {
            
            $item_file = '../../' . DB_DIR_PATH . $elang . '/catalog/' . $orig_category . '/' . trim($orig_url) . '.tpl';
            
            if (file_exists($item_file)) {
                unlink($item_file);
            }
        }
        
        // Новый файл шаблона для записи
        $item_file = '../../' . DB_DIR_PATH . $elang . '/catalog/' . $category . '/' . $new_seo_url . '.tpl';
        
        // Открываем файл и если его не существует, создаем его
        $item_data = fopen($item_file, 'w');
        
        flock($item_data, LOCK_EX); // блокируем файл

            // Записываем данные
            if (fwrite($item_data, $editordata) === FALSE) {
                return false;
            }
            
        flock($item_data, LOCK_UN); // снимаем блокировку
        
        // Закрываем файл
        fclose($item_data);
        return true;
    }
    else {
        fclose($item_data);
        return false;
    }
}

// Функция клонирования товара
function itemClone($id, $category_id, $lang, $elang) {
    
    $category = getCategorySeoUrl($category_id, $elang); // Узнаем URL категории
    
    // Подключение к БД и открытие файла
    $items_file_path = '../../' . DB_DIR_PATH . $elang . '/items.csv';
    $items = fopen($items_file_path, "rt") or die('Error Establishing a Database Connection...');
    
     if($items) {
        
        flock($items, LOCK_EX); // блокируем файл
        
        for ($i = 0; $data = fgetcsv($items, 0, ","); $i++) {
                
            if ($data[0] == $id) {
                
                $seo_url_previous = trim($data[2]);
                
                $db_items[] = [
                    'id' => trim($data[0]),
                    'page_layout' => trim($data[1]),
                    'url' => trim($data[2]),
                    'title' => trim($data[3]),
                    'keywords' => trim($data[4]),
                    'description' => trim($data[5]),
                    'h1' => trim($data[6]),
                    'category_id' => trim($data[7]),
                    'main_image_1x1' => trim($data[8]),
                    'main_image_4x3' => trim($data[9]),
                    'main_image_16x9' => trim($data[10]),
                    'main_image_preview' => trim($data[11]),
                    'main_image_alt' => trim($data[12]),
                    'additional_images' => trim($data[13]),
                    'shortdescription' => trim($data[14]),
                    'price' => trim($data[15]),
                    'discountprice' => trim($data[16]),
                    'sku' => trim($data[17]),
                    'availability' => trim($data[18]),
                    'brand' => trim($data[19])
                ];

                $page_id = trim($data[0]);
                $page_layout = trim($data[1]);
                $seo_url = trim($data[2]);
                $title = trim($data[3]);
                $keywords = trim($data[4]);
                $description = trim($data[5]);
                $h1 = trim($data[6]);
                $category_id = trim($data[7]);
                $main_image_1x1 = trim($data[8]);
                $main_image_4x3 = trim($data[9]);
                $main_image_16x9 = trim($data[10]);
                $main_image_preview = trim($data[11]);
                $main_image_alt = trim($data[12]);
                $additional_images = trim($data[13]);
                $shortdescription = trim($data[14]);
                $price = trim($data[15]);
                $discountprice = trim($data[16]);
                $sku = trim($data[17]);
                $availability = trim($data[18]);
                $brand = trim($data[19]);

            }
            else {
                
                $page_id = trim($data[0]);
                
                $db_items[] = [
                    'id' => trim($data[0]),
                    'page_layout' => trim($data[1]),
                    'url' => trim($data[2]),
                    'title' => trim($data[3]),
                    'keywords' => trim($data[4]),
                    'description' => trim($data[5]),
                    'h1' => trim($data[6]),
                    'category_id' => trim($data[7]),
                    'main_image_1x1' => trim($data[8]),
                    'main_image_4x3' => trim($data[9]),
                    'main_image_16x9' => trim($data[10]),
                    'main_image_preview' => trim($data[11]),
                    'main_image_alt' => trim($data[12]),
                    'additional_images' => trim($data[13]),
                    'shortdescription' => trim($data[14]),
                    'price' => trim($data[15]),
                    'discountprice' => trim($data[16]),
                    'sku' => trim($data[17]),
                    'availability' => trim($data[18]),
                    'brand' => trim($data[19])
                ];
            }
        }
        
        flock($items, LOCK_UN); // снимаем блокировку
        
        
        // Добавление новой копии товара с новым ID и URL
        $page_id = $page_id + 1;
        $seo_url = $seo_url . '-' . $page_id;
        
        $db_items[] = [
            'id' => $page_id,
            'page_layout' => $page_layout,
            'url' => $seo_url,
            'title' => $title,
            'keywords' => $keywords,
            'description' => $description,
            'h1' => $h1,
            'category_id' => $category_id,
            'main_image_1x1' => $main_image_1x1,
            'main_image_4x3' => $main_image_4x3,
            'main_image_16x9' => $main_image_16x9,
            'main_image_preview' => $main_image_preview,
            'main_image_alt' => $main_image_alt,
            'additional_images' => $additional_images,
            'shortdescription' => $shortdescription,
            'price' => $price,
            'discountprice' => $discountprice,
            'sku' => $sku,
            'availability' => $availability,
            'brand' => $brand
        ];
    }
    
    fclose($items);
    
    // Запись новых данных в БД товара
    // ===============================
    $items = fopen($items_file_path, "w") or die('Error Establishing a Database Connection...');

    flock($items, LOCK_EX); // блокируем файл

    foreach ($db_items as $line => $column) {
        fputcsv($items, $column);
    }

    flock($items, LOCK_UN); // снимаем блокировку
    fclose($items);
    
    // Запись новых данных в новый файл шаблона, если он существует 
    // у товара, с которого создается копия
    // ===================================
    $item_file_previous = '../../' . DB_DIR_PATH . $elang . '/catalog/' . $category . '/' . $seo_url_previous . '.tpl';
    $item_file = '../../' . DB_DIR_PATH . $elang . '/catalog/' . $category . '/' . $seo_url . '.tpl';
    
    if (file_exists($item_file_previous)) {
        
        // Чтение содержимого файла с которого создается копия
        $content = file_get_contents($item_file_previous);
        
        // Создаем новый файл шаблона для копии и записываем данные с предыдущего файла
        $item_data = fopen($item_file, 'w');
        
        flock($item_data, LOCK_EX); // блокируем файл

            // Записываем данные
            if (fwrite($item_data, $content) === FALSE) {
                return false;
            }
            
        flock($item_data, LOCK_UN); // снимаем блокировку
        
        // Закрываем файл
        fclose($item_data);
        return true;
        
    }
    return true;
}

// Функция удаления товара
function itemDelete($id, $category_id, $lang, $elang) {
    
    $category = getCategorySeoUrl($category_id, $elang); // Узнаем URL категории
    
    // Подключение к БД и открытие файла
    $items_file_path = '../../' . DB_DIR_PATH . $elang . '/items.csv';
    $items = fopen($items_file_path, "rt") or die('Error Establishing a Database Connection...');
    
    flock($items, LOCK_EX); // блокируем файл
        
        for ($i = 0; $data = fgetcsv($items, 0, ","); $i++) {
                
            if ($data[0] != $id) {
                
                $db_items[] = [
                    'id' => trim($data[0]),
                    'page_layout' => trim($data[1]),
                    'url' => trim($data[2]),
                    'title' => trim($data[3]),
                    'keywords' => trim($data[4]),
                    'description' => trim($data[5]),
                    'h1' => trim($data[6]),
                    'category_id' => trim($data[7]),
                    'main_image_1x1' => trim($data[8]),
                    'main_image_4x3' => trim($data[9]),
                    'main_image_16x9' => trim($data[10]),
                    'main_image_preview' => trim($data[11]),
                    'main_image_alt' => trim($data[12]),
                    'additional_images' => trim($data[13]),
                    'shortdescription' => trim($data[14]),
                    'price' => trim($data[15]),
                    'discountprice' => trim($data[16]),
                    'sku' => trim($data[17]),
                    'availability' => trim($data[18]),
                    'brand' => trim($data[19])
                ];
            }
            elseif ($data[0] == $id) {
                $seo_url = trim($data[2]);
            }
        }
        
    flock($items, LOCK_UN); // снимаем блокировку
    fclose($items);
    
    // Запись новых данных в БД товара
    // ===============================
    $items = fopen($items_file_path, "w") or die('Error Establishing a Database Connection...');

    flock($items, LOCK_EX); // блокируем файл

    foreach ($db_items as $line => $column) {
        fputcsv($items, $column);
    }

    flock($items, LOCK_UN); // снимаем блокировку
    fclose($items);
    
    // Удаление файла шаблона товара
    $item_file = '../../' . DB_DIR_PATH . $elang . '/catalog/' . $category . '/' . $seo_url . '.tpl';

    if (file_exists($item_file)) {
        unlink($item_file);
    }
    
    return true;
    
}
