<?php

// Основной файл конфигурации сайта
require_once '../../config.php';

// Проверка SEO URL страницы на дубликат по ID кроме самой себя
function duplicateSeoUrl($id, $seo_url, $file_name, $elang) {
    
    // Подключение к БД
    $file_path = '../../' . DB_DIR_PATH . $elang . '/' . $file_name . '.csv';
    $pages = fopen($file_path, "rt") or die('Error Establishing a Database Connection...');
    
    if($pages) {
        
        flock($pages, LOCK_EX); // блокируем файл
        
        for ($i = 0; $data = fgetcsv($pages, 0, ","); $i++) {
            
            if ($data[0] != $id && $data[2] == $seo_url) {
                
                return true;
            }
            
        }
        
        flock($pages, LOCK_UN); // снимаем блокировку
        fclose($pages);
    }
    
    return false;
}

// Функция создания новой записи
function recordAdd($elang,
        $page_id, 
        $page_layout, 
        $seo_url, 
        $title, 
        $keywords, 
        $description, 
        $h1, 
        $editordata, 
        $record_date, 
        // $record_mod_date, 
        $main_image_1x1, 
        $main_image_4x3, 
        $main_image_16x9, 
        $main_image_preview,
        $main_image_alt,
        $record_author,
        $record_author_type
    ) {
    
    // Новая дана изменения для новой записи (2020-05-28T09:11:00)
    $new_record_mod_date = date('Y-m-d\TH:i:s');
    
    // Подключение к БД
    $records_file_path = '../../' . DB_DIR_PATH . $elang . '/' . $page_layout . '.csv';
    $records = fopen($records_file_path, "rt") or die('Error Establishing a Database Connection...');
    
    if($records) {
        
        $seo_url = trim($seo_url);
                
        // Проверим на дубликат SEO URL
        if (duplicateSeoUrl($page_id, $seo_url, $page_layout, $elang)) {

            $new_seo_url = $seo_url . '-' . $page_id;
        }
        else {
            $new_seo_url = $seo_url;
        }
        
        flock($records, LOCK_EX); // блокируем файл
        
        for ($i = 0; $data = fgetcsv($records, 0, ","); $i++) {
                
            $db_records[] = [
                'id' => trim($data[0]),
                'page_layout' => trim($data[1]),
                'url' => trim($data[2]),
                'title' => trim($data[3]),
                'keywords' => trim($data[4]),
                'description' => trim($data[5]),
                'h1' => trim($data[6]),
                'record_date' => $data[7],
                'record_mod_date' => $data[8],
                'main_image_1x1' => trim($data[9]),
                'main_image_4x3' => trim($data[10]),
                'main_image_16x9' => trim($data[11]),
                'main_image_preview' => trim($data[12]),
                'main_image_alt' => trim($data[13]),
                'record_author' => trim($data[14]),
                'record_author_type' => trim($data[15])
            ];
            
            $final_id = trim($data[0]);
        }
        
        flock($records, LOCK_UN); // снимаем блокировку
        fclose($records);
        
        $db_records[] = [
            'id' => $final_id + 1,
            'page_layout' => trim($page_layout),
            'url' => $new_seo_url,
            'title' => trim(str_replace('"', '&quot;', $title)),
            'keywords' => trim(str_replace('"', '&quot;', $keywords)),
            'description' => trim(str_replace('"', '&quot;', $description)),
            'h1' => trim(str_replace('"', '&quot;', $h1)),
            'record_date' => $record_date,
            'record_mod_date' => $new_record_mod_date,
            'main_image_1x1' => $main_image_1x1,
            'main_image_4x3' => $main_image_4x3,
            'main_image_16x9' => $main_image_16x9,
            'main_image_preview' => $main_image_preview,
            'main_image_alt' => trim(str_replace('"', '&quot;', $main_image_alt)),
            'record_author' => trim(str_replace('"', '&quot;', $record_author)),
            'record_author_type' => $record_author_type
        ];
        
        // Запись новых данных в БД товара
        // ===============================
        $records = fopen($records_file_path, "w") or die('Error Establishing a Database Connection...');

        flock($records, LOCK_EX); // блокируем файл

        foreach ($db_records as $line => $column) {
            fputcsv($records, $column);
        }

        flock($records, LOCK_UN); // снимаем блокировку
        fclose($records);
        
        // Новый файл шаблона для записи
        $record_file = '../../' . DB_DIR_PATH . $elang . '/' . $page_layout . '/' . $new_seo_url . '.tpl';
        
        // Открываем файл и если его не существует, создаем его
        $record_data = fopen($record_file, 'w');
        
        flock($record_data, LOCK_EX); // блокируем файл

            // Записываем данные
            if (fwrite($record_data, $editordata) === FALSE) {
                return false;
            }
            
        flock($record_data, LOCK_UN); // снимаем блокировку
        
        // Закрываем файл
        fclose($record_data);
        
        return true;
    }
    
}

// Функция сохранения страниц с записями
function recordSave($elang, $orig_url,
        $page_id, 
        $page_layout, 
        $seo_url, 
        $title, 
        $keywords, 
        $description, 
        $h1, 
        $editordata, 
        $record_date, 
        // $record_mod_date, 
        $main_image_1x1, 
        $main_image_4x3, 
        $main_image_16x9, 
        $main_image_preview,
        $main_image_alt,
        $record_author,
        $record_author_type
    ) {
    
    // Новая дата редактирования для записи (2020-05-28T09:11:00)
    $new_record_mod_date = date('Y-m-d\TH:i:s');
    
    // Подключение к БД
    $records_file_path = '../../' . DB_DIR_PATH . $elang . '/' . $page_layout . '.csv';
    $records = fopen($records_file_path, "rt") or die('Error Establishing a Database Connection...');
    
    if($records) {
        
        $seo_url = trim($seo_url);
                
        // Проверим на дубликат SEO URL
        if (duplicateSeoUrl($page_id, $seo_url, $page_layout, $elang)) {

            $new_seo_url = $seo_url . '-' . $page_id;
        }
        else {
            $new_seo_url = $seo_url;
        }
        
        flock($records, LOCK_EX); // блокируем файл
        
        for ($i = 0; $data = fgetcsv($records, 0, ","); $i++) {
                
            if ($data[0] == $page_id) {
                
                $db_records[] = [
                    'id' => $page_id,
                    'page_layout' => trim($page_layout),
                    'url' => $new_seo_url,
                    'title' => trim(str_replace('"', '&quot;', $title)),
                    'keywords' => trim(str_replace('"', '&quot;', $keywords)),
                    'description' => trim(str_replace('"', '&quot;', $description)),
                    'h1' => trim(str_replace('"', '&quot;', $h1)),
                    'record_date' => $record_date,
                    'record_mod_date' => $new_record_mod_date,
                    'main_image_1x1' => $main_image_1x1,
                    'main_image_4x3' => $main_image_4x3,
                    'main_image_16x9' => $main_image_16x9,
                    'main_image_preview' => $main_image_preview,
                    'main_image_alt' => trim(str_replace('"', '&quot;', $main_image_alt)),
                    'record_author' => trim(str_replace('"', '&quot;', $record_author)),
                    'record_author_type' => $record_author_type
                ];
            }
            else {
                
                $db_records[] = [
                    'id' => trim($data[0]),
                    'page_layout' => trim($data[1]),
                    'url' => trim($data[2]),
                    'title' => trim($data[3]),
                    'keywords' => trim($data[4]),
                    'description' => trim($data[5]),
                    'h1' => trim($data[6]),
                    'record_date' => $data[7],
                    'record_mod_date' => $data[8],
                    'main_image_1x1' => trim($data[9]),
                    'main_image_4x3' => trim($data[10]),
                    'main_image_16x9' => trim($data[11]),
                    'main_image_preview' => trim($data[12]),
                    'main_image_alt' => trim($data[13]),
                    'record_author' => trim($data[14]),
                    'record_author_type' => trim($data[15])
                ];
            }
        }
        
        flock($records, LOCK_UN); // снимаем блокировку
        fclose($records);
        
        // Запись новых данных в БД записей
        // ================================
        $records = fopen($records_file_path, "w") or die('Error Establishing a Database Connection...');

        flock($records, LOCK_EX); // блокируем файл

        foreach ($db_records as $line => $column) {
            fputcsv($records, $column);
        }

        flock($records, LOCK_UN); // снимаем блокировку
        fclose($records);
        
        // Запись изменений в файл шаблона записи
        // ======================================
        
        // Удаление старого файла шаблона, если произошла смена URL адреса
        if (trim($seo_url) != trim($orig_url)) {
            
            $record_file = '../../' . DB_DIR_PATH . $elang . '/' . $page_layout . '/' . trim($orig_url) . '.tpl';
            
            if (file_exists($record_file)) {
                unlink($record_file);
            }
        }
        
        // Новый файл шаблона для записи
        $record_file = '../../' . DB_DIR_PATH . $elang . '/' . $page_layout . '/' . $new_seo_url . '.tpl';
        
        // Открываем файл и если его не существует, создаем его
        $record_data = fopen($record_file, 'w');
        
        flock($record_data, LOCK_EX); // блокируем файл

            // Записываем данные
            if (fwrite($record_data, $editordata) === FALSE) {
                return false;
            }
            
        flock($record_data, LOCK_UN); // снимаем блокировку
        
        // Закрываем файл
        fclose($record_data);
        return true;
    }
    else {
        fclose($record_data);
        return false;
    }
}

// Функция клонирования товара
function recordClone($id, $page_layout, $lang, $elang) {
    
    // Новые даты для новой записи (2020-05-28T09:11:00)
    $new_record_date = date('Y-m-d\TH:i:s');
    $new_record_mod_date = date('Y-m-d\TH:i:s');
    
    // Подключение к БД и открытие файла
    $records_file_path = '../../' . DB_DIR_PATH . $elang . '/' . $page_layout . '.csv';
    $records = fopen($records_file_path, "rt") or die('Error Establishing a Database Connection...');
    
     if($records) {
        
        flock($records, LOCK_EX); // блокируем файл
        
        for ($i = 0; $data = fgetcsv($records, 0, ","); $i++) {
                
            if ($data[0] == $id) {
                
                $seo_url_previous = trim($data[2]);
                
                $db_records[] = [
                    'id' => trim($data[0]),
                    'page_layout' => trim($data[1]),
                    'url' => trim($data[2]),
                    'title' => trim($data[3]),
                    'keywords' => trim($data[4]),
                    'description' => trim($data[5]),
                    'h1' => trim($data[6]),
                    'record_date' => $data[7],
                    'record_mod_date' => $data[8],
                    'main_image_1x1' => trim($data[9]),
                    'main_image_4x3' => trim($data[10]),
                    'main_image_16x9' => trim($data[11]),
                    'main_image_preview' => trim($data[12]),
                    'main_image_alt' => trim($data[13]),
                    'record_author' => trim($data[14]),
                    'record_author_type' => trim($data[15])
                ];

                $page_id = trim($data[0]);
                $page_layout = trim($data[1]);
                $seo_url = trim($data[2]);
                $title = trim($data[3]);
                $keywords = trim($data[4]);
                $description = trim($data[5]);
                $h1 = trim($data[6]);
                $record_date = $data[7];
                $record_mod_date = $data[8];
                $main_image_1x1 = trim($data[9]);
                $main_image_4x3 = trim($data[10]);
                $main_image_16x9 = trim($data[11]);
                $main_image_preview = trim($data[12]);
                $main_image_alt = trim($data[13]);
                $record_author = trim($data[14]);
                $record_author_type = $data[15];

            }
            else {
                
                $page_id = trim($data[0]);
                
                $db_records[] = [
                    'id' => trim($data[0]),
                    'page_layout' => trim($data[1]),
                    'url' => trim($data[2]),
                    'title' => trim($data[3]),
                    'keywords' => trim($data[4]),
                    'description' => trim($data[5]),
                    'h1' => trim($data[6]),
                    'record_date' => $data[7],
                    'record_mod_date' => $data[8],
                    'main_image_1x1' => trim($data[9]),
                    'main_image_4x3' => trim($data[10]),
                    'main_image_16x9' => trim($data[11]),
                    'main_image_preview' => trim($data[12]),
                    'main_image_alt' => trim($data[13]),
                    'record_author' => trim($data[14]),
                    'record_author_type' => trim($data[15])
                ];
            }
        }
        
        flock($records, LOCK_UN); // снимаем блокировку
        
        
        // Добавление новой копии записи с новыми ID, URL и датой
        $page_id = $page_id + 1;
        $seo_url = $seo_url . '-' . $page_id;
        
        $db_records[] = [
            'id' => $page_id,
            'page_layout' => $page_layout,
            'url' => $seo_url,
            'title' => $title,
            'keywords' => $keywords,
            'description' => $description,
            'h1' => $h1,
            'record_date' => $new_record_date,
            'record_mod_date' => $new_record_mod_date,
            'main_image_1x1' => $main_image_1x1,
            'main_image_4x3' => $main_image_4x3,
            'main_image_16x9' => $main_image_16x9,
            'main_image_preview' => $main_image_preview,
            'main_image_alt' => $main_image_alt,
            'record_author' => $record_author,
            'record_author_type' => $record_author_type
        ];
    }
    
    fclose($records);
    
    // Запись новых данных в БД товара
    // ===============================
    $records = fopen($records_file_path, "w") or die('Error Establishing a Database Connection...');

    flock($records, LOCK_EX); // блокируем файл

    foreach ($db_records as $line => $column) {
        fputcsv($records, $column);
    }

    flock($records, LOCK_UN); // снимаем блокировку
    fclose($records);
    
    // Запись новых данных в новый файл шаблона, если он существует 
    // у записи, с которой создается копия
    // ===================================
    $record_file_previous = '../../' . DB_DIR_PATH . $elang . '/' . $page_layout . '/' . $seo_url_previous . '.tpl';
    $record_file = '../../' . DB_DIR_PATH . $elang . '/' . $page_layout . '/' . $seo_url . '.tpl';
    
    if (file_exists($record_file_previous)) {
        
        // Чтение содержимого файла с которого создается копия
        $content = file_get_contents($record_file_previous);
        
        // Создаем новый файл шаблона для копии и записываем данные с предыдущего файла
        $record_data = fopen($record_file, 'w');
        
        flock($record_data, LOCK_EX); // блокируем файл

            // Записываем данные
            if (fwrite($record_data, $content) === FALSE) {
                return false;
            }
            
        flock($record_data, LOCK_UN); // снимаем блокировку
        
        // Закрываем файл
        fclose($record_data);
        return true;
        
    }
    return true;
}

// Функция удаления записи
function recordDelete($id, $page_layout, $lang, $elang) {
    
    // Подключение к БД и открытие файла
    $records_file_path = '../../' . DB_DIR_PATH . $elang . '/' . $page_layout . '.csv';
    $records = fopen($records_file_path, "rt") or die('Error Establishing a Database Connection...');
    
    flock($records, LOCK_EX); // блокируем файл
        
        for ($i = 0; $data = fgetcsv($records, 0, ","); $i++) {
                
            if ($data[0] != $id) {
                
                $db_records[] = [
                    'id' => trim($data[0]),
                    'page_layout' => trim($data[1]),
                    'url' => trim($data[2]),
                    'title' => trim($data[3]),
                    'keywords' => trim($data[4]),
                    'description' => trim($data[5]),
                    'h1' => trim($data[6]),
                    'record_date' => $data[7],
                    'record_mod_date' => $data[8],
                    'main_image_1x1' => trim($data[9]),
                    'main_image_4x3' => trim($data[10]),
                    'main_image_16x9' => trim($data[11]),
                    'main_image_preview' => trim($data[12]),
                    'main_image_alt' => trim($data[13]),
                    'record_author' => trim($data[14]),
                    'record_author_type' => trim($data[15])
                ];
            }
            elseif ($data[0] == $id) {
                $seo_url = trim($data[2]);
            }
        }
        
    flock($records, LOCK_UN); // снимаем блокировку
    fclose($records);
    
    // Запись новых данных в БД товара
    // ===============================
    $records = fopen($records_file_path, "w") or die('Error Establishing a Database Connection...');

    flock($records, LOCK_EX); // блокируем файл

    foreach ($db_records as $line => $column) {
        fputcsv($records, $column);
    }

    flock($records, LOCK_UN); // снимаем блокировку
    fclose($records);
    
    // Удаление файла шаблона товара
    $record_file = '../../' . DB_DIR_PATH . $elang . '/' . $page_layout . '/' . $seo_url . '.tpl';

    if (file_exists($record_file)) {
        unlink($record_file);
    }
    
    return true;
    
}
