<?php

// Основной файл конфигурации сайта
require_once '../../config.php';

// Функция удаления Основных страниц
function pageDelete($elang, $epage) {
    
}

// Функция сохранения Основных страниц
function pageSave($elang, $id, $epage, $inputtitle, $inputh1, $inputpagelayout, $keywordstextarea, $descriptiontextarea, $editordata) {
    
    // Подключение к БД
    $pages_file_path = '../../' . DB_DIR_PATH . $elang . '/pages.csv';
    $pages = fopen($pages_file_path, "rt") or die('Error Establishing a Database Connection...');
    
    if($pages) {
        
        flock($pages, LOCK_EX); // блокируем файл
        
        for ($i = 0; $data = fgetcsv($pages, 0, ","); $i++) {
                
            if (trim($data[2]) == trim($epage)) {
                
                $db_pages[] = [
                    'id' => $id,
                    'page_layout' => trim($inputpagelayout),
                    'url' => trim($epage),
                    'title' => trim(str_replace('"', '&quot;', $inputtitle)),
                    'keywords' => trim(str_replace('"', '&quot;', $keywordstextarea)),
                    'description' => trim(str_replace('"', '&quot;', $descriptiontextarea)),
                    'h1' => trim(str_replace('"', '&quot;', $inputh1)),
                ];
            }
            else {
                
                $db_pages[] = [
                    'id' => trim($data[0]),
                    'page_layout' => trim($data[1]),
                    'url' => trim($data[2]),
                    'title' => trim($data[3]),
                    'keywords' => trim($data[4]),
                    'description' => trim($data[5]),
                    'h1' => trim($data[6])
                ];
            }
        }
        
        flock($pages, LOCK_UN); // снимаем блокировку
        fclose($pages);
        
        
        // Запись изменений в файл шаблона страницы
        // ========================================
        $page_file = '../../' . DB_DIR_PATH . $elang . '/pages/' . trim($epage) . '.tpl';
        
        // Убедимся, что файл существует и доступен для записи
        if (is_writable($page_file)) {
            
            // Открываем файл для записи
            if (!$handle = fopen($page_file, 'w')) {
                 return false;
            }

            // Записываем данные
            if (fwrite($handle, $editordata) === FALSE) {
                return false;
            }

            // Закрываем файл
            fclose($handle);
            
        }
        else {
            return false;
        }
        
        // Запись новых данных в БД страниц
        // ================================
        $pages = fopen($pages_file_path, "w") or die('Error Establishing a Database Connection...');

        flock($pages, LOCK_EX); // блокируем файл

        foreach ($db_pages as $line => $column) {
            fputcsv($pages, $column);
        }

        flock($pages, LOCK_UN); // снимаем блокировку
        fclose($pages);
        
        return true;
        
    }
    else {
        fclose($pages);
        return false;
    }
}

// Функция создаиння новых Основных страниц
function pageAdd($elang, $epage) {
    
}
