<?php
// Проверка доступных для редактирования языков сайта, язык по умолчанию
include 'components/lang_site.php';

// На каком языке будет редактироваться контент сайта
if (isset($_GET['elang'])) {
    $elang = $_GET['elang'];
    //Какой язык по умолчанию
    foreach ($db_lang_site as $line => $column) {
        if ($elang == $column['lang_site_code']) {
            $elang_name = $column['lang_site_name'];
            break;
        }
    }
}
 else {
    $elang = $lang_site_default;
    $elang_name = $lang_site_name;
}

// Проверяем, есть ли данные на редактирование определенной страницы или записи
if (isset($_GET['epage'])) {
    $epage = $_GET['epage'];
    
    include_once 'components/page_delete.php'; // форма для подтверждения удаления
}
 else {
    $epage = null;
} ?>

<!-- MAIN MENU -->
<nav class="navbar navbar-expand-md navbar-dark bg-blue fixed-top">
    <a class="navbar-brand" href="editor/index.php?lang=<?php echo $lang; ?>">
        <img src="editor/images/logo/logo.png" width="34" height="34" alt="Web Platinum logo">
        <?php echo $brand; ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <?php include 'components/menu.php';?>
    </div>
</nav>

<!-- MAIN CONTENT -->
<main class="mb-5 mt-3">

    <div class="container">
        <h1 class="h3 mb-4"><?php echo $h1; ?></h1>
    </div>
    
    <hr>

    <div class="container">
        
        <p class="mt-5">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-circle-fill mb-1 mr-2" viewBox="0 0 16 16">
                <circle cx="8" cy="8" r="8"/>
            </svg>
            <?php echo $page_off; ?>
        </p>

    </div>

</main>

<footer class="bg-light pt-5 pb-5">
    <div class="container">
        <p class="text-muted text-center">&copy; <?php echo $editor_ver; ?></p>
    </div>
</footer>
