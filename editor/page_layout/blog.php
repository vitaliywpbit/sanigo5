<?php
// Проверка доступных для редактирования языков сайта, язык по умолчанию
include 'components/lang_site.php';

// На каком языке будет редактироваться контент сайта
if (isset($_GET['elang'])) {
    $elang = $_GET['elang'];
    //Какой язык по умолчанию
    foreach ($db_lang_site as $line => $column) {
        if ($elang == $column['lang_site_code']) {
            $elang_name = $column['lang_site_name'];
            break;
        }
    }
}
 else {
    $elang = $lang_site_default;
    $elang_name = $lang_site_name;
}

// Проверяем, есть ли данные на редактирование определенной страницы или записи
if (isset($_GET['epage'])) {
    $epage = $_GET['epage'];
    
    include_once 'components/page_save_info.php'; // модальное  окно с результатом сохранения
    include_once 'components/filemanager.php'; // модальное  окно с загрузкой изображения
}
 else {
    $epage = null;
} ?>

<!-- MAIN MENU -->
<nav class="navbar navbar-expand-md navbar-dark bg-blue fixed-top">
    <a class="navbar-brand" href="editor/index.php?lang=<?php echo $lang; ?>">
        <img src="editor/images/logo/logo.png" width="34" height="34" alt="Web Platinum logo">
        <?php echo $brand; ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <?php include 'components/menu.php';?>
    </div>
</nav>

<!-- MAIN CONTENT -->
<main class="mb-5 mt-3">

    <div class="container">
        
        <div class="row align-items-center">
            <div class="col-md-6 mt-1">
                <h1 class="h3"><?php echo $h1; ?></h1>
            </div>
            <div class="col-md-6 mt-1">
                <?php include 'components/search.php'; ?>
            </div>
        </div>

        <ul class="nav mb-3">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownLangMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $elang_name; ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownLangMenu">
                    <?php include 'components/lang_edit_menu.php'; //Блок вывода языкового меню ?>
                </div>
            </li>
            <?php
            if ($epage && $page_num == 1) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="editor/index.php?page=<?php echo $page; ?>&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-return-left" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M14.5 1.5a.5.5 0 0 1 .5.5v4.8a2.5 2.5 0 0 1-2.5 2.5H2.707l3.347 3.346a.5.5 0 0 1-.708.708l-4.2-4.2a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 8.3H12.5A1.5 1.5 0 0 0 14 6.8V2a.5.5 0 0 1 .5-.5z"/>
                        </svg>
                        <?php echo $back_to_list_text; ?>
                    </a>
                </li>
            <?php }
            elseif ($epage && $page_num > 1) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="editor/index.php?page=<?php echo $page; ?>&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>&p=<?php echo $page_num; ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-return-left" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M14.5 1.5a.5.5 0 0 1 .5.5v4.8a2.5 2.5 0 0 1-2.5 2.5H2.707l3.347 3.346a.5.5 0 0 1-.708.708l-4.2-4.2a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 8.3H12.5A1.5 1.5 0 0 0 14 6.8V2a.5.5 0 0 1 .5-.5z"/>
                        </svg>
                        <?php echo $back_to_list_text; ?>
                    </a>
                </li>
            <?php } ?>
        </ul>

    </div>

    <hr>

    <div class="container">
        
        <?php 
        // Какую запись редактировать или вывод всего списка записей
        if ($epage) {
            echo '<div class="container">';
            include 'modules/blog.php'; // Блок вывода определенной записи
            echo '</div>';
        }
         else {
            echo '<div class="container">';
            include 'components/blog.php'; // Блок вывода всего списка записей
            echo '</div>';
        } ?>

    </div>

</main>

<footer class="bg-light pt-5 pb-5">
    <div class="container">
        <p class="text-muted text-center">&copy; <?php echo $editor_ver; ?></p>
    </div>
</footer>
