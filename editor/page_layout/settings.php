<?php
// Подключение к БД
include 'model/settings.php';

// Проверка доступных для редактирования языков сайта, язык по умолчанию
include 'components/lang_site.php';

// На каком языке будет редактироваться контент сайта
if (isset($_GET['elang'])) {
    $elang = $_GET['elang'];
    //Какой язык по умолчанию
    foreach ($db_lang_site as $line => $column) {
        if ($elang == $column['lang_site_code']) {
            $elang_name = $column['lang_site_name'];
            break;
        }
    }
}
 else {
    $elang = $lang_site_default;
    $elang_name = $lang_site_name;
}

include_once 'components/page_save_info.php'; // модальное  окно с результатом сохранения
?>

<!-- MAIN MENU -->
<nav class="navbar navbar-expand-md navbar-dark bg-blue fixed-top">
    <a class="navbar-brand" href="editor/index.php?lang=<?php echo $lang; ?>">
        <img src="editor/images/logo/logo.png" width="34" height="34" alt="Web Platinum logo">
        <?php echo $brand; ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <?php include 'components/menu.php';?>
    </div>
</nav>

<!-- MAIN CONTENT -->
<main class="mb-5 mt-3">

    <div class="container">
        <h1 class="h3 mb-4"><?php echo $h1; ?></h1>
    </div>
    
    <hr>

    <div class="container">
        
        <form class="mt-5" method="POST" id="settingsSaveForm" action="javascript:void(null);" onsubmit="saveSettings()">
            
            <div class="form-group row">
                <label for="selectHttpProtocol" class="col-sm-2 col-form-label"><?php echo $http_protocol_text; ?></label>
                <div class="col-sm-10">
                    <select name="http_protocol" class="form-control" id="selectHttpProtocol" required>
                        <?php
                        if ($http_protocol == 'http://') { ?>
                            <option>http://</option>
                            <option>http://</option>
                        <?php }
                        elseif ($http_protocol == 'http://') { ?>
                            <option>http://</option>
                            <option>http://</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputDomain" class="col-sm-2 col-form-label"><?php echo $domain_name_text; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="domain" class="form-control" id="inputDomain" value="<?php echo $domain_name; ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputDefaultPageSeoUrl" class="col-sm-2 col-form-label"><?php echo $home_page_text; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="default_page" class="form-control" id="inputDefaultPageSeoUrl" value="<?php echo $page_default; ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputEmail" class="col-sm-2 col-form-label"><?php echo $main_email_text; ?></label>
                <div class="col-sm-10">
                    <input type="email" name="email" class="form-control" id="inputEmail" value="<?php echo $main_send_email; ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputOgImage" class="col-sm-2 col-form-label"><?php echo $og_image_text; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="og_image" class="form-control" id="inputOgImage" value="<?php echo $img_default; ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="selectTimeZone" class="col-sm-2 col-form-label"><?php echo $time_zone_text; ?></label>
                <div class="col-sm-10">
                    <select name="time_zone" class="form-control" id="selectTimeZone" required>
                        <option><?php echo $time_zone; ?></option>
                        <?php
                         foreach ($db_timezone as $key => $value) {
                             echo '<option>' . $value . '</option>';
                         }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputGtag" class="col-sm-2 col-form-label"><?php echo $gtag_id_text; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="gtag" class="form-control" id="inputGtag" value="<?php echo $google_id; ?>">
                </div>
            </div>
            
            <input type="hidden" name="lang" value="<?php echo $lang; ?>">
            
            <div class="text-center mt-5">
                <button type="submit" class="btn btn-primary mt-3 mr-2"><?php echo $save_button; ?></button>
            </div>
        </form>
        
    </div>

</main>

<footer class="bg-light pt-5 pb-5">
    <div class="container">
        <p class="text-muted text-center">&copy; <?php echo $editor_ver; ?></p>
    </div>
</footer>


<script>
    function saveSettings() {
        var msg   = $('#settingsSaveForm').serialize();
        $.ajax({
            type: 'POST',
            url: 'editor/modules/save_settings.php',
            data: msg,
            success: function(data) {
                $('#pageSaveResault').html(data);
                $('#pageSaveInfo').modal('show');
            },
            error:  function(xhr, str){
                alert('Error: ' + xhr.responseCode);
            }
        });
    }
</script>
