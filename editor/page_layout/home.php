<?php
// Подключение к БД
include 'model/dashboard.php';
?>

<!-- MAIN MENU -->
<nav class="navbar navbar-expand-md navbar-dark bg-blue fixed-top">
    <a class="navbar-brand" href="editor/index.php?lang=<?php echo $lang; ?>">
        <img src="editor/images/logo/logo.png" width="34" height="34" alt="Web Platinum logo">
        <?php echo $brand; ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <?php include 'components/menu.php';?>
    </div>
</nav>

<!-- MAIN CONTENT -->
<main class="mb-5 mt-3">
    
    <div class="container">
        
        <h1 class="h3"><?php echo $h1; ?></h1>
        
        <ul class="nav mb-5">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownLangMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $elang_name; ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownLangMenu">
                    <?php include 'components/lang_edit_menu.php'; //Блок вывода языкового меню ?>
                </div>
            </li>
        </ul>
        
        <?php ## echo $content; //Вывод основного контента страницы ?>
        
        <div class="row">
            <div class="col-md-7 mb-4">
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <a href="editor/index.php?page=pages&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>" class="text-reset">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-layout-text-window-reverse mr-2 mb-1" viewBox="0 0 16 16">
                                <path d="M13 6.5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5zm0 3a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5zm-.5 2.5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1 0-1h5z"/>
                                <path d="M14 0a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12zM2 1a1 1 0 0 0-1 1v1h14V2a1 1 0 0 0-1-1H2zM1 4v10a1 1 0 0 0 1 1h2V4H1zm4 0v11h9a1 1 0 0 0 1-1V4H5z"/>
                            </svg>
                            <?php echo $total_main_pages_text; ?>
                        </a>
                        <span class="badge badge-primary badge-pill"><?php echo $number_of_pages; ?></span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <a href="editor/index.php?page=blog&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>" class="text-reset">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-book mr-2 mb-1" viewBox="0 0 16 16">
                                <path d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811V2.828zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492V2.687zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"/>
                            </svg>
                            <?php echo $total_blog_posts_text; ?>
                        </a>
                        <span class="badge badge-primary badge-pill"><?php echo $number_of_blog_posts; ?></span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <a href="editor/index.php?page=items&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>" class="text-reset">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-tags mr-2 mb-1" viewBox="0 0 16 16">
                                <path d="M3 2v4.586l7 7L14.586 9l-7-7H3zM2 2a1 1 0 0 1 1-1h4.586a1 1 0 0 1 .707.293l7 7a1 1 0 0 1 0 1.414l-4.586 4.586a1 1 0 0 1-1.414 0l-7-7A1 1 0 0 1 2 6.586V2z"/>
                                <path d="M5.5 5a.5.5 0 1 1 0-1 .5.5 0 0 1 0 1zm0 1a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zM1 7.086a1 1 0 0 0 .293.707L8.75 15.25l-.043.043a1 1 0 0 1-1.414 0l-7-7A1 1 0 0 1 0 7.586V3a1 1 0 0 1 1-1v5.086z"/>
                            </svg>
                            <?php echo $total_items_text; ?>
                        </a>
                        <span class="badge badge-primary badge-pill"><?php echo $number_of_products; ?></span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <a href="editor/index.php?page=orders&lang=<?php echo $lang; ?>" class="text-reset">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-basket mr-2 mb-1" viewBox="0 0 16 16">
                                <path d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1v4.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 13.5V9a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h1.217L5.07 1.243a.5.5 0 0 1 .686-.172zM2 9v4.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V9H2zM1 7v1h14V7H1zm3 3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 4 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 6 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 8 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5z"/>
                            </svg>
                            <?php echo $total_orders_text; ?>
                        </a>
                        <span class="badge badge-primary badge-pill"><?php echo $number_of_orders; ?></span>
                    </li>
                </ul>
            </div>
            <div class="col-md-5 mb-4">
                <div class="border p-2">
                    <p>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-headset mr-2 mb-1" viewBox="0 0 16 16">
                            <path d="M8 1a5 5 0 0 0-5 5v1h1a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V6a6 6 0 1 1 12 0v6a2.5 2.5 0 0 1-2.5 2.5H9.366a1 1 0 0 1-.866.5h-1a1 1 0 1 1 0-2h1a1 1 0 0 1 .866.5H11.5A1.5 1.5 0 0 0 13 12h-1a1 1 0 0 1-1-1V8a1 1 0 0 1 1-1h1V6a5 5 0 0 0-5-5z"/>
                        </svg>
                        <ins><?php echo $project_support_text; ?></ins>
                    </p>
                    <p><a href="mailto:info@web-platinum.net"><i>info@web-platinum.net</i></a></p>
                    <p><a href="http://web-platinum.net/" target="_blank"><i>http://web-platinum.net</i></a></p>
                </div>
            </div>
        </div>
        
        <h2 class="h3 mt-4 mb-4"><?php echo $quick_links_text; ?></h2>
        
        <div class="row">
            <div class="col-md-4 mt-3">
                <a href="editor/index.php?page=pages&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>" role="button" class="btn btn-outline-info btn-block">
                    <?php echo $add_new_page_text; ?>
                </a>
            </div>
            <div class="col-md-4 mt-3">
                <a href="editor/index.php?page=blog&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>&epage=newrecord" role="button" class="btn btn-outline-info btn-block">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                    </svg>
                    <?php echo $add_new_blog_post_text; ?>
                </a>
            </div>
            <div class="col-md-4 mt-3">
                <a href="editor/index.php?page=items&lang=<?php echo $lang; ?>&elang=<?php echo $elang; ?>&epage=newitem" role="button" class="btn btn-outline-info btn-block">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                    </svg>
                    <?php echo $add_new_product_text; ?>
                </a>
            </div>
        </div>
        
    </div>

</main>

<footer class="bg-light pt-5 pb-5">
    <div class="container">
        <p class="text-muted text-center">&copy; <?php echo $editor_ver; ?></p>
    </div>
</footer>
