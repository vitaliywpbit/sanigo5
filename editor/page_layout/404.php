<!-- MAIN MENU -->
<nav class="navbar navbar-expand-md navbar-dark bg-blue fixed-top">
    <a class="navbar-brand" href="editor/index.php?lang=<?php echo $lang; ?>">
        <img src="editor/images/logo/logo.png" width="34" height="34" alt="Web Platinum logo">
        <?php echo $brand; ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <?php include 'components/menu.php';?>
    </div>
</nav>

<!-- MAIN CONTENT -->
<main>
    <div class="container mb-5 mt-3">
        <h1 class="mb-5"><?php echo $h1; ?></h1>
        <?php echo $content; //Вывод основного контента страницы ?>
    </div>
</main>
